﻿using System;
using System.Drawing;
using System.Collections.Generic;
using System.Windows.Forms;

using System.Diagnostics;

namespace Sounding
{
	public partial class WaveDrawer : UserControl
	{
		Queue<float> values = new Queue<float>();

		public WaveDrawer()
		{
			InitializeComponent();
			//UpdatePoints(new float[] {.0f, .0f});
		}
		/*public void Clear()
		{
			UpdatePoints(new float[] {.0f});
		}*/
		public void DrawData(float[] buffer, int offset, int sampleCount, int channels, int channel = 0)
		{
			int count = (sampleCount / channels);
			float[] data = new float[count];
			for(int iii = 0; iii < data.Length; iii++)
			{
				data[iii] = buffer[iii * channels + channel + offset];
			}

			System.Threading.ThreadPool.QueueUserWorkItem(unused => UpdatePoints(data));
			//UpdatePoints(data);
		}
		public void DrawData(float[] buffer, int channels, int channel = 0)
		{
			int count = (buffer.Length / channels);
			float[] data = new float[count];
			for(int iii = 0; iii < data.Length; iii++)
			{
				data[iii] = buffer[iii * channels + channel];
			}
			
			System.Threading.ThreadPool.QueueUserWorkItem(unused => UpdatePoints(data));
			//UpdatePoints(data);
		}
		void UpdatePoints(float[] data)
		{
			int mult = DeviceSettings.SampleRate / ClientSize.Width;
			if(mult < 1)
				mult = 1;

			float height = (float)ClientSize.Height * .9f;
			float offset = (float)ClientSize.Height * .05f;

			lock(values)
			{
				for(int iii = 0; iii < data.Length; iii += mult)
				{
					float temp = SMath.Clamp(((data[iii] + 1.0f) * .5f) * height + offset, offset, height);
					if(float.IsNaN(temp))
						temp = height * .5f;
					values.Enqueue(temp);
					if(values.Count > ClientSize.Width)
						values.Dequeue();
				}
			}

			if(!IsDisposed)
				Invoke(new MethodInvoker(Redraw));
		}
		void Redraw()
		{
			Refresh();
		}
		protected override void OnPaint(PaintEventArgs e)
		{
			using(Pen pen = new Pen(ForeColor))
			{
				float[] points;
				lock(values)
				{
					points = values.ToArray();
				}
				for(int iii = 1; iii < points.Length; iii++)
				{
					/*if(float.IsNaN(points[iii]))
						points[iii] = 0;
					if(float.IsNaN(points[iii - 1]))
						points[iii - 1] = 0;*/
					e.Graphics.DrawLine(pen, iii - 1, points[iii - 1], iii, points[iii]); 
				}
			}
		}
		
	}
}
