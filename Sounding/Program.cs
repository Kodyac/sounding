﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace Sounding
{
	static class Designer
	{
		private static readonly string[] _designerProcessNames = new[] { "xdesproc", "devenv" };

		private static bool? _runningFromVisualStudioDesigner = null;
		public static bool RunningFromVisualStudioDesigner
		{
		  get
		  {
			if (!_runningFromVisualStudioDesigner.HasValue)
			{
			  using (System.Diagnostics.Process currentProcess = System.Diagnostics.Process.GetCurrentProcess())
			  {
				_runningFromVisualStudioDesigner = _designerProcessNames.Contains(currentProcess.ProcessName.ToLower().Trim());
			  }
			}

			return _runningFromVisualStudioDesigner.Value;
		  }
		}
	}
	static class Program
	{
		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main(string[] args)
		{
			string orchestra = Settings.ReadSetting("Orchestra");
			Orchestra.LoadOrchestra(orchestra);

			Application.EnableVisualStyles();
			Application.SetCompatibleTextRenderingDefault(false);

			if(args.Length > 0)
				Application.Run(new Editor.MidiEditor(args[0]));
			else
				Application.Run(new Editor.MidiEditor());
			
			Orchestra.SaveOrchestra();
			Settings.WriteSetting("Orchestra", Orchestra.Name);
		}
	}
}
