﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Windows.Forms.Design;

using System.Diagnostics;

namespace Sounding
{
	[ToolStripItemDesignerAvailability(ToolStripItemDesignerAvailability.ToolStrip)]
	public class ToolSpinBox : ToolStripControlHost
	{
		public ToolSpinBox() : base(new SpinBox())
		{
			SpinBoxControl.ValueChanged += Changed;
		}
		public SpinBox SpinBoxControl { get { return Control as SpinBox; } }

		public string Label { get { return SpinBoxControl.Label; } set { SpinBoxControl.Label = value; } }
		public decimal MinValue { get { return SpinBoxControl.MinValue; } set { SpinBoxControl.MinValue = value; } }
		public decimal MaxValue { get { return SpinBoxControl.MaxValue; } set { SpinBoxControl.MaxValue = value; } }
		public decimal Increment { get { return SpinBoxControl.Increment; } set { SpinBoxControl.Increment = value; } }
		public int DecimalPlaces { get { return SpinBoxControl.DecimalPlaces; } set { SpinBoxControl.DecimalPlaces = value; } }
		public decimal Value { get { return SpinBoxControl.Value; } set { SpinBoxControl.Value = value; } }

		public event EventHandler<SpinBox.ValueChangedArgs> ValueChanged;

		void Changed(object sender, SpinBox.ValueChangedArgs e)
		{
			if(ValueChanged != null)
				ValueChanged(sender, e);
		}

		public void SetValueWithoutEvent(decimal value)
		{
			if(value < MinValue)
				value = MinValue;
			else if(value > MaxValue)
				value = MaxValue;

			SpinBoxControl.SetValueWithoutEvent(value);
		}
	}

	public partial class SpinBox : UserControl
	{
		public class ValueChangedArgs : EventArgs
		{
			public float Float { get { return(float)Value; } }
			public decimal Value { get; private set; }
			public decimal previousValue { get; private set; }
			public ValueChangedArgs(decimal value, decimal previousValue)
			{
				Value = value;
				this.previousValue = previousValue;
			}
		}
		public event EventHandler<ValueChangedArgs> ValueChanged;

		[DefaultValue("Label")]
		public string Label { get; set; }
		public decimal MinValue { get; set; }
		[DefaultValue(1.0)]
		public decimal MaxValue { get; set; }
		[DefaultValue(.05)]
		public decimal Increment { get; set; }

		public int DecimalPlaces { get; set; }
		public decimal Value
		{
			get { return this.value; }
			set
			{
				decimal prev = this.value;
				SetValueWithoutEvent(value);

				if(ValueChanged != null)
					ValueChanged(this, new ValueChangedArgs(this.value, prev));
			}
		}
		decimal value = 0;

		[Browsable(false)]
		public float Float { get { return (float)Value; } set { Value = (decimal) value; } }

		public void SetValueWithoutEvent(float f)
		{
			decimal value = (decimal)f;
			if(value < MinValue)
				this.value = MinValue;
			else if(value > MaxValue)
				this.value = MaxValue;
			else
				this.value = value;
			Refresh();
		}
		public void SetValueWithoutEvent(decimal value)
		{
			if(value < MinValue)
				this.value = MinValue;
			else if(value > MaxValue)
				this.value = MaxValue;
			else
				this.value = value;
			Refresh();
		}

		public enum EnabledState
		{
			Enabled,
			Disabled,
			PartialAbled
		}
		EnabledState state = EnabledState.Enabled;
		public EnabledState State
		{
			get { return state; }
			set { state = value; Refresh(); }
		}
		
		public new bool Enabled { get { return !Disabled; } set { Disabled = !value; } }
		public bool Disabled
		{
			get { return State == EnabledState.Disabled; }
			set 
			{ 
				if(value)
					State = EnabledState.Disabled;
				else
					State = EnabledState.Enabled;
				Refresh(); 
			}
		}
		
		public Font drawFont = new Font("Verdana", 10);

		bool hovering = false;
		bool keyboard = false;
		decimal keyBackup = 0;

		string toValue = "";

		void Update(object sender, ValueChangedArgs e)
		{
			Refresh();
		}
		public override Size GetPreferredSize(Size proposedSize)
		{
			return MinimumSize;
		}
		public override Size MinimumSize
		{
			get 
			{
 				string format = "F" + DecimalPlaces.ToString();
				string disp = Label + MaxValue.ToString(format);
				return TextRenderer.MeasureText(disp, Font) + new Size(9,3); 
			}
			set	{ base.MinimumSize = value; }
		}
		public SpinBox()
		{
			ValueChanged = Update;
			InitializeComponent();
		}

		protected override void OnPaint(PaintEventArgs e)
		{
			
			if(hovering)
				ControlPaint.DrawFocusRectangle(e.Graphics, ClientRectangle);

			Color c = ForeColor;
			if(Disabled)
				c = Color.DimGray;

			Size test = TextRenderer.MeasureText(Label, Font);
			TextRenderer.DrawText(e.Graphics, Label, Font, new Rectangle(0, 0, test.Width, test.Height), c);
			
			if(State == EnabledState.PartialAbled)
				return;

			string valueDisplay;
			Color valueColor = c;
			if(string.IsNullOrEmpty(toValue))
			{
				string format = "F" + DecimalPlaces.ToString();
				valueDisplay = Value.ToString(format);
			}
			else
			{
				valueColor = Color.Green;
				valueDisplay = toValue;
			}

			Size test2 = TextRenderer.MeasureText(valueDisplay, Font);
			TextRenderer.DrawText(e.Graphics, valueDisplay, Font, new Rectangle(test.Width, 0, test2.Width, test2.Height), valueColor);

			
		}
		void FinishKeyboard()
		{
			keyboard = false;
			
			value = keyBackup;
			decimal test;
			if(decimal.TryParse(toValue, out test))
				Value = test;

			toValue = "";
			
			Refresh();
		}
		protected override void OnKeyPress(KeyPressEventArgs e)
		{
			base.OnKeyPress(e);
			
			if(!keyboard || Disabled)
				return;

			toValue += e.KeyChar;

			decimal test;
			if(decimal.TryParse(toValue, out test))
			{
				SetValueWithoutEvent(test);
				Refresh();
			}
		}
		protected override void OnKeyUp(KeyEventArgs e)
		{
			base.OnKeyDown(e);
			if(Disabled)
				return;

			if(e.KeyCode == Keys.Enter)
			{
				FinishKeyboard();
			}
		}
		protected override void OnMouseWheel(MouseEventArgs e)
		{
			//base.OnMouseWheel(e);
			if(!hovering || Disabled)
				return;
			((HandledMouseEventArgs)e).Handled = true;
			Value += Increment * Math.Sign(e.Delta);
			Refresh();
		}

		protected override void OnMouseEnter(EventArgs e)
		{
			base.OnMouseEnter(e);
			if(Disabled)
				return;
			if(!ParentForm.ContainsFocus)
				return;
			
			hovering = true;
			Select();
			
			Refresh();
		}
		protected override void OnMouseLeave(EventArgs e)
		{
			ForeColor = DefaultForeColor;
			hovering = false;

			if(keyboard == true)
				FinishKeyboard();
			
			Refresh();
			base.OnMouseLeave(e);
		}
		protected override void OnMouseClick(MouseEventArgs e)
		{
			if(Disabled)
				return;
			base.OnMouseClick(e);

			if(keyboard == true)
				FinishKeyboard();
			else
			{
				keyBackup = Value;
				keyboard = true;
			}
		}
	}
}
