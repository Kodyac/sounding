﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Windows.Forms.Design;
using System.ComponentModel;

namespace Sounding
{
	[ToolStripItemDesignerAvailability(ToolStripItemDesignerAvailability.ToolStrip)]
    public class ToolstripNumber : ToolStripControlHost
    {
        public ToolstripNumber() : base(new NumericUpDown())
        { 
        }
		
		public decimal Increment { get { return ((NumericUpDown)NumericUpDownControl).Increment; } set { ((NumericUpDown)NumericUpDownControl).Increment = value; } }
		public decimal Maximum { get { return ((NumericUpDown)NumericUpDownControl).Maximum; } set { ((NumericUpDown)NumericUpDownControl).Maximum = value; } }
		public decimal Minimum { get { return ((NumericUpDown)NumericUpDownControl).Minimum; } set { ((NumericUpDown)NumericUpDownControl).Minimum = value; } }
		public int DecimalPlaces { get{ return ((NumericUpDown)NumericUpDownControl).DecimalPlaces; } set{ ((NumericUpDown)NumericUpDownControl).DecimalPlaces = value; } }

		public decimal Value
		{
			get
			{
				return ((NumericUpDown)NumericUpDownControl).Value;
			}
			set
			{
				if(value <= ((NumericUpDown)NumericUpDownControl).Maximum && value >= ((NumericUpDown)NumericUpDownControl).Minimum)
					((NumericUpDown)NumericUpDownControl).Value = value;
			}
		}

        protected override void OnSubscribeControlEvents(Control control)
        {
            base.OnSubscribeControlEvents(control);
            ((NumericUpDown)control).ValueChanged += new EventHandler(OnValueChanged);
        }

        protected override void OnUnsubscribeControlEvents(Control control)
        {
            base.OnUnsubscribeControlEvents(control);
            ((NumericUpDown)control).ValueChanged -= new EventHandler(OnValueChanged);
        }

        public event EventHandler ValueChanged;

        public Control NumericUpDownControl
        {
            get { return Control as NumericUpDown; }
        }

        public void OnValueChanged(object sender, EventArgs e)
        {
            if (ValueChanged != null)
            {
                ValueChanged(this, e);
            }
        }
    }
}
