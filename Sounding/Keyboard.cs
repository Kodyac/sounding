﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

using System.Diagnostics;

using System.Runtime.InteropServices;


namespace Sounding
{
	public class Keyboard
	{
		public static Instrument instrument = new Instrument();

		static Keyboard()
		{
			SoundWave.AddRead(Read);
		}
		public delegate void Record(ActiveNote recordedNote);
		public static Record recording = null;

		static public float currentBeat = .0f;
		static public float volume = 1.0f;
		static public int octave = 4;
		static List<ActiveNote> currentNotes = new List<ActiveNote>();
		public static List<ActiveNote> CurrentNotes { get { return currentNotes; } }

		public static void ClearNotes()
		{
			currentNotes.Clear();
		}

		public static void StartNote(Key note)
		{
			lock(currentNotes)
			{
				//currentNotes.Add(new ActiveNote(note, 1.0f));
				int[] intervals = Chord.IntervalOffsets();
				for(int iii = 0; iii < intervals.Length; iii++)
				{
					currentNotes.Add(new ActiveNote(note + intervals[iii], 1.0f));
				}

				if(recording != null)
					recording(currentNotes[currentNotes.Count - 1]);
				
			}
		}
		public static void EndNote(Key note)
		{
			lock(currentNotes)
			{
				for(int iii = 0; iii < currentNotes.Count; iii++)
				{
					/*if(currentNotes[iii].key == note && currentNotes[iii].Alive)
						currentNotes[iii].Alive = false;*/

					int[] intervals = Chord.IntervalOffsets();
					for(int ccc = 0; ccc < intervals.Length; ccc++)
					{
						if(currentNotes[iii].key == (note + intervals[ccc]) && currentNotes[iii].Alive)
							currentNotes[iii].Alive = false;
					}
				}
			}
		}
		public static void Read(float[] buffer, int offset, int sampleCount, int channels)
		{
			float[] data = new float[sampleCount];

			lock(currentNotes)
			{
				//instrument.GetSamples(currentNotes, ref data, volume);
				for(int keyIndex = currentNotes.Count - 1; keyIndex > -1; keyIndex--)
				{
					bool dead = instrument.GetSamples(currentNotes[keyIndex], ref data, volume);
					if(dead)
					{
						currentNotes.RemoveAt(keyIndex);
						continue;
					}
				}
			}

			currentBeat += (((float)sampleCount / channels) / (float)SoundWave.sampleRate) * DeviceSettings.beatsPerSecond;
			
			for(int iii = 0; iii < sampleCount; iii++)
				buffer[iii + offset] += data[iii];

			if(currentNotes.Count < 1 && recording == null)
				currentBeat = .0f;
		}

		public static void OnKeyDown(Keys key)
		{
			bool useKey;
			Key n = KeyNote(key, out useKey);
			if(!useKey)
				return;

			bool contains = false;
			for(int iii = 0; iii < currentNotes.Count; iii++)	//this mess is needed to stop key repeats from firing this event over and over
			{
				//if(currentNotes[iii].note == n && currentNotes[iii].beatEnd > currentBeat)
				if(currentNotes[iii].key == n && currentNotes[iii].Alive)
					contains = true;
			}
			if(!contains)
				StartNote(n);
		}
		public static void OnKeyUp(Keys key)
		{
			bool useKey;
			Key n = KeyNote(key, out useKey);
			if(!useKey)
				return;

			EndNote(n);
		}
		
		static public Key KeyNote(Keys k, out bool valid)
		{
			int curOctave = octave;
			if(Control.IsKeyLocked(Keys.CapsLock))
				curOctave++;

			Key ret = new Key('C', curOctave);
			
			valid = true;
			switch (k)
			{
				case Keys.Q:
					ret = new Key('C', curOctave);
					break;
				case Keys.D2:
					ret = new Key('C', curOctave, 1);
					break;
				case Keys.W:
					ret = new Key('D', curOctave);
					break;
				case Keys.D3:
					ret = new Key('D', curOctave, 1);
					break;
				case Keys.E:
					ret = new Key('E', curOctave);
					break;
				case Keys.R:
					ret = new Key('F', curOctave);
					break;
				case Keys.D5:
					ret = new Key('F', curOctave, 1);
					break;
				case Keys.T:
					ret = new Key('G', curOctave);
					break;
				case Keys.D6:
					ret = new Key('G', curOctave, 1);
					break;
				case Keys.Y:
					ret = new Key('A', curOctave + 1);
					break;
				case Keys.D7:
					ret = new Key('A', curOctave + 1, 1);
					break;
				case Keys.U:
					ret = new Key('B', curOctave + 1);
					break;
				case Keys.I:
					ret = new Key('C', curOctave + 1);
					break;
				case Keys.D9:
					ret = new Key('C', curOctave + 1, 1);
					break;
				case Keys.O:
					ret = new Key('D', curOctave + 1);
					break;
				case Keys.D0:
					ret = new Key('D', curOctave + 1, 1);
					break;
				case Keys.P:
					ret = new Key('E', curOctave + 1);
					break;
				//lower
				case Keys.Z:
					ret = new Key('C', curOctave - 1);
					break;
				case Keys.S:
					ret = new Key('C', curOctave - 1, 1);
					break;
				case Keys.X:
					ret = new Key('D', curOctave - 1);
					break;
				case Keys.D:
					ret = new Key('D', curOctave - 1, 1);
					break;
				case Keys.C:
					ret = new Key('E', curOctave - 1);
					break;
				case Keys.V:
					ret = new Key('F', curOctave - 1);
					break;
				case Keys.G:
					ret = new Key('F', curOctave - 1, 1);
					break;
				case Keys.B:
					ret = new Key('G', curOctave - 1);
					break;
				case Keys.H:
					ret = new Key('G', curOctave - 1, 1);
					break;
				case Keys.N:
					ret = new Key('A', curOctave);
					break;
				case Keys.J:
					ret = new Key('A', curOctave, 1);
					break;
				case Keys.M:
					ret = new Key('B', curOctave);
					break;
				case Keys.Oemcomma:
					ret = new Key('C', curOctave);
					break;
				case Keys.L:
					ret = new Key('C', curOctave, 1);
					break;
				case Keys.OemPeriod:
					ret = new Key('D', curOctave);
					break;
				case Keys.OemSemicolon:
					ret = new Key('D', curOctave, 1);
					break;
				case Keys.OemQuestion://(Keys)191:
					ret = new Key('E', curOctave);
					break;
				default:
					valid = false;
					break;
			}
			return ret;
		}
	}
}
