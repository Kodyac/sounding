﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using System.Drawing;

namespace Sounding
{
	public static class GuiExtensions
	{
		static public Point Add(this Point a, Point b)
		{
			return new Point(a.X + b.X, a.Y + b.Y);
		}
		static public void FillFromEnum(this ComboBox combo, Type en)
		{
			string[] strings = new string[Enum.GetValues(en).Length];
			
			if(strings.Length < 1)
				return;

			for(int iii = 0; iii < strings.Length; iii++)
				strings[iii] = Enum.GetValues(en).GetValue(iii).ToString();
			
			combo.Items.Clear();
			combo.Items.AddRange(strings);
			combo.SelectedIndex = 0;
		}
		static public void FillFromEnum(this ToolStripComboBox combo, Type en)
		{
			combo.ComboBox.FillFromEnum(en);
		}

		public class ListViewItemComparer : IComparer
		{
			SortOrder order;
			private int col;
			public ListViewItemComparer()
			{
				col = 0;
			}
			public ListViewItemComparer(int column, SortOrder sortOrder)
			{
				order = sortOrder;
				col = column;
			}
			public int Compare(object x, object y)
			{
				int returnVal;

				if(((ListViewItem)x).SubItems[0].Text.CompareTo("<new>") == 0)
					return 1;
				else if(((ListViewItem)y).SubItems[0].Text.CompareTo("<new>") == 0)
					return -1;

				if(((ListViewItem)x).SubItems.Count - 1 < col || ((ListViewItem)y).SubItems.Count - 1 < col)
				{
					if(((ListViewItem)x).SubItems.Count < ((ListViewItem)y).SubItems.Count)
						returnVal = 1;
					else
						returnVal = -1;
				}
				else
				{
					System.DateTime firstDate;
					System.DateTime secondDate;
					if(	System.DateTime.TryParse(((ListViewItem)x).SubItems[col].Text, out firstDate) &&
						System.DateTime.TryParse(((ListViewItem)y).SubItems[col].Text, out secondDate))
						returnVal = DateTime.Compare(firstDate, secondDate);
					else
						returnVal = String.Compare(((ListViewItem)x).SubItems[col].Text, ((ListViewItem)y).SubItems[col].Text);
				}

				if (order == SortOrder.Descending)
					returnVal *= -1;
				return returnVal;
			}
		}

		[System.Flags]
		public enum RectBorders : uint
		{
			none	= 0x00000000,
			left	= 0x00000001,
			right	= 0x00000010,
			up		= 0x00000100,
			down	= 0x00001000
		}
		public static RectBorders GetRectBorder(RectangleF r, Size allowance, Point cursorPos)
		{
			RectBorders ret = RectBorders.none;
			if(cursorPos.Y > (r.Top - allowance.Height) && cursorPos.Y < (r.Bottom + allowance.Height))
			{
				if(cursorPos.X > (r.Left - allowance.Width) && cursorPos.X < (r.Left + allowance.Width))
					ret |= RectBorders.left;
				else if(cursorPos.X > (r.Right - allowance.Width) && cursorPos.X < (r.Right + allowance.Width))
					ret |= RectBorders.right;
			}

			if(cursorPos.X > (r.Left - allowance.Width) && cursorPos.X < (r.Right + allowance.Width))
			{
				if(cursorPos.Y > (r.Top - allowance.Height) && cursorPos.Y < (r.Top + allowance.Height))
					ret |= RectBorders.up;
				else if(cursorPos.Y > (r.Bottom - allowance.Height) && cursorPos.Y < (r.Bottom + allowance.Height))
					ret |= RectBorders.down;
			}

			return ret;
		}
		public class SelectionList<T> : List<T>
		{
			public delegate void SelUpdate();
			public SelUpdate UpdateSelection;

			public SelectionList(SelUpdate updater) : base()
			{
				UpdateSelection = updater;
			}

			public void Add(List<T> items)
			{
				for(int iii = 0; iii < items.Count; iii++)
				{
					if(!Contains(items[iii]))
						base.Add(items[iii]);
				}
				UpdateSelection();
			}
			public new void Add(T item)
			{
				if(!Contains(item))
				{
					base.Add(item);
				}
				UpdateSelection();
			}
			public new void Clear()
			{
				if(base.Count < 1)
					return;
				base.Clear();
				UpdateSelection();
			}
		}
	}
}
