﻿namespace Sounding.Editor
{
	partial class Prompt
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.mainLayout = new System.Windows.Forms.TableLayoutPanel();
			this.controlsLayout = new System.Windows.Forms.TableLayoutPanel();
			this.buttonLayout = new System.Windows.Forms.TableLayoutPanel();
			this.confirm = new System.Windows.Forms.Button();
			this.cancel = new System.Windows.Forms.Button();
			this.mainLayout.SuspendLayout();
			this.buttonLayout.SuspendLayout();
			this.SuspendLayout();
			// 
			// mainLayout
			// 
			this.mainLayout.AutoSize = true;
			this.mainLayout.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
			this.mainLayout.ColumnCount = 1;
			this.mainLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.mainLayout.Controls.Add(this.controlsLayout, 0, 0);
			this.mainLayout.Controls.Add(this.buttonLayout, 0, 1);
			this.mainLayout.Dock = System.Windows.Forms.DockStyle.Fill;
			this.mainLayout.Location = new System.Drawing.Point(0, 0);
			this.mainLayout.Margin = new System.Windows.Forms.Padding(0);
			this.mainLayout.Name = "mainLayout";
			this.mainLayout.RowCount = 2;
			this.mainLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.mainLayout.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.mainLayout.Size = new System.Drawing.Size(284, 262);
			this.mainLayout.TabIndex = 0;
			// 
			// controlsLayout
			// 
			this.controlsLayout.AutoSize = true;
			this.controlsLayout.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
			this.controlsLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
			this.controlsLayout.Dock = System.Windows.Forms.DockStyle.Fill;
			this.controlsLayout.Location = new System.Drawing.Point(3, 3);
			this.controlsLayout.Name = "controlsLayout";
			this.controlsLayout.RowCount = 1;
			this.controlsLayout.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.controlsLayout.Size = new System.Drawing.Size(278, 227);
			this.controlsLayout.TabIndex = 0;
			// 
			// buttonLayout
			// 
			this.buttonLayout.Anchor = System.Windows.Forms.AnchorStyles.Right;
			this.buttonLayout.AutoSize = true;
			this.buttonLayout.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
			this.buttonLayout.ColumnCount = 2;
			this.buttonLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.buttonLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.buttonLayout.Controls.Add(this.confirm, 0, 0);
			this.buttonLayout.Controls.Add(this.cancel, 1, 0);
			this.buttonLayout.Location = new System.Drawing.Point(122, 233);
			this.buttonLayout.Margin = new System.Windows.Forms.Padding(0);
			this.buttonLayout.Name = "buttonLayout";
			this.buttonLayout.RowCount = 1;
			this.buttonLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.buttonLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.buttonLayout.Size = new System.Drawing.Size(162, 29);
			this.buttonLayout.TabIndex = 3;
			// 
			// confirm
			// 
			this.confirm.Location = new System.Drawing.Point(3, 3);
			this.confirm.Name = "confirm";
			this.confirm.Size = new System.Drawing.Size(75, 23);
			this.confirm.TabIndex = 2;
			this.confirm.Text = "Confirm";
			this.confirm.UseVisualStyleBackColor = true;
			this.confirm.Click += new System.EventHandler(this.confirm_Click);
			// 
			// cancel
			// 
			this.cancel.Location = new System.Drawing.Point(84, 3);
			this.cancel.Name = "cancel";
			this.cancel.Size = new System.Drawing.Size(75, 23);
			this.cancel.TabIndex = 1;
			this.cancel.Text = "Cancel";
			this.cancel.UseVisualStyleBackColor = true;
			this.cancel.Click += new System.EventHandler(this.cancel_Click);
			// 
			// Prompt
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.AutoSize = true;
			this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
			this.ClientSize = new System.Drawing.Size(284, 262);
			this.Controls.Add(this.mainLayout);
			this.Name = "Prompt";
			this.Text = "Prompt";
			this.mainLayout.ResumeLayout(false);
			this.mainLayout.PerformLayout();
			this.buttonLayout.ResumeLayout(false);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.TableLayoutPanel mainLayout;
		private System.Windows.Forms.TableLayoutPanel controlsLayout;
		private System.Windows.Forms.Button cancel;
		private System.Windows.Forms.Button confirm;
		private System.Windows.Forms.TableLayoutPanel buttonLayout;
	}
}