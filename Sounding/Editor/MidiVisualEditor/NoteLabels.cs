﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Sounding.Editor
{
	public partial class NoteLabels : UserControl
	{
		
		new MidiVisualEditor Parent { get { return base.Parent.Parent as MidiVisualEditor; } }
		MidiVisualCanvas Canvas { get { return Parent.Canvas; } }
		int noteCount { get { return MidiVisualCanvas.noteCount; } }
		int noteHeight { get { return Canvas.noteHeight; } }// = Canvas.noteHeight;
		Size ViewSize { get { return Canvas.CanvasSize; } }// Canvas.CanvasSize;
		Point scrollPos { get { return Canvas.scrollPos; } }//= Canvas.scrollPos;

		Font noteFont { get { return new Font("Courier New", Canvas.noteHeight * .6f); } }
		int xMargin { get { return TextRenderer.MeasureText("G9#", noteFont).Width; } }

		public NoteLabels()
		{
			InitializeComponent();
		}
		
		
		public static Key Root = new Key('C', 4);
		Color KeyColour = Color.FromArgb(0, 128, 0);
		Color HiglightColour = Color.FromArgb(64, 64, 0);

		public bool ScaleFunction(int interval)
		{
			int[] scales = Sounding.Scale.IntervalOffsets();
			for(int iii = 1; iii < scales.Length; iii++)
			{
				if((interval % 12) == (Root.Interval + scales[iii]) % 12)
					return true;
			}
			return false;

			/*switch (Sounding.Scale.current)
			{
				case Sounding.Scale.Types.Major:
					return Major(interval);
				case Sounding.Scale.Types.Minor:
					return Minor(interval);
			}
			return false;;*/
		}
		static bool Major(int interval)
		{
			return ((interval % 12) == ((Root.Interval + 4) % 12) || (interval % 12) == ((Root.Interval + 7) % 12));
		}
		static bool Minor(int interval)
		{
			return ((interval % 12) == ((Root.Interval + 3) % 12) || (interval % 12) == ((Root.Interval + 7) % 12));
		}
		protected override void OnPaint(PaintEventArgs e)
		{
			if(Parent == null)
				return;
			Parent.mainLayout.ColumnStyles[0] = new ColumnStyle(SizeType.Absolute, xMargin);

			Graphics g = e.Graphics;
			using(SolidBrush brush = new SolidBrush(Color.White))
			using(Pen pen = new Pen(Color.FromArgb(32, 255, 255, 255)))
			{
				g.ResetTransform();
				
				
				for(int iii = 0; iii < noteCount; iii++)
				{
					int yPos = ViewSize.Height - iii * noteHeight - noteHeight;


					Key k = new Key(iii);
					
					yPos -= scrollPos.Y;
					
					if((k.Interval % 12) == (Root.Interval % 12))
					{
						brush.Color = KeyColour;
						g.FillRectangle(brush, .0f, yPos, ClientSize.Width,  noteHeight);
					}
					else if(ScaleFunction(k.Interval))
					{
						brush.Color = HiglightColour;
						g.FillRectangle(brush, .0f, yPos, ClientSize.Width,  noteHeight);
					}

					brush.Color = Color.White;

					g.DrawString(k.ToString(), noteFont, brush, new Point(0, yPos));

					yPos = ViewSize.Height - iii * noteHeight - scrollPos.Y;
					g.DrawLine(pen, new Point(0, yPos), new Point(ClientSize.Width, yPos));

					
				}

				g.DrawLine(pen, new Point(xMargin, 0), new Point(xMargin, noteHeight * noteCount));
			}
			base.OnPaint(e);
		}

		protected override void OnMouseDown(MouseEventArgs e)
		{
			if(e.Button == System.Windows.Forms.MouseButtons.Left)
				Root = Canvas.MouseKey(e.Y);
			
			base.OnMouseDown(e);
		}

		public void majorToolStripMenuItem_Click(object sender, EventArgs e)
		{
			ClearChordChecks();
			Sounding.Scale.current = Sounding.Scale.Types.Major;
			majorToolStripMenuItem.Checked = true;
		}
		public void naturalMinorToolStripMenuItem_Click(object sender, EventArgs e)
		{
			ClearChordChecks();
			Sounding.Scale.current = Sounding.Scale.Types.NaturalMinor;
			minorToolStripMenuItem.Checked = true;
		}
		public void harmonicMinorToolStripMenuItem_Click(object sender, EventArgs e)
		{
			ClearChordChecks();
			Sounding.Scale.current = Sounding.Scale.Types.HarmonicMinor;
			harmonicMinorToolStripMenuItem.Checked = true;
		}

		public void melodicMinorToolStripMenuItem_Click(object sender, EventArgs e)
		{
			ClearChordChecks();
			Sounding.Scale.current = Sounding.Scale.Types.MelodicMinor;
			melodicMinorToolStripMenuItem.Checked = true;
		}

		void ClearChordChecks()
		{
			majorToolStripMenuItem.Checked = false;
			minorToolStripMenuItem.Checked = false;
			harmonicMinorToolStripMenuItem.Checked = false;
			melodicMinorToolStripMenuItem.Checked = false;
		}
		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);
		}

		
		
	}
}

