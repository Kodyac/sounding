﻿namespace Sounding.Editor
{
	partial class MidiVisualCanvas
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.contextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
			this.programEventToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.controlChangeEventToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
			this.tempoEventToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.timeSignatureEventToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.textEventToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.contextMenu.SuspendLayout();
			this.SuspendLayout();
			// 
			// contextMenu
			// 
			this.contextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.programEventToolStripMenuItem,
            this.controlChangeEventToolStripMenuItem,
            this.toolStripSeparator1,
            this.tempoEventToolStripMenuItem,
            this.timeSignatureEventToolStripMenuItem,
            this.textEventToolStripMenuItem});
			this.contextMenu.Name = "contextMenu";
			this.contextMenu.Size = new System.Drawing.Size(191, 142);
			// 
			// programEventToolStripMenuItem
			// 
			this.programEventToolStripMenuItem.Name = "programEventToolStripMenuItem";
			this.programEventToolStripMenuItem.Size = new System.Drawing.Size(190, 22);
			this.programEventToolStripMenuItem.Text = "Program Event";
			this.programEventToolStripMenuItem.Click += new System.EventHandler(this.programEventToolStripMenuItem_Click);
			// 
			// controlChangeEventToolStripMenuItem
			// 
			this.controlChangeEventToolStripMenuItem.Name = "controlChangeEventToolStripMenuItem";
			this.controlChangeEventToolStripMenuItem.Size = new System.Drawing.Size(190, 22);
			this.controlChangeEventToolStripMenuItem.Text = "Control Change Event";
			this.controlChangeEventToolStripMenuItem.Click += new System.EventHandler(this.controlChangeEventToolStripMenuItem_Click);
			// 
			// toolStripSeparator1
			// 
			this.toolStripSeparator1.Name = "toolStripSeparator1";
			this.toolStripSeparator1.Size = new System.Drawing.Size(187, 6);
			// 
			// tempoEventToolStripMenuItem
			// 
			this.tempoEventToolStripMenuItem.Name = "tempoEventToolStripMenuItem";
			this.tempoEventToolStripMenuItem.Size = new System.Drawing.Size(190, 22);
			this.tempoEventToolStripMenuItem.Text = "Tempo Event";
			this.tempoEventToolStripMenuItem.Click += new System.EventHandler(this.tempoEventToolStripMenuItem_Click);
			// 
			// timeSignatureEventToolStripMenuItem
			// 
			this.timeSignatureEventToolStripMenuItem.Name = "timeSignatureEventToolStripMenuItem";
			this.timeSignatureEventToolStripMenuItem.Size = new System.Drawing.Size(190, 22);
			this.timeSignatureEventToolStripMenuItem.Text = "Time Signature Event";
			this.timeSignatureEventToolStripMenuItem.Click += new System.EventHandler(this.timeSignatureEventToolStripMenuItem_Click);
			// 
			// textEventToolStripMenuItem
			// 
			this.textEventToolStripMenuItem.Name = "textEventToolStripMenuItem";
			this.textEventToolStripMenuItem.Size = new System.Drawing.Size(190, 22);
			this.textEventToolStripMenuItem.Text = "Text Event";
			this.textEventToolStripMenuItem.Click += new System.EventHandler(this.textEventToolStripMenuItem_Click);
			// 
			// MidiVisualCanvas
			// 
			this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(4)))), ((int)(((byte)(8)))), ((int)(((byte)(2)))));
			this.ContextMenuStrip = this.contextMenu;
			this.DoubleBuffered = true;
			this.ForeColor = System.Drawing.Color.Aqua;
			this.MinimumSize = new System.Drawing.Size(400, 200);
			this.Name = "MidiVisualCanvas";
			this.Size = new System.Drawing.Size(400, 200);
			this.Load += new System.EventHandler(this.MidiVisualEditor_Load);
			this.contextMenu.ResumeLayout(false);
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.ContextMenuStrip contextMenu;
		private System.Windows.Forms.ToolStripMenuItem programEventToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem controlChangeEventToolStripMenuItem;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
		private System.Windows.Forms.ToolStripMenuItem tempoEventToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem timeSignatureEventToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem textEventToolStripMenuItem;

	}
}
