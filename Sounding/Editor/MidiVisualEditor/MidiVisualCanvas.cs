﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using System.Diagnostics;

namespace Sounding.Editor
{
	public partial class MidiVisualCanvas : UserControl
	{
		//public ChordType chordType = ChordType.None;

		public MidiEditor parent = null;

		public HScrollBar horizontalBar;
		public VScrollBar verticalBar;
		
		const int resizeAllowance = 3;
		
		const int bnoteHeight = 4;
		const int bbeatWidth = 16;
		public const int noteCount = 128/*Key.IntervalsInOctave * Key.OctaveRange*/;

		public virtual int noteHeight { get { return (int)(bnoteHeight * zoomSize); } }
		protected virtual int beatWidth		{ get { return (int)(bbeatWidth * zoomSize); } }
		protected virtual int noteWidth		{ get { return beatWidth * 4; } }
		protected float zoomSize	{ get { return (izoomSize < 1.0f) ? 1.0f : izoomSize; } set { izoomSize = value; } }
		protected float izoomSize = 4;

		protected float beatStart = .0f;
		protected float beatEnd = 4.0f;

		public float CurrentBeat = .0f;


		public float BeatStart 
		{ 
			get { return beatStart; } 
			set { beatStart = value; } 
		}
		public float BeatEnd 
		{ 
			get { return beatEnd; } 
			set { beatEnd = value; } 
		}

		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public Song Song
		{
			get { if(parent == null) return null; return parent.song; }
			set { parent.song = value; EvaluateSize(); ScrollToNote(new Key('C', 4)); }
		}

		//public event EventHandler<EventArgs> PlayRangeChanged;
		public event EventHandler<EventArgs> SongChanged;

	//	public delegate void Zoom();
	//	public Zoom zoom;
		
		protected Timer loadTimer = new Timer();
		protected Timer redrawTimer = new Timer();

		public List<int> trackViewIndexes = new List<int>();
		public List<int> channelViewIndexes = new List<int>();

		Track SelectedTrack  { get { return parent.SelectedTrack; } }// = null;
		//Track SelectedTrack { get { return parent.SelectedTrack; } }
		public byte channelEditIndex;

		/// <summary>notesize in Notes</summary>
		public float defaultNoteSize = .25f;
		bool recording = false;
		public bool playing = false;
		public int snapIndex = 2;
		//public TimeSignature timeSignature = new TimeSignature(4, 4);
		
		[Browsable(false)]
		public GuiExtensions.SelectionList<Event> selectedEvents;
		GuiExtensions.SelectionList<Event> SelectedEvents { get { return selectedEvents; } }
		List<Event> copyBuffer = new List<Event>();

		List<NoteOnEvent> recordingNotes = new List<NoteOnEvent>();
		List<NoteOnEvent> recordingNotesInProgress = new List<NoteOnEvent>();

		/// <summary>Colour of the instrument at specified time in the current track & channel</summary>
		Color CurrentColour(int tick = 0)
		{
			Color ret = Color.White;
			if(SelectedTrack == null)
				return ret;
			NoteEvent progChange = SelectedTrack.GetNoteEventAtTime(tick, MidiEventsCodes.ProgramChange, channelEditIndex);
			if(progChange != null)
				ret = InstrumentEditor.GetColour(Orchestra.Programs[progChange.lsb]);
			
			return ret;
		}

		void ChangedTrack()
		{
			SelectedTrack.SortTrack();
			EvaluateSize();
			if(SongChanged != null)
				SongChanged(this, EventArgs.Empty);
		}
		
		void SnapNote(params Event[] e)
		{
			if(snapIndex >= 0 && snapIndex < MidiEditor.noteValues.Count - 1)
			{
				for(int iii = 0; iii < e.Length; iii++)
				{
					//float duration = e[iii].beatDuration;
					e[iii].absoluteTime = TickSnap(e[iii].absoluteTime);
					//e[iii].beatEnd = Snap(e[iii].beatStart + duration);
				}
			}
		}

		protected Size canvasSize;
		public Size CanvasSize { get { return canvasSize; } }

		public Point scrollPos
		{
			get
			{
				if(horizontalBar == null)
					return new Point();
				return new Point(horizontalBar.Value, verticalBar.Value);
			}
			set
			{
				if(horizontalBar == null)
					return;
				if(value.X < horizontalBar.Minimum)
					value.X = horizontalBar.Minimum;
				else if(value.X > horizontalBar.Maximum)
					value.X = horizontalBar.Maximum;

				if(value.Y < verticalBar.Minimum)
					value.Y = verticalBar.Minimum;
				else if(value.Y > verticalBar.Maximum)
					value.Y = verticalBar.Maximum;
				
				horizontalBar.Value = value.X;
				verticalBar.Value = value.Y;
			}
		}
		public void EvaluateSize()
		{
			int width = ClientSize.Width + noteWidth;
			if(Song != null && Song.tracks != null && Song.tracks.Count > 0)
			{
				int beatPixelWidth = ((int)Math.Ceiling(Song.TicksToBeat(Song.TickLength))) * beatWidth + noteWidth * 5;
				if(width < beatPixelWidth)
					width = beatPixelWidth;
			}
			canvasSize = new Size(width, noteHeight * noteCount); 

			if(horizontalBar == null)
				return;

			horizontalBar.Maximum = canvasSize.Width - ClientSize.Width;
			horizontalBar.Minimum = 0;
			
			verticalBar.Maximum = canvasSize.Height - ClientSize.Height;
			verticalBar.Minimum = 0;
			
			Invalidate();
		}

		protected Point camPanOrigin;
		protected Point mousePosition;

		public MidiVisualCanvas()
		{
			InitializeComponent();

			SetStyle(ControlStyles.AllPaintingInWmPaint | ControlStyles.UserPaint, true);
			SetStyle(ControlStyles.OptimizedDoubleBuffer, true);

		}
		private void MidiVisualEditor_Load(object sender, EventArgs e)
		{
			if(Song != null && !string.IsNullOrEmpty(Song.name))
			{
				loadTimer.Interval = 2;
				loadTimer.Tick += LoadSettings;
				loadTimer.Start();
			}

			if(Designer.RunningFromVisualStudioDesigner)
				return;
			redrawTimer.Interval = 50;
			redrawTimer.Tick += Redraw;
			redrawTimer.Start();
		}

		protected virtual void Redraw(object sender, EventArgs e)
		{
			if(parent.Scrolling)
				ScrollToBeat(CurrentBeat);

			/*if(ClientRectangle.Contains(PointToClient(Control.MousePosition)))
				Focus();*/

			Refresh();
		}
		
		protected virtual void LoadSettings(object sender, EventArgs e)
		{
			EvaluateSize();
			TopLevelControl.BringToFront();
			loadTimer.Stop();
		}

		protected override void OnScroll(ScrollEventArgs se)
		{
			base.OnScroll(se);
			EvaluateSize();
		}
		protected override void OnSizeChanged(EventArgs e)
		{
			base.OnSizeChanged(e);
			EvaluateSize();
		}
		protected override void OnPaint(PaintEventArgs e)
		{
			base.OnPaint(e);
			
			using(SolidBrush brush = new SolidBrush(Color.White))
			using(Pen pen = new Pen(Color.White))
			{
				DrawNoteLines(e.Graphics);
				
				if(Designer.RunningFromVisualStudioDesigner)
					return;

				if(Song != null)
				{
					DrawPlayBars(e.Graphics);
					
					DrawBackgroundNotes(e.Graphics);

					for(int ccc = 0; ccc < channelViewIndexes.Count; ccc++)
					{
						if(channelViewIndexes[ccc] == channelEditIndex)
							continue;
						DrawNotes(e.Graphics, Song.currentTrack, channelViewIndexes[ccc]);
					}
					DrawNotes(e.Graphics, Song.currentTrack, channelEditIndex);

					if(Recording)
						DrawRecordingNotes(e.Graphics);
					
					DrawDragNotes(e.Graphics);
				}

				DrawTimeSignature(e.Graphics);
				
				DrawSnap(e.Graphics);
				
				if(ClientRectangle.Contains(PointToClient(Control.MousePosition)) && ContainsFocus)
				{
					DrawSelRect(e.Graphics);

					DrawHighlights(e.Graphics);
				}
			}

		}
		void DrawBackgroundNotes(Graphics g)
		{
			for(int ttt = 0; ttt < trackViewIndexes.Count; ttt++)
			{	
				for(int ccc = 0; ccc < channelViewIndexes.Count; ccc++)
				{
					if(trackViewIndexes[ttt] == Song.currentTrack && channelViewIndexes[ccc] == channelEditIndex)
						continue;
					DrawNotes(g, trackViewIndexes[ttt], channelViewIndexes[ccc]/*, /*scrollPos, ClientSize.Width*/);
				}
			}
		}
		void DrawSnap(Graphics g)
		{
			if(snapIndex == MidiEditor.noteValues.Count - 1)
				return;

			g.ResetTransform();
			
			float gridWidth = (MidiEditor.noteValues[snapIndex].Value * 4.0f) * beatWidth;
			
			using(Pen pen = new Pen(Color.FromArgb(32, Color.White)))
			{
				for(float iii = -scrollPos.X; iii < ClientSize.Width + scrollPos.X; iii += gridWidth)
				{
					g.DrawLine(pen, new PointF((int)iii, 0), new PointF((int)iii, noteHeight * noteCount));
				}
			}
		}
		void DrawHighlights(Graphics g)
		{
			g.ResetTransform();
			//g.TranslateTransform(-scrollPos.X, 0);

			using(SolidBrush brush = new SolidBrush(Color.FromArgb(32, 255, 255, 255)))
			using(Pen pen = new Pen(Color.FromArgb(64, 255, 255, 255)))
			{
				for(int iii = 0; iii < Keyboard.CurrentNotes.Count; iii++)
				{
					ActiveNote an = Keyboard.CurrentNotes[iii];
					bool dead;
					float[] volume = Keyboard.instrument.AmplitudeAtSample(an, out dead);
					for(int vvv = 0; vvv < volume.Length; vvv++)
						volume[vvv] /= Keyboard.instrument.volume;

					if(dead)
						continue;
					brush.Color = Color.FromArgb((int)(volume[0] * .5f * 255), InstrumentEditor.GetColour(Keyboard.instrument));
					int yTarget = canvasSize.Height - scrollPos.Y - (an.key.Interval + 1) * noteHeight;
					RectangleF r = new RectangleF(0, yTarget, ClientSize.Width, noteHeight);
					g.FillRectangle(brush, r);
				}

				pen.Width = 3;
				
				if(noteEditing.state == NoteEditing.State.Nothing && Cursor.Current == DefaultCursor)
				{
					NoteOnEvent non = CreateNoteFromPixelPos((mousePosition));
					RectangleF noteRect = ScreenEventRect(non);
					RectangleF line = new RectangleF(0, noteRect.Y, ClientSize.Width, noteRect.Height);

					g.DrawLine(pen, new PointF(noteRect.X, 0), new PointF(noteRect.X, noteHeight * noteCount));

					brush.Color = Color.FromArgb(32, 255, 255, 255);
					g.FillRectangle(brush, line);

					brush.Color = Color.FromArgb(64, CurrentColour(ScreenToTick(mousePosition.X)));
					int interval = non.key;
					int[] offsets = Chord.IntervalOffsets();
					for(int iii = 0; iii < offsets.Length; iii++)
					{
						non.key = interval + offsets[iii];
						noteRect = ScreenEventRect(non);
						g.FillRectangle(brush, noteRect);
					}
				}
			}
		}
		List< TimeSignatureEvent > MakeTimeSigList(Track track)
		{
			List< TimeSignatureEvent > signatures = new List< TimeSignatureEvent >();
			for(int iii = 0; iii < track.events.Count; iii++)
			{
				TimeSignatureEvent teve = track.events[iii] as TimeSignatureEvent;
				if(teve == null)
					continue;
				signatures.Add(teve);
			}

			return signatures;
		}
		void DrawTimeSignature(Graphics g)
		{
			g.ResetTransform();
			
			int measure = 1;
			
			List< TimeSignatureEvent > signatures;
			if(Song.trackType == Sounding.Song.TrackTypes.Synchronous)
				signatures = MakeTimeSigList(Song.tracks[0]);
			else
				signatures = MakeTimeSigList(SelectedTrack);

			int sIndex = 0;

			TimeSignature timeSignature = new TimeSignature(4,4);
			if(signatures.Count > 0 && signatures[0].absoluteTime == 0)
				timeSignature = signatures[sIndex++].time;

			using(SolidBrush brush = new SolidBrush(Color.White))
			using(Pen pen = new Pen(Color.White))
			{
				float gridWidth = noteWidth / (float)timeSignature.value;
				int counter = 0;
				
				for(float iii = -scrollPos.X; iii < ClientSize.Width + scrollPos.X; iii += gridWidth)
				{
					int ticks = Song.BeatToTicks((iii) / (float)beatWidth);
					ticks = ScreenToTick((int)iii);
					while(sIndex < signatures.Count && ticks >= signatures[sIndex].absoluteTime)
					{
						timeSignature = signatures[sIndex].time;
						sIndex++;
						gridWidth = noteWidth / (float)timeSignature.value;
						counter = 0;
					}

					if(counter++ % timeSignature.counts == 0)
					{
						g.DrawString(measure.ToString(), Font, brush, iii, 0.0f);
						measure++;

						pen.Width = 3;
						pen.Color = Color.FromArgb(96, 255, 255, 255);
					}
					else
					{
						pen.Width = 1;
						pen.Color = Color.FromArgb(48, 255, 255, 255);
					}
					
					g.DrawLine(pen, new PointF((int)iii, 0), new PointF((int)iii, noteHeight * noteCount));
				}
			}
		}
		protected virtual void DrawNoteLines(Graphics g)
		{
			using(SolidBrush brush = new SolidBrush(Color.White))
			using(Pen pen = new Pen(Color.FromArgb(32, 255, 255, 255)))
			{
				g.ResetTransform();
				brush.Color = Color.White;
				
				for(int iii = 0; iii < noteCount; iii++)
				{
					int yPos = canvasSize.Height - iii * noteHeight - scrollPos.Y;
					g.DrawLine(pen, new Point(0, yPos), new Point(ClientSize.Width, yPos));
				}
			}
		}
		void DrawPlayBars(Graphics g)
		{
			g.ResetTransform();
			g.TranslateTransform(-scrollPos.X, 0);
			using(Pen pen = new Pen(Color.FromArgb(196, Color.Cyan)))
			{
				pen.Color = Color.FromArgb(255, 255, 64, 255);
				pen.Width = 1;

				g.DrawLine(pen, new PointF(beatStart * beatWidth, 0), new PointF(beatStart * beatWidth, noteHeight * noteCount));
				g.DrawLine(pen, new PointF(beatEnd * beatWidth, 0), new PointF(beatEnd * beatWidth, noteHeight * noteCount));

				if(parent.playButton.Checked)
					g.DrawLine(pen, new PointF(CurrentBeat * beatWidth, 0), new PointF(CurrentBeat * beatWidth, noteHeight * noteCount));
			}
		}
		void DrawRecordingNotes(Graphics g)
		{
			g.ResetTransform();
			using(Pen pen = new Pen(Color.White))
			{
				for(int iii = 0; iii < recordingNotes.Count; iii++)
				{
					RectangleF rect = ScreenEventRect(recordingNotes[iii]);

					g.DrawRectangle(pen, rect.X, rect.Y, rect.Width, rect.Height);
				}
				for(int iii = 0; iii < recordingNotesInProgress.Count; iii++)
				{
					RectangleF rect = ScreenEventRect(recordingNotesInProgress[iii]);

					g.DrawRectangle(pen, rect.X, rect.Y, rect.Width, rect.Height);
				}
			}
		}
		void DrawDragNotes(Graphics g)
		{
			if(noteEditing.state != NoteEditing.State.Dragging && noteEditing.state != NoteEditing.State.Resizing)
				return;
			
			g.ResetTransform();
			
			Point difference = ScreenToCanvas(noteEditing.mouse) - (Size)noteEditing.startOffsetCanvas;

			using(SolidBrush brush = new SolidBrush(Color.FromArgb(64, 64, 96, 224)))
			using(Pen pen = new Pen(Color.White))
			{
				if(noteEditing.state == NoteEditing.State.Dragging)
				{
					if(selectedEvents.Count == 1)
					{
						if(selectedEvents[0] is NoteOnEvent)
						{
							NoteOnEvent on = CreateNoteFromPixelPos(noteEditing.mouse);
							on.NoteOff.absoluteTime = ((NoteOnEvent)selectedEvents[0]).TickDuration + on.absoluteTime;
							g.FillRectangle(brush, ScreenNoteRect(on));
						}
					}
					else
					{
						for(int iii = 0; iii < selectedEvents.Count; iii++)
						{
							NoteOnEvent selOn = selectedEvents[iii] as NoteOnEvent;
							if(selOn == null)
								continue;
							NoteOnEvent on = selOn.Copy() as NoteOnEvent;//new NoteOnEvent(selOn);
							GetNoteFromPixelOffset(ref on, difference);
							g.FillRectangle(brush, ScreenNoteRect(on));
					
						}
					}
				}
				else if(noteEditing.state == NoteEditing.State.Resizing)
				{
					for(int iii = 0; iii < selectedEvents.Count; iii++)
					{
						NoteOnEvent selOn = selectedEvents[iii] as NoteOnEvent;
						if(selOn == null)
							continue;

						NoteOnEvent on = selOn.Copy() as NoteOnEvent;//new NoteOnEvent(selOn);
						on.NoteOff.absoluteTime = TickSnap(on.NoteOff.absoluteTime + Song.BeatToTicks(difference.X / (float)beatWidth));
						RectangleF rect = ScreenNoteRect(on);
						g.FillRectangle(brush, rect);
						g.DrawRectangle(pen, rect.X, rect.Y, rect.Width, rect.Height);
					}
				}
			}
		}
		
		protected virtual void DrawNotes(Graphics g, int tIndex, int cIndex)
		{
			if(Song == null || Song.tracks.Count <= tIndex)
				return;

			g.ResetTransform();

			Color colour = Color.White;
			float volume = 1.0f;

			int startTick = Song.BeatToTicks(scrollPos.X / beatWidth);
			int endTick = Song.BeatToTicks(ClientSize.Width / beatWidth) + startTick + Song.BeatToTicks(4.0f);
			
			using(SolidBrush brush = new SolidBrush(Color.White))
			using(Pen pen = new Pen(Color.White))
			{
				Track track = Song.tracks[tIndex];

				for(int iii = 0; iii < track.events.Count; iii++)
				{
					Event curEvent = track.events[iii] as Event;
					
					if(curEvent.absoluteTime > endTick)
						break;

					if(curEvent is NoteEvent && ((NoteEvent)curEvent).Channel != cIndex)
						continue;

					if(curEvent.Code == MidiEventsCodes.ProgramChange)
						colour = InstrumentEditor.GetColour(Orchestra.Programs[curEvent.lsb]);
					else if(curEvent.Code == MidiEventsCodes.ControlChange)
					{
						if((ControlChangeCodes)curEvent.lsb == ControlChangeCodes.ChannelVolume)
							volume = (float)curEvent.msb / 127.0f;
					}

					if(curEvent is NoteOnEvent)
					{
						if(((NoteOnEvent)curEvent).NoteOff.absoluteTime < startTick)
							continue;
					}
					else
					{
						if(curEvent.absoluteTime < startTick - Song.BeatToTicks(4.0f))
							continue;
					}
										
					if(curEvent is NoteOnEvent && ((NoteOnEvent)curEvent).Channel == 9)
						brush.Color = Color.FromArgb((int)(volume * 255), InstrumentEditor.GetColour(Orchestra.Percussion[((NoteOnEvent)curEvent).key.Interval]));
					else
						brush.Color = Color.FromArgb((int)(volume * 255), colour);
						
					DrawEvent(g, curEvent, brush, pen, tIndex == Song.currentTrack, cIndex == channelEditIndex);
				}
					
			}
		}
		void DrawEvent(Graphics g, Event eve, SolidBrush brush, Pen pen, bool selectedTrack, bool selectedChannel)
		{
			if(selectedEvents.Contains(eve))
				pen.DashStyle = System.Drawing.Drawing2D.DashStyle.Solid;
			/*else if(selectedChannel)
				pen.DashStyle = System.Drawing.Drawing2D.DashStyle.Dash;*/
			else
				pen.DashStyle = System.Drawing.Drawing2D.DashStyle.Dot;

			if(eve is NoteOnEvent)
			{
				NoteOnEvent non = eve as NoteOnEvent;
				
				RectangleF rect = ScreenEventRect(non);

				List< KeyValuePair<RectangleF, float> > noteRects = ScreenNoteRects(non);
				float inAlpha = brush.Color.A;

				for(int iii = 0; iii < noteRects.Count; iii++)
				{
					brush.Color = Color.FromArgb((int)(noteRects[iii].Value * inAlpha), brush.Color);

					g.FillRectangle(brush, noteRects[iii].Key);
				}

			//	brush.Color = Color.FromArgb((int)(non.velocity * brush.Color.A), brush.Color);

			//	g.FillRectangle(brush, rect);

				if(selectedTrack && selectedChannel)
				{
					pen.Color = Color.FromArgb(255, 255, 255, 255);

					g.DrawRectangle(pen, rect.X, rect.Y, rect.Width, rect.Height);

					//if(selectedChannel)
					{
						pen.Width = 3;
						pen.DashStyle = System.Drawing.Drawing2D.DashStyle.Solid;
						g.DrawLine(pen, new PointF(rect.X + rect.Width, rect.Y), new PointF(rect.X + rect.Width, rect.Y + rect.Height));
						pen.Width = 1;
					}
				}
				return;
			}
			
			if(!selectedTrack)
				return;
			if(!DrawnEvent(eve))
				return;
			switch (eve.Code)
			{
				default:
				{
					pen.Color = Color.FromArgb(255, 255, 255, 255);
					RectangleF rect = ScreenEventRect(eve);

					g.DrawRectangle(pen, rect.X, rect.Y, rect.Width, rect.Height);

					g.DrawString(eve.ToString(), Font, brush, rect.Location);
				}
					break;
			}
			
		}
		
		void DrawSelRect(Graphics g)
		{
			if(noteEditing.state != NoteEditing.State.Recting || (Control.MouseButtons & MouseButtons.Left) != MouseButtons.Left)
				return;
			g.ResetTransform();
			g.TranslateTransform(-scrollPos.X, -scrollPos.Y);
			using(Pen pen = new Pen(Color.FromArgb(224, 128, 255, 255)))
			{
				g.DrawRectangle(pen, noteEditing.selRect.X, noteEditing.selRect.Y, noteEditing.selRect.Width, noteEditing.selRect.Height);
			}
		}

		List< KeyValuePair<RectangleF, float> > ScreenNoteRects(NoteOnEvent on)
		{
			List< KeyValuePair<RectangleF, float>> ret = new List< KeyValuePair<RectangleF, float> >();
			List<NoteKeyEvent> ns = on.SubEvents();
			for(int iii = 0; iii < ns.Count - 1; iii++)
			{
				int tickDuration = ns[iii + 1].absoluteTime - ns[iii].absoluteTime;
				if(tickDuration < 0)
					tickDuration = Song.BeatToTicks(CurrentBeat) - ns[iii].absoluteTime;

				float rx = Song.TicksToBeat(ns[iii].absoluteTime) * beatWidth;
				float ry = canvasSize.Height - ns[iii].lsb * noteHeight - noteHeight;
				rx -= scrollPos.X;
				ry -= scrollPos.Y;
				float rWidth = Song.TicksToBeat(tickDuration) * beatWidth;
				float rHeight = noteHeight;

				RectangleF r = new RectangleF(rx, ry, rWidth, rHeight);

				float volume = (float)ns[iii].msb / 127.0f;
				ret.Add(new KeyValuePair<RectangleF,float>(r, volume));
			}
			return ret;
		}
		RectangleF ScreenEventRect(Event eve)
		{
			RectangleF canvasRect = CanvasEventRect(eve);
			canvasRect.X -= scrollPos.X;
			canvasRect.Y -= scrollPos.Y;
			return canvasRect;
		}
		RectangleF CanvasEventRect(Event eve)
		{
			float eveStart = Song.TicksToBeat(eve.absoluteTime);
			float eveWidth;

			float height;

			float x = eveStart * beatWidth;
			float y;

			if(eve is NoteOnEvent)
			{
				NoteOnEvent non = eve as NoteOnEvent;
				int tickDuration = non.TickDuration;
				if(tickDuration == -1)
					tickDuration = Song.BeatToTicks(CurrentBeat) - non.absoluteTime;

				eveWidth = Song.TicksToBeat(tickDuration) * beatWidth;
				y = canvasSize.Height - non.key.Interval * noteHeight - noteHeight;
				height = noteHeight;
			}
			else
			{				
				Size siz = TextRenderer.MeasureText(eve.ToString(), Font);
				eveWidth = siz.Width;
				height = siz.Height;
				y = scrollPos.Y + siz.Height * GetEventRow(eve);
			}

			return new RectangleF(x, y, (eveWidth), height);
		}
		/// <summary>returns a rect in screen coordinates</summary>
		protected virtual RectangleF ScreenNoteRect(NoteOnEvent eve)
		{
			RectangleF canvasRect = CanvasNoteRect(eve);
			canvasRect.X -= scrollPos.X;
			canvasRect.Y -= scrollPos.Y;
			return canvasRect;
		}
		/// <summary>returns a rect in canvas coordinates</summary>
		protected virtual RectangleF CanvasNoteRect(NoteOnEvent eve)
		{
			float noteStart = Song.TicksToBeat(eve.absoluteTime);//startTicks / song.ticksPerBeat;
			float noteWidth = Song.TicksToBeat(eve.TickDuration);//lengthTicks / song.ticksPerBeat;
			
			float x = noteStart * beatWidth;
			float y = canvasSize.Height - eve.key.Interval * noteHeight - noteHeight;

			return new RectangleF(x, y, (noteWidth * beatWidth), noteHeight);
		}
		
		protected int ScreenToCanvas(int screenX)
		{
			return screenX + scrollPos.X;
		}
		/// <summary>from screen loc to whole canvas position</summary>
		protected Point ScreenToCanvas(Point screenLoc)
		{
			Point mp = GuiExtensions.Add(screenLoc, scrollPos);
			
			return mp;
		}
		int ScreenToTick(int screenX)
		{
			return Song.BeatToTicks( (float)ScreenToCanvas(screenX) / beatWidth);
		}
		bool DrawnEvent(Event eve)
		{
			switch (eve.Code)
			{
				case Sounding.MidiEventsCodes.ControlChange:
				case Sounding.MidiEventsCodes.ProgramChange:
					return true;
					
				case Sounding.MidiEventsCodes.MetaEvent:
					MetaEvent meve = eve as MetaEvent;
					switch (meve.MetaCode)
					{
						case Sounding.MetaEventCodes.EndOfTrack:
						case Sounding.MetaEventCodes.TimeSignature:
						case Sounding.MetaEventCodes.SetTempo:
						case Sounding.MetaEventCodes.TextEvent:
							return true;
						
						default:
							return false;
					}
				default:
					return false;
			}
			//return false;
		}
		int GetEventRow(Event eve)
		{
			foreach(Track t in Song.tracks)
			{
				if(t.events.Contains(eve))
					return GetEventRow(eve, t);
			}
			return 0;
		}
		int GetEventRow(Event eve, Track track)
		{
			int row = 0;

			int index = track.events.IndexOf(eve);
			while(index < track.events.Count && track.events[index].absoluteTime == eve.absoluteTime)
			{
				if(DrawnEvent(track.events[index]))
				{
					row++;
				}
				index++;
			}

			return row;
		}

		public void CopyEventsToBuffer()
		{
			copyBuffer.Clear();
			for(int iii = 0; iii < selectedEvents.Count; iii++)
			{
				copyBuffer.Add(selectedEvents[iii].Copy());
			}
		}
		public void PasteBufferEvents()
		{
			UndoStack.Push(Song);
			selectedEvents.Clear();
			List<Event> toSelect = new List<Event>();
			if(copyBuffer.Count > 0)
			{
				float mouseBeat = BeatSnap(ScreenToCanvas(mousePosition).X / (float)beatWidth);
				int tickOff = Song.BeatToTicks(mouseBeat) - copyBuffer[0].absoluteTime;
				for(int iii = 0; iii < copyBuffer.Count; iii++)
				{
					Event eve = copyBuffer[iii].Copy();
					//eve.absoluteTime += tickOff;

					
					if(eve is NoteOnEvent)
					{
						NoteOnEvent non = eve as NoteOnEvent;
						non.SetChannel(channelEditIndex);
						non.SetTime(eve.absoluteTime + tickOff);
						foreach(NoteKeyEvent sub in non.SubEvents())
							SelectedTrack.events.Add(sub);
						//editTrack.events.Add(non.noteOff);
					}
					else
					{
						if(eve is NoteEvent)
							((NoteEvent)eve).Channel = channelEditIndex;
						SelectedTrack.events.Add(eve);
						eve.absoluteTime += tickOff;
					}

					toSelect.Add(eve);
					//selectedEvents.Add(eve);
				}

				selectedEvents.Add(toSelect);
			}
					
			ChangedTrack();
		}
		protected override void OnKeyUp(KeyEventArgs e)
		{
			base.OnKeyUp(e);

			if(recording)
				return;
		}
		protected override void OnKeyDown(KeyEventArgs e)
		{
			base.OnKeyDown(e);

			bool valid;
			Key k = Keyboard.KeyNote(e.KeyCode, out valid);
			if(valid)
				ScrollToNote(k);
			
			if(recording)
				return;
		}
		protected override void OnMouseWheel(MouseEventArgs e)
		{
			PointF canvasMouse = ScreenToCanvas(e.Location);
			canvasMouse.X = canvasMouse.X / CanvasSize.Width;
			canvasMouse.Y = canvasMouse.Y / CanvasSize.Height;

			zoomSize += Math.Sign(e.Delta) * (1.0f / zoomSize);
			
			if(float.IsNaN(zoomSize))
				zoomSize = 1;
			if(zoomSize < 1)
				zoomSize = 1;
			if(zoomSize > 20)
				zoomSize = 20;
			
			EvaluateSize();
			
			canvasMouse.Y *= CanvasSize.Height;
			canvasMouse.X *= CanvasSize.Width;

			int yTarget = (int)canvasMouse.Y - e.Location.Y;
			int xTarget = (int)canvasMouse.X - e.Location.X;

			scrollPos = new Point(xTarget, yTarget);
			
			Invalidate();
		}
		protected override void OnMouseDown(MouseEventArgs e)
		{
			base.OnMouseDown(e);

			if((e.Button & System.Windows.Forms.MouseButtons.Middle) == System.Windows.Forms.MouseButtons.Middle)
			{
				camPanOrigin = e.Location;
			}
			
			if((e.Button & System.Windows.Forms.MouseButtons.XButton1) == System.Windows.Forms.MouseButtons.XButton1)
			{
				float mouseBeat = BeatSnap(ScreenToCanvas(e.Location).X / (float)beatWidth);// * beatWidth;
				
				Song.GotoTick(Song.BeatToTicks(mouseBeat));
			}
			
			if((e.Button & System.Windows.Forms.MouseButtons.Left) == 0)
				return;

			if(Song == null || recording)
				return;

			noteEditing.state = NoteEditing.State.Nothing;
			noteEditing.startOffsetCanvas = ScreenToCanvas(e.Location);
			noteEditing.startOffsetScreen = e.Location;
			noteEditing.selectedThisMouseDown = null;

			if(SelectedTrack == null)
				return;
			
			for(int iii = 0; iii < SelectedTrack.events.Count; iii++)
			{
				NoteOnEvent eve = SelectedTrack.events[iii] as NoteOnEvent;
				if(eve == null || eve.Channel != channelEditIndex)
					continue;
				if((GuiExtensions.GetRectBorder(ScreenNoteRect(eve), new Size(resizeAllowance, resizeAllowance / 2), e.Location) & GuiExtensions.RectBorders.right) == GuiExtensions.RectBorders.right)
				{
					Cursor.Current = Cursors.SizeWE;

						noteEditing.selectedThisMouseDown = eve;
						noteEditing.state = NoteEditing.State.Resizing;
					
					break;
					
				}
			}
			
			for(int iii = 0; iii < SelectedTrack.events.Count; iii++)
			{
				NoteOnEvent eve = SelectedTrack.events[iii] as NoteOnEvent;
				if(eve == null || eve.Channel != channelEditIndex)
					continue;

				if(ScreenNoteRect(eve).Contains(e.Location))
				{
					noteEditing.selectedThisMouseDown = eve;
					break;
				}
			}
			
		}
		protected override void OnMouseMove(MouseEventArgs e)
		{
			base.OnMouseMove(e);
			
			if(Designer.RunningFromVisualStudioDesigner)
				return;

			mousePosition = e.Location;

			if((e.Button & System.Windows.Forms.MouseButtons.Middle) == System.Windows.Forms.MouseButtons.Middle)
			{
				Point camDiff = e.Location - (Size)camPanOrigin;
				
				scrollPos = new Point(scrollPos.X - camDiff.X, scrollPos.Y - camDiff.Y);
				
				camPanOrigin = e.Location;

			}

			const int dragThreshold = 5;
			
			noteEditing.mouse = e.Location;

			if(Song == null || recording)
				return;
			
			if(SelectedTrack == null)
				return;

		//	Track sl = SelectedTrack;
			
			for(int iii = 0; iii < SelectedTrack.events.Count; iii++)
			{
				NoteOnEvent eve = SelectedTrack.events[iii] as NoteOnEvent;
				if(eve == null || eve.Channel != channelEditIndex)
					continue;

				if((GuiExtensions.GetRectBorder(ScreenNoteRect(eve), new Size(resizeAllowance, resizeAllowance / 2), e.Location) & GuiExtensions.RectBorders.right) == GuiExtensions.RectBorders.right)
				{
					Cursor.Current = Cursors.SizeWE;

					break;
				}
			}
			
			if((e.Button & System.Windows.Forms.MouseButtons.Left) == 0)
				return;

			Point difference = ScreenToCanvas(e.Location) - (Size)noteEditing.startOffsetCanvas;
			
			if(noteEditing.selectedThisMouseDown != null)
			{
				if(Math.Abs(difference.X) > dragThreshold || Math.Abs(difference.Y) > dragThreshold)
				{
					if(selectedEvents.Count > 0 && !selectedEvents.Contains(noteEditing.selectedThisMouseDown))
					{
						selectedEvents.Clear();
						selectedEvents.Add(noteEditing.selectedThisMouseDown);
					}
					else if(!selectedEvents.Contains(noteEditing.selectedThisMouseDown))
						selectedEvents.Add(noteEditing.selectedThisMouseDown);
					
					if(noteEditing.state == NoteEditing.State.Resizing)
					{
					}
					else
						noteEditing.state = NoteEditing.State.Dragging;
				}
			}
			else
			{
				if(Math.Abs(difference.X) > dragThreshold || Math.Abs(difference.Y) > dragThreshold)
				{
					noteEditing.state = NoteEditing.State.Recting;
					
					Point c = ScreenToCanvas(e.Location);
					Point p = new Point(noteEditing.startOffsetCanvas.X < c.X ? noteEditing.startOffsetCanvas.X : c.X,
										noteEditing.startOffsetCanvas.Y < c.Y ? noteEditing.startOffsetCanvas.Y : c.Y);
					Size s = new Size(	noteEditing.startOffsetCanvas.X < c.X ? c.X - noteEditing.startOffsetCanvas.X : noteEditing.startOffsetCanvas.X - c.X ,
										noteEditing.startOffsetCanvas.Y < c.Y ? c.Y - noteEditing.startOffsetCanvas.Y : noteEditing.startOffsetCanvas.Y - c.Y);

					noteEditing.selRect = new Rectangle(p, s);
						
				}
			}

			Refresh();
		}
		protected override void OnMouseUp(MouseEventArgs e)
		{
			if(!Focused)
				return;

			base.OnMouseUp(e);

			if(!ClientRectangle.Contains(PointToClient(Control.MousePosition)))
				return;

			if((e.Button & System.Windows.Forms.MouseButtons.Right) == System.Windows.Forms.MouseButtons.Right)
				noteEditing.RMousePos = e.Location;

			if((e.Button & System.Windows.Forms.MouseButtons.Left) == 0)
				return;

			if(Song == null || recording)
				return;

			Point difference = ScreenToCanvas(e.Location) - (Size)noteEditing.startOffsetCanvas;

			if(SelectedTrack == null)
				return;
			
			if(noteEditing.state == NoteEditing.State.Dragging)
			{
				UndoStack.Push(Song);
				if(selectedEvents.Count == 1)
				{
					NoteOnEvent selOn = selectedEvents[0] as NoteOnEvent;
					if(selOn != null)
					{
						int tickDuration = selOn.TickDuration;
						NoteOnEvent on = CreateNoteFromPixelPos(noteEditing.mouse);
						selOn.SetTime(on.absoluteTime);
						selOn.key = on.key;
					}
				}
				else
				{
					for(int iii = 0; iii < selectedEvents.Count; iii++)
					{
						NoteOnEvent selOn = selectedEvents[iii] as NoteOnEvent;
						if(selOn == null)
							continue;
					
						GetNoteFromPixelOffset(ref selOn, difference);
					}
				}
				ChangedTrack();
			}
			else if(noteEditing.state == NoteEditing.State.Resizing)
			{
				if(selectedEvents.Count > 0)
				{
					UndoStack.Push(Song);
					for(int iii = 0; iii < selectedEvents.Count; iii++)
					{
						NoteOnEvent selOn = selectedEvents[iii] as NoteOnEvent;
						if(selOn == null)
							continue;
						selOn.NoteOff.absoluteTime = TickSnap(selOn.NoteOff.absoluteTime + Song.BeatToTicks(difference.X / (float)beatWidth));
						if(selOn.NoteOff.absoluteTime < selOn.absoluteTime + 2)
							selOn.NoteOff.absoluteTime = selOn.absoluteTime + 2;
						for(int vvv = selOn.volumeChanges.Count - 1; vvv >= 0; vvv--)
						{
							if(selOn.volumeChanges[vvv].absoluteTime > selOn.NoteOff.absoluteTime - 2)
							{
								SelectedTrack.events.Remove(selOn.volumeChanges[vvv]);
								selOn.volumeChanges.RemoveAt(vvv);
							}
						}
					}
					selectedEvents.Add(selectedEvents[0]);	//just a hacky solution to get new note size/event editor setup right.
					
					ChangedTrack();
				}
			}
			else if(noteEditing.state == NoteEditing.State.Recting)
			{
				if((Control.ModifierKeys & Keys.Control) != Keys.Control)
					selectedEvents.Clear();
				
				List<Event> adds = new List<Event>();
				for(int iii = 0; iii < SelectedTrack.events.Count; iii++)
				{
					NoteOnEvent noteon = SelectedTrack.events[iii] as NoteOnEvent;
					if(noteon == null || noteon.Channel != channelEditIndex)
						continue;

					RectangleF nr = CanvasNoteRect(noteon);

					if(nr.IntersectsWith(noteEditing.selRect) || noteEditing.selRect.Contains(nr))
						adds.Add(noteon);

				}
				selectedEvents.Add(adds);
			}
			else if(noteEditing.state == NoteEditing.State.Nothing)
			{
				for(int iii = 0; iii < SelectedTrack.events.Count; iii++)
				{
					if(DrawnEvent(SelectedTrack.events[iii]))
					{
						if(ScreenEventRect(SelectedTrack.events[iii]).Contains(e.Location))
						{
							if(selectedEvents.Count == 1 && selectedEvents[0] == SelectedTrack.events[iii])
							{
								NoteEvent neve = selectedEvents[0] as NoteEvent;
								if(neve != null && neve.Code == MidiEventsCodes.ProgramChange)
								{
									InstrumentEditor.Open(Orchestra.Programs[neve.lsb].name);
								}
							}
							else
							{
								selectedEvents.Clear();
								selectedEvents.Add(SelectedTrack.events[iii]);
							}

							return;
						}
					}
				}
			}
			
			if(noteEditing.selectedThisMouseDown == null && noteEditing.state == NoteEditing.State.Nothing)
			{
				selectedEvents.Clear();
			}
			else if(noteEditing.state == NoteEditing.State.Nothing)
			{
				if((Control.ModifierKeys & Keys.Control) == 0)
					selectedEvents.Clear();
				NoteOnEvent noteon = noteEditing.selectedThisMouseDown as NoteOnEvent;
				if(noteon != null)
					selectedEvents.Add(noteon);
			}
			
			if(selectedEvents.Count == 0 && noteEditing.state == NoteEditing.State.Nothing)
			{
				UndoStack.Push(Song);
				AddNotePair(CreateNoteFromPixelPos(e.Location));
				ChangedTrack();
			}
			noteEditing.state = NoteEditing.State.Nothing;
		}
		protected override void OnMouseEnter(EventArgs e)
		{
			Focus();
		}
		
		void AddNotePair(NoteOnEvent note)
		{
			if(SelectedTrack == null)
				return;

			Debug.Assert(note.NoteOff != null);

			List<Event> selectionEvents = new List<Event>();
			selectionEvents.Add(note);

			List<NoteOnEvent> newNotes = new List<NoteOnEvent>();
			newNotes.Add(note);
			
			int interval = note.key;
			int[] offsets = Chord.IntervalOffsets();
			for(int iii = 1; iii < offsets.Length; iii++)
			{
				NoteOnEvent temp = note.Copy() as NoteOnEvent;
				
				temp.key = interval + offsets[iii];

				newNotes.Add(temp);
			}
			
			for(int iii = 0; iii < newNotes.Count; iii++)
			{
				SelectedTrack.events.Add(newNotes[iii]);
				SelectedTrack.events.Add(newNotes[iii].NoteOff);
				selectionEvents.Add(newNotes[iii]);
			}

			selectedEvents.Clear();
			selectedEvents.Add(selectionEvents);

			SelectedTrack.SortTrack();
		}
		NoteEditing noteEditing = new NoteEditing();
		class NoteEditing
		{
			public enum State
			{
				Nothing,
				Dragging,
				Recting,
			//	Creating,
				Resizing
			}
			public Point startOffsetScreen;
			public Point startOffsetCanvas;
			public Point mouse;
			public Point RMousePos;

			public RectangleF selRect = new Rectangle();
			public State state = State.Nothing;
			public NoteOnEvent selectedThisMouseDown = null;
		}
		NoteOnEvent CreateNoteFromPixelPos(Point pixelPos)
		{
			int tickDuration = Song.BeatToTicks(defaultNoteSize * 4f);

			int tickStart = TickSnap(Song.BeatToTicks(ScreenToCanvas(pixelPos).X / (float)beatWidth));

			NoteOnEvent on = new NoteOnEvent(tickStart);
			on.msb = 64;
			on.NoteOff = new NoteKeyEvent(tickStart + tickDuration);
			
			on.NoteOff.msb = 0;
			on.key = MouseKey(pixelPos.Y);
			on.SetChannel(channelEditIndex);
			
			return on;
		}
		void GetNoteFromPixelOffset(ref NoteOnEvent on, Point diff)
		{
			int tickDuration = on.TickDuration;

			int tickDiff = Song.BeatToTicks((float)diff.X / (float)beatWidth);
			on.SetTime(TickSnap(tickDiff + on.absoluteTime));

			int difkey = diff.Y / noteHeight;
			on.key = on.key - difkey;
		}

		int TickSnap(int ticks)
		{
			if(snapIndex < 0 || snapIndex >= MidiEditor.noteValues.Count - 1)
				return ticks;
			int snap = Song.BeatToTicks((MidiEditor.noteValues[snapIndex].Value) * 4.0f);
			int count = ticks / snap;

			return count * snap;
		}
		float BeatSnap(float beat)
		{
			if(snapIndex < 0 || snapIndex >= MidiEditor.noteValues.Count - 1)
				return beat;
			float snap = (MidiEditor.noteValues[snapIndex].Value * 4.0f);
			float count = (float)Math.Floor(beat / snap);

			return count * snap;
		}
		void ScrollToBeat(float beat)
		{
			int y = scrollPos.Y;
			int x = (int)(beat * beatWidth);
			x -= ClientSize.Width / 2;
			
			int offset = ClientSize.Width / 3;
			if((x - scrollPos.X) > offset)
				scrollPos = new Point(x - offset, y);
			else if(scrollPos.X > (x += ClientSize.Width / 2))
				scrollPos = new Point(x, y);
		}
		protected virtual void ScrollToNote(Key n)
		{
			int sp = scrollPos.Y;
			int yTarget = canvasSize.Height - (ClientSize.Height / 2) - n.Interval * noteHeight;
			if(yTarget < sp - (ClientSize.Height / 2) + noteHeight || yTarget > sp + (ClientSize.Height / 2))
				scrollPos = new Point(scrollPos.X, yTarget);
		}
		/// <summary>Gets the key specified by the y position of the control</summary>
		public Key MouseKey(int yPos)
		{
			return new Key((canvasSize.Height - (yPos + scrollPos.Y)) / noteHeight);
		}
		public void SetPlayRangeStart(int tick)
		{
			beatStart = Song.TicksToBeat(tick);
		}
		public void SetPlayRangeEnd(int tick)
		{
			beatEnd = Song.TicksToBeat(tick);
		}
		public int GetPlayRangeStartFromMouse()
		{
			int tick = TickSnap(ScreenToTick(mousePosition.X));
			beatStart = Song.TicksToBeat(tick);
			return tick;
		}
		public int GetPlayRangeEndFromMouse()
		{
			int tick = TickSnap(ScreenToTick(mousePosition.X));
			beatEnd = Song.TicksToBeat(tick);
			return tick;
		}
		private void programEventToolStripMenuItem_Click(object sender, EventArgs e)
		{
			if(SelectedTrack == null)
				return;

			UndoStack.Push(Song);

			int tick = TickSnap(ScreenToTick(noteEditing.RMousePos.X));//TickSnap(song.BeatToTicks( (float)ScreenToCanvas(noteEditing.RMousePos).X / beatWidth));

			NoteEvent programChange = new NoteEvent(tick, MidiEventsCodes.ProgramChange);
			programChange.Channel = (byte)(channelEditIndex);
			programChange.lsb = programChange.msb = 1;

			selectedEvents.Clear();
			selectedEvents.Add(programChange);

			SelectedTrack.events.Add(programChange);
			SelectedTrack.SortTrack();
		}

		private void controlChangeEventToolStripMenuItem_Click(object sender, EventArgs e)
		{
			if(SelectedTrack == null)
				return;

			UndoStack.Push(Song);

			int tick = TickSnap(ScreenToTick(noteEditing.RMousePos.X));

			NoteEvent controlChange = new NoteEvent(tick, MidiEventsCodes.ControlChange);
			controlChange.Channel = (byte)(channelEditIndex);
			controlChange.lsb = (byte)ControlChangeCodes.ChannelVolume;
			controlChange.msb = 127;

			selectedEvents.Clear();
			selectedEvents.Add(controlChange);

			SelectedTrack.events.Add(controlChange);
			SelectedTrack.SortTrack();
		}

		private void tempoEventToolStripMenuItem_Click(object sender, EventArgs e)
		{
			if(SelectedTrack == null)
				return;

			UndoStack.Push(Song);

			int tick = TickSnap(ScreenToTick(noteEditing.RMousePos.X));

			TempoEvent tempo = new TempoEvent(tick);
			tempo.Tempo = 140.0f;

			selectedEvents.Clear();
			selectedEvents.Add(tempo);

			SelectedTrack.events.Add(tempo);
			SelectedTrack.SortTrack();
		}

		private void timeSignatureEventToolStripMenuItem_Click(object sender, EventArgs e)
		{
			if(SelectedTrack == null)
				return;

			UndoStack.Push(Song);

			int tick = TickSnap(ScreenToTick(noteEditing.RMousePos.X));

			TimeSignatureEvent timeSig = new TimeSignatureEvent(tick);
			timeSig.time = new TimeSignature(4,4);

			selectedEvents.Clear();
			selectedEvents.Add(timeSig);

			SelectedTrack.events.Add(timeSig);
			SelectedTrack.SortTrack();
		}

		private void textEventToolStripMenuItem_Click(object sender, EventArgs e)
		{
			if(SelectedTrack == null)
				return;

			UndoStack.Push(Song);

			int tick = TickSnap(ScreenToTick(noteEditing.RMousePos.X));

			TextEvent text = new TextEvent(tick, MetaEventCodes.TextEvent);
			text.text = "Text Event";

			selectedEvents.Clear();
			selectedEvents.Add(text);

			SelectedTrack.events.Add(text);
			SelectedTrack.SortTrack();
		}

		public void KeyDownRecord(Keys k)
		{
			bool valid = false;
			Key key = Keyboard.KeyNote(k, out valid);
			if(!valid)
				return;

			for(int iii = recordingNotesInProgress.Count - 1; iii > -1 ; iii--)
			{
				if(recordingNotesInProgress[iii].key == key)
					return;
			}

			NoteOnEvent on = new NoteOnEvent(Song.BeatToTicks(CurrentBeat));
			on.key = key;
			on.Channel = channelEditIndex;
			on.velocity = 1.0f;
			recordingNotesInProgress.Add(on);
		}
		public void KeyUpRecord(Keys k)
		{
			bool valid = false;
			Key key = Keyboard.KeyNote(k, out valid);
			if(!valid)
				return;

			for(int iii = recordingNotesInProgress.Count - 1; iii > -1 ; iii--)
			{
				if(recordingNotesInProgress[iii].key == key)
				{
					recordingNotesInProgress[iii].MakeNoteOffByAbsolute(Song.BeatToTicks(CurrentBeat));
					recordingNotes.Add(recordingNotesInProgress[iii]);
					recordingNotesInProgress.RemoveAt(iii);
					return;
				}
			}
		}
		public bool Recording
		{
			get { return recording; }
			set
			{
				if(recording)
				{
					UndoStack.Push(Song);
					for(int iii = 0; iii < recordingNotes.Count; iii++)
					{
						SelectedTrack.events.Add(recordingNotes[iii]);
						SelectedTrack.events.Add(recordingNotes[iii].NoteOff);
					}
					SelectedTrack.SortTrack();
				}
				recordingNotesInProgress.Clear();
				recordingNotes.Clear();
				recording = value;
			}
		}
		
	}
}
