﻿namespace Sounding.Editor
{
	partial class NoteLabels
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
			this.majorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.minorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.harmonicMinorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.melodicMinorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.contextMenuStrip1.SuspendLayout();
			this.SuspendLayout();
			// 
			// contextMenuStrip1
			// 
			this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.majorToolStripMenuItem,
            this.minorToolStripMenuItem,
            this.harmonicMinorToolStripMenuItem,
            this.melodicMinorToolStripMenuItem});
			this.contextMenuStrip1.Name = "contextMenuStrip1";
			this.contextMenuStrip1.Size = new System.Drawing.Size(163, 114);
			// 
			// majorToolStripMenuItem
			// 
			this.majorToolStripMenuItem.Checked = true;
			this.majorToolStripMenuItem.CheckOnClick = true;
			this.majorToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
			this.majorToolStripMenuItem.Name = "majorToolStripMenuItem";
			this.majorToolStripMenuItem.Size = new System.Drawing.Size(162, 22);
			this.majorToolStripMenuItem.Text = "Major";
			this.majorToolStripMenuItem.Click += new System.EventHandler(this.majorToolStripMenuItem_Click);
			// 
			// minorToolStripMenuItem
			// 
			this.minorToolStripMenuItem.CheckOnClick = true;
			this.minorToolStripMenuItem.Name = "minorToolStripMenuItem";
			this.minorToolStripMenuItem.Size = new System.Drawing.Size(162, 22);
			this.minorToolStripMenuItem.Text = "Natural Minor";
			this.minorToolStripMenuItem.Click += new System.EventHandler(this.naturalMinorToolStripMenuItem_Click);
			// 
			// harmonicMinorToolStripMenuItem
			// 
			this.harmonicMinorToolStripMenuItem.Name = "harmonicMinorToolStripMenuItem";
			this.harmonicMinorToolStripMenuItem.Size = new System.Drawing.Size(162, 22);
			this.harmonicMinorToolStripMenuItem.Text = "Harmonic Minor";
			this.harmonicMinorToolStripMenuItem.Click += new System.EventHandler(this.harmonicMinorToolStripMenuItem_Click);
			// 
			// melodicMinorToolStripMenuItem
			// 
			this.melodicMinorToolStripMenuItem.Name = "melodicMinorToolStripMenuItem";
			this.melodicMinorToolStripMenuItem.Size = new System.Drawing.Size(162, 22);
			this.melodicMinorToolStripMenuItem.Text = "Melodic Minor";
			this.melodicMinorToolStripMenuItem.Click += new System.EventHandler(this.melodicMinorToolStripMenuItem_Click);
			// 
			// NoteLabels
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ContextMenuStrip = this.contextMenuStrip1;
			this.DoubleBuffered = true;
			this.Name = "NoteLabels";
			this.contextMenuStrip1.ResumeLayout(false);
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
		private System.Windows.Forms.ToolStripMenuItem majorToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem minorToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem harmonicMinorToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem melodicMinorToolStripMenuItem;
	}
}
