﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Sounding.Editor
{
	public partial class MidiVisualEditor : UserControl
	{
		public MidiVisualCanvas Canvas { get { return midiVisualCanvas; } }
		public NoteLabels NoteLabels { get { return noteLabelsPanel; } }
		
		public event EventHandler<EventArgs> PlayRangeChanged;
		public event EventHandler<EventArgs> SongChanged;
		
		public MidiVisualEditor()
		{
			InitializeComponent();
			midiVisualCanvas.horizontalBar = horizontalBar;
			midiVisualCanvas.verticalBar = verticalBar;
		}

		private void midiVisualCanvas_Paint(object sender, PaintEventArgs e)
		{
			noteLabelsPanel.Refresh();
		}

		private void midiVisualCanvas_SongChanged(object sender, EventArgs e)
		{
			if(SongChanged != null)
				SongChanged(sender, e);
		}

		private void midiVisualCanvas_PlayRangeChanged(object sender, EventArgs e)
		{
			if(PlayRangeChanged != null)
				PlayRangeChanged(sender, e);
		}
	}
}
