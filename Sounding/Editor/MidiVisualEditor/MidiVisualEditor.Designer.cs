﻿namespace Sounding.Editor
{
	partial class MidiVisualEditor
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.mainLayout = new System.Windows.Forms.TableLayoutPanel();
			this.horizontalBar = new System.Windows.Forms.HScrollBar();
			this.verticalBar = new System.Windows.Forms.VScrollBar();
			this.noteLabelsPanel = new Sounding.Editor.NoteLabels();
			this.midiVisualCanvas = new Sounding.Editor.MidiVisualCanvas();
			this.mainLayout.SuspendLayout();
			this.SuspendLayout();
			// 
			// mainLayout
			// 
			this.mainLayout.AutoSize = true;
			this.mainLayout.ColumnCount = 3;
			this.mainLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.mainLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.mainLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 17F));
			this.mainLayout.Controls.Add(this.horizontalBar, 0, 1);
			this.mainLayout.Controls.Add(this.verticalBar, 2, 0);
			this.mainLayout.Controls.Add(this.noteLabelsPanel, 0, 0);
			this.mainLayout.Controls.Add(this.midiVisualCanvas, 1, 0);
			this.mainLayout.Dock = System.Windows.Forms.DockStyle.Fill;
			this.mainLayout.Location = new System.Drawing.Point(0, 0);
			this.mainLayout.Margin = new System.Windows.Forms.Padding(0);
			this.mainLayout.Name = "mainLayout";
			this.mainLayout.RowCount = 2;
			this.mainLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.mainLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 17F));
			this.mainLayout.Size = new System.Drawing.Size(460, 225);
			this.mainLayout.TabIndex = 0;
			// 
			// horizontalBar
			// 
			this.mainLayout.SetColumnSpan(this.horizontalBar, 2);
			this.horizontalBar.Dock = System.Windows.Forms.DockStyle.Fill;
			this.horizontalBar.Location = new System.Drawing.Point(1, 209);
			this.horizontalBar.Margin = new System.Windows.Forms.Padding(1);
			this.horizontalBar.Name = "horizontalBar";
			this.horizontalBar.Size = new System.Drawing.Size(441, 15);
			this.horizontalBar.TabIndex = 0;
			// 
			// verticalBar
			// 
			this.verticalBar.Dock = System.Windows.Forms.DockStyle.Fill;
			this.verticalBar.Location = new System.Drawing.Point(444, 1);
			this.verticalBar.Margin = new System.Windows.Forms.Padding(1);
			this.verticalBar.Name = "verticalBar";
			this.verticalBar.Size = new System.Drawing.Size(15, 206);
			this.verticalBar.TabIndex = 1;
			// 
			// noteLabelsPanel
			// 
			this.noteLabelsPanel.BackColor = System.Drawing.Color.Black;
			this.noteLabelsPanel.Dock = System.Windows.Forms.DockStyle.Fill;
			this.noteLabelsPanel.Location = new System.Drawing.Point(1, 1);
			this.noteLabelsPanel.Margin = new System.Windows.Forms.Padding(1);
			this.noteLabelsPanel.Name = "noteLabelsPanel";
			this.noteLabelsPanel.Size = new System.Drawing.Size(18, 206);
			this.noteLabelsPanel.TabIndex = 2;
			this.noteLabelsPanel.TabStop = false;
			// 
			// midiVisualCanvas
			// 
			this.midiVisualCanvas.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(4)))), ((int)(((byte)(8)))), ((int)(((byte)(2)))));
			this.midiVisualCanvas.BeatEnd = 4F;
			this.midiVisualCanvas.BeatStart = 0F;
			this.midiVisualCanvas.Dock = System.Windows.Forms.DockStyle.Fill;
			this.midiVisualCanvas.ForeColor = System.Drawing.Color.Aqua;
			this.midiVisualCanvas.Location = new System.Drawing.Point(21, 1);
			this.midiVisualCanvas.Margin = new System.Windows.Forms.Padding(1);
			this.midiVisualCanvas.MinimumSize = new System.Drawing.Size(400, 200);
			this.midiVisualCanvas.Name = "midiVisualCanvas";
			this.midiVisualCanvas.Recording = false;
			this.midiVisualCanvas.scrollPos = new System.Drawing.Point(0, 0);
			this.midiVisualCanvas.Size = new System.Drawing.Size(421, 206);
			this.midiVisualCanvas.TabIndex = 3;
			this.midiVisualCanvas.SongChanged += new System.EventHandler<System.EventArgs>(this.midiVisualCanvas_SongChanged);
			this.midiVisualCanvas.Paint += new System.Windows.Forms.PaintEventHandler(this.midiVisualCanvas_Paint);
			// 
			// MidiVisualEditor
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.AutoSize = true;
			this.BackColor = System.Drawing.Color.LightBlue;
			this.Controls.Add(this.mainLayout);
			this.Margin = new System.Windows.Forms.Padding(0);
			this.MinimumSize = new System.Drawing.Size(460, 225);
			this.Name = "MidiVisualEditor";
			this.Size = new System.Drawing.Size(460, 225);
			this.mainLayout.ResumeLayout(false);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		public  System.Windows.Forms.TableLayoutPanel mainLayout;
		private System.Windows.Forms.HScrollBar horizontalBar;
		private System.Windows.Forms.VScrollBar verticalBar;
		private MidiVisualCanvas midiVisualCanvas;
		private NoteLabels noteLabelsPanel;
	}
}
