﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using System.Diagnostics;

namespace Sounding.Editor
{
	public partial class EventEditor : UserControl
	{
		[Browsable(false)]
		public MidiEditor parent = null;

		Song Song { get { if(parent == null) return null; return parent.song; } }
		
		[Browsable(false)]
		public GuiExtensions.SelectionList<Event> selectedEvents;

		SpinBox MakeVolumeSpin()
		{
			SpinBox volumeSpin = new SpinBox();
			volumeSpin.MaxValue = 127.0m;
			volumeSpin.DecimalPlaces = 0;
			volumeSpin.Increment = 1;
			volumeSpin.Label = "Volume";
			volumeSpin.ValueChanged += volumeChanged;
			return volumeSpin;
		}
		SpinBox MakeChannelSpin()
		{
			SpinBox chan = new SpinBox();
			chan.MaxValue = 16m;
			chan.DecimalPlaces = 0;
			chan.Increment = 1m;
			chan.MinValue = 1m;
			chan.Label = "Channel";
			chan.ValueChanged += channelChanged;
			return chan;
		}
		public EventEditor()
		{
			InitializeComponent();
		}

		public void SetupUI()
		{
			eventLayout.Controls.Clear();
			
			countLabel.Text = "";

			if(selectedEvents == null || selectedEvents.Count < 1)
			{
				codeLabel.Text = "";
				timeSpin.Value = 0;
				timeSpin.Enabled = false;
				return;
			}
			
			if(selectedEvents.Count > 1)		//if more than one event is selected they should all be NoteOns. Forcing that to be sure.
			{
				countLabel.Text = selectedEvents.Count.ToString();
				foreach(Event eve in selectedEvents)
					Debug.Assert(eve.Code == MidiEventsCodes.NoteOn);
			}

			timeSpin.Enabled = true;
			codeLabel.Text = selectedEvents[0].Code.ToString();
			timeSpin.SetValueWithoutEvent((decimal)Song.TicksToBeat(selectedEvents[0].absoluteTime));
			
			switch (selectedEvents[0].Code)
			{
				case MidiEventsCodes.NoteOn:
				{
					NoteOnEvent non = selectedEvents[0] as NoteOnEvent;
				
					Label nlabel = new Label();
					nlabel.Text = non.key.ToString();
					Label wlabel = new Label();
					wlabel.Text = "Beats: " + Song.TicksToBeat(non.TickDuration).ToString();

					SpinBox chanSpin = MakeChannelSpin();
					chanSpin.SetValueWithoutEvent((decimal)non.Channel + 1);
					
					eventLayout.Controls.Add(chanSpin,		0, 1);
					
					eventLayout.Controls.Add(nlabel,		0, 2);
					eventLayout.Controls.Add(wlabel,		1, 2);

					NoteOnEditor neditor = new NoteOnEditor(parent);
					neditor.Dock = DockStyle.Fill;
					eventLayout.Controls.Add(neditor, 0, 3);
					eventLayout.SetColumnSpan(neditor, 2);
				}	break;
				case MidiEventsCodes.ControlChange:
				{
					NoteEvent nev = selectedEvents[0] as NoteEvent;
					SpinBox volumeSpin = MakeVolumeSpin();
					
					volumeSpin.SetValueWithoutEvent((decimal)nev.msb);

					SpinBox chanSpin = MakeChannelSpin();
					chanSpin.SetValueWithoutEvent((decimal)nev.Channel + 1);
					
					eventLayout.Controls.Add(chanSpin,		0, 1);
					eventLayout.Controls.Add(volumeSpin,	1, 1);
				}	break;
				case MidiEventsCodes.ProgramChange:
				{
					NoteEvent nev = selectedEvents[0] as NoteEvent;

					Keyboard.instrument = Orchestra.Programs[nev.lsb];

					ComboBox instrumentCombo = new ComboBox();

					foreach(KeyValuePair<int, Instrument> prog in Orchestra.Programs)
					{
						if(prog.Key == 0)
							continue;
						int index = instrumentCombo.Items.Add(prog.Value.name);
						if(nev.lsb == prog.Key)
							instrumentCombo.SelectedIndex = index;
					}
//					for(int iii = 0; iii < 128; iii++)
//						instrumentCombo.Items.Add(iii.ToString() + " " + Orchestra.Programs[iii].name);
					
					instrumentCombo.Dock = DockStyle.Fill;
					instrumentCombo.DropDownStyle = ComboBoxStyle.DropDownList;

					//instrumentCombo.SelectedIndex = nev.lsb;

					instrumentCombo.SelectedIndexChanged += instrumentChanged;
					
					SpinBox chanSpin = MakeChannelSpin();
					chanSpin.SetValueWithoutEvent((decimal)nev.Channel + 1);
					
					eventLayout.Controls.Add(chanSpin,			0, 1);
					eventLayout.Controls.Add(instrumentCombo,	1, 1);
				}	break;
				case MidiEventsCodes.MetaEvent:
				{
					MetaEvent met = selectedEvents[0] as MetaEvent;
					switch (met.MetaCode)
					{
						case MetaEventCodes.TextEvent:
						case MetaEventCodes.Copyright:
						case MetaEventCodes.SequenceName:
						case MetaEventCodes.InstrumentName:
						case MetaEventCodes.Lyric:
						case MetaEventCodes.Marker:
						case MetaEventCodes.CuePoint:
						{
							ComboBox textTypeCombo = new ComboBox();
							textTypeCombo.Dock = DockStyle.Fill;
							textTypeCombo.DropDownStyle = ComboBoxStyle.DropDownList;

							textTypeCombo.Items.Add("Text Event");
							textTypeCombo.Items.Add("Copyright");
							textTypeCombo.Items.Add("Sequence Name");
							textTypeCombo.Items.Add("Instrument Name");
							textTypeCombo.Items.Add("Lyric");
							textTypeCombo.Items.Add("Marker");
							textTypeCombo.Items.Add("Cue Point");

							textTypeCombo.SelectedIndex = met.MetaCode - MetaEventCodes.TextEvent;

							textTypeCombo.SelectedIndexChanged += textTypeChanged;

							if(met.MetaCode == MetaEventCodes.SequenceName)
								textTypeCombo.Enabled = false;

							TextBox textBox = new TextBox();
							textBox.Text = ((TextEvent)met).text;
							textBox.Dock = DockStyle.Fill;
							textBox.Multiline = true;

							textBox.TextChanged += textChanged;

							eventLayout.Controls.Add(textTypeCombo, 0, 0);
							eventLayout.Controls.Add(textBox, 0, 1);
							
							eventLayout.SetColumnSpan(textBox, 2);

						}
							break;

						case MetaEventCodes.SetTempo:
						{
							SpinBox tempo = new SpinBox();
							tempo.MaxValue = 600;
							tempo.DecimalPlaces = 2;
							tempo.Increment = 10.0m;
							tempo.Label = "Tempo";
							tempo.ValueChanged += tempoChanged;
					
							tempo.SetValueWithoutEvent((float)((TempoEvent)selectedEvents[0]).Tempo);

							eventLayout.Controls.Add(tempo, 0, 0);
						}
							break;
						case MetaEventCodes.TimeSignature:
						{
							SpinBox count = new SpinBox();
							count.MaxValue = 32;
							count.MinValue = 01;
							count.Increment = 1.0m;
							count.Label = "";
							count.ValueChanged += timeCountChanged;
					
							count.SetValueWithoutEvent((float)((TimeSignatureEvent)selectedEvents[0]).time.counts);

							SpinBox value = new SpinBox();
							value.MaxValue = 32;
							value.MinValue = 01;
							value.Increment = 1.0m;
							value.Label = "";
							value.ValueChanged += timeValueChanged;
					
							value.SetValueWithoutEvent((float)((TimeSignatureEvent)selectedEvents[0]).time.value);

							Label l = new Label();
							l.Text = "Time Signature ";

							eventLayout.Controls.Add(l		, 0, 0);
							eventLayout.Controls.Add(count	, 0, 1);
							eventLayout.Controls.Add(value	, 1, 1);
						}	break;
						default:
							break;
					}
				}
					break;
				default:
					break;
			}
		}

		void textChanged(object sender, EventArgs e)
		{
			TextEvent tex = selectedEvents[0] as TextEvent;
			TextBox tbox = sender as TextBox;
			tex.text = tbox.Text;
			if(tex.MetaCode == MetaEventCodes.SequenceName)
				parent.UpdateTrackViewTitles();
			
			parent.UpdateTitle(true);
		}
		void textTypeChanged(object sender, EventArgs e)
		{
			TextEvent tex = selectedEvents[0] as TextEvent;
			ComboBox combo = sender as ComboBox;
			tex.SetTextType((MetaEventCodes)(combo.SelectedIndex + (int)MetaEventCodes.TextEvent));

			parent.UpdateTitle(true);
		}

		void timeCountChanged(object sender, SpinBox.ValueChangedArgs e)
		{
			TimeSignatureEvent tem = selectedEvents[0] as TimeSignatureEvent;
			if(tem != null)
				tem.time.counts = (int)e.Value;

			parent.UpdateTitle(true);
		}
		void timeValueChanged(object sender, SpinBox.ValueChangedArgs e)
		{
			TimeSignatureEvent tem = selectedEvents[0] as TimeSignatureEvent;
			if(tem != null)
				tem.time.value = (int)e.Value;

			parent.UpdateTitle(true);
		}

		void tempoChanged(object sender, SpinBox.ValueChangedArgs e)
		{
			TempoEvent tem = selectedEvents[0] as TempoEvent;
			if(tem != null)
				tem.Tempo = (double)e.Value;

			parent.UpdateTitle(true);
		}

		void channelChanged(object sender, SpinBox.ValueChangedArgs e)
		{
			for(int iii = 0; iii < selectedEvents.Count; iii++)
			{
				NoteEvent nev = selectedEvents[iii] as NoteEvent;
				if(nev != null)
					nev.Channel = (byte)(e.Value - 1);
			}

			parent.UpdateTitle(true);
		}
		void volumeChanged(object sender, SpinBox.ValueChangedArgs e)
		{
			for(int iii = 0; iii < selectedEvents.Count; iii++)
			{
				NoteOnEvent non = selectedEvents[iii] as NoteOnEvent;
				if(non != null)
				{
					non.velocity = (float)e.Value;
					continue;
				}
				NoteEvent nev = selectedEvents[iii] as NoteEvent;
				if(nev != null)
					nev.msb = (byte)(e.Value);
			}
			parent.UpdateTitle(true);
		}
		void instrumentChanged(object sender, EventArgs e)
		{
			ComboBox instrumentCombo = sender as ComboBox;
			NoteEvent prog = selectedEvents[0] as NoteEvent;
			if(prog == null)
				return;

			int instrumentNumber = 0;
			foreach(KeyValuePair<int, Instrument> pair in Orchestra.Programs)
			{
				if(pair.Value.name == instrumentCombo.Text)
					instrumentNumber = pair.Key;
			}
			

			prog.lsb = prog.msb = (byte)instrumentNumber;//instrumentCombo.SelectedIndex;
			Keyboard.instrument = Orchestra.Programs[prog.lsb];

			parent.UpdateTitle(true);
		}
		private void timeSpin_ValueChanged(object sender, SpinBox.ValueChangedArgs e)
		{
			if(selectedEvents == null || selectedEvents.Count < 1)
				return;

			if(selectedEvents.Count == 1)
			{
				if(selectedEvents[0] is NoteOnEvent)
				{
					NoteOnEvent non = selectedEvents[0] as NoteOnEvent;
					non.SetTime( Song.BeatToTicks((int)e.Value) );
				}
				else
					selectedEvents[0].absoluteTime = Song.BeatToTicks((int)e.Value);
			}
			else
			{
				int diff = Song.BeatToTicks((float)e.Value) - selectedEvents[0].absoluteTime;
				for(int iii = 0; iii < selectedEvents.Count; iii++)
				{
					NoteOnEvent non = selectedEvents[iii] as NoteOnEvent;
					non.SetTime(selectedEvents[iii].absoluteTime + diff);
				}
			}
			
			if(parent.TrackIndex < 0)
				return;

			Song.tracks[parent.TrackIndex].SortTrack();

			parent.UpdateTitle(true);
		}
	}
}
