﻿namespace Sounding.Editor
{
	partial class EventEditor
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.mainLayout = new System.Windows.Forms.TableLayoutPanel();
			this.codeLabel = new System.Windows.Forms.Label();
			this.timeSpin = new Sounding.SpinBox();
			this.eventContainer = new System.Windows.Forms.Panel();
			this.countLabel = new System.Windows.Forms.Label();
			this.eventLayout = new System.Windows.Forms.TableLayoutPanel();
			this.mainLayout.SuspendLayout();
			this.eventContainer.SuspendLayout();
			this.SuspendLayout();
			// 
			// mainLayout
			// 
			this.mainLayout.AutoSize = true;
			this.mainLayout.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
			this.mainLayout.ColumnCount = 2;
			this.mainLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.mainLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.mainLayout.Controls.Add(this.codeLabel, 0, 0);
			this.mainLayout.Controls.Add(this.timeSpin, 1, 0);
			this.mainLayout.Controls.Add(this.eventContainer, 0, 1);
			this.mainLayout.Dock = System.Windows.Forms.DockStyle.Fill;
			this.mainLayout.Location = new System.Drawing.Point(0, 0);
			this.mainLayout.Name = "mainLayout";
			this.mainLayout.RowCount = 1;
			this.mainLayout.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.mainLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.mainLayout.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.mainLayout.Size = new System.Drawing.Size(338, 41);
			this.mainLayout.TabIndex = 0;
			// 
			// codeLabel
			// 
			this.codeLabel.Anchor = System.Windows.Forms.AnchorStyles.Left;
			this.codeLabel.AutoSize = true;
			this.codeLabel.Location = new System.Drawing.Point(3, 4);
			this.codeLabel.Name = "codeLabel";
			this.codeLabel.Size = new System.Drawing.Size(32, 13);
			this.codeLabel.TabIndex = 0;
			this.codeLabel.Text = "Code";
			// 
			// timeSpin
			// 
			this.timeSpin.AutoSize = true;
			this.timeSpin.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.timeSpin.DecimalPlaces = 6;
			this.timeSpin.Disabled = false;
			this.timeSpin.Float = 0F;
			this.timeSpin.Increment = new decimal(new int[] {
            1,
            0,
            0,
            0});
			this.timeSpin.Label = "Beat Time";
			this.timeSpin.Location = new System.Drawing.Point(172, 3);
			this.timeSpin.MaxValue = new decimal(new int[] {
            2147483647,
            0,
            0,
            0});
			this.timeSpin.MinimumSize = new System.Drawing.Size(163, 16);
			this.timeSpin.MinValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
			this.timeSpin.Name = "timeSpin";
			this.timeSpin.Size = new System.Drawing.Size(163, 16);
			this.timeSpin.State = Sounding.SpinBox.EnabledState.Enabled;
			this.timeSpin.TabIndex = 1;
			this.timeSpin.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
			this.timeSpin.ValueChanged += new System.EventHandler<Sounding.SpinBox.ValueChangedArgs>(this.timeSpin_ValueChanged);
			// 
			// eventContainer
			// 
			this.eventContainer.AutoSize = true;
			this.eventContainer.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
			this.mainLayout.SetColumnSpan(this.eventContainer, 2);
			this.eventContainer.Controls.Add(this.countLabel);
			this.eventContainer.Controls.Add(this.eventLayout);
			this.eventContainer.Dock = System.Windows.Forms.DockStyle.Fill;
			this.eventContainer.Location = new System.Drawing.Point(3, 25);
			this.eventContainer.Name = "eventContainer";
			this.eventContainer.Size = new System.Drawing.Size(332, 13);
			this.eventContainer.TabIndex = 4;
			// 
			// countLabel
			// 
			this.countLabel.AutoSize = true;
			this.countLabel.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.countLabel.Location = new System.Drawing.Point(0, 0);
			this.countLabel.Name = "countLabel";
			this.countLabel.Size = new System.Drawing.Size(0, 13);
			this.countLabel.TabIndex = 3;
			this.countLabel.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
			// 
			// eventLayout
			// 
			this.eventLayout.AutoSize = true;
			this.eventLayout.ColumnCount = 2;
			this.eventLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.eventLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.eventLayout.Dock = System.Windows.Forms.DockStyle.Fill;
			this.eventLayout.Location = new System.Drawing.Point(0, 0);
			this.eventLayout.Name = "eventLayout";
			this.eventLayout.RowCount = 2;
			this.eventLayout.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.eventLayout.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.eventLayout.Size = new System.Drawing.Size(332, 13);
			this.eventLayout.TabIndex = 2;
			// 
			// EventEditor
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.AutoSize = true;
			this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
			this.Controls.Add(this.mainLayout);
			this.Name = "EventEditor";
			this.Size = new System.Drawing.Size(338, 41);
			this.mainLayout.ResumeLayout(false);
			this.mainLayout.PerformLayout();
			this.eventContainer.ResumeLayout(false);
			this.eventContainer.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.TableLayoutPanel mainLayout;
		private System.Windows.Forms.Label codeLabel;
		private SpinBox timeSpin;
		private System.Windows.Forms.TableLayoutPanel eventLayout;
		private System.Windows.Forms.Label countLabel;
		private System.Windows.Forms.Panel eventContainer;
	}
}
