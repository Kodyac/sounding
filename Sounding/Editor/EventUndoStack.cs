﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sounding.Editor
{

	class UndoStack
	{
		static UndoStack undoStack;

		const int maxSize = 20;
		List<Song> list;
		Song load;

		static public void NewStack(Song load)
		{
			undoStack = new UndoStack();
			undoStack.list = new List<Song>();
			undoStack.load = new Song(load);
		}
		static public void Push(Song item)
		{
			if(undoStack.list.Count >= maxSize)
				undoStack.list.RemoveAt(0);
			undoStack.list.Add(new Song(item));
		}
		static public Song Pop()
		{
			if(undoStack.list.Count > 0)
			{
				Song temp = undoStack.list[undoStack.list.Count - 1];
				undoStack.list.RemoveAt(undoStack.list.Count - 1);
				return temp;
			}
			return new Song(undoStack.load);
		}

	}
	
	
	

}
