﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using Sounding.IO;

namespace Sounding.Editor
{
	public partial class OrchestraEditor : Form
	{
		public event EventHandler<EventArgs> Changed;

		public static void AddChangedEvent(EventHandler<EventArgs> t)
		{
			if(instance.Changed == null || !instance.Changed.GetInvocationList().Contains(t))
				instance.Changed += t;
		}
		static OrchestraEditor instance;
		static public void Open()
		{
			if(instance != null)
			{
				instance.BringToFront();
				return;
			}

			instance = new OrchestraEditor();
			instance.Show();
		}

		const string newString = "<new>";
		const string unusedString = "Unused";
		const string defaultString = "Default";

		SortOrder lastOrder = SortOrder.Descending;


		void EmitChange()
		{
			if(Changed != null)
				Changed(this, EventArgs.Empty);
		}
		OrchestraEditor()
		{
			InitializeComponent();

			instrumentList.Columns.Add("Instrument", -1);
			instrumentList.Columns.Add("Prog", -1);
			instrumentList.Columns.Add("Perc", -1);
			instrumentList.Columns.Add("Modified", -1);
			
			//instrumentList.ColumnClick += instrumentList_ColumnClick;
			orchestraName.Text = Orchestra.Name;
			FillList();
		}

		void instrumentList_ColumnClick(object sender, ColumnClickEventArgs e)
		{
			if(lastOrder == SortOrder.Descending)
				lastOrder = SortOrder.Ascending;
			else
				lastOrder = SortOrder.Descending;

			instrumentList.ListViewItemSorter = new GuiExtensions.ListViewItemComparer(e.Column, lastOrder);
			
			instrumentList.Sort();
		}

		void FillList(/*string directory, string extension, ListView viewer*/)
		{
			string directory = InstrumentFileHandler.fullFolder;
			string extension = InstrumentFileHandler.extension;

			DirectoryInfo dir = new DirectoryInfo(directory);
			FileInfo[] files = dir.GetFiles("*" + extension, SearchOption.AllDirectories);
			
			instrumentList.Items.Clear();

			for(int iii = 0; iii < files.Length; iii++)
			{				
				string name = files[iii].FullName;
				name = name.Replace(directory, "");
				name = name.Replace(extension, "");

				int program = Orchestra.Programs.GetProgram(name);
				int percussion = Orchestra.Percussion.GetProgram(name);

				string programName = unusedString;
				if(program == 0)
					programName = defaultString;
				if(program > 0)
					programName = program.ToString("D3");

				string percussionName = unusedString;
				if(percussion == 0)
					percussionName = defaultString;
				if(percussion > 0)
					percussionName = percussion.ToString("D3");

				ListViewItem item = new ListViewItem(new string[] {name, programName, percussionName, File.GetLastWriteTime(files[iii].FullName).ToShortDateString() } );

				item.BackColor = InstrumentEditor.GetColour(Instrument.GetInstrument(name));

				instrumentList.Items.Add(item);
			}

			ListViewItem newer = new ListViewItem(new string[] { newString, "" } );

			instrumentList.Items.Add(newer);

			lastOrder = SortOrder.Ascending;
			instrumentList.ListViewItemSorter = new GuiExtensions.ListViewItemComparer(1, lastOrder);
		}
		
		private void instrumentList_ItemActivate(object sender, EventArgs e)
		{
			if(instrumentList.SelectedItems.Count != 1)
				return;

			string selected = instrumentList.SelectedItems[0].SubItems[0].Text;

			if(selected.CompareTo(newString) == 0)
			{
				InstrumentEditor sw = new InstrumentEditor("");
				sw.Show();
				return;
			}

			InstrumentEditor.Open(selected);
			/*for(int jjj = 0; jjj < InstrumentEditor.windows.Count; jjj++)
			{
				if(InstrumentEditor.windows[jjj].instrument.name.CompareTo(selected) == 0)
				{
					InstrumentEditor.windows[jjj].BringToFront();
					return;
				}
			}
			InstrumentEditor window = new InstrumentEditor(selected);
			window.Show();*/
		}

		private void instrumentList_SelectedIndexChanged(object sender, EventArgs e)
		{
			if(instrumentList.SelectedItems.Count != 1)
				return;

			if(instrumentList.SelectedItems[0].SubItems[0].Text.CompareTo(newString) == 0)
			{
				instrumentLabel.Text = "";
				instrumentLabel.Enabled = false;
				programSpin.Enabled = false;
				return;
			}
			else
			{
				instrumentLabel.Enabled = true;
				programSpin.Enabled = true;
			}

			instrumentLabel.Text = instrumentList.SelectedItems[0].SubItems[0].Text;

			int program;
			if(instrumentList.SelectedItems[0].SubItems[1].Text.CompareTo(unusedString) == 0)
				programSpin.SetValueWithoutEvent(-1m);
			else if(instrumentList.SelectedItems[0].SubItems[1].Text.CompareTo(defaultString) == 0)
				programSpin.SetValueWithoutEvent(0m);
			else if(int.TryParse(instrumentList.SelectedItems[0].SubItems[1].Text, out program))
				programSpin.SetValueWithoutEvent((decimal)program);

			int percussion;
			if(instrumentList.SelectedItems[0].SubItems[2].Text.CompareTo(unusedString) == 0)
				percussionSpin.SetValueWithoutEvent(-1m);
			else if(instrumentList.SelectedItems[0].SubItems[2].Text.CompareTo(defaultString) == 0)
				percussionSpin.SetValueWithoutEvent(0m);
			else if(int.TryParse(instrumentList.SelectedItems[0].SubItems[2].Text, out percussion))
				percussionSpin.SetValueWithoutEvent((decimal)percussion);
		}

		private void percussionSpin_ValueChanged(object sender, SpinBox.ValueChangedArgs e)
		{
			if(instrumentList.SelectedItems.Count != 1)
				return;

			int index = (int)e.Value;
			
			int prevIndex = (int)e.previousValue;
			Orchestra.Percussion.Remove(prevIndex);

			if(index > -1)
				Orchestra.Percussion[index] = Instrument.GetInstrument(instrumentLabel.Text);
			
			FillList();
		}
		private void programSpin_ValueChanged(object sender, SpinBox.ValueChangedArgs e)
		{
			if(instrumentList.SelectedItems.Count != 1)
				return;

			int index = (int)e.Value;
			
			int prevIndex = (int)e.previousValue;
			Orchestra.Programs.Remove(prevIndex);

			if(index > -1)
				Orchestra.Programs[index] = Instrument.GetInstrument(instrumentLabel.Text);
			
			FillList();
		}

		protected override void OnFormClosing(FormClosingEventArgs e)
		{
			Orchestra.SaveOrchestra();
			instance = null;
			base.OnFormClosing(e);
		}

		private void orchestraName_TextChanged(object sender, EventArgs e)
		{
			Orchestra.Name = orchestraName.Text;
			EmitChange();
		}


		private void instrumentWatcherEvent(object sender, EventArgs e)
		{
			FillList();
			EmitChange();
		}

		private void loadOrchestra_Click(object sender, EventArgs e)
		{
			OpenFileDialog dia = new OpenFileDialog();

			dia.InitialDirectory = Orchestra.fullFolder;

			dia.Filter = "Orchestra files (*.orchestra)|*.orchestra";
			dia.RestoreDirectory = true;

			if(dia.ShowDialog() == DialogResult.OK)
			{
				FileInfo info = new FileInfo(dia.FileName);
			}
			else
				return;

			Orchestra.LoadOrchestra(dia.FileName);
		}

		private void OrchestraEditor_Activated(object sender, EventArgs e)
		{
			FillList();
		}

		

		

	}
}
