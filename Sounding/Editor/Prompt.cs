﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Sounding.Editor
{
	public partial class Prompt : Form
	{
		public Prompt()
		{
			InitializeComponent();
			
		}
		private void confirm_Click(object sender, EventArgs e)
		{
			DialogResult = System.Windows.Forms.DialogResult.OK;
			Close();
		}

		private void cancel_Click(object sender, EventArgs e)
		{
			DialogResult = System.Windows.Forms.DialogResult.Cancel;
			Close();
		}

		public class FloatPrompt : Prompt
		{
			SpinBox spin;

			public float Float { get { return spin.Float; } }

			public FloatPrompt(string label, string description, string title)
			{
				Text = title;
				
				Label d = new Label();
				d.Text = description;
				d.Dock = DockStyle.Fill;
				d.MinimumSize = TextRenderer.MeasureText(description, d.Font);

				spin = new SpinBox();
				spin.MaxValue = 2.0m;
				spin.MinValue = .01m;
				spin.Value = 1.0m;
				spin.Increment = .05m;
				spin.DecimalPlaces = 4;
				spin.Label = label;
				spin.Dock = DockStyle.Fill;
				
				controlsLayout.SuspendLayout();
				controlsLayout.Controls.Add(d, 0, 0);
				controlsLayout.Controls.Add(spin, 0, 1);
				
				controlsLayout.ResumeLayout(true);
				Refresh();
			}
		}
	}
}
