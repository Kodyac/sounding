﻿namespace Sounding.Editor
{
	partial class MidiEditor
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.Windows.Forms.ListViewItem listViewItem1 = new System.Windows.Forms.ListViewItem("01");
			System.Windows.Forms.ListViewItem listViewItem2 = new System.Windows.Forms.ListViewItem("02");
			System.Windows.Forms.ListViewItem listViewItem3 = new System.Windows.Forms.ListViewItem("03");
			System.Windows.Forms.ListViewItem listViewItem4 = new System.Windows.Forms.ListViewItem("04");
			System.Windows.Forms.ListViewItem listViewItem5 = new System.Windows.Forms.ListViewItem("05");
			System.Windows.Forms.ListViewItem listViewItem6 = new System.Windows.Forms.ListViewItem("06");
			System.Windows.Forms.ListViewItem listViewItem7 = new System.Windows.Forms.ListViewItem("07");
			System.Windows.Forms.ListViewItem listViewItem8 = new System.Windows.Forms.ListViewItem("08");
			System.Windows.Forms.ListViewItem listViewItem9 = new System.Windows.Forms.ListViewItem("09");
			System.Windows.Forms.ListViewItem listViewItem10 = new System.Windows.Forms.ListViewItem("10");
			System.Windows.Forms.ListViewItem listViewItem11 = new System.Windows.Forms.ListViewItem("11");
			System.Windows.Forms.ListViewItem listViewItem12 = new System.Windows.Forms.ListViewItem("12");
			System.Windows.Forms.ListViewItem listViewItem13 = new System.Windows.Forms.ListViewItem("13");
			System.Windows.Forms.ListViewItem listViewItem14 = new System.Windows.Forms.ListViewItem("14");
			System.Windows.Forms.ListViewItem listViewItem15 = new System.Windows.Forms.ListViewItem("15");
			System.Windows.Forms.ListViewItem listViewItem16 = new System.Windows.Forms.ListViewItem("16");
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MidiEditor));
			this.menuStrip1 = new System.Windows.Forms.MenuStrip();
			this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.newToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.saveAsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.saveWavToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.midiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.trackTypeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.singleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.synchronousToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.asynchronousToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.orchestraToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.mainLayout = new System.Windows.Forms.TableLayoutPanel();
			this.trackLayout = new System.Windows.Forms.TableLayoutPanel();
			this.channelView = new System.Windows.Forms.ListView();
			this.header = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.events = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.channelContextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
			this.selectAllChannels = new System.Windows.Forms.ToolStripMenuItem();
			this.deCheckAllChannels = new System.Windows.Forms.ToolStripMenuItem();
			this.trackView = new System.Windows.Forms.ListView();
			this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.trackContextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
			this.selectAllTracks = new System.Windows.Forms.ToolStripMenuItem();
			this.deCheckAllTracks = new System.Windows.Forms.ToolStripMenuItem();
			this.addNewTrackToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.deleteTrackToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.eventEditor = new Sounding.Editor.EventEditor();
			this.toolBar = new System.Windows.Forms.ToolStrip();
			this.playButton = new System.Windows.Forms.ToolStripButton();
			this.playRangeButton = new System.Windows.Forms.ToolStripButton();
			this.recordButton = new System.Windows.Forms.ToolStripButton();
			this.autoScrollButton = new System.Windows.Forms.ToolStripButton();
			this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
			this.keyOctaveSpin = new Sounding.ToolSpinBox();
			this.keyVolumeSpin = new Sounding.ToolSpinBox();
			this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
			this.noteLengthCombo = new System.Windows.Forms.ToolStripComboBox();
			this.snapCombo = new System.Windows.Forms.ToolStripComboBox();
			this.chordCombo = new System.Windows.Forms.ToolStripComboBox();
			this.profileButton = new System.Windows.Forms.ToolStripButton();
			this.toolSpinBox1 = new Sounding.ToolSpinBox();
			this.tempoSpinBox = new Sounding.ToolSpinBox();
			this.a4Spin = new Sounding.ToolSpinBox();
			this.visualEditorControl = new Sounding.Editor.MidiVisualEditor();
			this.statusStrip = new System.Windows.Forms.StatusStrip();
			this.durationLabel = new System.Windows.Forms.ToolStripStatusLabel();
			this.debugLabel = new System.Windows.Forms.ToolStripStatusLabel();
			this.buildTimeLabel = new System.Windows.Forms.ToolStripStatusLabel();
			this.menuStrip1.SuspendLayout();
			this.mainLayout.SuspendLayout();
			this.trackLayout.SuspendLayout();
			this.channelContextMenu.SuspendLayout();
			this.trackContextMenu.SuspendLayout();
			this.toolBar.SuspendLayout();
			this.statusStrip.SuspendLayout();
			this.SuspendLayout();
			// 
			// menuStrip1
			// 
			this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.midiToolStripMenuItem,
            this.orchestraToolStripMenuItem});
			this.menuStrip1.Location = new System.Drawing.Point(0, 0);
			this.menuStrip1.Name = "menuStrip1";
			this.menuStrip1.Size = new System.Drawing.Size(1184, 24);
			this.menuStrip1.TabIndex = 0;
			this.menuStrip1.Text = "menuStrip1";
			// 
			// fileToolStripMenuItem
			// 
			this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newToolStripMenuItem,
            this.openToolStripMenuItem,
            this.saveToolStripMenuItem,
            this.saveAsToolStripMenuItem,
            this.saveWavToolStripMenuItem});
			this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
			this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
			this.fileToolStripMenuItem.Text = "File";
			// 
			// newToolStripMenuItem
			// 
			this.newToolStripMenuItem.Name = "newToolStripMenuItem";
			this.newToolStripMenuItem.Size = new System.Drawing.Size(186, 22);
			this.newToolStripMenuItem.Text = "New";
			this.newToolStripMenuItem.Click += new System.EventHandler(this.newToolStripMenuItem_Click);
			// 
			// openToolStripMenuItem
			// 
			this.openToolStripMenuItem.Name = "openToolStripMenuItem";
			this.openToolStripMenuItem.Size = new System.Drawing.Size(186, 22);
			this.openToolStripMenuItem.Text = "Open";
			this.openToolStripMenuItem.Click += new System.EventHandler(this.openToolStripMenuItem_Click);
			// 
			// saveToolStripMenuItem
			// 
			this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
			this.saveToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
			this.saveToolStripMenuItem.Size = new System.Drawing.Size(186, 22);
			this.saveToolStripMenuItem.Text = "Save";
			this.saveToolStripMenuItem.Click += new System.EventHandler(this.saveToolStripMenuItem_Click);
			// 
			// saveAsToolStripMenuItem
			// 
			this.saveAsToolStripMenuItem.Name = "saveAsToolStripMenuItem";
			this.saveAsToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.S)));
			this.saveAsToolStripMenuItem.Size = new System.Drawing.Size(186, 22);
			this.saveAsToolStripMenuItem.Text = "Save As";
			this.saveAsToolStripMenuItem.Click += new System.EventHandler(this.saveAsToolStripMenuItem_Click);
			// 
			// saveWavToolStripMenuItem
			// 
			this.saveWavToolStripMenuItem.Name = "saveWavToolStripMenuItem";
			this.saveWavToolStripMenuItem.Size = new System.Drawing.Size(186, 22);
			this.saveWavToolStripMenuItem.Text = "Save Wav";
			this.saveWavToolStripMenuItem.Click += new System.EventHandler(this.saveWavToolStripMenuItem_Click);
			// 
			// midiToolStripMenuItem
			// 
			this.midiToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.trackTypeToolStripMenuItem});
			this.midiToolStripMenuItem.Name = "midiToolStripMenuItem";
			this.midiToolStripMenuItem.Size = new System.Drawing.Size(43, 20);
			this.midiToolStripMenuItem.Text = "Midi";
			// 
			// trackTypeToolStripMenuItem
			// 
			this.trackTypeToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.singleToolStripMenuItem,
            this.synchronousToolStripMenuItem,
            this.asynchronousToolStripMenuItem});
			this.trackTypeToolStripMenuItem.Name = "trackTypeToolStripMenuItem";
			this.trackTypeToolStripMenuItem.Size = new System.Drawing.Size(132, 22);
			this.trackTypeToolStripMenuItem.Text = "Track Type";
			// 
			// singleToolStripMenuItem
			// 
			this.singleToolStripMenuItem.CheckOnClick = true;
			this.singleToolStripMenuItem.Name = "singleToolStripMenuItem";
			this.singleToolStripMenuItem.Size = new System.Drawing.Size(150, 22);
			this.singleToolStripMenuItem.Text = "Single";
			this.singleToolStripMenuItem.Click += new System.EventHandler(this.TrackTypeChanged);
			// 
			// synchronousToolStripMenuItem
			// 
			this.synchronousToolStripMenuItem.CheckOnClick = true;
			this.synchronousToolStripMenuItem.Name = "synchronousToolStripMenuItem";
			this.synchronousToolStripMenuItem.Size = new System.Drawing.Size(150, 22);
			this.synchronousToolStripMenuItem.Text = "Synchronous";
			this.synchronousToolStripMenuItem.Click += new System.EventHandler(this.TrackTypeChanged);
			// 
			// asynchronousToolStripMenuItem
			// 
			this.asynchronousToolStripMenuItem.CheckOnClick = true;
			this.asynchronousToolStripMenuItem.Name = "asynchronousToolStripMenuItem";
			this.asynchronousToolStripMenuItem.Size = new System.Drawing.Size(150, 22);
			this.asynchronousToolStripMenuItem.Text = "Asynchronous";
			this.asynchronousToolStripMenuItem.Click += new System.EventHandler(this.TrackTypeChanged);
			// 
			// orchestraToolStripMenuItem
			// 
			this.orchestraToolStripMenuItem.Name = "orchestraToolStripMenuItem";
			this.orchestraToolStripMenuItem.Size = new System.Drawing.Size(70, 20);
			this.orchestraToolStripMenuItem.Text = "Orchestra";
			this.orchestraToolStripMenuItem.Click += new System.EventHandler(this.orchestraToolStripMenuItem_Click);
			// 
			// mainLayout
			// 
			this.mainLayout.AutoSize = true;
			this.mainLayout.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
			this.mainLayout.ColumnCount = 1;
			this.mainLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
			this.mainLayout.Controls.Add(this.trackLayout, 0, 2);
			this.mainLayout.Controls.Add(this.toolBar, 0, 0);
			this.mainLayout.Controls.Add(this.visualEditorControl, 0, 1);
			this.mainLayout.Controls.Add(this.statusStrip, 0, 3);
			this.mainLayout.Dock = System.Windows.Forms.DockStyle.Fill;
			this.mainLayout.Location = new System.Drawing.Point(0, 24);
			this.mainLayout.Name = "mainLayout";
			this.mainLayout.RowCount = 4;
			this.mainLayout.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.mainLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.mainLayout.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.mainLayout.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.mainLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.mainLayout.Size = new System.Drawing.Size(1184, 612);
			this.mainLayout.TabIndex = 1;
			// 
			// trackLayout
			// 
			this.trackLayout.AutoSize = true;
			this.trackLayout.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
			this.trackLayout.ColumnCount = 3;
			this.trackLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 256F));
			this.trackLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 128F));
			this.trackLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.trackLayout.Controls.Add(this.channelView, 1, 0);
			this.trackLayout.Controls.Add(this.trackView, 0, 0);
			this.trackLayout.Controls.Add(this.eventEditor, 2, 0);
			this.trackLayout.Dock = System.Windows.Forms.DockStyle.Fill;
			this.trackLayout.GrowStyle = System.Windows.Forms.TableLayoutPanelGrowStyle.AddColumns;
			this.trackLayout.Location = new System.Drawing.Point(3, 381);
			this.trackLayout.Name = "trackLayout";
			this.trackLayout.RowCount = 1;
			this.trackLayout.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.trackLayout.Size = new System.Drawing.Size(1178, 206);
			this.trackLayout.TabIndex = 2;
			// 
			// channelView
			// 
			this.channelView.CheckBoxes = true;
			this.channelView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.header,
            this.events});
			this.channelView.ContextMenuStrip = this.channelContextMenu;
			this.channelView.Dock = System.Windows.Forms.DockStyle.Fill;
			this.channelView.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.channelView.FullRowSelect = true;
			this.channelView.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
			this.channelView.HideSelection = false;
			listViewItem1.StateImageIndex = 0;
			listViewItem2.StateImageIndex = 0;
			listViewItem3.StateImageIndex = 0;
			listViewItem4.StateImageIndex = 0;
			listViewItem5.StateImageIndex = 0;
			listViewItem6.StateImageIndex = 0;
			listViewItem7.StateImageIndex = 0;
			listViewItem8.StateImageIndex = 0;
			listViewItem9.StateImageIndex = 0;
			listViewItem10.StateImageIndex = 0;
			listViewItem11.StateImageIndex = 0;
			listViewItem12.StateImageIndex = 0;
			listViewItem13.StateImageIndex = 0;
			listViewItem14.StateImageIndex = 0;
			listViewItem15.StateImageIndex = 0;
			listViewItem16.StateImageIndex = 0;
			this.channelView.Items.AddRange(new System.Windows.Forms.ListViewItem[] {
            listViewItem1,
            listViewItem2,
            listViewItem3,
            listViewItem4,
            listViewItem5,
            listViewItem6,
            listViewItem7,
            listViewItem8,
            listViewItem9,
            listViewItem10,
            listViewItem11,
            listViewItem12,
            listViewItem13,
            listViewItem14,
            listViewItem15,
            listViewItem16});
			this.channelView.Location = new System.Drawing.Point(259, 3);
			this.channelView.MinimumSize = new System.Drawing.Size(122, 200);
			this.channelView.MultiSelect = false;
			this.channelView.Name = "channelView";
			this.channelView.Size = new System.Drawing.Size(122, 200);
			this.channelView.TabIndex = 6;
			this.channelView.UseCompatibleStateImageBehavior = false;
			this.channelView.View = System.Windows.Forms.View.Details;
			this.channelView.ItemChecked += new System.Windows.Forms.ItemCheckedEventHandler(this.channelView_ItemChecked);
			this.channelView.SelectedIndexChanged += new System.EventHandler(this.channelView_SelectedIndexChanged);
			// 
			// header
			// 
			this.header.Text = "C#";
			this.header.Width = 38;
			// 
			// events
			// 
			this.events.Text = "Events";
			this.events.Width = 63;
			// 
			// channelContextMenu
			// 
			this.channelContextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.selectAllChannels,
            this.deCheckAllChannels});
			this.channelContextMenu.Name = "channelContextMenu";
			this.channelContextMenu.Size = new System.Drawing.Size(156, 48);
			// 
			// selectAllChannels
			// 
			this.selectAllChannels.Name = "selectAllChannels";
			this.selectAllChannels.Size = new System.Drawing.Size(155, 22);
			this.selectAllChannels.Text = "Check All";
			this.selectAllChannels.Click += new System.EventHandler(this.selectAllChannels_Click);
			// 
			// deCheckAllChannels
			// 
			this.deCheckAllChannels.Name = "deCheckAllChannels";
			this.deCheckAllChannels.Size = new System.Drawing.Size(155, 22);
			this.deCheckAllChannels.Text = "Check Just This";
			this.deCheckAllChannels.Click += new System.EventHandler(this.deCheckAllChannels_Click);
			// 
			// trackView
			// 
			this.trackView.CheckBoxes = true;
			this.trackView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1});
			this.trackView.ContextMenuStrip = this.trackContextMenu;
			this.trackView.Dock = System.Windows.Forms.DockStyle.Fill;
			this.trackView.FullRowSelect = true;
			this.trackView.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
			this.trackView.HideSelection = false;
			this.trackView.Location = new System.Drawing.Point(3, 3);
			this.trackView.MultiSelect = false;
			this.trackView.Name = "trackView";
			this.trackView.Size = new System.Drawing.Size(250, 200);
			this.trackView.TabIndex = 7;
			this.trackView.UseCompatibleStateImageBehavior = false;
			this.trackView.View = System.Windows.Forms.View.Details;
			this.trackView.ItemChecked += new System.Windows.Forms.ItemCheckedEventHandler(this.trackView_ItemChecked);
			this.trackView.SelectedIndexChanged += new System.EventHandler(this.trackView_SelectedIndexChanged);
			// 
			// columnHeader1
			// 
			this.columnHeader1.Text = "Tracks";
			this.columnHeader1.Width = 246;
			// 
			// trackContextMenu
			// 
			this.trackContextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.selectAllTracks,
            this.deCheckAllTracks,
            this.addNewTrackToolStripMenuItem,
            this.deleteTrackToolStripMenuItem});
			this.trackContextMenu.Name = "trackContextMenu";
			this.trackContextMenu.Size = new System.Drawing.Size(156, 92);
			this.trackContextMenu.Text = "Track";
			// 
			// selectAllTracks
			// 
			this.selectAllTracks.Name = "selectAllTracks";
			this.selectAllTracks.Size = new System.Drawing.Size(155, 22);
			this.selectAllTracks.Text = "Check All";
			this.selectAllTracks.Click += new System.EventHandler(this.selectAllTracks_Click);
			// 
			// deCheckAllTracks
			// 
			this.deCheckAllTracks.Name = "deCheckAllTracks";
			this.deCheckAllTracks.Size = new System.Drawing.Size(155, 22);
			this.deCheckAllTracks.Text = "Check Just This";
			this.deCheckAllTracks.Click += new System.EventHandler(this.deCheckAllTracks_Click);
			// 
			// addNewTrackToolStripMenuItem
			// 
			this.addNewTrackToolStripMenuItem.Name = "addNewTrackToolStripMenuItem";
			this.addNewTrackToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
			this.addNewTrackToolStripMenuItem.Text = "Add New Track";
			this.addNewTrackToolStripMenuItem.Click += new System.EventHandler(this.addNewTrackToolStripMenuItem_Click);
			// 
			// deleteTrackToolStripMenuItem
			// 
			this.deleteTrackToolStripMenuItem.Name = "deleteTrackToolStripMenuItem";
			this.deleteTrackToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
			this.deleteTrackToolStripMenuItem.Text = "Delete Track";
			this.deleteTrackToolStripMenuItem.Click += new System.EventHandler(this.deleteTrackToolStripMenuItem_Click);
			// 
			// eventEditor
			// 
			this.eventEditor.AutoSize = true;
			this.eventEditor.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
			this.eventEditor.Dock = System.Windows.Forms.DockStyle.Fill;
			this.eventEditor.Location = new System.Drawing.Point(387, 3);
			this.eventEditor.Name = "eventEditor";
			this.eventEditor.Size = new System.Drawing.Size(788, 200);
			this.eventEditor.TabIndex = 8;
			// 
			// toolBar
			// 
			this.toolBar.GripMargin = new System.Windows.Forms.Padding(0);
			this.toolBar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.playButton,
            this.playRangeButton,
            this.recordButton,
            this.autoScrollButton,
            this.toolStripSeparator2,
            this.keyOctaveSpin,
            this.keyVolumeSpin,
            this.toolStripSeparator1,
            this.noteLengthCombo,
            this.snapCombo,
            this.chordCombo,
            this.profileButton,
            this.toolSpinBox1,
            this.tempoSpinBox,
            this.a4Spin});
			this.toolBar.Location = new System.Drawing.Point(0, 0);
			this.toolBar.Name = "toolBar";
			this.toolBar.Size = new System.Drawing.Size(1184, 25);
			this.toolBar.TabIndex = 3;
			this.toolBar.Text = "toolStrip1";
			// 
			// playButton
			// 
			this.playButton.CheckOnClick = true;
			this.playButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.playButton.Image = global::Sounding.Resource.play;
			this.playButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
			this.playButton.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.playButton.Name = "playButton";
			this.playButton.Size = new System.Drawing.Size(23, 22);
			this.playButton.Text = "Play";
			this.playButton.Click += new System.EventHandler(this.playButton_Click);
			// 
			// playRangeButton
			// 
			this.playRangeButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.playRangeButton.Image = global::Sounding.Resource.playrange;
			this.playRangeButton.ImageTransparentColor = System.Drawing.Color.Maroon;
			this.playRangeButton.Name = "playRangeButton";
			this.playRangeButton.Size = new System.Drawing.Size(23, 22);
			this.playRangeButton.Text = "Set play range to whole song";
			this.playRangeButton.Click += new System.EventHandler(this.playRangeButton_Click);
			// 
			// recordButton
			// 
			this.recordButton.CheckOnClick = true;
			this.recordButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.recordButton.Image = global::Sounding.Resource.record;
			this.recordButton.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.recordButton.Name = "recordButton";
			this.recordButton.Size = new System.Drawing.Size(23, 22);
			this.recordButton.Text = "Record Keystrokes";
			this.recordButton.Click += new System.EventHandler(this.recordButton_Click);
			// 
			// autoScrollButton
			// 
			this.autoScrollButton.Checked = true;
			this.autoScrollButton.CheckOnClick = true;
			this.autoScrollButton.CheckState = System.Windows.Forms.CheckState.Checked;
			this.autoScrollButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.autoScrollButton.Image = global::Sounding.Resource.playrange;
			this.autoScrollButton.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.autoScrollButton.Name = "autoScrollButton";
			this.autoScrollButton.Size = new System.Drawing.Size(23, 22);
			this.autoScrollButton.Text = "toolStripButton1";
			this.autoScrollButton.ToolTipText = "Auto scroll while playing";
			// 
			// toolStripSeparator2
			// 
			this.toolStripSeparator2.Name = "toolStripSeparator2";
			this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
			// 
			// keyOctaveSpin
			// 
			this.keyOctaveSpin.DecimalPlaces = 0;
			this.keyOctaveSpin.Increment = new decimal(new int[] {
            1,
            0,
            0,
            0});
			this.keyOctaveSpin.Label = "Keyboard Octave";
			this.keyOctaveSpin.MaxValue = new decimal(new int[] {
            9,
            0,
            0,
            0});
			this.keyOctaveSpin.MinValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
			this.keyOctaveSpin.Name = "keyOctaveSpin";
			this.keyOctaveSpin.Size = new System.Drawing.Size(112, 22);
			this.keyOctaveSpin.Text = "toolSpinBox1";
			this.keyOctaveSpin.Value = new decimal(new int[] {
            4,
            0,
            0,
            0});
			this.keyOctaveSpin.ValueChanged += new System.EventHandler<Sounding.SpinBox.ValueChangedArgs>(this.keyOctaveSpin_ValueChanged);
			// 
			// keyVolumeSpin
			// 
			this.keyVolumeSpin.DecimalPlaces = 2;
			this.keyVolumeSpin.Increment = new decimal(new int[] {
            5,
            0,
            0,
            131072});
			this.keyVolumeSpin.Label = "Keyboard Volume";
			this.keyVolumeSpin.MaxValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
			this.keyVolumeSpin.MinValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
			this.keyVolumeSpin.Name = "keyVolumeSpin";
			this.keyVolumeSpin.Size = new System.Drawing.Size(131, 22);
			this.keyVolumeSpin.Text = "toolSpinBox1";
			this.keyVolumeSpin.Value = new decimal(new int[] {
            5,
            0,
            0,
            65536});
			this.keyVolumeSpin.ValueChanged += new System.EventHandler<Sounding.SpinBox.ValueChangedArgs>(this.keyVolumeSpin_ValueChanged);
			// 
			// toolStripSeparator1
			// 
			this.toolStripSeparator1.Name = "toolStripSeparator1";
			this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
			// 
			// noteLengthCombo
			// 
			this.noteLengthCombo.AutoSize = false;
			this.noteLengthCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.noteLengthCombo.DropDownWidth = 128;
			this.noteLengthCombo.Items.AddRange(new object[] {
            "1/4"});
			this.noteLengthCombo.Name = "noteLengthCombo";
			this.noteLengthCombo.Size = new System.Drawing.Size(128, 23);
			this.noteLengthCombo.ToolTipText = "Default note size";
			this.noteLengthCombo.SelectedIndexChanged += new System.EventHandler(this.noteLengthCombo_SelectedIndexChanged);
			// 
			// snapCombo
			// 
			this.snapCombo.AutoSize = false;
			this.snapCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.snapCombo.DropDownWidth = 128;
			this.snapCombo.Name = "snapCombo";
			this.snapCombo.Size = new System.Drawing.Size(128, 23);
			this.snapCombo.ToolTipText = "Snap notes";
			this.snapCombo.SelectedIndexChanged += new System.EventHandler(this.snapCombo_SelectedIndexChanged);
			// 
			// chordCombo
			// 
			this.chordCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.chordCombo.Items.AddRange(new object[] {
            "Single",
            "Major Triad",
            "Minor Triad"});
			this.chordCombo.Name = "chordCombo";
			this.chordCombo.Size = new System.Drawing.Size(121, 25);
			this.chordCombo.SelectedIndexChanged += new System.EventHandler(this.chordCombo_SelectedIndexChanged);
			// 
			// profileButton
			// 
			this.profileButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
			this.profileButton.Image = ((System.Drawing.Image)(resources.GetObject("profileButton.Image")));
			this.profileButton.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.profileButton.Name = "profileButton";
			this.profileButton.Size = new System.Drawing.Size(45, 22);
			this.profileButton.Text = "Profile";
			this.profileButton.Click += new System.EventHandler(this.profileButton_Click);
			// 
			// toolSpinBox1
			// 
			this.toolSpinBox1.DecimalPlaces = 0;
			this.toolSpinBox1.Increment = new decimal(new int[] {
            1,
            0,
            0,
            0});
			this.toolSpinBox1.Label = "Sample Rate";
			this.toolSpinBox1.MaxValue = new decimal(new int[] {
            48000,
            0,
            0,
            0});
			this.toolSpinBox1.MinValue = new decimal(new int[] {
            1000,
            0,
            0,
            0});
			this.toolSpinBox1.Name = "toolSpinBox1";
			this.toolSpinBox1.Size = new System.Drawing.Size(111, 22);
			this.toolSpinBox1.Text = "sampleRateSpin";
			this.toolSpinBox1.Value = new decimal(new int[] {
            48000,
            0,
            0,
            0});
			this.toolSpinBox1.ValueChanged += new System.EventHandler<Sounding.SpinBox.ValueChangedArgs>(this.toolSpinBox1_ValueChanged);
			// 
			// tempoSpinBox
			// 
			this.tempoSpinBox.DecimalPlaces = 2;
			this.tempoSpinBox.Increment = new decimal(new int[] {
            5,
            0,
            0,
            131072});
			this.tempoSpinBox.Label = "Tempo Multiplier";
			this.tempoSpinBox.MaxValue = new decimal(new int[] {
            10,
            0,
            0,
            0});
			this.tempoSpinBox.MinValue = new decimal(new int[] {
            5,
            0,
            0,
            131072});
			this.tempoSpinBox.Name = "tempoSpinBox";
			this.tempoSpinBox.Size = new System.Drawing.Size(135, 22);
			this.tempoSpinBox.Text = "toolSpinBox2";
			this.tempoSpinBox.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
			this.tempoSpinBox.ValueChanged += new System.EventHandler<Sounding.SpinBox.ValueChangedArgs>(this.tempoSpinBox_ValueChanged);
			// 
			// a4Spin
			// 
			this.a4Spin.DecimalPlaces = 1;
			this.a4Spin.ForeColor = System.Drawing.SystemColors.ControlText;
			this.a4Spin.Increment = new decimal(new int[] {
            1,
            0,
            0,
            0});
			this.a4Spin.Label = "A4";
			this.a4Spin.MaxValue = new decimal(new int[] {
            1000,
            0,
            0,
            0});
			this.a4Spin.MinValue = new decimal(new int[] {
            20,
            0,
            0,
            0});
			this.a4Spin.Name = "a4Spin";
			this.a4Spin.Size = new System.Drawing.Size(63, 22);
			this.a4Spin.Text = "toolSpinBox3";
			this.a4Spin.Value = new decimal(new int[] {
            440,
            0,
            0,
            0});
			this.a4Spin.ValueChanged += new System.EventHandler<Sounding.SpinBox.ValueChangedArgs>(this.a4Spin_ValueChanged);
			// 
			// visualEditorControl
			// 
			this.visualEditorControl.AutoSize = true;
			this.visualEditorControl.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
			this.visualEditorControl.BackColor = System.Drawing.Color.LightBlue;
			this.visualEditorControl.Dock = System.Windows.Forms.DockStyle.Fill;
			this.visualEditorControl.Location = new System.Drawing.Point(0, 25);
			this.visualEditorControl.Margin = new System.Windows.Forms.Padding(0);
			this.visualEditorControl.MinimumSize = new System.Drawing.Size(460, 225);
			this.visualEditorControl.Name = "visualEditorControl";
			this.visualEditorControl.Size = new System.Drawing.Size(1184, 353);
			this.visualEditorControl.TabIndex = 4;
			this.visualEditorControl.SongChanged += new System.EventHandler<System.EventArgs>(this.visualEditorControl_SongChanged);
			// 
			// statusStrip
			// 
			this.statusStrip.Dock = System.Windows.Forms.DockStyle.Fill;
			this.statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.durationLabel,
            this.debugLabel,
            this.buildTimeLabel});
			this.statusStrip.Location = new System.Drawing.Point(0, 590);
			this.statusStrip.Name = "statusStrip";
			this.statusStrip.Size = new System.Drawing.Size(1184, 22);
			this.statusStrip.TabIndex = 2;
			this.statusStrip.Text = "statusStrip1";
			// 
			// durationLabel
			// 
			this.durationLabel.Name = "durationLabel";
			this.durationLabel.Size = new System.Drawing.Size(0, 17);
			// 
			// debugLabel
			// 
			this.debugLabel.Name = "debugLabel";
			this.debugLabel.Size = new System.Drawing.Size(0, 17);
			this.debugLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// buildTimeLabel
			// 
			this.buildTimeLabel.Name = "buildTimeLabel";
			this.buildTimeLabel.Size = new System.Drawing.Size(0, 17);
			// 
			// MidiEditor
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1184, 636);
			this.Controls.Add(this.mainLayout);
			this.Controls.Add(this.menuStrip1);
			this.KeyPreview = true;
			this.MainMenuStrip = this.menuStrip1;
			this.MinimumSize = new System.Drawing.Size(1200, 480);
			this.Name = "MidiEditor";
			this.Text = "MidiEditor";
			this.Activated += new System.EventHandler(this.MidiEditor_Activated);
			this.Deactivate += new System.EventHandler(this.MidiEditor_Deactivate);
			this.Load += new System.EventHandler(this.MidiEditor_Load);
			this.menuStrip1.ResumeLayout(false);
			this.menuStrip1.PerformLayout();
			this.mainLayout.ResumeLayout(false);
			this.mainLayout.PerformLayout();
			this.trackLayout.ResumeLayout(false);
			this.trackLayout.PerformLayout();
			this.channelContextMenu.ResumeLayout(false);
			this.trackContextMenu.ResumeLayout(false);
			this.toolBar.ResumeLayout(false);
			this.toolBar.PerformLayout();
			this.statusStrip.ResumeLayout(false);
			this.statusStrip.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.MenuStrip menuStrip1;
		private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
		private System.Windows.Forms.TableLayoutPanel mainLayout;
		private System.Windows.Forms.TableLayoutPanel trackLayout;
		private System.Windows.Forms.ToolStrip toolBar;
		public System.Windows.Forms.ToolStripButton playButton;
		private System.Windows.Forms.ToolStripMenuItem midiToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem orchestraToolStripMenuItem;
		private ToolSpinBox keyOctaveSpin;
		private ToolSpinBox keyVolumeSpin;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
		private MidiVisualEditor visualEditorControl;
		private System.Windows.Forms.ListView channelView;
		private System.Windows.Forms.ColumnHeader header;
		private System.Windows.Forms.ColumnHeader events;
		private System.Windows.Forms.ListView trackView;
		private System.Windows.Forms.ColumnHeader columnHeader1;
		private System.Windows.Forms.ToolStripComboBox noteLengthCombo;
		private System.Windows.Forms.ToolStripComboBox snapCombo;
		private System.Windows.Forms.ContextMenuStrip channelContextMenu;
		private System.Windows.Forms.ToolStripMenuItem selectAllChannels;
		private System.Windows.Forms.ContextMenuStrip trackContextMenu;
		private System.Windows.Forms.ToolStripMenuItem selectAllTracks;
		private System.Windows.Forms.ToolStripMenuItem deCheckAllChannels;
		private System.Windows.Forms.ToolStripMenuItem deCheckAllTracks;
		private EventEditor eventEditor;
		private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem saveAsToolStripMenuItem;
		private System.Windows.Forms.ToolStripButton recordButton;
		private System.Windows.Forms.ToolStripMenuItem newToolStripMenuItem;
		private System.Windows.Forms.ToolStripButton playRangeButton;
		private System.Windows.Forms.ToolStripButton autoScrollButton;
		private System.Windows.Forms.ToolStripMenuItem trackTypeToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem singleToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem synchronousToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem asynchronousToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem addNewTrackToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem deleteTrackToolStripMenuItem;
		private System.Windows.Forms.StatusStrip statusStrip;
		private System.Windows.Forms.ToolStripStatusLabel durationLabel;
		private System.Windows.Forms.ToolStripStatusLabel debugLabel;
		private System.Windows.Forms.ToolStripMenuItem saveWavToolStripMenuItem;
		private System.Windows.Forms.ToolStripComboBox chordCombo;
		private System.Windows.Forms.ToolStripStatusLabel buildTimeLabel;
		private System.Windows.Forms.ToolStripButton profileButton;
		private ToolSpinBox toolSpinBox1;
		private ToolSpinBox tempoSpinBox;
		private ToolSpinBox a4Spin;
	}
}