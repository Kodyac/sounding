﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using System.Diagnostics;

namespace Sounding
{
	public partial class WaveEditor : UserControl
	{
		Rectangle editRect
		{
			get
			{
				return new Rectangle((int)(ClientSize.Width * borderSize), 0, ClientSize.Width - (int)(ClientSize.Width * borderSize * 2.0f), ClientSize.Height);
			}
		}
		const float borderSize = .1f;
		//float[] waveTable;
		//int selectedVert = -1;
		
		//public event EventHandler<EventArgs> WaveChanged;

		public Oscillator oscillator;
		
		public WaveEditor()
		{
			InitializeComponent();
		}

		float YToCanvas(float y)
		{
			return (y + 1.0f) * .5f * (ClientSize.Height - 4) + 2;
		}

		protected override void OnPaint(PaintEventArgs e)
		{
			base.OnPaint(e);

			if(oscillator == null)
				return;

			float[] values;
			PointF[] points = new PointF[ClientSize.Width];
		
			values = oscillator.WaveTableCopyAtResolution(editRect.Width);
			
			for(int iii = 0; iii < points.Length; iii++)
			{
				float percent = ((float)iii - (float)editRect.X) / (float)editRect.Width;
				int vindex = (values.Length + (int)(percent * values.Length)) % values.Length;

				points[iii].X = ((float)iii / (float)points.Length) * ClientSize.Width;
				points[iii].Y = YToCanvas(values[vindex]);

			}


			/*using(SolidBrush brush = new SolidBrush(Color.FromArgb(128, 255, 255, 0)))
			{
				if(selectedVert >= 0)
				{
					PointF p = new PointF();
					p.Y = YToCanvas(waveTable[selectedVert]);
					p.X = editRect.X + ((float)selectedVert / (float)waveTable.Length) * editRect.Width;
					e.Graphics.FillEllipse(brush, p.X - 2, p.Y - 2, 5, 5);
				}
			}*/

			using(Pen pen = new Pen(ForeColor))
			{
				e.Graphics.DrawLines(pen, points);

				pen.Color = Color.FromArgb(128, Color.Gray);
				
				e.Graphics.DrawLine(pen, editRect.X, 0, editRect.X, ClientSize.Height);
				e.Graphics.DrawLine(pen, editRect.Right, 0, editRect.Right, ClientSize.Height);

				int half = ClientSize.Height / 2;
				e.Graphics.DrawLine(pen, 0, half, ClientSize.Width, half);
			}
			
		}
		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);
			if(oscillator == null)
				return;
		}
		public void UpdateOscillator()
		{
			Refresh();
		}
		
	}
}
