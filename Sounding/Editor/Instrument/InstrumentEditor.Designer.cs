﻿namespace Sounding
{
	partial class InstrumentEditor
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.menuStrip1 = new System.Windows.Forms.MenuStrip();
			this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.colourToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.visualizerGroup = new System.Windows.Forms.GroupBox();
			this.previewLayout = new System.Windows.Forms.TableLayoutPanel();
			this.waveDrawerLeft = new Sounding.WaveDrawer();
			this.oscillatorsVisualizer = new System.Windows.Forms.Panel();
			this.volumeSpin = new Sounding.SpinBox();
			this.animateCheck = new System.Windows.Forms.CheckBox();
			this.addOscButton = new System.Windows.Forms.Button();
			this.tabs = new System.Windows.Forms.TabControl();
			this.oscillatorTab = new System.Windows.Forms.TabPage();
			this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
			this.oscillatorsLayout = new System.Windows.Forms.TableLayoutPanel();
			this.envelopeTab = new System.Windows.Forms.TabPage();
			this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
			this.envelopeEditor = new Sounding.EnvelopeEditor();
			this.filterEditor1 = new Sounding.FilterEditor();
			this.menuStrip1.SuspendLayout();
			this.visualizerGroup.SuspendLayout();
			this.previewLayout.SuspendLayout();
			this.tabs.SuspendLayout();
			this.oscillatorTab.SuspendLayout();
			this.tableLayoutPanel2.SuspendLayout();
			this.envelopeTab.SuspendLayout();
			this.tableLayoutPanel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// menuStrip1
			// 
			this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.colourToolStripMenuItem});
			this.menuStrip1.Location = new System.Drawing.Point(0, 0);
			this.menuStrip1.Name = "menuStrip1";
			this.menuStrip1.Size = new System.Drawing.Size(384, 24);
			this.menuStrip1.TabIndex = 0;
			this.menuStrip1.Text = "menuStrip1";
			// 
			// fileToolStripMenuItem
			// 
			this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.saveToolStripMenuItem});
			this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
			this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
			this.fileToolStripMenuItem.Text = "File";
			// 
			// saveToolStripMenuItem
			// 
			this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
			this.saveToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
			this.saveToolStripMenuItem.Size = new System.Drawing.Size(138, 22);
			this.saveToolStripMenuItem.Text = "&Save";
			this.saveToolStripMenuItem.Click += new System.EventHandler(this.saveToolStripMenuItem_Click);
			// 
			// colourToolStripMenuItem
			// 
			this.colourToolStripMenuItem.Name = "colourToolStripMenuItem";
			this.colourToolStripMenuItem.Size = new System.Drawing.Size(55, 20);
			this.colourToolStripMenuItem.Text = "Colour";
			this.colourToolStripMenuItem.Click += new System.EventHandler(this.colourToolStripMenuItem_Click);
			// 
			// visualizerGroup
			// 
			this.visualizerGroup.AutoSize = true;
			this.visualizerGroup.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
			this.visualizerGroup.Controls.Add(this.previewLayout);
			this.visualizerGroup.Dock = System.Windows.Forms.DockStyle.Top;
			this.visualizerGroup.Location = new System.Drawing.Point(0, 24);
			this.visualizerGroup.Margin = new System.Windows.Forms.Padding(1, 3, 1, 3);
			this.visualizerGroup.Name = "visualizerGroup";
			this.visualizerGroup.Padding = new System.Windows.Forms.Padding(1, 3, 1, 3);
			this.visualizerGroup.Size = new System.Drawing.Size(384, 202);
			this.visualizerGroup.TabIndex = 1;
			this.visualizerGroup.TabStop = false;
			this.visualizerGroup.Text = "Previewer";
			// 
			// previewLayout
			// 
			this.previewLayout.AutoSize = true;
			this.previewLayout.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
			this.previewLayout.ColumnCount = 2;
			this.previewLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
			this.previewLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
			this.previewLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.previewLayout.Controls.Add(this.waveDrawerLeft, 0, 0);
			this.previewLayout.Controls.Add(this.oscillatorsVisualizer, 0, 1);
			this.previewLayout.Controls.Add(this.volumeSpin, 0, 2);
			this.previewLayout.Controls.Add(this.animateCheck, 1, 2);
			this.previewLayout.Dock = System.Windows.Forms.DockStyle.Fill;
			this.previewLayout.Location = new System.Drawing.Point(1, 16);
			this.previewLayout.Name = "previewLayout";
			this.previewLayout.RowCount = 2;
			this.previewLayout.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.previewLayout.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.previewLayout.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.previewLayout.Size = new System.Drawing.Size(382, 183);
			this.previewLayout.TabIndex = 3;
			// 
			// waveDrawerLeft
			// 
			this.waveDrawerLeft.AutoSize = true;
			this.waveDrawerLeft.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
			this.waveDrawerLeft.BackColor = System.Drawing.Color.Black;
			this.waveDrawerLeft.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.previewLayout.SetColumnSpan(this.waveDrawerLeft, 2);
			this.waveDrawerLeft.Dock = System.Windows.Forms.DockStyle.Top;
			this.waveDrawerLeft.ForeColor = System.Drawing.Color.LightPink;
			this.waveDrawerLeft.Location = new System.Drawing.Point(0, 0);
			this.waveDrawerLeft.Margin = new System.Windows.Forms.Padding(0);
			this.waveDrawerLeft.MinimumSize = new System.Drawing.Size(256, 64);
			this.waveDrawerLeft.Name = "waveDrawerLeft";
			this.waveDrawerLeft.Size = new System.Drawing.Size(382, 64);
			this.waveDrawerLeft.TabIndex = 0;
			// 
			// oscillatorsVisualizer
			// 
			this.oscillatorsVisualizer.BackColor = System.Drawing.Color.Black;
			this.previewLayout.SetColumnSpan(this.oscillatorsVisualizer, 2);
			this.oscillatorsVisualizer.Dock = System.Windows.Forms.DockStyle.Fill;
			this.oscillatorsVisualizer.ForeColor = System.Drawing.Color.Pink;
			this.oscillatorsVisualizer.Location = new System.Drawing.Point(3, 67);
			this.oscillatorsVisualizer.Name = "oscillatorsVisualizer";
			this.oscillatorsVisualizer.Size = new System.Drawing.Size(376, 90);
			this.oscillatorsVisualizer.TabIndex = 4;
			this.oscillatorsVisualizer.Paint += new System.Windows.Forms.PaintEventHandler(this.oscillatorsVisualizer_Paint);
			// 
			// volumeSpin
			// 
			this.volumeSpin.AutoSize = true;
			this.volumeSpin.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.volumeSpin.DecimalPlaces = 3;
			this.volumeSpin.Disabled = false;
			this.volumeSpin.Float = 0F;
			this.volumeSpin.Increment = new decimal(new int[] {
            5,
            0,
            0,
            131072});
			this.volumeSpin.Label = "Instrument Volume: ";
			this.volumeSpin.Location = new System.Drawing.Point(3, 163);
			this.volumeSpin.MaxValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
			this.volumeSpin.MinimumSize = new System.Drawing.Size(136, 16);
			this.volumeSpin.MinValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
			this.volumeSpin.Name = "volumeSpin";
			this.volumeSpin.Size = new System.Drawing.Size(136, 16);
			this.volumeSpin.State = Sounding.SpinBox.EnabledState.Enabled;
			this.volumeSpin.TabIndex = 3;
			this.volumeSpin.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
			this.volumeSpin.ValueChanged += new System.EventHandler<Sounding.SpinBox.ValueChangedArgs>(this.spinBox1_ValueChanged);
			// 
			// animateCheck
			// 
			this.animateCheck.AutoSize = true;
			this.animateCheck.Checked = true;
			this.animateCheck.CheckState = System.Windows.Forms.CheckState.Checked;
			this.animateCheck.Dock = System.Windows.Forms.DockStyle.Fill;
			this.animateCheck.Location = new System.Drawing.Point(145, 163);
			this.animateCheck.Name = "animateCheck";
			this.animateCheck.Size = new System.Drawing.Size(234, 17);
			this.animateCheck.TabIndex = 3;
			this.animateCheck.Text = "Animate Visualizer";
			this.animateCheck.UseVisualStyleBackColor = true;
			this.animateCheck.CheckedChanged += new System.EventHandler(this.animateCheck_CheckedChanged);
			// 
			// addOscButton
			// 
			this.addOscButton.Dock = System.Windows.Forms.DockStyle.Fill;
			this.addOscButton.Location = new System.Drawing.Point(3, 257);
			this.addOscButton.Name = "addOscButton";
			this.addOscButton.Size = new System.Drawing.Size(364, 24);
			this.addOscButton.TabIndex = 3;
			this.addOscButton.Text = "Add";
			this.addOscButton.UseVisualStyleBackColor = true;
			this.addOscButton.Click += new System.EventHandler(this.addOscButton_Click);
			// 
			// tabs
			// 
			this.tabs.Controls.Add(this.oscillatorTab);
			this.tabs.Controls.Add(this.envelopeTab);
			this.tabs.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tabs.HotTrack = true;
			this.tabs.Location = new System.Drawing.Point(0, 226);
			this.tabs.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
			this.tabs.Name = "tabs";
			this.tabs.SelectedIndex = 0;
			this.tabs.Size = new System.Drawing.Size(384, 336);
			this.tabs.TabIndex = 3;
			// 
			// oscillatorTab
			// 
			this.oscillatorTab.Controls.Add(this.tableLayoutPanel2);
			this.oscillatorTab.Location = new System.Drawing.Point(4, 22);
			this.oscillatorTab.Name = "oscillatorTab";
			this.oscillatorTab.Padding = new System.Windows.Forms.Padding(3);
			this.oscillatorTab.Size = new System.Drawing.Size(376, 310);
			this.oscillatorTab.TabIndex = 0;
			this.oscillatorTab.Text = "Oscillators";
			this.oscillatorTab.UseVisualStyleBackColor = true;
			// 
			// tableLayoutPanel2
			// 
			this.tableLayoutPanel2.ColumnCount = 1;
			this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.tableLayoutPanel2.Controls.Add(this.addOscButton, 0, 2);
			this.tableLayoutPanel2.Controls.Add(this.oscillatorsLayout, 0, 1);
			this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 3);
			this.tableLayoutPanel2.Margin = new System.Windows.Forms.Padding(0);
			this.tableLayoutPanel2.Name = "tableLayoutPanel2";
			this.tableLayoutPanel2.RowCount = 4;
			this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
			this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.tableLayoutPanel2.Size = new System.Drawing.Size(370, 304);
			this.tableLayoutPanel2.TabIndex = 0;
			// 
			// oscillatorsLayout
			// 
			this.oscillatorsLayout.AutoScroll = true;
			this.oscillatorsLayout.ColumnCount = 1;
			this.oscillatorsLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.oscillatorsLayout.Dock = System.Windows.Forms.DockStyle.Fill;
			this.oscillatorsLayout.Location = new System.Drawing.Point(3, 3);
			this.oscillatorsLayout.Name = "oscillatorsLayout";
			this.oscillatorsLayout.RowCount = 1;
			this.oscillatorsLayout.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.oscillatorsLayout.Size = new System.Drawing.Size(364, 248);
			this.oscillatorsLayout.TabIndex = 0;
			// 
			// envelopeTab
			// 
			this.envelopeTab.Controls.Add(this.tableLayoutPanel1);
			this.envelopeTab.Location = new System.Drawing.Point(4, 22);
			this.envelopeTab.Name = "envelopeTab";
			this.envelopeTab.Padding = new System.Windows.Forms.Padding(3);
			this.envelopeTab.Size = new System.Drawing.Size(376, 310);
			this.envelopeTab.TabIndex = 1;
			this.envelopeTab.Text = "Envelope\\Filter";
			this.envelopeTab.UseVisualStyleBackColor = true;
			// 
			// tableLayoutPanel1
			// 
			this.tableLayoutPanel1.ColumnCount = 1;
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.tableLayoutPanel1.Controls.Add(this.envelopeEditor, 0, 0);
			this.tableLayoutPanel1.Controls.Add(this.filterEditor1, 0, 1);
			this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 3);
			this.tableLayoutPanel1.Name = "tableLayoutPanel1";
			this.tableLayoutPanel1.RowCount = 4;
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.tableLayoutPanel1.Size = new System.Drawing.Size(370, 304);
			this.tableLayoutPanel1.TabIndex = 0;
			// 
			// envelopeEditor
			// 
			this.envelopeEditor.AutoSize = true;
			this.envelopeEditor.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
			this.envelopeEditor.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.envelopeEditor.Dock = System.Windows.Forms.DockStyle.Fill;
			this.envelopeEditor.Location = new System.Drawing.Point(3, 3);
			this.envelopeEditor.MinimumSize = new System.Drawing.Size(256, 2);
			this.envelopeEditor.Name = "envelopeEditor";
			this.envelopeEditor.Size = new System.Drawing.Size(364, 146);
			this.envelopeEditor.TabIndex = 0;
			// 
			// filterEditor1
			// 
			this.filterEditor1.AutoSize = true;
			this.filterEditor1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
			this.filterEditor1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.filterEditor1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.filterEditor1.Filter = null;
			this.filterEditor1.Location = new System.Drawing.Point(3, 155);
			this.filterEditor1.Name = "filterEditor1";
			this.filterEditor1.Size = new System.Drawing.Size(364, 53);
			this.filterEditor1.TabIndex = 0;
			this.filterEditor1.Changed += new System.EventHandler<System.EventArgs>(this.filterEditor1_Changed);
			// 
			// InstrumentEditor
			// 
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
			this.ClientSize = new System.Drawing.Size(384, 562);
			this.Controls.Add(this.tabs);
			this.Controls.Add(this.visualizerGroup);
			this.Controls.Add(this.menuStrip1);
			this.MainMenuStrip = this.menuStrip1;
			this.MaximizeBox = false;
			this.MaximumSize = new System.Drawing.Size(400, 1200);
			this.MinimumSize = new System.Drawing.Size(400, 600);
			this.Name = "InstrumentEditor";
			this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
			this.Text = "InstrumentEditor";
			this.Activated += new System.EventHandler(this.InstrumentEditor_Activated);
			this.Deactivate += new System.EventHandler(this.InstrumentEditor_Deactivate);
			this.Load += new System.EventHandler(this.InstrumentEditor_Load);
			this.menuStrip1.ResumeLayout(false);
			this.menuStrip1.PerformLayout();
			this.visualizerGroup.ResumeLayout(false);
			this.visualizerGroup.PerformLayout();
			this.previewLayout.ResumeLayout(false);
			this.previewLayout.PerformLayout();
			this.tabs.ResumeLayout(false);
			this.oscillatorTab.ResumeLayout(false);
			this.tableLayoutPanel2.ResumeLayout(false);
			this.envelopeTab.ResumeLayout(false);
			this.tableLayoutPanel1.ResumeLayout(false);
			this.tableLayoutPanel1.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.MenuStrip menuStrip1;
		private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
		private WaveDrawer waveDrawerLeft;
		private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
		private System.Windows.Forms.GroupBox visualizerGroup;
		private System.Windows.Forms.TableLayoutPanel previewLayout;
		private System.Windows.Forms.CheckBox animateCheck;
		private System.Windows.Forms.Button addOscButton;
		private System.Windows.Forms.TabControl tabs;
		private System.Windows.Forms.TabPage oscillatorTab;
		private System.Windows.Forms.TabPage envelopeTab;
		private EnvelopeEditor envelopeEditor;
		private SpinBox volumeSpin;
		private System.Windows.Forms.ToolStripMenuItem colourToolStripMenuItem;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
		private FilterEditor filterEditor1;
		private System.Windows.Forms.TableLayoutPanel oscillatorsLayout;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
		private System.Windows.Forms.Panel oscillatorsVisualizer;

	}
}