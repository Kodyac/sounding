﻿namespace Sounding
{
	partial class EnvelopeEditor
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.visualizer = new System.Windows.Forms.Panel();
			this.deleteButton = new System.Windows.Forms.Button();
			this.mainLayout = new System.Windows.Forms.TableLayoutPanel();
			this.attackSpin = new Sounding.SpinBox();
			this.attackValueSpin = new Sounding.SpinBox();
			this.decaySpin = new Sounding.SpinBox();
			this.sustainValueSpin = new Sounding.SpinBox();
			this.releaseSpin = new Sounding.SpinBox();
			this.visualizer.SuspendLayout();
			this.mainLayout.SuspendLayout();
			this.SuspendLayout();
			// 
			// visualizer
			// 
			this.visualizer.AutoSize = true;
			this.visualizer.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
			this.visualizer.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.visualizer.Controls.Add(this.deleteButton);
			this.visualizer.Dock = System.Windows.Forms.DockStyle.Top;
			this.visualizer.Location = new System.Drawing.Point(0, 0);
			this.visualizer.MinimumSize = new System.Drawing.Size(200, 32);
			this.visualizer.Name = "visualizer";
			this.visualizer.Size = new System.Drawing.Size(254, 33);
			this.visualizer.TabIndex = 0;
			this.visualizer.Paint += new System.Windows.Forms.PaintEventHandler(this.PaintVisualizer);
			// 
			// deleteButton
			// 
			this.deleteButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.deleteButton.Location = new System.Drawing.Point(199, 3);
			this.deleteButton.MaximumSize = new System.Drawing.Size(48, 23);
			this.deleteButton.MinimumSize = new System.Drawing.Size(48, 23);
			this.deleteButton.Name = "deleteButton";
			this.deleteButton.Size = new System.Drawing.Size(48, 23);
			this.deleteButton.TabIndex = 0;
			this.deleteButton.Text = "Delete";
			this.deleteButton.UseVisualStyleBackColor = true;
			this.deleteButton.Click += new System.EventHandler(this.deleteButton_Click);
			// 
			// mainLayout
			// 
			this.mainLayout.AutoSize = true;
			this.mainLayout.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
			this.mainLayout.ColumnCount = 5;
			this.mainLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
			this.mainLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
			this.mainLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
			this.mainLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
			this.mainLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
			this.mainLayout.Controls.Add(this.attackSpin, 0, 1);
			this.mainLayout.Controls.Add(this.attackValueSpin, 0, 0);
			this.mainLayout.Controls.Add(this.decaySpin, 1, 1);
			this.mainLayout.Controls.Add(this.sustainValueSpin, 2, 0);
			this.mainLayout.Controls.Add(this.releaseSpin, 3, 1);
			this.mainLayout.Dock = System.Windows.Forms.DockStyle.Fill;
			this.mainLayout.Location = new System.Drawing.Point(0, 33);
			this.mainLayout.Name = "mainLayout";
			this.mainLayout.RowCount = 6;
			this.mainLayout.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.mainLayout.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.mainLayout.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.mainLayout.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.mainLayout.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.mainLayout.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.mainLayout.Size = new System.Drawing.Size(254, 44);
			this.mainLayout.TabIndex = 1;
			// 
			// attackSpin
			// 
			this.attackSpin.AutoSize = true;
			this.attackSpin.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.attackSpin.DecimalPlaces = 3;
			this.attackSpin.Disabled = false;
			this.attackSpin.Float = 0.005F;
			this.attackSpin.Increment = new decimal(new int[] {
            5,
            0,
            0,
            196608});
			this.attackSpin.Label = "Attack";
			this.attackSpin.Location = new System.Drawing.Point(3, 25);
			this.attackSpin.MaxValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
			this.attackSpin.MinimumSize = new System.Drawing.Size(74, 16);
			this.attackSpin.MinValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
			this.attackSpin.Name = "attackSpin";
			this.attackSpin.Size = new System.Drawing.Size(74, 16);
			this.attackSpin.State = Sounding.SpinBox.EnabledState.Enabled;
			this.attackSpin.TabIndex = 14;
			this.attackSpin.Value = new decimal(new int[] {
            5,
            0,
            0,
            196608});
			this.attackSpin.ValueChanged += new System.EventHandler<Sounding.SpinBox.ValueChangedArgs>(this.UpdateEnvelope);
			// 
			// attackValueSpin
			// 
			this.attackValueSpin.AutoSize = true;
			this.attackValueSpin.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.mainLayout.SetColumnSpan(this.attackValueSpin, 2);
			this.attackValueSpin.DecimalPlaces = 2;
			this.attackValueSpin.Disabled = false;
			this.attackValueSpin.Float = 1F;
			this.attackValueSpin.Increment = new decimal(new int[] {
            5,
            0,
            0,
            131072});
			this.attackValueSpin.Label = "Attack Volume";
			this.attackValueSpin.Location = new System.Drawing.Point(3, 3);
			this.attackValueSpin.MaxValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
			this.attackValueSpin.MinimumSize = new System.Drawing.Size(106, 16);
			this.attackValueSpin.MinValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
			this.attackValueSpin.Name = "attackValueSpin";
			this.attackValueSpin.Size = new System.Drawing.Size(106, 16);
			this.attackValueSpin.State = Sounding.SpinBox.EnabledState.Enabled;
			this.attackValueSpin.TabIndex = 12;
			this.attackValueSpin.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
			this.attackValueSpin.ValueChanged += new System.EventHandler<Sounding.SpinBox.ValueChangedArgs>(this.UpdateEnvelope);
			// 
			// decaySpin
			// 
			this.decaySpin.AutoSize = true;
			this.decaySpin.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.mainLayout.SetColumnSpan(this.decaySpin, 2);
			this.decaySpin.DecimalPlaces = 3;
			this.decaySpin.Disabled = false;
			this.decaySpin.Float = 0.01F;
			this.decaySpin.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
			this.decaySpin.Label = "Decay";
			this.decaySpin.Location = new System.Drawing.Point(83, 25);
			this.decaySpin.MaxValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
			this.decaySpin.MinimumSize = new System.Drawing.Size(74, 16);
			this.decaySpin.MinValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
			this.decaySpin.Name = "decaySpin";
			this.decaySpin.Size = new System.Drawing.Size(74, 16);
			this.decaySpin.State = Sounding.SpinBox.EnabledState.Enabled;
			this.decaySpin.TabIndex = 15;
			this.decaySpin.Value = new decimal(new int[] {
            1,
            0,
            0,
            131072});
			this.decaySpin.ValueChanged += new System.EventHandler<Sounding.SpinBox.ValueChangedArgs>(this.UpdateEnvelope);
			// 
			// sustainValueSpin
			// 
			this.sustainValueSpin.AutoSize = true;
			this.sustainValueSpin.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.mainLayout.SetColumnSpan(this.sustainValueSpin, 2);
			this.sustainValueSpin.DecimalPlaces = 2;
			this.sustainValueSpin.Disabled = false;
			this.sustainValueSpin.Float = 0.8F;
			this.sustainValueSpin.Increment = new decimal(new int[] {
            5,
            0,
            0,
            131072});
			this.sustainValueSpin.Label = "Sustain Volume";
			this.sustainValueSpin.Location = new System.Drawing.Point(115, 3);
			this.sustainValueSpin.MaxValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
			this.sustainValueSpin.MinimumSize = new System.Drawing.Size(110, 16);
			this.sustainValueSpin.MinValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
			this.sustainValueSpin.Name = "sustainValueSpin";
			this.sustainValueSpin.Size = new System.Drawing.Size(110, 16);
			this.sustainValueSpin.State = Sounding.SpinBox.EnabledState.Enabled;
			this.sustainValueSpin.TabIndex = 13;
			this.sustainValueSpin.Value = new decimal(new int[] {
            8,
            0,
            0,
            65536});
			this.sustainValueSpin.ValueChanged += new System.EventHandler<Sounding.SpinBox.ValueChangedArgs>(this.UpdateEnvelope);
			// 
			// releaseSpin
			// 
			this.releaseSpin.AutoSize = true;
			this.releaseSpin.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.releaseSpin.DecimalPlaces = 3;
			this.releaseSpin.Disabled = false;
			this.releaseSpin.Float = 0.005F;
			this.releaseSpin.Increment = new decimal(new int[] {
            5,
            0,
            0,
            196608});
			this.releaseSpin.Label = "Release";
			this.releaseSpin.Location = new System.Drawing.Point(163, 25);
			this.releaseSpin.MaxValue = new decimal(new int[] {
            2,
            0,
            0,
            0});
			this.releaseSpin.MinimumSize = new System.Drawing.Size(82, 16);
			this.releaseSpin.MinValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
			this.releaseSpin.Name = "releaseSpin";
			this.releaseSpin.Size = new System.Drawing.Size(82, 16);
			this.releaseSpin.State = Sounding.SpinBox.EnabledState.Enabled;
			this.releaseSpin.TabIndex = 16;
			this.releaseSpin.Value = new decimal(new int[] {
            5,
            0,
            0,
            196608});
			this.releaseSpin.ValueChanged += new System.EventHandler<Sounding.SpinBox.ValueChangedArgs>(this.UpdateEnvelope);
			// 
			// EnvelopeEditor
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.AutoSize = true;
			this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
			this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.Controls.Add(this.mainLayout);
			this.Controls.Add(this.visualizer);
			this.MinimumSize = new System.Drawing.Size(256, 2);
			this.Name = "EnvelopeEditor";
			this.Size = new System.Drawing.Size(254, 77);
			this.visualizer.ResumeLayout(false);
			this.mainLayout.ResumeLayout(false);
			this.mainLayout.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Panel visualizer;
		private System.Windows.Forms.TableLayoutPanel mainLayout;
		private SpinBox attackValueSpin;
		private SpinBox sustainValueSpin;
		private SpinBox attackSpin;
		private SpinBox decaySpin;
		private SpinBox releaseSpin;
		private System.Windows.Forms.Button deleteButton;
	}
}
