﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Sounding
{
	public partial class FilterEditor : UserControl
	{
		public event EventHandler<EventArgs> Changed;

		[Browsable(false)]
		public Filter Filter
		{
			get
			{
				if(formCombo.SelectedIndex == (int)Filter.PassType.Everything)
					return null;
				
				return filter;
			}
			set
			{
				if(value == null)
					formCombo.SelectedIndex = (int)Filter.PassType.Everything;
				else
					filter = value;

				UpdateGUI();
			}
		}
		Filter filter = new Filter();

		public FilterEditor()
		{
			InitializeComponent();

			formCombo.FillFromEnum(typeof(Filter.PassType));
		}

		void UpdateGUI()
		{
			if(filter.Pass != Sounding.Filter.PassType.Everything)
			{
				cutoffSpin.Enabled = true;
				qSpin.Enabled = true;
				cutoffSpin.SetValueWithoutEvent(filter.Cutoff);
				qSpin.SetValueWithoutEvent(filter.Resonance);
				formCombo.SelectedIndex = (int)filter.Pass;
			}
			else
			{
				cutoffSpin.Enabled = false;
				qSpin.Enabled = false;
				formCombo.SelectedIndex = (int)Filter.PassType.Everything;
			}
		}

		private void formCombo_SelectedIndexChanged(object sender, EventArgs e)
		{
			filter.Pass = (Filter.PassType)formCombo.SelectedIndex;
			UpdateGUI();
			if(Changed != null)
				Changed(this, EventArgs.Empty);
		}

		private void cutoffSpin_ValueChanged(object sender, SpinBox.ValueChangedArgs e)
		{
			filter.Cutoff = cutoffSpin.Float;
			if(Changed != null)
				Changed(this, EventArgs.Empty);
		}

		private void qSpin_ValueChanged(object sender, SpinBox.ValueChangedArgs e)
		{
			filter.Resonance = qSpin.Float;
			if(Changed != null)
				Changed(this, EventArgs.Empty);
		}
	}
}
