﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using System.Diagnostics;

namespace Sounding
{
	public partial class EnvelopeEditor : UserControl
	{
//		public delegate void UpdateInstrument();
//		public UpdateInstrument updater = null;

		public event EventHandler<EventArgs> DeleteMe;
		public event EventHandler<EventArgs> Changed;

		Envelope envelope = null;
		public Envelope Envelope { get { return envelope; } }
		public EnvelopeEditor()
		{
			InitializeComponent();


		}

		public void LoadEnvelope(Envelope env)
		{
			envelope = null;

			attackValueSpin.Value	= (decimal)env.AttackValue;
			sustainValueSpin.Value	= (decimal)env.SustainValue;
			attackSpin.Value		= (decimal)env.Attack;
			decaySpin.Value			= (decimal)env.Decay;
			releaseSpin.Value		= (decimal)env.Release;

			envelope = env;
		}

		void UpdateEnvelope(object sender, EventArgs e)
		{
			if(envelope == null)
				return;

			envelope.AttackValue	= (float)attackValueSpin.Value;
			envelope.SustainValue	= (float)sustainValueSpin.Value;
			envelope.Attack			= (float)attackSpin.Value;
			envelope.Decay			= (float)decaySpin.Value;
			envelope.Release		= (float)releaseSpin.Value;

			if(Changed != null)
				Changed(this, EventArgs.Empty);

			visualizer.Refresh();
		}

		private void PaintVisualizer(object sender, PaintEventArgs e)
		{
			if(envelope == null)
				return;

			float length = envelope.Attack + envelope.Decay + envelope.Release;

			float max = (float)attackSpin.MaxValue;
			max += (float)decaySpin.MaxValue;
			max += (float)releaseSpin.MaxValue;

			float sustWidth = .0f;
			sustWidth = max - length;
			
			length += sustWidth;

			float width = (float)visualizer.ClientSize.Width * (max / (max * 1.25f));
			float height = (float)visualizer.ClientSize.Height;

			PointF[] points = new PointF[6];

			points[0] = new PointF(.0f, height);

			points[1] = new PointF(	(envelope.Attack / length) * width, 
									height - envelope.AttackValue * height );

			points[2] = new PointF(	((envelope.Attack + envelope.Decay) / length) * width, 
									height - envelope.SustainValue * height);

			points[3] = new PointF(	((envelope.Attack + envelope.Decay + sustWidth) / length) * width, 
									height - envelope.SustainValue * height);

			points[4] = new PointF(	width, 
									height);

			points[5] = new PointF((float)visualizer.ClientSize.Width, height);

			using(Pen pen = new Pen(DefaultForeColor))
			{
				for(int iii = 1; iii < points.Length; iii++)
				{
					e.Graphics.DrawLine(pen, points[iii - 1], points[iii]);
				}
			}
		}

		private void deleteButton_Click(object sender, EventArgs e)
		{
			if(DeleteMe != null)
				DeleteMe(this, e);
		}

		/*private void UpdateEnvelope(object sender, SpinBox.ValueChangedArgs e)
		{

		}*/
	}
}
