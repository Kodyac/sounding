﻿namespace Sounding
{
	partial class FilterEditor
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
			this.cutoffSpin = new Sounding.SpinBox();
			this.typeLabel = new System.Windows.Forms.Label();
			this.formCombo = new System.Windows.Forms.ComboBox();
			this.qSpin = new Sounding.SpinBox();
			this.tableLayoutPanel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// tableLayoutPanel1
			// 
			this.tableLayoutPanel1.AutoSize = true;
			this.tableLayoutPanel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
			this.tableLayoutPanel1.ColumnCount = 3;
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
			this.tableLayoutPanel1.Controls.Add(this.cutoffSpin, 0, 1);
			this.tableLayoutPanel1.Controls.Add(this.typeLabel, 0, 0);
			this.tableLayoutPanel1.Controls.Add(this.formCombo, 1, 0);
			this.tableLayoutPanel1.Controls.Add(this.qSpin, 2, 1);
			this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
			this.tableLayoutPanel1.Name = "tableLayoutPanel1";
			this.tableLayoutPanel1.RowCount = 3;
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.tableLayoutPanel1.Size = new System.Drawing.Size(190, 49);
			this.tableLayoutPanel1.TabIndex = 0;
			// 
			// cutoffSpin
			// 
			this.cutoffSpin.AutoSize = true;
			this.cutoffSpin.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.tableLayoutPanel1.SetColumnSpan(this.cutoffSpin, 2);
			this.cutoffSpin.DecimalPlaces = 2;
			this.cutoffSpin.Disabled = false;
			this.cutoffSpin.Float = 500F;
			this.cutoffSpin.Increment = new decimal(new int[] {
            10,
            0,
            0,
            0});
			this.cutoffSpin.Label = "Cutoff";
			this.cutoffSpin.Location = new System.Drawing.Point(3, 30);
			this.cutoffSpin.MaxValue = new decimal(new int[] {
            20000,
            0,
            0,
            0});
			this.cutoffSpin.MinimumSize = new System.Drawing.Size(89, 16);
			this.cutoffSpin.MinValue = new decimal(new int[] {
            10,
            0,
            0,
            0});
			this.cutoffSpin.Name = "cutoffSpin";
			this.cutoffSpin.Size = new System.Drawing.Size(89, 16);
			this.cutoffSpin.State = Sounding.SpinBox.EnabledState.Enabled;
			this.cutoffSpin.TabIndex = 0;
			this.cutoffSpin.Value = new decimal(new int[] {
            500,
            0,
            0,
            0});
			this.cutoffSpin.ValueChanged += new System.EventHandler<Sounding.SpinBox.ValueChangedArgs>(this.cutoffSpin_ValueChanged);
			// 
			// typeLabel
			// 
			this.typeLabel.Anchor = System.Windows.Forms.AnchorStyles.Right;
			this.typeLabel.AutoSize = true;
			this.typeLabel.Location = new System.Drawing.Point(3, 7);
			this.typeLabel.Name = "typeLabel";
			this.typeLabel.Size = new System.Drawing.Size(57, 13);
			this.typeLabel.TabIndex = 2;
			this.typeLabel.Text = "Pass Type";
			// 
			// formCombo
			// 
			this.tableLayoutPanel1.SetColumnSpan(this.formCombo, 2);
			this.formCombo.Dock = System.Windows.Forms.DockStyle.Fill;
			this.formCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.formCombo.FormattingEnabled = true;
			this.formCombo.Location = new System.Drawing.Point(66, 3);
			this.formCombo.Name = "formCombo";
			this.formCombo.Size = new System.Drawing.Size(121, 21);
			this.formCombo.TabIndex = 3;
			this.formCombo.SelectedIndexChanged += new System.EventHandler(this.formCombo_SelectedIndexChanged);
			// 
			// qSpin
			// 
			this.qSpin.AutoSize = true;
			this.qSpin.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.qSpin.DecimalPlaces = 2;
			this.qSpin.Disabled = false;
			this.qSpin.Float = 1F;
			this.qSpin.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
			this.qSpin.Label = "Q";
			this.qSpin.Location = new System.Drawing.Point(98, 30);
			this.qSpin.MaxValue = new decimal(new int[] {
            25,
            0,
            0,
            0});
			this.qSpin.MinimumSize = new System.Drawing.Size(51, 16);
			this.qSpin.MinValue = new decimal(new int[] {
            1,
            0,
            0,
            131072});
			this.qSpin.Name = "qSpin";
			this.qSpin.Size = new System.Drawing.Size(51, 16);
			this.qSpin.State = Sounding.SpinBox.EnabledState.Enabled;
			this.qSpin.TabIndex = 1;
			this.qSpin.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
			this.qSpin.ValueChanged += new System.EventHandler<Sounding.SpinBox.ValueChangedArgs>(this.qSpin_ValueChanged);
			// 
			// FilterEditor
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.AutoSize = true;
			this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
			this.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.Controls.Add(this.tableLayoutPanel1);
			this.Name = "FilterEditor";
			this.Size = new System.Drawing.Size(190, 49);
			this.tableLayoutPanel1.ResumeLayout(false);
			this.tableLayoutPanel1.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
		private SpinBox cutoffSpin;
		private SpinBox qSpin;
		private System.Windows.Forms.Label typeLabel;
		private System.Windows.Forms.ComboBox formCombo;
	}
}
