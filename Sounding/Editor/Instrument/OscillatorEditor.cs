﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Sounding
{
	public partial class OscillatorEditor : UserControl
	{
		static public void MakeOscillatorCombo(ref ComboBox oscillatorCombo)
		{
			string[] oscStrings = new string[Enum.GetValues(typeof(Oscillator.WaveForm)).Length];
			for(int iii = 0; iii < oscStrings.Length; iii++)
			{
				oscStrings[iii] = Enum.GetValues(typeof(Oscillator.WaveForm)).GetValue(iii).ToString();
			}
			oscillatorCombo.Items.Clear();
			oscillatorCombo.Items.AddRange(oscStrings);
		}

		public event EventHandler<EventArgs> OscillatorChanged;

		public bool DeleteAble { get { return delete.Visible; } set { delete.Visible = value; } }
		
		public event EventHandler<EventArgs> DeleteMe;

		public Oscillator Oscillator
		{
			get { return oscillator; }
			set 
			{ 
				waveEditor.oscillator = oscillator = value;
				volumeSpin.SetValueWithoutEvent(value.volume);

				frequencyOffsetSpin.SetValueWithoutEvent(value.semitoneOffset);
				phaseSpin.SetValueWithoutEvent(value.phaseOffset);
				frequencySpin.SetValueWithoutEvent(value.frequency);
			
				for(int iii = 0; iii < formCombo.Items.Count; iii++)
				{
					if(formCombo.Items[iii].ToString().CompareTo(oscillator.Form.ToString()) == 0)
						formCombo.SelectedIndex = iii;
				}
				
				typeCombo.SelectedIndex = (int)value.type;

				waveEditor.UpdateOscillator();
			
				oscillatorChanged(this, EventArgs.Empty);
			}
		}
		Oscillator oscillator = null;

		public OscillatorEditor()
		{
			Construct(new Oscillator());
		}
		public OscillatorEditor(Oscillator oscillator)
		{
			Construct(oscillator);
		}
		void Construct(Oscillator oscillator)
		{
			InitializeComponent();

			MakeOscillatorCombo(ref formCombo);

			
			typeCombo.FillFromEnum(typeof(Oscillator.MixType));
			typeCombo.SelectedIndex = (int)oscillator.type >= typeCombo.Items.Count ? typeCombo.Items.Count - 1 : (int)oscillator.type;

			Oscillator = oscillator;

			if(Oscillator.envelope == null)
			{
				envelopeEditor.Visible = false;
				addEnvelopeButton.Visible = true;
			}
			else
			{
				envelopeEditor.Visible = true;
				addEnvelopeButton.Visible = false;

				envelopeEditor.LoadEnvelope(oscillator.envelope);
			}
		}

		void oscillatorChanged(object sender, EventArgs e)
		{
			if(Oscillator == null)
				return;

			Oscillator.Form = (Oscillator.WaveForm)Enum.Parse(typeof(Oscillator.WaveForm), formCombo.Text);

			Oscillator.volume = volumeSpin.Float;
			Oscillator.semitoneOffset = frequencyOffsetSpin.Float;
			Oscillator.phaseOffset = phaseSpin.Float;
			Oscillator.frequency = frequencySpin.Float;
			Oscillator.type = (Oscillator.MixType)typeCombo.SelectedIndex;

			waveEditor.UpdateOscillator();
			
			if(OscillatorChanged != null)
				OscillatorChanged(this, EventArgs.Empty);
		}

		private void delete_Click(object sender, EventArgs e)
		{
			if(DeleteMe == null)
				return;
			DeleteMe(this, e);
		}

		private void waveEditor_WaveChanged(object sender, EventArgs e)
		{
			oscillatorChanged(sender, e);
		}

		private void oscillatorSpinsChanged(object sender, SpinBox.ValueChangedArgs e)
		{
			oscillatorChanged(sender, e);
		}

		private void phaseSpin_ValueChanged(object sender, SpinBox.ValueChangedArgs e)
		{
			oscillatorChanged(sender,e );
		}

		private void envelopeEditor_Changed(object sender, EventArgs e)
		{

		}

		private void envelopeEditor_DeleteMe(object sender, EventArgs e)
		{
			envelopeEditor.Visible = false;
			addEnvelopeButton.Visible = true;

			oscillator.envelope = null;
		}

		private void addEnvelopeButton_Click(object sender, EventArgs e)
		{
			envelopeEditor.Visible = true;
			addEnvelopeButton.Visible = false;

			if(envelopeEditor.Envelope == null)
				envelopeEditor.LoadEnvelope(new Envelope());
			oscillator.envelope = envelopeEditor.Envelope;

			oscillator.type = (Oscillator.MixType)typeCombo.SelectedIndex;
		}

		private void frequencySpin_ValueChanged(object sender, SpinBox.ValueChangedArgs e)
		{
			oscillatorChanged(sender, e);
		}

		private void typeCombo_SelectedIndexChanged(object sender, EventArgs e)
		{
			oscillatorChanged(sender,e );
		}




	}
}
