﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

using Sounding.IO;

using System.Diagnostics;

namespace Sounding
{
	public partial class InstrumentEditor : Form
	{
		bool mustSaveAs = false;
		public static List<InstrumentEditor> windows = new List<InstrumentEditor>();
		//List<OscillatorEditor> oscillatorEditors = new List<OscillatorEditor>();
		List<ActiveNote> currentNotes = new List<ActiveNote>();
		public Instrument instrument;// = new Instrument();
		Timer displayTimer = new Timer();
		Timer loadTimer = new Timer();
		int sample = 0;

		float currentBeat = .0f;

		public InstrumentEditor(string instrumentName)
		{
			InitializeComponent();

			if(string.IsNullOrEmpty(instrumentName))
			{
				instrument = new Instrument();
				mustSaveAs = true;
			}
			else
				instrument = Instrument.GetInstrument(instrumentName);

			if(instrument == null)
			{
				mustSaveAs = true;
				instrument = new Instrument();
			}
			SetTitle();//Text = instrument.name;

			volumeSpin.Value = (decimal)instrument.volume;
			
			envelopeEditor.LoadEnvelope(instrument.envelope);
			envelopeEditor.Changed += Changed;

		//	amplitudeLFOEditor.LFO = instrument.amplitudeLFO;
		//	frequencyLFOEditor.LFO = instrument.frequencyLFO;

			filterEditor1.Filter = instrument.filter;

			displayTimer.Interval = 50;
			displayTimer.Tick += displayTimer_Tick;
			displayTimer.Start();

			UpdateOscillators();

			windows.Add(this);

			KeyPreview = true;

			loadTimer.Interval = 5;
			loadTimer.Tick += loadTimer_Tick;
			loadTimer.Start();
		}
		public static void Open(string instrumentName)
		{
			for(int jjj = 0; jjj < InstrumentEditor.windows.Count; jjj++)
			{
				if(InstrumentEditor.windows[jjj].instrument.name.CompareTo(instrumentName) == 0)
				{
					InstrumentEditor.windows[jjj].BringToFront();
					return;
				}
			}
			InstrumentEditor window = new InstrumentEditor(instrumentName);
			window.Show();
		}
		public void Changed(object sender, EventArgs e) { Changed(); }
		public void Changed() 
		{ 
			SetTitle(true); 
			oscillatorsVisualizer.Invalidate();
			oscillatorsVisualizer.Refresh();
		}
		void SetTitle(bool changed = false)
		{
			Text = instrument.name + (changed ? " *" : "");
		}
		void DeleteOscillator(Oscillator osc)
		{
			if(instrument.oscillators.Count < 2)
				return;
			Changed();
			lock(instrument.oscillators)
			{
				instrument.oscillators.Remove(osc);
			}
			UpdateOscillators();
		}
		void UpdateOscillators()
		{
			oscillatorsLayout.Controls.Clear();

			for(int iii = 0; iii < instrument.oscillators.Count; iii++)
			{
				OscillatorEditor oe = new OscillatorEditor(instrument.oscillators[iii]);
				oe.DeleteMe += oe_DeleteMe;
				oe.OscillatorChanged += Changed;
				oscillatorsLayout.Controls.Add(oe);
				//oe.Dock = DockStyle.Fill;
				//Debug.Write(oe.Size);
			}
			
			oscillatorsLayout.PerformLayout();
			oscillatorsVisualizer.Invalidate();
		}

		void oe_DeleteMe(object sender, EventArgs e)
		{
			OscillatorEditor oe = sender as OscillatorEditor;
			if(oe == null)
				return;
			DeleteOscillator(oe.Oscillator);
		}

		void displayTimer_Tick(object sender, EventArgs e)
		{
			currentBeat += ((float)displayTimer.Interval * .001f) * DeviceSettings.beatsPerSecond;
			if(animateCheck.Checked)
				UpdateDrawers();
		}
		
		private void animateCheck_CheckedChanged(object sender, EventArgs e)
		{
			currentNotes.Clear();
		}
		void UpdateDrawers()
		{
			//int channels = 2;
			
			int sampleLength = DeviceSettings.SampleRate;
			if(animateCheck.Checked)
				sampleLength = (int)((float)DeviceSettings.SampleRate * ((float)displayTimer.Interval * .001f));

			int count = sampleLength * DeviceSettings.Channels;
			float[] data;
			data = new float[count];
			
			if(currentNotes.Count < 1)
			{
				currentBeat = .0f;
				sample = 0;
				waveDrawerLeft.DrawData(data, DeviceSettings.Channels, 0);
				//waveDrawerRight.DrawData(data, channels, 1);
					
				return;
			}
			
			//instrument.GetSamples(currentNotes, ref data, 1.0f / instrument.volume);
			for(int nnn = currentNotes.Count - 1; nnn >= 0; nnn--)
			{
				bool noteDead = instrument.GetSamples(currentNotes[nnn], ref data, 1.0f / instrument.volume);
				//bool noteDead = instrument.GetSamples(Keyboard.CurrentNotes[nnn], ref data);

				if(!animateCheck.Checked)
					currentNotes[nnn].sample = 0;
				else if(noteDead)
					currentNotes.RemoveAt(nnn);
			}
			
			sample += sampleLength;

			waveDrawerLeft.DrawData(data, DeviceSettings.Channels, 0);
			//waveDrawerRight.DrawData(data, channels, 1);
		}

		protected override void OnKeyDown(KeyEventArgs e)
		{
			base.OnKeyDown(e);

			Keyboard.OnKeyDown(e.KeyCode);

			bool valid;
			Key n = Keyboard.KeyNote(e.KeyCode, out valid);
			if(!valid)
				return;

			for(int iii = 0; iii < currentNotes.Count; iii++)	//this mess is needed to stop key repeats from firing this event over and over
			{
				if(currentNotes[iii].key == n && currentNotes[iii].Alive)
					return;
			}
			
			currentNotes.Add(new ActiveNote(n, 1.0f));
			if(!animateCheck.Checked)
				UpdateDrawers();
		}
		protected override void OnKeyUp(KeyEventArgs e)
		{
			base.OnKeyDown(e);

			Keyboard.OnKeyUp(e.KeyCode);

			bool valid;
			Key n = Keyboard.KeyNote(e.KeyCode, out valid);
			if(!valid)
				return;

			if(animateCheck.Checked)
			{
				for(int iii = 0; iii < currentNotes.Count; iii++)
				{
					if(currentNotes[iii].key == n && currentNotes[iii].Alive)
						currentNotes[iii].Alive = false;
				}
			}
			else
				currentNotes.Clear();
		}

		private void addOscButton_Click(object sender, EventArgs e)
		{
			Changed();
			instrument.oscillators.Add(new Oscillator());
			UpdateOscillators();
		}

		private void InstrumentEditor_Activated(object sender, EventArgs e)
		{
			Keyboard.instrument = instrument;
		}
		private void saveToolStripMenuItem_Click(object sender, EventArgs e)
		{
			Save();
		}

		public void Save()
		{
			if(mustSaveAs)
				SaveAs();
			IOERR err = InstrumentFileHandler.Save(instrument);
			if(err != IOERR.SUCCESS)
			{
				FileHandling.IOSaveErrorMessage(instrument.name, err);
				return;
			}
			SetTitle();//Text = instrument.name;
		}
		void SaveAs()
		{
			SaveFileDialog dia = new SaveFileDialog();
			Directory.CreateDirectory(InstrumentFileHandler.fullFolder);
			dia.InitialDirectory = InstrumentFileHandler.fullFolder;
			if(string.IsNullOrEmpty(dia.InitialDirectory))
				dia.InitialDirectory = Settings.Folder();
			dia.Filter = "instrument files (*.instrument)|*.instrument";
			dia.RestoreDirectory = true;

			if(dia.ShowDialog() == DialogResult.OK)
			{
				FileInfo info = new FileInfo(dia.FileName);
				string name = InstrumentFileHandler.NameFromFilename(info.FullName);
				instrument.name = name;
			}
			else
				return;
			
			mustSaveAs = false;
			Save();
		}

		void loadTimer_Tick(object sender, EventArgs e)
		{
			loadTimer.Stop();
			BringToFront();
		}
		void LoadSettings()
		{
			if(instrument == null || string.IsNullOrEmpty(instrument.name))
				return;
			string hkey = instrument.SettingsFile();

			FormWindowState stateTest;
			if(Settings.TryParseEnumSetting<FormWindowState>(hkey, "_state", out stateTest))
				WindowState = stateTest;

			Point locTest;
			if(Settings.TryParseStructSetting<Point>(hkey, "_desktoplocation", out locTest))
				DesktopLocation = locTest;
			
			Size sizeTest;
			if(Settings.TryParseStructSetting<Size>(hkey, "_size", out sizeTest))
				Size = sizeTest;
			
			int colourTest;
			if(Settings.TryParseSetting(hkey, "_colour", out colourTest))
				colourToolStripMenuItem.BackColor = Color.FromArgb(colourTest);
			
			SetTitle();//Text = instrument.name;
		}
		void SaveSettings()
		{
			if(instrument == null || string.IsNullOrEmpty(instrument.name))
				return;
			string hkey = instrument.SettingsFile();
			
			Settings.WriteSetting(hkey, "_state", WindowState.ToString());
			Settings.WriteStructSetting(hkey, "_desktoplocation", DesktopLocation);
			Settings.WriteStructSetting(hkey, "_size", Size);
		}
		protected override void OnFormClosing(FormClosingEventArgs e)
		{
			if(Text.EndsWith("*"))
			{
				DialogResult dr = MessageBox.Show(instrument.name + " has unsaved changes. Save?", "Sounding", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);

				if(dr == System.Windows.Forms.DialogResult.Cancel)
				{
					e.Cancel = true;
				}
				else if(dr == System.Windows.Forms.DialogResult.Yes)
				{
					Save();
				}
			}

			SaveSettings();
			
			windows.Remove(this);
			base.OnFormClosing(e);
		}
		public static Color GetColour(Instrument instrument)
		{
			if(instrument == null)
				return Color.Gray;
			string hkey = instrument.SettingsFile();
			int colourTest;
			if(Settings.TryParseSetting(hkey, "_colour", out colourTest))
				return Color.FromArgb(colourTest);
			return Color.Magenta;
		}
		private void InstrumentEditor_Load(object sender, EventArgs e)
		{
			LoadSettings();
		}

		private void spinBox1_ValueChanged(object sender, SpinBox.ValueChangedArgs e)
		{
			Changed();
			instrument.volume = (float)e.Value;
		}

		private void colourToolStripMenuItem_Click(object sender, EventArgs e)
		{
			ColorDialog dia = new ColorDialog();
			dia.AllowFullOpen = true;
			dia.FullOpen = true;
			dia.Color = GetColour(instrument);
			DialogResult res = dia.ShowDialog();
			if(res == System.Windows.Forms.DialogResult.OK)
			{
				colourToolStripMenuItem.BackColor = dia.Color;

				if(instrument == null || string.IsNullOrEmpty(instrument.name))
					return;
				string hkey = instrument.SettingsFile();
				Settings.WriteSetting(hkey, "_colour", dia.Color.ToArgb());
			}
		}

		private void filterEditor1_Changed(object sender, EventArgs e)
		{
			instrument.filter = filterEditor1.Filter;
			Changed();
		}

		private void oscillatorsVisualizer_Paint(object sender, PaintEventArgs e)
		{
			base.OnPaint(e);

			System.Drawing.Size cSize = oscillatorsVisualizer.ClientSize;

			float borderSize = .1f;
			Rectangle editRect = new Rectangle((int)(cSize.Width * borderSize), 0, cSize.Width - (int)(cSize.Width * borderSize * 2.0f), cSize.Height);


			float[] values = new float[editRect.Width % DeviceSettings.Channels == 0 ? editRect.Width : ((editRect.Width / DeviceSettings.Channels) * DeviceSettings.Channels)];

			instrument.GetSamples(new ActiveNote(67, 1.0f), ref values, 1.0f / instrument.volume);

			PointF[] points = new PointF[cSize.Width];

			for(int iii = 0; iii < points.Length; iii++)
			{
				float percent = ((float)iii - (float)editRect.X) / (float)editRect.Width;
				int vindex = (values.Length + (int)(percent * values.Length)) % values.Length;

				points[iii].X = ((float)iii / (float)points.Length) * cSize.Width;
				points[iii].Y = (values[vindex] + 1.0f) * .5f * (cSize.Height - 4) + 2;//YToCanvas(values[vindex]);
				if(float.IsNaN(points[iii].Y))
					points[iii].Y = .0f;

			}

			using(Pen pen = new Pen(oscillatorsVisualizer.ForeColor))
			{
				e.Graphics.DrawLines(pen, points);

				pen.Color = Color.FromArgb(128, Color.Gray);
				
				e.Graphics.DrawLine(pen, editRect.X, 0, editRect.X, cSize.Height);
				e.Graphics.DrawLine(pen, editRect.Right, 0, editRect.Right, cSize.Height);

				int half = cSize.Height / 2;
				e.Graphics.DrawLine(pen, 0, half, cSize.Width, half);
			}
		}

		private void InstrumentEditor_Deactivate(object sender, EventArgs e)
		{
			Keyboard.ClearNotes();
		}

	}
}
