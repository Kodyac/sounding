﻿namespace Sounding
{
	partial class OscillatorEditor
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.formLabel = new System.Windows.Forms.Label();
			this.tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
			this.envelopeEditor = new Sounding.EnvelopeEditor();
			this.waveEditor = new Sounding.WaveEditor();
			this.addEnvelopeButton = new System.Windows.Forms.Button();
			this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
			this.label1 = new System.Windows.Forms.Label();
			this.formCombo = new System.Windows.Forms.ComboBox();
			this.delete = new System.Windows.Forms.Button();
			this.typeCombo = new System.Windows.Forms.ComboBox();
			this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
			this.phaseSpin = new Sounding.SpinBox();
			this.frequencySpin = new Sounding.SpinBox();
			this.frequencyOffsetSpin = new Sounding.SpinBox();
			this.volumeSpin = new Sounding.SpinBox();
			this.tableLayoutPanel.SuspendLayout();
			this.tableLayoutPanel1.SuspendLayout();
			this.tableLayoutPanel2.SuspendLayout();
			this.SuspendLayout();
			// 
			// formLabel
			// 
			this.formLabel.Anchor = System.Windows.Forms.AnchorStyles.Left;
			this.formLabel.AutoSize = true;
			this.formLabel.Location = new System.Drawing.Point(3, 8);
			this.formLabel.Name = "formLabel";
			this.formLabel.Size = new System.Drawing.Size(30, 13);
			this.formLabel.TabIndex = 0;
			this.formLabel.Text = "Form";
			this.formLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// tableLayoutPanel
			// 
			this.tableLayoutPanel.AutoSize = true;
			this.tableLayoutPanel.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
			this.tableLayoutPanel.ColumnCount = 2;
			this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
			this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
			this.tableLayoutPanel.Controls.Add(this.envelopeEditor, 0, 3);
			this.tableLayoutPanel.Controls.Add(this.waveEditor, 0, 1);
			this.tableLayoutPanel.Controls.Add(this.addEnvelopeButton, 1, 2);
			this.tableLayoutPanel.Controls.Add(this.tableLayoutPanel1, 0, 0);
			this.tableLayoutPanel.Controls.Add(this.volumeSpin, 0, 2);
			this.tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tableLayoutPanel.Location = new System.Drawing.Point(0, 0);
			this.tableLayoutPanel.Margin = new System.Windows.Forms.Padding(0);
			this.tableLayoutPanel.Name = "tableLayoutPanel";
			this.tableLayoutPanel.RowCount = 4;
			this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.tableLayoutPanel.Size = new System.Drawing.Size(334, 264);
			this.tableLayoutPanel.TabIndex = 1;
			// 
			// envelopeEditor
			// 
			this.envelopeEditor.AutoSize = true;
			this.envelopeEditor.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
			this.envelopeEditor.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.tableLayoutPanel.SetColumnSpan(this.envelopeEditor, 2);
			this.envelopeEditor.Dock = System.Windows.Forms.DockStyle.Fill;
			this.envelopeEditor.Location = new System.Drawing.Point(3, 115);
			this.envelopeEditor.MinimumSize = new System.Drawing.Size(256, 2);
			this.envelopeEditor.Name = "envelopeEditor";
			this.envelopeEditor.Size = new System.Drawing.Size(335, 146);
			this.envelopeEditor.TabIndex = 2;
			this.envelopeEditor.DeleteMe += new System.EventHandler<System.EventArgs>(this.envelopeEditor_DeleteMe);
			this.envelopeEditor.Changed += new System.EventHandler<System.EventArgs>(this.envelopeEditor_Changed);
			// 
			// waveEditor
			// 
			this.waveEditor.AutoSize = true;
			this.waveEditor.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
			this.waveEditor.BackColor = System.Drawing.Color.Black;
			this.tableLayoutPanel.SetColumnSpan(this.waveEditor, 2);
			this.waveEditor.Dock = System.Windows.Forms.DockStyle.Fill;
			this.waveEditor.ForeColor = System.Drawing.Color.Pink;
			this.waveEditor.Location = new System.Drawing.Point(0, 51);
			this.waveEditor.Margin = new System.Windows.Forms.Padding(0);
			this.waveEditor.MinimumSize = new System.Drawing.Size(256, 32);
			this.waveEditor.Name = "waveEditor";
			this.waveEditor.Size = new System.Drawing.Size(341, 32);
			this.waveEditor.TabIndex = 8;
			// 
			// addEnvelopeButton
			// 
			this.addEnvelopeButton.Dock = System.Windows.Forms.DockStyle.Fill;
			this.addEnvelopeButton.Location = new System.Drawing.Point(87, 86);
			this.addEnvelopeButton.Name = "addEnvelopeButton";
			this.addEnvelopeButton.Size = new System.Drawing.Size(251, 23);
			this.addEnvelopeButton.TabIndex = 10;
			this.addEnvelopeButton.Text = "Add Envelope";
			this.addEnvelopeButton.UseVisualStyleBackColor = true;
			this.addEnvelopeButton.Click += new System.EventHandler(this.addEnvelopeButton_Click);
			// 
			// tableLayoutPanel1
			// 
			this.tableLayoutPanel1.AutoSize = true;
			this.tableLayoutPanel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
			this.tableLayoutPanel1.ColumnCount = 5;
			this.tableLayoutPanel.SetColumnSpan(this.tableLayoutPanel1, 2);
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
			this.tableLayoutPanel1.Controls.Add(this.label1, 2, 0);
			this.tableLayoutPanel1.Controls.Add(this.formLabel, 0, 0);
			this.tableLayoutPanel1.Controls.Add(this.formCombo, 1, 0);
			this.tableLayoutPanel1.Controls.Add(this.delete, 4, 0);
			this.tableLayoutPanel1.Controls.Add(this.typeCombo, 3, 0);
			this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 1);
			this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
			this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(0);
			this.tableLayoutPanel1.Name = "tableLayoutPanel1";
			this.tableLayoutPanel1.RowCount = 2;
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.tableLayoutPanel1.Size = new System.Drawing.Size(341, 51);
			this.tableLayoutPanel1.TabIndex = 9;
			// 
			// label1
			// 
			this.label1.Anchor = System.Windows.Forms.AnchorStyles.Left;
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(166, 8);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(31, 13);
			this.label1.TabIndex = 12;
			this.label1.Text = "Type";
			// 
			// formCombo
			// 
			this.formCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.formCombo.FormattingEnabled = true;
			this.formCombo.Location = new System.Drawing.Point(39, 3);
			this.formCombo.Name = "formCombo";
			this.formCombo.Size = new System.Drawing.Size(121, 21);
			this.formCombo.TabIndex = 0;
			this.formCombo.SelectedIndexChanged += new System.EventHandler(this.oscillatorChanged);
			// 
			// delete
			// 
			this.delete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.delete.Location = new System.Drawing.Point(290, 3);
			this.delete.Name = "delete";
			this.delete.Size = new System.Drawing.Size(48, 23);
			this.delete.TabIndex = 5;
			this.delete.Text = "Delete";
			this.delete.UseVisualStyleBackColor = true;
			this.delete.Click += new System.EventHandler(this.delete_Click);
			// 
			// typeCombo
			// 
			this.typeCombo.Dock = System.Windows.Forms.DockStyle.Fill;
			this.typeCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.typeCombo.FormattingEnabled = true;
			this.typeCombo.Location = new System.Drawing.Point(203, 3);
			this.typeCombo.Name = "typeCombo";
			this.typeCombo.Size = new System.Drawing.Size(81, 21);
			this.typeCombo.TabIndex = 10;
			this.typeCombo.SelectedIndexChanged += new System.EventHandler(this.typeCombo_SelectedIndexChanged);
			// 
			// tableLayoutPanel2
			// 
			this.tableLayoutPanel2.AutoSize = true;
			this.tableLayoutPanel2.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
			this.tableLayoutPanel2.ColumnCount = 3;
			this.tableLayoutPanel1.SetColumnSpan(this.tableLayoutPanel2, 5);
			this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
			this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
			this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
			this.tableLayoutPanel2.Controls.Add(this.phaseSpin, 2, 0);
			this.tableLayoutPanel2.Controls.Add(this.frequencySpin, 0, 0);
			this.tableLayoutPanel2.Controls.Add(this.frequencyOffsetSpin, 1, 0);
			this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tableLayoutPanel2.GrowStyle = System.Windows.Forms.TableLayoutPanelGrowStyle.AddColumns;
			this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 29);
			this.tableLayoutPanel2.Margin = new System.Windows.Forms.Padding(0);
			this.tableLayoutPanel2.Name = "tableLayoutPanel2";
			this.tableLayoutPanel2.RowCount = 1;
			this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.tableLayoutPanel2.Size = new System.Drawing.Size(341, 22);
			this.tableLayoutPanel2.TabIndex = 11;
			// 
			// phaseSpin
			// 
			this.phaseSpin.AutoSize = true;
			this.phaseSpin.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.phaseSpin.DecimalPlaces = 2;
			this.phaseSpin.Disabled = false;
			this.phaseSpin.Float = 0F;
			this.phaseSpin.Increment = new decimal(new int[] {
            5,
            0,
            0,
            131072});
			this.phaseSpin.Label = "Phase";
			this.phaseSpin.Location = new System.Drawing.Point(219, 3);
			this.phaseSpin.MaxValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
			this.phaseSpin.MinimumSize = new System.Drawing.Size(67, 16);
			this.phaseSpin.MinValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
			this.phaseSpin.Name = "phaseSpin";
			this.phaseSpin.Size = new System.Drawing.Size(67, 16);
			this.phaseSpin.State = Sounding.SpinBox.EnabledState.Enabled;
			this.phaseSpin.TabIndex = 8;
			this.phaseSpin.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
			this.phaseSpin.ValueChanged += new System.EventHandler<Sounding.SpinBox.ValueChangedArgs>(this.phaseSpin_ValueChanged);
			// 
			// frequencySpin
			// 
			this.frequencySpin.AutoSize = true;
			this.frequencySpin.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.frequencySpin.DecimalPlaces = 4;
			this.frequencySpin.Disabled = false;
			this.frequencySpin.Float = 0F;
			this.frequencySpin.Increment = new decimal(new int[] {
            1,
            0,
            0,
            0});
			this.frequencySpin.Label = "Frequency";
			this.frequencySpin.Location = new System.Drawing.Point(3, 3);
			this.frequencySpin.MaxValue = new decimal(new int[] {
            20000,
            0,
            0,
            0});
			this.frequencySpin.MinimumSize = new System.Drawing.Size(123, 16);
			this.frequencySpin.MinValue = new decimal(new int[] {
            20000,
            0,
            0,
            -2147483648});
			this.frequencySpin.Name = "frequencySpin";
			this.frequencySpin.Size = new System.Drawing.Size(123, 16);
			this.frequencySpin.State = Sounding.SpinBox.EnabledState.Enabled;
			this.frequencySpin.TabIndex = 9;
			this.frequencySpin.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
			this.frequencySpin.ValueChanged += new System.EventHandler<Sounding.SpinBox.ValueChangedArgs>(this.frequencySpin_ValueChanged);
			// 
			// frequencyOffsetSpin
			// 
			this.frequencyOffsetSpin.AutoSize = true;
			this.frequencyOffsetSpin.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.frequencyOffsetSpin.DecimalPlaces = 1;
			this.frequencyOffsetSpin.Disabled = false;
			this.frequencyOffsetSpin.Float = 0F;
			this.frequencyOffsetSpin.Increment = new decimal(new int[] {
            1,
            0,
            0,
            0});
			this.frequencyOffsetSpin.Label = "Semitone";
			this.frequencyOffsetSpin.Location = new System.Drawing.Point(132, 3);
			this.frequencyOffsetSpin.MaxValue = new decimal(new int[] {
            60,
            0,
            0,
            0});
			this.frequencyOffsetSpin.MinimumSize = new System.Drawing.Size(81, 16);
			this.frequencyOffsetSpin.MinValue = new decimal(new int[] {
            60,
            0,
            0,
            -2147483648});
			this.frequencyOffsetSpin.Name = "frequencyOffsetSpin";
			this.frequencyOffsetSpin.Size = new System.Drawing.Size(81, 16);
			this.frequencyOffsetSpin.State = Sounding.SpinBox.EnabledState.Enabled;
			this.frequencyOffsetSpin.TabIndex = 7;
			this.frequencyOffsetSpin.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
			this.frequencyOffsetSpin.ValueChanged += new System.EventHandler<Sounding.SpinBox.ValueChangedArgs>(this.oscillatorSpinsChanged);
			// 
			// volumeSpin
			// 
			this.volumeSpin.AutoSize = true;
			this.volumeSpin.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.volumeSpin.DecimalPlaces = 3;
			this.volumeSpin.Disabled = false;
			this.volumeSpin.Float = 0.3F;
			this.volumeSpin.Increment = new decimal(new int[] {
            5,
            0,
            0,
            131072});
			this.volumeSpin.Label = "Volume";
			this.volumeSpin.Location = new System.Drawing.Point(3, 86);
			this.volumeSpin.MaxValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
			this.volumeSpin.MinimumSize = new System.Drawing.Size(78, 16);
			this.volumeSpin.MinValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
			this.volumeSpin.Name = "volumeSpin";
			this.volumeSpin.Size = new System.Drawing.Size(78, 16);
			this.volumeSpin.State = Sounding.SpinBox.EnabledState.Enabled;
			this.volumeSpin.TabIndex = 6;
			this.volumeSpin.Value = new decimal(new int[] {
            3,
            0,
            0,
            65536});
			this.volumeSpin.ValueChanged += new System.EventHandler<Sounding.SpinBox.ValueChangedArgs>(this.oscillatorSpinsChanged);
			// 
			// OscillatorEditor
			// 
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
			this.AutoSize = true;
			this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
			this.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.Controls.Add(this.tableLayoutPanel);
			this.MaximumSize = new System.Drawing.Size(338, 0);
			this.MinimumSize = new System.Drawing.Size(338, 0);
			this.Name = "OscillatorEditor";
			this.Size = new System.Drawing.Size(334, 264);
			this.tableLayoutPanel.ResumeLayout(false);
			this.tableLayoutPanel.PerformLayout();
			this.tableLayoutPanel1.ResumeLayout(false);
			this.tableLayoutPanel1.PerformLayout();
			this.tableLayoutPanel2.ResumeLayout(false);
			this.tableLayoutPanel2.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label formLabel;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel;
		private System.Windows.Forms.ComboBox formCombo;
		private System.Windows.Forms.Button delete;
		private SpinBox volumeSpin;
		private SpinBox frequencyOffsetSpin;
		private WaveEditor waveEditor;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
		private SpinBox phaseSpin;
		private EnvelopeEditor envelopeEditor;
		private System.Windows.Forms.Button addEnvelopeButton;
		private SpinBox frequencySpin;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.ComboBox typeCombo;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;

	}
}
