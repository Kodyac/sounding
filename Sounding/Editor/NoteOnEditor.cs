﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Sounding.Editor
{
	public partial class NoteOnEditor : UserControl
	{
		[Browsable(false)]
		public MidiEditor parent = null;

		Song Song { get { if(parent == null) return null; return parent.song; } }
		
		//[Browsable(false)]
		GuiExtensions.SelectionList<Event> selectedEvents
		{
			get
			{
				if(parent == null)
					return null;
				return parent.SelectedEvents;
			}
		}

		/*public NoteOnEvent NoteOn
		{
			get { return noteOn; }
			set { noteOn = value; UpdateUI(); }
		}*/
		public NoteOnEditor()
		{
			InitializeComponent();
		}
		public NoteOnEditor(MidiEditor parent)
		{
			InitializeComponent();
			
			this.parent = parent;
			UpdateUI();
		}

		void UpdateUI(int selIndex = 0)
		{
			UpdateEventList(selIndex);

			UpdateVolumeSpin();
		}
		void UpdateEventList(int selIndex)
		{
			if(selectedEvents.Count == 1)
			{
				NoteOnEvent noteOn = selectedEvents[0] as NoteOnEvent;
				if(noteOn == null)
					return;

				List<NoteKeyEvent> eves = noteOn.SubEvents();

				eventList.Items.Clear();
				for(int iii = 0; iii < eves.Count; iii++)
				{
					int time = eves[iii].absoluteTime - eves[0].absoluteTime;
				
					string t = "Tick: " + (eves[iii].absoluteTime - noteOn.absoluteTime).ToString("D4") + "	 Volume: " + eves[iii].velocity.ToString("F2");
					eventList.Items.Add(t);
					
				}
				eventList.SelectedIndex = selIndex;
			}
		}
		void UpdateVolumeSpin()
		{
			if(selectedEvents.Count > 0)
			{
				int vol = 0;
				for(int iii = 0; iii < selectedEvents.Count; iii++)
					vol += selectedEvents[iii].msb;
				volumeSpin.SetValueWithoutEvent((decimal)(vol / selectedEvents.Count));
			}
		}

		private void eventList_SelectedIndexChanged(object sender, EventArgs e)
		{
			if(selectedEvents.Count != 1 || eventList.SelectedIndex < 0)
				return;
			NoteOnEvent noteOn = selectedEvents[0] as NoteOnEvent;
			if(noteOn == null)
				return;

			List<NoteKeyEvent> eves = noteOn.SubEvents();
			NoteEvent ne = eves[eventList.SelectedIndex];
			volumeSpin.SetValueWithoutEvent((decimal)ne.msb);
			
			timeSpin.MinValue = 0;
			timeSpin.MaxValue = noteOn.TickDuration;
			timeSpin.SetValueWithoutEvent((decimal)(ne.absoluteTime - noteOn.absoluteTime));

			if(eventList.SelectedIndex == eves.Count - 1)
			{
				volumeSpin.Enabled = false;
				removeButton.Enabled = false;
				volumeSpin.SetValueWithoutEvent(.0f);
			}
			else if(eventList.SelectedIndex == 0)
			{
				volumeSpin.Enabled = true;
				removeButton.Enabled = false;
			}
			else
			{
				volumeSpin.Enabled = true;
				removeButton.Enabled = true;
			}
		}

		private void volumeSpin_ValueChanged(object sender, SpinBox.ValueChangedArgs e)
		{
			if(selectedEvents.Count != 1)
			{
				for(int iii = 0; iii < selectedEvents.Count; iii++)
				{
					if(selectedEvents[iii].Code == MidiEventsCodes.NoteOn)
						selectedEvents[iii].msb = (byte)(e.Value);
				}
				parent.UpdateTitle(true);
				return;
			}
			NoteOnEvent noteOn = selectedEvents[0] as NoteOnEvent;
			if(noteOn == null)
				return;

			List<NoteKeyEvent> eves = noteOn.SubEvents();
			int a = eventList.SelectedIndex;
			if(a == eventList.Items.Count - 1)
			{
				eves[a].msb = 0;
				volumeSpin.SetValueWithoutEvent(0m);
				return;
			}

			eves[a].msb = (byte)e.Value;
			if(eves[a].msb < 1)
				eves[a].msb = 1;

			volumeSpin.SetValueWithoutEvent((decimal)eves[a].msb);
			UpdateListItem();
			parent.UpdateTitle(true);
		}

		private void addButton_Click(object sender, EventArgs e)
		{
			if(selectedEvents.Count != 1)
				return;
			NoteOnEvent noteOn = selectedEvents[0] as NoteOnEvent;
			if(noteOn == null)
				return;

			int a = eventList.SelectedIndex;
			if(a == eventList.Items.Count - 1)
				a--;
			int b = a + 1;
			List<NoteKeyEvent> eves = noteOn.SubEvents();
			int time = eves[b].absoluteTime - eves[a].absoluteTime;
			
			if(time < 10)	//too few ticks to bother with a volume change. And it'll bug out the timechange 
				return;

			time = time / 2;
			time += eves[a].absoluteTime;

			NoteKeyEvent keyVent = new NoteKeyEvent(time);
			keyVent.Channel = noteOn.Channel;
			keyVent.key = noteOn.key;
			keyVent.msb = (byte)((eves[a].msb + eves[b].msb) / 2);
			if(keyVent.msb < 1)
				keyVent.msb = 1;

			noteOn.volumeChanges.Add(keyVent);
			Song.tracks[parent.TrackIndex].events.Add(keyVent);

			noteOn.volumeChanges.Sort(new KeyEventSorter());
			Song.tracks[parent.TrackIndex].SortTrack();

			UpdateUI(a + 1);
			parent.UpdateTitle(true);
		}

		private void removeButton_Click(object sender, EventArgs e)
		{
			if(selectedEvents.Count != 1)
				return;
			NoteOnEvent noteOn = selectedEvents[0] as NoteOnEvent;
			if(noteOn == null)
				return;

			int a = eventList.SelectedIndex;
			if(a == eventList.Items.Count - 1 || a == 0)
				return;

			Song.tracks[parent.TrackIndex].events.Remove(noteOn.SubEvents()[a]);
			noteOn.volumeChanges.Remove(noteOn.SubEvents()[a]);
			UpdateUI();
			parent.UpdateTitle(true);
		}

		private void timeSpin_ValueChanged(object sender, SpinBox.ValueChangedArgs e)
		{
			if(selectedEvents.Count != 1)
				return;
			NoteOnEvent noteOn = selectedEvents[0] as NoteOnEvent;
			if(noteOn == null)
				return;

			int a = eventList.SelectedIndex;
			if(a == eventList.Items.Count - 1 || a == 0)
				return;

			List<NoteKeyEvent> eves = noteOn.SubEvents();
			eves[a].absoluteTime = (int)e.Value + noteOn.absoluteTime;
			if(eves[a].absoluteTime < eves[a - 1].absoluteTime + 2)
				eves[a].absoluteTime = eves[a - 1].absoluteTime + 2;
			else if(eves[a].absoluteTime > eves[a + 1].absoluteTime - 2)
				eves[a].absoluteTime = eves[a + 1].absoluteTime - 2;
			
			timeSpin.SetValueWithoutEvent((decimal)(eves[a].absoluteTime - noteOn.absoluteTime));

			noteOn.volumeChanges.Sort(new KeyEventSorter());
			Song.tracks[parent.TrackIndex].SortTrack();

			/*string t = "Tick: " + (eves[a].absoluteTime - noteOn.absoluteTime).ToString("D4") + "	 Volume: " + eves[a].velocity.ToString("F2");
			
			int selIndex = eventList.SelectedIndex;
			eventList.Items[eventList.SelectedIndex] = t;
			eventList.SelectedIndex = selIndex;*/
			UpdateListItem();
		}
		void UpdateListItem()
		{
			if(selectedEvents.Count != 1)
				return;
			NoteOnEvent noteOn = selectedEvents[0] as NoteOnEvent;
			if(noteOn == null)
				return;

			int a = eventList.SelectedIndex;
			if(a >= eventList.Items.Count - 1 || a < 0)
				return;

			List<NoteKeyEvent> eves = noteOn.SubEvents();

			string t = "Tick: " + (eves[a].absoluteTime - noteOn.absoluteTime).ToString("D4") + "	 Volume: " + eves[a].velocity.ToString("F2");
			
			int selIndex = eventList.SelectedIndex;
			eventList.Items[eventList.SelectedIndex] = t;
			eventList.SelectedIndex = selIndex;
		}

		private void volumeChanger_Click(object sender, EventArgs e)
		{
			Prompt.FloatPrompt fp = new Prompt.FloatPrompt("Percent", "Multiply Volume of All Selected Notes", "Multiply Volume");
			DialogResult res = fp.ShowDialog(this);
			
			if(res != DialogResult.OK)
				return;

			float percent = fp.Float;

			if(selectedEvents.Count != 1)
			{
				for(int iii = 0; iii < selectedEvents.Count; iii++)
				{
					NoteOnEvent neve = selectedEvents[iii] as NoteOnEvent;
					if(neve != null && neve.Code == MidiEventsCodes.NoteOn)
					{
						List<NoteKeyEvent> subs = neve.SubEvents();
						for(int jjj = 0; jjj < subs.Count - 1; jjj++) //-1 cuz last event should be noteOff and have msb of 0
						{
							int msb = (int)((float)subs[jjj].msb * percent);
							if(msb < 1)
								msb = 1;
							else if(msb > 127)
								msb = 127;
							subs[jjj].msb = (byte)(msb);
						}
					}
				}
			}

			UpdateUI();
			parent.UpdateTitle(true);
		}

		
	}
}
