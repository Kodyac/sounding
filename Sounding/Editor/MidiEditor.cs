﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using System.IO;

using System.Diagnostics;

namespace Sounding.Editor
{
	public partial class MidiEditor : Form
	{
		
		public Song song = null;
		string fileName = "";

		int playTickStart;
		int playTickEnd;
		float currentBeat;

		public bool Scrolling { get { return (playButton.Checked && autoScrollButton.Checked) || recordButton.Checked; } }
		
		[Browsable(false)]
		public GuiExtensions.SelectionList<Event> SelectedEvents;

		MidiVisualCanvas Canvas { get { return visualEditorControl.Canvas; } }

		public static List< KeyValuePair<string, float> > noteValues = new List<KeyValuePair<string,float>>() 
		{ 
			new KeyValuePair<string, float>(	"Whole",	1.0f		),
			new KeyValuePair<string, float>(	"1/2",		1.0f/2.0f	),
			new KeyValuePair<string, float>(	"1/4",		1.0f/4.0f	),
			new KeyValuePair<string, float>(	"1/8",		1.0f/8.0f	),
			new KeyValuePair<string, float>(	"1/16",		1.0f/16.0f	),
			new KeyValuePair<string, float>(	"1/32",		1.0f/32.0f	),
			new KeyValuePair<string, float>(	"1/64",		1.0f/64.0f	),
			new KeyValuePair<string, float>(	"1/128",	1.0f/128.0f	),
			new KeyValuePair<string, float>(	"1/3",		1.0f/3.0f	),
			new KeyValuePair<string, float>(	"1/6",		1.0f/6.0f	),
			new KeyValuePair<string, float>(	"1/12",		1.0f/12.0f	),
			new KeyValuePair<string, float>(	"1/24",		1.0f/24.0f	),
			new KeyValuePair<string, float>(	"1/48",		1.0f/48.0f	),
			new KeyValuePair<string, float>(	"1/96",		1.0f/96.0f	),
			new KeyValuePair<string, float>(	"1/192",	1.0f/192.0f	),
			new KeyValuePair<string, float>(	"Previous",	1.0f		)
		};
		
		public int TrackIndex 
		{ 
			get 
			{
				if(trackView.FocusedItem == null)
				{
					if(song.tracks.Count > 1 && song.trackType == Song.TrackTypes.Synchronous)
						trackIndex = 1;
					trackIndex = 0;
				}
				else
					trackIndex = trackView.FocusedItem.Index;
				return trackIndex;
			}
			set
			{
				for(int iii = 0; iii < trackView.Items.Count; iii++)
				{
					trackView.Items[iii].Focused = false;
					trackView.Items[iii].Selected = false;
				}

				if(value < 0)
					trackIndex = 0;
				else if(value > trackView.Items.Count)
					trackIndex = trackView.Items.Count - 1;
				else
					trackIndex = value;
				
				trackView.Items[trackIndex].Focused = true;
				trackView.Items[trackIndex].Selected = true;
			}
		}
		public int channelIndex
		{
			get
			{
				if(channelView.FocusedItem == null)
					return 0;
				return channelView.FocusedItem.Index;
			}
		}
		int trackIndex = 0;
		public Track SelectedTrack	{ get { return song.tracks[trackIndex]; } }
		List<Event> TrackEvents { get { return SelectedTrack.events; } }
		Channel SelectedChannel { get { return SelectedTrack.channels[channelIndex]; } }

		public MidiEditor(string loadFile = null)
		{
			SelectedEvents = new GuiExtensions.SelectionList<Event>(UpdatedSelection);
			InitializeComponent();
			Canvas.parent = this;

			eventEditor.parent = this;

			chordCombo.FillFromEnum(typeof(Chord.Types));

			SoundWave.AddRead(Playing);

			Canvas.selectedEvents = SelectedEvents;
			eventEditor.selectedEvents = SelectedEvents;

			noteLengthCombo.Items.Clear();
			for(int iii = 0; iii < noteValues.Count; iii++)
				noteLengthCombo.Items.Add("Note length " + noteValues[iii].Key);
			noteLengthCombo.SelectedIndex = 2;
			
			for(int iii = 0; iii < noteValues.Count - 1; iii++)
				snapCombo.Items.Add("Snap to " + noteValues[iii].Key);
			snapCombo.Items.Add("∞");
			snapCombo.SelectedIndex = 2;

			KeyPreview = true;

			if(string.IsNullOrEmpty(loadFile) || !File.Exists(loadFile))
				NewMidi();
			else
				LoadMidi(loadFile);
		}
		Instrument GetInstrument()
		{
			if(channelIndex == 9)
			{
				return Orchestra.Percussion[128];
			}
			NoteEvent progChange = song.tracks[TrackIndex].GetNoteEventAtTime(0, MidiEventsCodes.ProgramChange, channelIndex);
			if(progChange == null)
				progChange = song.tracks[TrackIndex].GetNoteEventAtTime(0, MidiEventsCodes.ProgramChange);

			if(progChange == null)
				return Keyboard.instrument;

			return Orchestra.Programs[progChange.lsb];
		}
		void UpdatedSelection()
		{
			eventEditor.SetupUI();

			int eventCount = song.tracks[TrackIndex].EventsInChannel(channelIndex);
			ListViewItem t = channelView.Items[channelIndex];
			if(t.SubItems.Count > 1)
				t.SubItems[1].Text = eventCount.ToString();
			
			if(SelectedEvents.Count < 1)
				return;

			if(noteLengthCombo.SelectedIndex == noteLengthCombo.Items.Count - 1)
			{
				NoteOnEvent non = SelectedEvents[0] as NoteOnEvent;
				if(non == null)
					return;

				int index = noteLengthCombo.Items.Count - 1;
				noteValues[index] = new KeyValuePair<string,float>(
					(song.TicksToBeat(non.TickDuration) / 4.0f).ToString("F4"),
					song.TicksToBeat(non.TickDuration) / 4.0f );

				noteLengthCombo.Items[index] = noteValues[index].Key;
				Canvas.defaultNoteSize = noteValues[index].Value;
			}
		}
		void NewMidi()
		{
			trackView.FocusedItem = null;

			song = new Song();
			trackIndex = 0;

			SetTrackTypeChecks();

			song.name = "Untitled";
			fileName = "";

			/*Track t = new Track(song);

			song.tracks.Add(t);*/
			song.tracks.Add(new Track(song, Song.TrackTypes.Asynchronous));

			Canvas.Song = song;
			//visualEditor.TrackEditIndex = 0;

			playTickStart = 0;
			playTickEnd = song.BeatToTicks(4.0f);

			UndoStack.NewStack(song);
			UpdateTitle(true);
			UpdateUI();
		}
		void LoadMidi(string fileName)
		{
			trackView.FocusedItem = null;
			trackIndex = 0;

			Cursor.Current = Cursors.WaitCursor;
			song = MidiFile.LoadMidi(fileName);

			SetTrackTypeChecks();

			this.fileName = fileName;
			FileInfo info = new FileInfo(fileName);
			song.name = info.Name;
			Canvas.Song = song;

			playRangeButton_Click(this, EventArgs.Empty);

			Canvas.SetPlayRangeStart(playTickStart);
			Canvas.SetPlayRangeEnd(playTickEnd);
			
			UndoStack.NewStack(song);

			UpdateTitle(false);
			UpdateUI();

			LoadSongSettings();
			Cursor.Current = Cursors.Default;
		}
		private void openToolStripMenuItem_Click(object sender, EventArgs e)
		{
			OpenFileDialog dia = new OpenFileDialog();

			string readLD = Settings.ReadSetting("LastDir");
			if(!string.IsNullOrEmpty(readLD))
				dia.InitialDirectory = readLD;

			dia.Filter = "mid files (*.mid)|*.mid";
			dia.RestoreDirectory = true;

			if(dia.ShowDialog() == DialogResult.OK)
				Settings.WriteSetting("LastDir", Path.GetDirectoryName(dia.FileName));
			else
				return;

			LoadMidi(dia.FileName);
		}
		
		public void UpdateTitle(bool changed = false)
		{
			if(!changed)
				Text = song.name;
			else
			{
				Text = song.name + " *";
				UpdateTime();
			}
		}
		public void Playing(float[] buffer, int offset, int sampleCount, int channels)
		{
			if(song == null)
				return;
			
			if(playButton.Checked)
			{
				if(song.CurrentTick >= playTickEnd || song.CurrentTick < playTickStart)
				{
					song.GotoTick(playTickStart);
				}

				currentBeat = song.CurrentBeat;

				float[] data = new float[sampleCount];
			
				song.GetSamples(ref data);
			
				for(int iii = 0; iii < sampleCount; iii++)
				{
					buffer[iii + offset] += data[iii];
				}
			}
			else if(recordButton.Checked)
			{
				float deltaTime = (((float)sampleCount / DeviceSettings.Channels) / (float)DeviceSettings.SampleRate);
				currentBeat += deltaTime * song.BeatsPerSecond;
			}

			Canvas.CurrentBeat = currentBeat;
		}

		void UpdateUI()
		{
			UpdateTime();
			UpdateTrackView();
						
			visualEditorControl.Invalidate();
		}
		void UpdateTime()
		{
			double seconds;
			if(song.trackType == Song.TrackTypes.Synchronous)
				seconds = song.SecondsLength;
			else
				seconds = SelectedTrack.SecondsLength;

			string time = "";
			int minutes = (int)(seconds / 60.0f);
			double leftover = seconds % 60.0;
			time += minutes.ToString("F0") + ":";
			if(leftover < 10.0f)
				time += "0";
			time += leftover.ToString("F2");
			durationLabel.Text = time;
		}

		public void UpdateTrackViewTitles()
		{
			for(int iii = 0; iii < trackView.Items.Count; iii++)
			{
				string name = song.tracks[iii].NameEvent.text;
				if(name == null)
					name = "Track " + (iii).ToString("D2");
				trackView.Items[iii].Text = name;
			}
		}
		void UpdateTrackView()
		{
			trackView.Items.Clear();

			for(int iii = 0; iii < song.tracks.Count; iii++)
			{
				string name = song.tracks[iii].NameEvent.text;
				if(name == null)
					name = "Track " + (iii).ToString("D2");
				ListViewItem item = new ListViewItem(new string[] { name } );

				if(song.trackType == Song.TrackTypes.Synchronous)
					item.Checked = true;
				else
					item.Checked = false;
				item.Selected = false;
				item.Focused = false;
				trackView.Items.Add(item);
			}
			
			if(trackView.Items.Count > 1 && song.trackType == Song.TrackTypes.Synchronous)
			{
				trackView.Items[1].Focused = true;
				trackView.Items[1].Selected = true;
			}
			else
			{
				trackView.Items[0].Focused = true;
				trackView.Items[0].Selected = true;
			}
			
			UpdateChannelView();

			channelView.Items[channelIndex].Selected = true;
		}
		
		void UpdateChannelView()
		{
			if(TrackIndex >= song.tracks.Count || TrackIndex < 0)
				return;

			channelView.Items.Clear();
			for(int iii = 0; iii < song.tracks[TrackIndex].channels.Length; iii++)
			{
				int eventCount = song.tracks[TrackIndex].EventsInChannel(iii);
				ListViewItem item = new ListViewItem(new string[] {(iii + 1).ToString("D2"), eventCount.ToString() } );
				
				item.Checked = true;

				channelView.Items.Add(item);
			}


			//channelView_SelectedIndexChanged(this, EventArgs.Empty);
		}
		void OrchestraChanged(object sender, EventArgs e)
		{
			//UpdateInstrumentCombo();
		}

		protected override void OnKeyDown(KeyEventArgs e)
		{
			if(ActiveControl != visualEditorControl)
			{
				debugLabel.Text = ActiveControl.ToString();
				return;
			}

			if(!e.Control)
				Keyboard.OnKeyDown(e.KeyCode);

			if(recordButton.Checked)
			{
				Canvas.KeyDownRecord(e.KeyCode);
				return;
			}

			if(e.Control)
			{
				if(e.KeyCode == Keys.Z)
				{
					int tindex = TrackIndex;
					SelectedEvents.Clear();
					song = UndoStack.Pop();

					Canvas.EvaluateSize();
					TrackIndex = tindex;
					UpdateTitle(true);
				}

				if(e.KeyCode == Keys.C)
				{
					Canvas.CopyEventsToBuffer();
				}
				if(e.KeyCode == Keys.V)
				{
					Canvas.PasteBufferEvents();
					UpdateTitle(true);
				}

				if(e.KeyCode == Keys.A)
				{
					SelectedEvents.Clear();
					List<Event> selection = new List<Event>();
					for(int iii = 0; iii < TrackEvents.Count; iii++)
					{
						NoteOnEvent non = TrackEvents[iii] as NoteOnEvent;
						if(non == null || non.Channel != channelIndex)
							continue;
						
						selection.Add(non);
					}
					SelectedEvents.Add(selection);
				}
				
			}
			else
			{
				if(e.KeyCode == Keys.OemOpenBrackets)
				{
					playTickStart = Canvas.GetPlayRangeStartFromMouse();
				}
				if(e.KeyCode == Keys.OemCloseBrackets)
				{
					playTickEnd = Canvas.GetPlayRangeEndFromMouse();
				}
				if(e.KeyCode == Keys.Delete && SelectedEvents.Count > 0)
				{
					UndoStack.Push(song);
					for(int iii = 0; iii < SelectedEvents.Count; iii++)
					{
						if(SelectedEvents[iii] is NoteOnEvent)
						{
							List<NoteKeyEvent> temp = ((NoteOnEvent)SelectedEvents[iii]).SubEvents();
							for(int jjj = 0; jjj < temp.Count; jjj++)
								song.tracks[TrackIndex].events.Remove(temp[jjj]);
						}
						else
							song.tracks[TrackIndex].events.Remove(SelectedEvents[iii]);
					}
					SelectedTrack.SortTrack();

					SelectedEvents.Clear();
					UpdateTitle(true);
				}

			}
			e.Handled = true;

			base.OnKeyDown(e);
		}
		protected override void OnKeyUp(KeyEventArgs e)
		{
			Keyboard.OnKeyUp(e.KeyCode);

			if(recordButton.Checked)
			{
				Canvas.KeyUpRecord(e.KeyCode);
				return;
			}

			base.OnKeyUp(e);
		}
		protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
		{
			Keys k = keyData & (~Keys.Modifiers);
			if(SelectedEvents.Count > 0)
			{
				if(k == Keys.Up || k == Keys.Down)
				{
					UndoStack.Push(song);
					for(int iii = 0; iii < SelectedEvents.Count; iii++)
					{
						NoteOnEvent selOn = SelectedEvents[iii] as NoteOnEvent;
						if(selOn == null)
							continue;
					
						int val = 1;
						if(k == Keys.Down)
							val = -val;

						if(Control.ModifierKeys == Keys.Shift)
							selOn.key += val * 12;
						else
							selOn.key += val;
					}
					SelectedTrack.SortTrack();

					UpdateTitle(true);

					return true;
				}

				if(k == Keys.Left || k == Keys.Right)
				{
					TimeSignature ts = new TimeSignature(4,4);
					if(SelectedEvents.Count > 0)
					{
						TimeSignatureEvent tse = SelectedTrack.GetMetaEventAtTime(SelectedEvents[0].absoluteTime, MetaEventCodes.TimeSignature) as TimeSignatureEvent;
						if(tse != null)
							ts = tse.time;
					}

					UndoStack.Push(song);
					for(int iii = 0; iii < SelectedEvents.Count; iii++)
					{
						NoteOnEvent selOn = SelectedEvents[iii] as NoteOnEvent;
						if(selOn == null)
							continue;
					
						int snap = song.BeatToTicks((MidiEditor.noteValues[snapCombo.SelectedIndex].Value) * 4.0f);
						if(Control.ModifierKeys == Keys.Shift)
							snap = song.BeatToTicks(((float)ts.counts / (float)ts.value) * 4.0f);

						if(k == Keys.Left)
							snap = -snap;

						selOn.SetTime(selOn.absoluteTime + snap);
					}
					SelectedTrack.SortTrack();

					UpdateTitle(true);

					return true;
				}
			}
			if(k == Keys.F1)
				noteLengthCombo.SelectedIndex = 0;
			else if(k == Keys.F2)
				noteLengthCombo.SelectedIndex = 1;
			else if(k == Keys.F3)
				noteLengthCombo.SelectedIndex = 2;
			else if(k == Keys.F4)
				noteLengthCombo.SelectedIndex = 3;
			else if(k == Keys.F5)
				noteLengthCombo.SelectedIndex = 4;
			else if(k == Keys.F6)
				noteLengthCombo.SelectedIndex = 5;
			else if(k == Keys.F7)
				noteLengthCombo.SelectedIndex = 6;
			else if(k == Keys.F8)
				noteLengthCombo.SelectedIndex = 7;
			else if(k == Keys.F9)
				noteLengthCombo.SelectedIndex = 8;
			else if(k == Keys.F10)
				noteLengthCombo.SelectedIndex = 9;
			else if(k == Keys.F11)
				noteLengthCombo.SelectedIndex = 10;
			else if(k == Keys.F12)
				noteLengthCombo.SelectedIndex = 11;
			else if(k == Keys.Space)
			{
				playButton.PerformClick();
				return true;
			}

			return base.ProcessCmdKey(ref msg, keyData);
		}

		private void orchestraToolStripMenuItem_Click(object sender, EventArgs e)
		{
			OrchestraEditor.Open();
			OrchestraEditor.AddChangedEvent(OrchestraChanged);
		}

		void LoadSettings()
		{
			if(DesignMode)
				return;
			int testOctave;
			if(Settings.TryParseSetting("keyOctave", out testOctave))
				keyOctaveSpin.Value = Keyboard.octave = testOctave;
			
			float testVolume;
			if(Settings.TryParseSetting("keyVolume", out testVolume))
				keyVolumeSpin.Value = (decimal)(Keyboard.volume = testVolume);

			int testDuration;
			if(Settings.TryParseSetting("noteDuration", out testDuration))
				noteLengthCombo.SelectedIndex = testDuration;

			int testSnap;
			if(Settings.TryParseSetting("noteSnap", out testSnap))
				snapCombo.SelectedIndex = testSnap;

			FormWindowState stateTest;
			if(Settings.TryParseEnumSetting("_state", out stateTest))
				WindowState = stateTest;

			Point locTest;
			if(Settings.TryParseStructSetting("_desktoplocation", out locTest))
				DesktopLocation = locTest;
			
			Size sizeTest;
			if(Settings.TryParseStructSetting("_size", out sizeTest))
				Size = sizeTest;

			int testSampleRate;
			if(Settings.TryParseSetting("sampleRate", out testSampleRate))
			{
				DeviceSettings.SampleRate = testSampleRate;
				SoundWave.SetSampleRate(DeviceSettings.SampleRate);
			}
		}
		void SaveSettings()
		{
			if(DesignMode)
				return;
			Settings.WriteSetting("keyOctave", Keyboard.octave.ToString());
			Settings.WriteSetting("keyVolume", Keyboard.volume.ToString());
			Settings.WriteSetting("noteDuration", noteLengthCombo.SelectedIndex.ToString());
			Settings.WriteSetting("noteSnap", snapCombo.SelectedIndex.ToString());

			Settings.WriteSetting("_state", WindowState.ToString());
			Settings.WriteStructSetting("_desktoplocation", DesktopLocation);
			Settings.WriteStructSetting("_size", Size);

			Settings.WriteSetting("sampleRate", DeviceSettings.SampleRate.ToString());
		}
		void LoadInstruments()
		{
			string setting = Settings.ReadSetting("OpenInstruments");
			string[] loads = setting.Split(new char[] {'|'}, StringSplitOptions.RemoveEmptyEntries);

			for(int iii = 0; iii < loads.Length; iii++)
			{
				FileInfo fi = new FileInfo(Sounding.IO.InstrumentFileHandler.FileName(loads[iii]));
				if(!fi.Exists)
					continue;
				InstrumentEditor window = new InstrumentEditor(loads[iii]);
				window.Show();
			}
		}
		void SaveInstruments()
		{
			string setting = "";
			for(int iii = 0; iii < InstrumentEditor.windows.Count; iii++)
			{
				setting += InstrumentEditor.windows[iii].instrument.name;
				setting += "|";
			}
			
			Settings.WriteSetting("OpenInstruments", setting);
		}
		private void keyOctaveSpin_ValueChanged(object sender, SpinBox.ValueChangedArgs e)
		{
			if(DesignMode)
				return;
			Keyboard.octave = (int)e.Value;
		}

		private void keyVolumeSpin_ValueChanged(object sender, SpinBox.ValueChangedArgs e)
		{
			if(DesignMode)
				return;
			Keyboard.volume = (float)e.Value;
		}

		protected override void OnFormClosing(FormClosingEventArgs e)
		{
			if(Text.EndsWith("*"))
			{
				DialogResult dr = MessageBox.Show(song.name + " has unsaved changes. Save?", "Sounding", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);

				if(dr == System.Windows.Forms.DialogResult.Cancel)
					e.Cancel = true;
				else if(dr == System.Windows.Forms.DialogResult.Yes)
					Save();
			}

			SaveSettings();
			SaveInstruments();
			base.OnFormClosing(e);
			if(!e.Cancel)
			{
				SoundWave.endDelegate = null;
				SoundWave.Instance.Dispose();
			}
		}

		private void playButton_Click(object sender, EventArgs e)
		{
			//song.Reset();
			song.Clear();
		}

		private void MidiEditor_Load(object sender, EventArgs e)
		{
			LoadSettings();
			LoadInstruments();
		}

		private void channelView_ItemChecked(object sender, ItemCheckedEventArgs e)
		{
			Canvas.channelViewIndexes.Clear();
			
			if(song == null)
				return;

			foreach(int index in channelView.CheckedIndices)
				if(!Canvas.channelViewIndexes.Contains(index))
					Canvas.channelViewIndexes.Add(index);

			Song.channelPlayIndexes = Canvas.channelViewIndexes;
		}

		private void channelView_SelectedIndexChanged(object sender, EventArgs e)
		{
			Canvas.channelEditIndex = (byte)channelIndex;//channelView.SelectedIndices[0];
			Keyboard.instrument = GetInstrument();
			SelectedEvents.Clear();
			//SetSelectedInstrument();
		}

		private void trackView_SelectedIndexChanged(object sender, EventArgs e)
		{
			//visualEditor.TrackEditIndex = trackIndex;
			song.currentTrack = TrackIndex;
			
			UpdateChannelView();

			SelectedEvents.Clear();
			SelectedEvents.Add(SelectedTrack.NameEvent);

			UpdateTime();
		}

		private void trackView_ItemChecked(object sender, ItemCheckedEventArgs e)
		{
			Canvas.trackViewIndexes.Clear();

			if(song == null)
				return;

			foreach(int index in trackView.CheckedIndices)
				if(!Canvas.trackViewIndexes.Contains(index))
					Canvas.trackViewIndexes.Add(index);

			Song.trackPlayIndexes = Canvas.trackViewIndexes;
		}

		private void MidiEditor_Activated(object sender, EventArgs e)
		{
			Keyboard.instrument = GetInstrument();
		}
		
		private void noteLengthCombo_SelectedIndexChanged(object sender, EventArgs e)
		{
			Canvas.defaultNoteSize = noteValues[noteLengthCombo.SelectedIndex].Value;
		}

		private void snapCombo_SelectedIndexChanged(object sender, EventArgs e)
		{
			Canvas.snapIndex = snapCombo.SelectedIndex;
		}

		private void selectAllChannels_Click(object sender, EventArgs e)
		{
			foreach(ListViewItem item in channelView.Items)
				item.Checked = true;
		}
		private void deCheckAllChannels_Click(object sender, EventArgs e)
		{
			foreach(ListViewItem item in channelView.Items)
				if(!item.Focused)
					item.Checked = false;
		}

		private void selectAllTracks_Click(object sender, EventArgs e)
		{
			foreach(ListViewItem item in trackView.Items)
				item.Checked = true;
		}
		private void deCheckAllTracks_Click(object sender, EventArgs e)
		{
			foreach(ListViewItem item in trackView.Items)
				if(!item.Focused)
					item.Checked = false;
		}

		private void saveToolStripMenuItem_Click(object sender, EventArgs e)
		{
			if(!File.Exists(fileName))
			{
				saveAsToolStripMenuItem_Click(sender, e);
				return;
			}

			Save();
		}

		private void saveAsToolStripMenuItem_Click(object sender, EventArgs e)
		{
			SaveFileDialog dia = new SaveFileDialog();

			string readLD = Settings.ReadSetting("LastDir");
			if(!string.IsNullOrEmpty(readLD))
				dia.InitialDirectory = readLD;

			dia.Filter = "mid files (*.mid)|*.mid";
			dia.RestoreDirectory = true;

			if(dia.ShowDialog() == DialogResult.OK)
				Settings.WriteSetting("LastDir", Path.GetDirectoryName(dia.FileName));
			else
				return;

			fileName = dia.FileName;
			Save();
		}

		void Save()
		{
			Cursor.Current = Cursors.WaitCursor;
			SelectedEvents.Clear();
			MidiFile.SaveMidi(fileName, song);
			FileInfo info = new FileInfo(fileName);
			song.name = info.Name;

			SaveSongSettings();

			UpdateTitle();
			Cursor.Current = Cursors.Default;
		}
		void SaveSongSettings()
		{
			string settingsName = song.name;
			settingsName = settingsName.Replace("/", ";");
			settingsName = settingsName.Replace("\\", ";");
			settingsName += ".ini";

			//Settings.WriteSetting(settingsName, "playTickStart", playTickStart);
			//Settings.WriteSetting(settingsName, "playTickEnd", playTickEnd);

			Settings.WriteSetting(settingsName, "chordType", Chord.current);
			Settings.WriteSetting(settingsName, "rootKey", NoteLabels.Root.Interval);
			Settings.WriteSetting(settingsName, "scale", Sounding.Scale.current);
		}
		void LoadSongSettings()
		{
			string settingsName = song.name;
			settingsName = settingsName.Replace("/", ";");
			settingsName = settingsName.Replace("\\", ";");
			settingsName += ".ini";

			int rootInterval;
			if(Settings.TryParseSetting(settingsName, "rootKey", out rootInterval))
				NoteLabels.Root = rootInterval;

			Chord.Types chord;
			if(Settings.TryParseEnumSetting(settingsName, "chordType", out chord))
			{
				Chord.current = chord;
				chordCombo.SelectedIndex = (int)chord;
			}

			Sounding.Scale.Types scale;
			if(Settings.TryParseEnumSetting(settingsName, "scale", out scale))
			{
				switch (scale)
				{
					case Sounding.Scale.Types.Major:
						visualEditorControl.NoteLabels.majorToolStripMenuItem_Click(this, EventArgs.Empty);
						break;
					case Sounding.Scale.Types.NaturalMinor:
						visualEditorControl.NoteLabels.naturalMinorToolStripMenuItem_Click(this, EventArgs.Empty);
						break;
					case Sounding.Scale.Types.HarmonicMinor:
						visualEditorControl.NoteLabels.harmonicMinorToolStripMenuItem_Click(this, EventArgs.Empty);
						break;
					case Sounding.Scale.Types.MelodicMinor:
						visualEditorControl.NoteLabels.melodicMinorToolStripMenuItem_Click(this, EventArgs.Empty);
						break;
					default:
						break;
				}
			}
		}
		private void MidiEditor_Deactivate(object sender, EventArgs e)
		{
			Keyboard.ClearNotes();
		}

		private void recordButton_Click(object sender, EventArgs e)
		{
			playButton.Checked = false;
			currentBeat = .0f;
			Canvas.Recording = recordButton.Checked;
		}

		private void newToolStripMenuItem_Click(object sender, EventArgs e)
		{
			NewMidi();
		}

		private void visualEditorControl_SongChanged(object sender, EventArgs e)
		{
			UpdateTitle(true);
		}

		private void playRangeButton_Click(object sender, EventArgs e)
		{
			playTickStart = 0;
			playTickEnd = song.TickLength;
			if(song.trackType == Song.TrackTypes.Asynchronous)
				playTickEnd = SelectedTrack.TickLength;

			Canvas.SetPlayRangeStart(playTickStart);
			Canvas.SetPlayRangeEnd(playTickEnd);
		}

		private void TrackTypeChanged(object sender, EventArgs e)
		{
			ToolStripMenuItem item = sender as ToolStripMenuItem;
			if(item == null)
				return;
			
			UndoStack.Push(song);

			if(item == singleToolStripMenuItem)
				song.trackType = Song.TrackTypes.Single;
			else if(item == synchronousToolStripMenuItem)
				song.trackType = Song.TrackTypes.Synchronous;
			else if(item == asynchronousToolStripMenuItem)
				song.trackType = Song.TrackTypes.Asynchronous;

			SetTrackTypeChecks();

			if(song.trackType == Song.TrackTypes.Single)
				ConvertToSingle();

			UpdateTitle(true);
		}

		void ConvertToSingle()
		{
			for(int iii = song.tracks.Count - 1; iii > 0; iii--)
			{
				for(int jjj = 0; jjj < song.tracks[iii].events.Count; jjj++)
					song.tracks[0].events.Add(song.tracks[iii].events[jjj]);
				song.tracks.RemoveAt(iii);
			}

			song.tracks[0].SortTrack();

			UpdateTrackView();
			UpdateTitle(true);
		}
		void SetTrackTypeChecks()
		{
			switch (song.trackType)
			{
				case Song.TrackTypes.Single:
					singleToolStripMenuItem.Checked = true;
					synchronousToolStripMenuItem.Checked = false;
					asynchronousToolStripMenuItem.Checked = false;
					break;
				case Song.TrackTypes.Synchronous:
					singleToolStripMenuItem.Checked = false;
					synchronousToolStripMenuItem.Checked = true;
					asynchronousToolStripMenuItem.Checked = false;
					break;
				case Song.TrackTypes.Asynchronous:
					singleToolStripMenuItem.Checked = false;
					synchronousToolStripMenuItem.Checked = false;
					asynchronousToolStripMenuItem.Checked = true;
					break;
			}
		}

		private void addNewTrackToolStripMenuItem_Click(object sender, EventArgs e)
		{
			if(song.trackType == Song.TrackTypes.Single)
				song.trackType = Song.TrackTypes.Asynchronous;
			song.tracks.Add(new Track(song, song.trackType));
			UpdateTrackView();
			UpdateTitle(true);
		}

		private void deleteTrackToolStripMenuItem_Click(object sender, EventArgs e)
		{
			song.tracks.RemoveAt(TrackIndex);
			UpdateTrackView();
			UpdateTitle(true);
		}
		
		private void saveWavToolStripMenuItem_Click(object sender, EventArgs e)
		{
			SaveFileDialog dia = new SaveFileDialog();

			string readLD = Settings.ReadSetting("LastDir");
			if(!string.IsNullOrEmpty(readLD))
				dia.InitialDirectory = readLD;

			dia.Filter = "wav files (*.wav)|*.wav";
			dia.RestoreDirectory = true;

			if(dia.ShowDialog() == DialogResult.OK)
				Settings.WriteSetting("LastDir", Path.GetDirectoryName(dia.FileName));
			else
				return;

			fileName = dia.FileName;
			UseWaitCursor = true;
			Application.DoEvents();
			
			long ms = 0;

			if(song.trackType == Song.TrackTypes.Asynchronous)
				ms = WavBuilder.WriteWav(fileName, SelectedTrack, DeviceSettings.SampleRate);
			else
				ms = WavBuilder.WriteWav(fileName, song, DeviceSettings.SampleRate);
			
			buildTimeLabel.Text = (((float)ms) * .001f).ToString();

			UseWaitCursor = false;
			//Cursor.Current = Cursor.Current;
			Cursor.Position = Cursor.Position;	//wierdo hack to get the wait cursor to actually turn off.
			Application.DoEvents();
		}

		private void chordCombo_SelectedIndexChanged(object sender, EventArgs e)
		{
			Chord.current = (Chord.Types)chordCombo.SelectedIndex;
		}

		private void profileButton_Click(object sender, EventArgs e)
		{
			Cursor c = Cursor;
			Cursor = Cursors.WaitCursor;

			long ms;
			const int times = 5;

			if(song.trackType == Song.TrackTypes.Asynchronous)
				ms = WavBuilder.Profile(SelectedTrack, times);
			else
				ms = WavBuilder.Profile(song, times);

			Cursor = c;

			buildTimeLabel.Text = (((float)ms) * .001f).ToString();
		}

		private void toolSpinBox1_ValueChanged(object sender, SpinBox.ValueChangedArgs e)
		{
			DeviceSettings.SampleRate = (int)e.Value;
			SoundWave.SetSampleRate(DeviceSettings.SampleRate);
		}

		private void tempoSpinBox_ValueChanged(object sender, SpinBox.ValueChangedArgs e)
		{
			DeviceSettings.tempoMultiplier = e.Float;
		}

		private void a4Spin_ValueChanged(object sender, SpinBox.ValueChangedArgs e)
		{
			Key.A4 = e.Float;
		}
	}
}
