﻿namespace Sounding.Editor
{
	partial class OrchestraEditor
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.mainLayout = new System.Windows.Forms.TableLayoutPanel();
			this.instrumentList = new System.Windows.Forms.ListView();
			this.editLayout = new System.Windows.Forms.TableLayoutPanel();
			this.instrumentLabel = new System.Windows.Forms.Label();
			this.orchestraName = new System.Windows.Forms.TextBox();
			this.programSpin = new Sounding.SpinBox();
			this.loadOrchestra = new System.Windows.Forms.Button();
			this.percussionSpin = new Sounding.SpinBox();
			this.instrumentWatcher = new System.IO.FileSystemWatcher();
			this.mainLayout.SuspendLayout();
			this.editLayout.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.instrumentWatcher)).BeginInit();
			this.SuspendLayout();
			// 
			// mainLayout
			// 
			this.mainLayout.ColumnCount = 2;
			this.mainLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.mainLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
			this.mainLayout.Controls.Add(this.instrumentList, 0, 0);
			this.mainLayout.Controls.Add(this.editLayout, 1, 0);
			this.mainLayout.Dock = System.Windows.Forms.DockStyle.Fill;
			this.mainLayout.Location = new System.Drawing.Point(0, 0);
			this.mainLayout.Name = "mainLayout";
			this.mainLayout.RowCount = 1;
			this.mainLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.mainLayout.Size = new System.Drawing.Size(624, 281);
			this.mainLayout.TabIndex = 0;
			// 
			// instrumentList
			// 
			this.instrumentList.Dock = System.Windows.Forms.DockStyle.Fill;
			this.instrumentList.Location = new System.Drawing.Point(3, 3);
			this.instrumentList.Name = "instrumentList";
			this.instrumentList.Size = new System.Drawing.Size(411, 275);
			this.instrumentList.TabIndex = 3;
			this.instrumentList.UseCompatibleStateImageBehavior = false;
			this.instrumentList.View = System.Windows.Forms.View.Details;
			this.instrumentList.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this.instrumentList_ColumnClick);
			this.instrumentList.ItemActivate += new System.EventHandler(this.instrumentList_ItemActivate);
			this.instrumentList.SelectedIndexChanged += new System.EventHandler(this.instrumentList_SelectedIndexChanged);
			// 
			// editLayout
			// 
			this.editLayout.AutoSize = true;
			this.editLayout.ColumnCount = 2;
			this.editLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
			this.editLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
			this.editLayout.Controls.Add(this.instrumentLabel, 0, 1);
			this.editLayout.Controls.Add(this.orchestraName, 0, 0);
			this.editLayout.Controls.Add(this.programSpin, 0, 2);
			this.editLayout.Controls.Add(this.loadOrchestra, 1, 0);
			this.editLayout.Controls.Add(this.percussionSpin, 1, 2);
			this.editLayout.Dock = System.Windows.Forms.DockStyle.Fill;
			this.editLayout.Location = new System.Drawing.Point(420, 3);
			this.editLayout.Name = "editLayout";
			this.editLayout.RowCount = 3;
			this.editLayout.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.editLayout.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.editLayout.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.editLayout.Size = new System.Drawing.Size(201, 275);
			this.editLayout.TabIndex = 4;
			// 
			// instrumentLabel
			// 
			this.instrumentLabel.AutoSize = true;
			this.instrumentLabel.Location = new System.Drawing.Point(3, 29);
			this.instrumentLabel.Name = "instrumentLabel";
			this.instrumentLabel.Size = new System.Drawing.Size(55, 13);
			this.instrumentLabel.TabIndex = 0;
			this.instrumentLabel.Text = "instrument";
			// 
			// orchestraName
			// 
			this.orchestraName.Location = new System.Drawing.Point(3, 3);
			this.orchestraName.Name = "orchestraName";
			this.orchestraName.Size = new System.Drawing.Size(100, 20);
			this.orchestraName.TabIndex = 2;
			this.orchestraName.TextChanged += new System.EventHandler(this.orchestraName_TextChanged);
			// 
			// programSpin
			// 
			this.programSpin.AutoSize = true;
			this.programSpin.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.programSpin.DecimalPlaces = 0;
			this.programSpin.Disabled = false;
			this.programSpin.Float = 1F;
			this.programSpin.Increment = new decimal(new int[] {
            0,
            0,
            0,
            0});
			this.programSpin.Label = "Program";
			this.programSpin.Location = new System.Drawing.Point(3, 45);
			this.programSpin.MaxValue = new decimal(new int[] {
            128,
            0,
            0,
            0});
			this.programSpin.MinimumSize = new System.Drawing.Size(76, 16);
			this.programSpin.MinValue = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
			this.programSpin.Name = "programSpin";
			this.programSpin.Size = new System.Drawing.Size(76, 16);
			this.programSpin.State = Sounding.SpinBox.EnabledState.Enabled;
			this.programSpin.TabIndex = 1;
			this.programSpin.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
			this.programSpin.ValueChanged += new System.EventHandler<Sounding.SpinBox.ValueChangedArgs>(this.programSpin_ValueChanged);
			// 
			// loadOrchestra
			// 
			this.loadOrchestra.Location = new System.Drawing.Point(109, 3);
			this.loadOrchestra.Name = "loadOrchestra";
			this.loadOrchestra.Size = new System.Drawing.Size(75, 23);
			this.loadOrchestra.TabIndex = 3;
			this.loadOrchestra.Text = "Load";
			this.loadOrchestra.UseVisualStyleBackColor = true;
			this.loadOrchestra.Click += new System.EventHandler(this.loadOrchestra_Click);
			// 
			// percussionSpin
			// 
			this.percussionSpin.AutoSize = true;
			this.percussionSpin.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.percussionSpin.DecimalPlaces = 0;
			this.percussionSpin.Disabled = false;
			this.percussionSpin.Float = 0F;
			this.percussionSpin.Increment = new decimal(new int[] {
            0,
            0,
            0,
            0});
			this.percussionSpin.Label = "Percussion";
			this.percussionSpin.Location = new System.Drawing.Point(109, 45);
			this.percussionSpin.MaxValue = new decimal(new int[] {
            128,
            0,
            0,
            0});
			this.percussionSpin.MinimumSize = new System.Drawing.Size(89, 16);
			this.percussionSpin.MinValue = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
			this.percussionSpin.Name = "percussionSpin";
			this.percussionSpin.Size = new System.Drawing.Size(89, 16);
			this.percussionSpin.State = Sounding.SpinBox.EnabledState.Enabled;
			this.percussionSpin.TabIndex = 4;
			this.percussionSpin.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
			this.percussionSpin.ValueChanged += new System.EventHandler<Sounding.SpinBox.ValueChangedArgs>(this.percussionSpin_ValueChanged);
			// 
			// instrumentWatcher
			// 
			this.instrumentWatcher.EnableRaisingEvents = true;
			this.instrumentWatcher.SynchronizingObject = this;
			this.instrumentWatcher.Changed += new System.IO.FileSystemEventHandler(this.instrumentWatcherEvent);
			this.instrumentWatcher.Created += new System.IO.FileSystemEventHandler(this.instrumentWatcherEvent);
			this.instrumentWatcher.Deleted += new System.IO.FileSystemEventHandler(this.instrumentWatcherEvent);
			this.instrumentWatcher.Renamed += new System.IO.RenamedEventHandler(this.instrumentWatcherEvent);
			// 
			// OrchestraEditor
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(624, 281);
			this.Controls.Add(this.mainLayout);
			this.MinimumSize = new System.Drawing.Size(640, 320);
			this.Name = "OrchestraEditor";
			this.Text = "OrchestraEditor";
			this.Activated += new System.EventHandler(this.OrchestraEditor_Activated);
			this.mainLayout.ResumeLayout(false);
			this.mainLayout.PerformLayout();
			this.editLayout.ResumeLayout(false);
			this.editLayout.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.instrumentWatcher)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.TableLayoutPanel mainLayout;
		private System.Windows.Forms.ListView instrumentList;
		private System.Windows.Forms.TableLayoutPanel editLayout;
		private System.Windows.Forms.Label instrumentLabel;
		private SpinBox programSpin;
		private System.Windows.Forms.TextBox orchestraName;
		private System.IO.FileSystemWatcher instrumentWatcher;
		private System.Windows.Forms.Button loadOrchestra;
		private SpinBox percussionSpin;
	}
}