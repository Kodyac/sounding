﻿namespace Sounding.Editor
{
	partial class NoteOnEditor
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.mainLayout = new System.Windows.Forms.TableLayoutPanel();
			this.eventList = new System.Windows.Forms.ListBox();
			this.volumeSpin = new Sounding.SpinBox();
			this.removeButton = new System.Windows.Forms.Button();
			this.addButton = new System.Windows.Forms.Button();
			this.timeSpin = new Sounding.SpinBox();
			this.volumeChanger = new System.Windows.Forms.Button();
			this.mainLayout.SuspendLayout();
			this.SuspendLayout();
			// 
			// mainLayout
			// 
			this.mainLayout.AutoSize = true;
			this.mainLayout.ColumnCount = 3;
			this.mainLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.mainLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
			this.mainLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
			this.mainLayout.Controls.Add(this.eventList, 0, 0);
			this.mainLayout.Controls.Add(this.volumeSpin, 1, 0);
			this.mainLayout.Controls.Add(this.removeButton, 1, 3);
			this.mainLayout.Controls.Add(this.addButton, 1, 2);
			this.mainLayout.Controls.Add(this.timeSpin, 1, 1);
			this.mainLayout.Controls.Add(this.volumeChanger, 2, 0);
			this.mainLayout.Dock = System.Windows.Forms.DockStyle.Fill;
			this.mainLayout.Location = new System.Drawing.Point(0, 0);
			this.mainLayout.Margin = new System.Windows.Forms.Padding(0);
			this.mainLayout.Name = "mainLayout";
			this.mainLayout.RowCount = 4;
			this.mainLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
			this.mainLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
			this.mainLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
			this.mainLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
			this.mainLayout.Size = new System.Drawing.Size(150, 80);
			this.mainLayout.TabIndex = 0;
			// 
			// eventList
			// 
			this.eventList.Dock = System.Windows.Forms.DockStyle.Fill;
			this.eventList.FormattingEnabled = true;
			this.eventList.Location = new System.Drawing.Point(1, 1);
			this.eventList.Margin = new System.Windows.Forms.Padding(1);
			this.eventList.Name = "eventList";
			this.mainLayout.SetRowSpan(this.eventList, 4);
			this.eventList.Size = new System.Drawing.Size(1, 78);
			this.eventList.TabIndex = 0;
			this.eventList.SelectedIndexChanged += new System.EventHandler(this.eventList_SelectedIndexChanged);
			// 
			// volumeSpin
			// 
			this.volumeSpin.AutoSize = true;
			this.volumeSpin.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.volumeSpin.DecimalPlaces = 0;
			this.volumeSpin.Disabled = false;
			this.volumeSpin.Dock = System.Windows.Forms.DockStyle.Fill;
			this.volumeSpin.Float = 127F;
			this.volumeSpin.Increment = new decimal(new int[] {
            1,
            0,
            0,
            0});
			this.volumeSpin.Label = "Volume";
			this.volumeSpin.Location = new System.Drawing.Point(-3, 3);
			this.volumeSpin.MaxValue = new decimal(new int[] {
            127,
            0,
            0,
            0});
			this.volumeSpin.MinimumSize = new System.Drawing.Size(69, 16);
			this.volumeSpin.MinValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
			this.volumeSpin.Name = "volumeSpin";
			this.volumeSpin.Size = new System.Drawing.Size(69, 16);
			this.volumeSpin.State = Sounding.SpinBox.EnabledState.Enabled;
			this.volumeSpin.TabIndex = 1;
			this.volumeSpin.Value = new decimal(new int[] {
            127,
            0,
            0,
            0});
			this.volumeSpin.ValueChanged += new System.EventHandler<Sounding.SpinBox.ValueChangedArgs>(this.volumeSpin_ValueChanged);
			// 
			// removeButton
			// 
			this.removeButton.Dock = System.Windows.Forms.DockStyle.Fill;
			this.removeButton.Enabled = false;
			this.removeButton.Location = new System.Drawing.Point(-3, 63);
			this.removeButton.Name = "removeButton";
			this.removeButton.Size = new System.Drawing.Size(69, 14);
			this.removeButton.TabIndex = 3;
			this.removeButton.Text = "Remove";
			this.removeButton.UseVisualStyleBackColor = true;
			this.removeButton.Click += new System.EventHandler(this.removeButton_Click);
			// 
			// addButton
			// 
			this.addButton.Dock = System.Windows.Forms.DockStyle.Fill;
			this.addButton.Location = new System.Drawing.Point(-3, 43);
			this.addButton.Name = "addButton";
			this.addButton.Size = new System.Drawing.Size(69, 14);
			this.addButton.TabIndex = 2;
			this.addButton.Text = "Add";
			this.addButton.UseVisualStyleBackColor = true;
			this.addButton.Click += new System.EventHandler(this.addButton_Click);
			// 
			// timeSpin
			// 
			this.timeSpin.AutoSize = true;
			this.timeSpin.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.timeSpin.DecimalPlaces = 0;
			this.timeSpin.Disabled = false;
			this.timeSpin.Dock = System.Windows.Forms.DockStyle.Fill;
			this.timeSpin.Float = 1F;
			this.timeSpin.Increment = new decimal(new int[] {
            1,
            0,
            0,
            0});
			this.timeSpin.Label = "Time";
			this.timeSpin.Location = new System.Drawing.Point(-3, 23);
			this.timeSpin.MaxValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
			this.timeSpin.MinimumSize = new System.Drawing.Size(45, 16);
			this.timeSpin.MinValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
			this.timeSpin.Name = "timeSpin";
			this.timeSpin.Size = new System.Drawing.Size(69, 16);
			this.timeSpin.State = Sounding.SpinBox.EnabledState.Enabled;
			this.timeSpin.TabIndex = 4;
			this.timeSpin.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
			this.timeSpin.ValueChanged += new System.EventHandler<Sounding.SpinBox.ValueChangedArgs>(this.timeSpin_ValueChanged);
			// 
			// volumeChanger
			// 
			this.volumeChanger.Dock = System.Windows.Forms.DockStyle.Fill;
			this.volumeChanger.Location = new System.Drawing.Point(72, 3);
			this.volumeChanger.Name = "volumeChanger";
			this.volumeChanger.Size = new System.Drawing.Size(75, 14);
			this.volumeChanger.TabIndex = 5;
			this.volumeChanger.Text = "Multiply Volume";
			this.volumeChanger.UseVisualStyleBackColor = true;
			this.volumeChanger.Click += new System.EventHandler(this.volumeChanger_Click);
			// 
			// NoteOnEditor
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.mainLayout);
			this.Margin = new System.Windows.Forms.Padding(0);
			this.Name = "NoteOnEditor";
			this.Size = new System.Drawing.Size(150, 80);
			this.mainLayout.ResumeLayout(false);
			this.mainLayout.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.TableLayoutPanel mainLayout;
		private System.Windows.Forms.ListBox eventList;
		private SpinBox volumeSpin;
		private System.Windows.Forms.Button addButton;
		private System.Windows.Forms.Button removeButton;
		private SpinBox timeSpin;
		private System.Windows.Forms.Button volumeChanger;
	}
}
