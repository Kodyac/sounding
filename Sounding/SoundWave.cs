﻿using System;
using NAudio.Wave;


namespace Sounding
{
	class SoundWave : WaveProvider32 , IDisposable
	{
		public static int sampleRate { get { return Instance.WaveFormat.SampleRate; } }
		static SoundWave instance;

		public static SoundWave Instance
		{
			get
			{
				if(instance == null)
					instance = new SoundWave();
				return instance;
			}
		}
		public static void AddRead(ReadDelegate read)
		{
			if(Instance.readDelegate == null)
				Instance.readDelegate = read;
			else
				Instance.readDelegate += read;
		}
		public static void RemoveRead(ReadDelegate read)
		{
			if(Instance.readDelegate == null)
				return;
			Instance.readDelegate -= read;
		}
		SoundWave() : base()
		{
			if(instance == null)
				instance = this;
			SetSampleRate(DeviceSettings.SampleRate);
		}
		public static void SetSampleRate(int rate)
		{
			if(Instance.waveOut != null)
			{
				Instance.waveOut.Stop();
				Instance.waveOut.Dispose();
				//Instance.waveOut = null;
			}

			DeviceSettings.SampleRate = rate;
			
			Instance.SetWaveFormat(DeviceSettings.SampleRate, DeviceSettings.Channels);
			
			NAudio.Wave.WaveOutEvent waveOut = new WaveOutEvent();
			waveOut.DesiredLatency = 50;
			//waveOut.DesiredLatency = (int)((1024.0 / (double)DeviceSettings.SampleRate) * 1000.0);
			waveOut.NumberOfBuffers = 10;

			waveOut.Init(Instance);

			waveOut.Play();

			Instance.waveOut = waveOut;
		}
		public static void Stop()
		{
			Instance.waveOut.Stop();
		}
		public static void Play()
		{
			Instance.waveOut.Play();
		}

		public delegate void ReadDelegate(float[] data, int offset, int sampleCount, int channels);
		ReadDelegate readDelegate;
		static public ReadDelegate endDelegate;

		public override int Read(float[] buffer, int offset, int sampleCount)
		{
			try
			{
				if(endDelegate != null)
					endDelegate(buffer, offset, sampleCount, this.waveOut.OutputWaveFormat.Channels);
			
				for(int iii = 0; iii < sampleCount; iii++)
				{
					buffer[iii + offset] = .0f;
				}
				if(readDelegate != null)
					readDelegate(buffer, offset, sampleCount, this.waveOut.OutputWaveFormat.Channels);
			}
			catch(NullReferenceException /*e*/) {}
			return sampleCount;
		}
		
		//NAudio.Wave.WaveOut waveOut;
		NAudio.Wave.WaveOutEvent waveOut = null;

		public void Dispose()
		{
			if(waveOut == null)
				return;
			waveOut.Stop();
			waveOut.Dispose();
			waveOut = null;
		}
		~SoundWave()
		{
			if(waveOut == null)
				return;
			waveOut.Stop();
			waveOut.Dispose();
			waveOut = null;
		}
	}
}
