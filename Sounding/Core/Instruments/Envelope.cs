﻿using System.IO;
using System.Collections.Generic;
using System;

namespace Sounding
{
	public class Envelope
	{
		

		//Dictionary<UInt16, float> previous = new Dictionary<UInt16,float>();
		float[] previous = new float[ActiveNote.MaxSimultaneous];
		float attackValue = 1.0f;
		float sustainValue = 1.0f;

		float attack = .0f;
		float decay = .0f;
		float release = .05f;
		public float Release
		{
			get { return release; }
			set { release = value; if(release < .0f) release = .0f; }
		}
		public float Attack
		{
			get { return attack; }
			set { attack = value; if(attack < .0f) attack = .0f; }
		}
		public float Decay
		{
			get { return decay; }
			set { decay = value; if(decay < .0f) decay = .0f; }
		}
		public float AttackValue
		{
			get { return attackValue; }
			set { attackValue = value; if(attackValue < .0f) attackValue = .0f; }
		}
		public float SustainValue
		{
			get { return sustainValue; }
			set { sustainValue = value; if(sustainValue < .0f) sustainValue = .0f; }
		}

		const float maxChange = .00005f;

		/*void BaseConstruct()
		{
			//identifier = IdentifierCounter++;
		}*/
		public Envelope(Envelope e)
		{
			//BaseConstruct();
			attackValue = e.attackValue;
			sustainValue = e.sustainValue;
			attack = e.attack;
			decay = e.decay;
			release = e.release;
		}
		public Envelope() { /*BaseConstruct(); */}
		public Envelope(float attack, float decay, float release, float attackValue, float sustainValue)
		{
			//BaseConstruct();
			this.attackValue = attackValue;
			this.sustainValue = sustainValue;
			this.attack = attack;
			this.decay = decay;
			this.release = release;
		}
			
		float releaseSamples { get { if(release < float.Epsilon) return 1; return DeviceSettings.SampleRate * release; } }

		public float GetMultiplier(ActiveNote n)
		{
			float temp;

			float value = previous[n.Identifier];

			if(!n.Alive)	//release
			{
				temp = (1.0f - (float)(n.sample - n.sampleDied) / (float)releaseSamples) * sustainValue;

				value = SMath.MoveTowards(value, temp, maxChange);

				if(value < float.Epsilon)
					value = .0f;
				else
					previous[n.Identifier] = value;
				
				return value;
			}

			float time = (float)n.sample * DeviceSettings.SamplePeriod;


			if(time < attack)
				temp = (time / attack) * attackValue;
			else if(time < (decay + attack))
				temp = SMath.Lerp(attackValue, sustainValue, (time - attack) / decay);
			else
				temp = sustainValue;

			if(n.sample == 0)
				value = temp;
			else
				value = SMath.MoveTowards(value, temp, maxChange);
			
			previous[n.Identifier] = value;
			return value;
		}
		
		public float GetValue(ActiveNote n)
		{
			return GetMultiplier(n) * 2.0f - 1.0f; 

		}

		public System.UInt32 SaveLength()
		{
			System.UInt32 size = 0;
			size += sizeof(float) * 5;	//attackValue, sustainValue, attack, decay, release

			return size;
		}
		public void Save(BinaryWriter writer)
		{
			writer.Write(SaveLength());
			writer.Write(attackValue);
			writer.Write(sustainValue);
			writer.Write(attack);
			writer.Write(decay);
			writer.Write(release);
		}
		static public Envelope Read(BinaryReader read)
		{
			Envelope env = new Envelope();
			env.attackValue		= read.ReadSingle();
			env.sustainValue	= read.ReadSingle();
			env.attack			= read.ReadSingle();
			env.decay			= read.ReadSingle();
			env.release			= read.ReadSingle();
			return env;
		}
	}
}
