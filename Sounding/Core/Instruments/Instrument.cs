﻿
using System.Collections;
using System.Collections.Generic;

#if UNITY
using UnityEngine;
#endif

namespace Sounding
{
	public class Instrument
	{
		string iname = "untitled";
		public string name { get { return iname; } set { iname = value.Replace(";", ""); } }

		static Dictionary<string, Instrument> cache = new Dictionary<string, Instrument>();

		public static Instrument GetInstrument(string name)
		{
			if(cache.ContainsKey(name))
				return cache[name];

			Instrument inst;
			IO.IOERR err = IO.InstrumentFileHandler.LoadFile(name, out inst);

			if(err != IO.IOERR.SUCCESS)
			{
				IO.FileHandling.IOLoadErrorMessage(name, err);
				return null;
			}
			cache.Add(name, inst);
			return inst;
		}

		protected float ivolume = .2f;

		public Filter filter = null;
		public Envelope envelope = new Envelope();

		public List<Oscillator> oscillators = new List<Oscillator> { new Oscillator(Oscillator.WaveForm.Sine) };

		public float volume
		{
			get { return ivolume; }
			set { ivolume = SMath.Clamp01(value); }
		}

		public Instrument() {}
		public Instrument(Instrument o)
		{
			volume = o.volume;
			envelope = new Envelope(o.envelope);
			oscillators.Clear();
			foreach(Oscillator osc in o.oscillators)
				oscillators.Add(new Oscillator(osc));

			if(o.filter != null)
				filter = new Filter(o.filter);
		}

		public float[] AmplitudeAtSample(ActiveNote n, out bool noteDead)
		{
			float[] ret = new float[DeviceSettings.Channels];
			
			float env = envelope.GetMultiplier(n);
			if(n.Alive || env > float.Epsilon)
				noteDead = false;
			else
			{
				noteDead = true;
				return ret;
			}
			
			if(DeviceSettings.Channels == 1)
				ret[0] = (volume * Equalizer.standard.GetGain(n.key.Frequency)) * n.velocity * env;
			else
				ret[0] = ret[1] = (volume * Equalizer.standard.GetGain(n.key.Frequency)) * n.velocity * env;
			
			return ret;
		}

		public bool GetSamples(ActiveNote an, ref float[] samples, float volumeMult = 1.0f, int sampleOffset = 0)
		{
			bool noteDead = false;

			float amplitude = .0f;
			float accumulator = .0f;

			float gain = Equalizer.standard.GetGain(an.key.Frequency);

			for(int iii = sampleOffset; iii < samples.Length; iii += DeviceSettings.Channels)
			{
				float env = envelope.GetMultiplier(an/*, env*/);
				if(an.Alive || env > float.Epsilon)
					noteDead = false;
				else
					noteDead = true;

				if(noteDead)
				{
					return noteDead;
				}
			
				amplitude = volume * gain * an.velocity * env;
				
				float frequency = an.key.Frequency;

				accumulator = .0f;
				lock(oscillators)
				{
					for(int ooo = 0; ooo < oscillators.Count; ooo++)
					{
						switch (oscillators[ooo].type)
						{
							case Oscillator.MixType.Mix:
								accumulator += oscillators[ooo].GetSample(an, frequency);
								break;
							case Oscillator.MixType.Phase:
								an.phase += oscillators[ooo].GetSample(an, frequency, false);
								break;
							case Oscillator.MixType.Frequency:
								frequency += oscillators[ooo].GetSample(an, an.key.Frequency, false);
								break;
							case Oscillator.MixType.Amplitude:
								accumulator *= (1.0f - oscillators[ooo].GetSample(an, frequency, false));
								break;
						}
							
					}
				}
				if(filter != null)
					accumulator = filter.GetFilteredValue(an.GetFilterData(1), accumulator);

				accumulator = accumulator * amplitude * volumeMult;

				samples[iii] += accumulator;

				for(int ccc = 1; ccc < DeviceSettings.Channels; ccc++)
					samples[iii + ccc] += accumulator;
			
				float phaseUp = 1.0f;

				an.phase += phaseUp;
				an.sample++;
			}

			//an.previousValue = env;

			return noteDead;
		}
	}
}
