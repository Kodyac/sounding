﻿using System.Collections.Generic;

namespace Sounding
{
	public class Oscillator
	{
		public enum MixType : int
		{
			Mix					= 0,
			Phase				= 1,
			Frequency			= 2,
			Amplitude			= 3
		}
		public enum WaveForm : int
		{
			Sine				= 0,
			Square				= 1,
			HarmonicSquare		= 2,
			Sawtooth			= 3,
			HarmonicSaw			= 4,
			Triangle			= 5,
			HarmonicTriangle	= 6,
			Flow				= 7,
			Roll				= 8,
			Pulse				= 9,
			//Custom				= 128,
			Noise				= 256
		}
		static Dictionary<WaveForm, float[]> waveTables = new Dictionary<WaveForm,float[]>();

		public float[] waveTable = null;

		public WaveForm Form
		{
			get { return form; }
			set
			{
				if(form == value)
					return;
				form = value;
				waveTable = waveTables[Form];
			}
		}
		WaveForm form = (WaveForm)(-1);
		
		public MixType type = 0;
		public float volume = 1.0f;
		public Envelope envelope;
		public float semitoneOffset = .0f;
		public float phaseOffset = .0f;
		public float frequency = .0f;

		public Oscillator(Oscillator other)
		{
			Form = other.Form;
			volume = other.volume;
			envelope = new Envelope(other.envelope);
			semitoneOffset = other.semitoneOffset;
		}
		public Oscillator()
		{
			Form = WaveForm.Sine;
		}

		public Oscillator(WaveForm form)
		{
			this.Form = form;
		}
	
		static Oscillator()
		{
			waveTables = BuildWaveTables(DeviceSettings.SampleRate);
		}
		public static void RebuildWaveTables(int resolution)
		{
			waveTables = BuildWaveTables(DeviceSettings.SampleRate);
		}
		static Dictionary<WaveForm, float[]> BuildWaveTables(int resolution)
		{
			Dictionary<WaveForm, float[]> ret = new Dictionary<WaveForm,float[]>();
			
			ret[WaveForm.Sine]				= BuildSine(resolution);
			ret[WaveForm.Square]			= BuildSquare(resolution);
			ret[WaveForm.HarmonicSquare]	= BuildHarmonic(resolution, 2, (a, b) => { return 1.0f / a; });
			ret[WaveForm.Sawtooth]			= BuildSawtooth(resolution);
			ret[WaveForm.HarmonicSaw]		= BuildHarmonic(resolution, 1, (a, b) => { return 1.0f / a; });
			ret[WaveForm.Triangle]			= BuildTriangle(resolution);
			ret[WaveForm.HarmonicTriangle]	= BuildHarmonic(resolution, 2, (a, b) => { return 1.0f / (a * a) * -System.Math.Sign(b); }, 3);
			ret[WaveForm.Flow]				= BuildFlow(resolution);
			ret[WaveForm.Roll]				= BuildHarmonic(resolution, 1, (a, b) => { return 1.0f / (a * a); });
			ret[WaveForm.Pulse]				= BuildPulse(resolution);
			ret[WaveForm.Noise]				= BuildNoise(resolution);

			return ret;
		}
		static void ClampWaveTable(ref float[] table)
		{
			float min = table[0];
			float max = table[0];
			for (int iii = 0; iii < table.Length; iii++)
			{
				if(table[iii] < min)
					min = table[iii];
				if(table[iii] > max)
					max = table[iii];
			}

			float range = max - min;
			if(range < float.Epsilon)
				return;

			float mult = 2.0f / range;
			for (int iii = 0; iii < table.Length; iii++)
				table[iii] *= mult;
		}
		static float[] BuildSine(int resolution)
		{
			float[] temp = new float[resolution];
			for (int iii = 0; iii < temp.Length; iii++)
				temp[iii] = (float)System.Math.Sin(((float)iii / temp.Length) * SMath.PI2);
			
			return temp;
		}
		static float[] BuildSquare(int resolution)
		{
			float[] temp = new float[resolution];
			for (int iii = 0; iii < temp.Length; iii++)
				temp[iii] = (float)System.Math.Sign(System.Math.Sin(((float)iii / temp.Length) * SMath.PI2));
			
			return temp;
		}
		static float[] BuildTriangle(int resolution)
		{
			float[] temp = new float[resolution];
			for (int iii = 0; iii < temp.Length; iii++)
			{
				int negOnePoint = temp.Length / 4;
				int onePoint = negOnePoint * 3;
				if(iii <= negOnePoint)
					temp[iii] = SMath.Lerp(.0f, 1.0f, (float)iii / (float)negOnePoint);
				else if(iii <= onePoint)
					temp[iii] = SMath.Lerp(1.0f, -1.0f, (float)(iii - negOnePoint) / (float)(onePoint - negOnePoint));
				else
					temp[iii] = SMath.Lerp(-1.0f, .0f, (float)(iii - onePoint) / (float)(temp.Length - onePoint));
			}
			
			return temp;
		}
		static float[] BuildPulse(int resolution)
		{
			float[] temp = new float[resolution];
			for (int iii = 0; iii < temp.Length; iii++)
			{
				if(iii < resolution - (resolution / 4))
					temp[iii] = 1;
				else 
					temp[iii] = -1;
			}
			
			return temp;
		}
		static float[] BuildHarmonic(int resolution, float harmonicOffset, System.Func<float, float, float> harmonicMultiplier, int harmonies = 11)
		{
			float[] temp = new float[resolution];
			float multiplier = -1.0f;
			
			for(float jjj = 1; jjj < (harmonies * harmonicOffset) + 1; jjj += harmonicOffset)
			{
				multiplier = harmonicMultiplier(jjj, multiplier);

				for (int iii = 0; iii < temp.Length; iii++)
					temp[iii] += (float)System.Math.Sin(((float)(iii * jjj) / temp.Length) * SMath.PI2) * multiplier;
			}

			ClampWaveTable(ref temp);
			return temp;
		}
		static float[] BuildFlow(int resolution)
		{
			float[] temp = new float[resolution];
			for (int iii = 0; iii < temp.Length; iii++)
			{
				int negOnePoint = temp.Length / 4;
				int onePoint = negOnePoint * 3;
				if(iii <= negOnePoint)
					temp[iii] = SMath.SmoothLerp(.0f, 1.0f, (float)iii / (float)negOnePoint);
				else if(iii <= onePoint)
					temp[iii] = SMath.SmoothLerp(1.0f, -1.0f, (float)(iii - negOnePoint) / (float)(onePoint - negOnePoint));
				else
					temp[iii] = SMath.SmoothLerp(-1.0f, .0f, (float)(iii - onePoint) / (float)(temp.Length - onePoint));
			}
			
			return temp;
		}

		static float[] BuildSawtooth(int resolution)
		{
			float[] temp = new float[resolution];
			for (int iii = 0; iii < temp.Length; iii++)
			{
				float bias = 21.0f/20.0f;

				float samplesPerWave = resolution;
				float percent = (float)(iii) / samplesPerWave;
				percent -= (float)System.Math.Floor(percent);
				percent = percent * bias;
				if(percent > 1.0f)
				{
					percent = System.Math.Abs((percent - 1.0f) / -1.0f/20.0f);
				}
				percent *= 2.0f;
				percent -= 1.0f;

				temp[iii] = -percent;
			}
				
			return temp;
		}
		static float[] BuildNoise(int resolution)
		{
			float[] temp = new float[resolution];
			System.Random rand = new System.Random();
			for (int iii = 0; iii < temp.Length; iii++)
				temp[iii] = (float)rand.NextDouble() * 2.0f - 1.0f;
			
			return temp;
		}

		public int WaveTableLength { get { return waveTable.Length; } }
		public float[] WaveTableCopyAtResolution(int resolution = -1)
		{
			if(resolution < 1)
				resolution = waveTable.Length;
			float[] ret = new float[resolution];

			if(waveTable.Length == ret.Length)
			{
				for(int iii = 0; iii < ret.Length; iii++)
					ret[iii] = waveTable[iii];
			}
			else
			{
				for(int iii = 0; iii < ret.Length; iii++)
				{
					float percent = (float)iii / (float)ret.Length;
					float fin = percent * (float)waveTable.Length;
					int index = (int)(fin);
					float balance = fin - index;
					ret[iii] = SMath.Lerp(waveTable[index], waveTable[(index + 1) % waveTable.Length], balance);
				}
			}
			
			return ret;
		}
		public static float[] GetWaveTable(WaveForm form, int resolution = -1)
		{
			if(resolution < 1)
				resolution = waveTables[form].Length;
			float[] ret = new float[resolution];

			if(waveTables[form].Length == ret.Length)
			{
				for(int iii = 0; iii < ret.Length; iii++)
					ret[iii] = waveTables[form][iii];
			}
			else
			{
				for(int iii = 0; iii < ret.Length; iii++)
				{
					float percent = (float)iii / (float)ret.Length;
					int index = (int)(percent * waveTables[form].Length);
					ret[iii] = waveTables[form][index];
				}
			}
			
			return ret;
		}
		
		public float GetFloat(float sample)
		{
			if(waveTable == null)// || sample < .0f)
				return .0f;
			while(sample < .0f)
				sample += waveTable.Length;
			uint index = (uint)((sample * DeviceSettings.SamplePeriod * waveTable.Length + phaseOffset * waveTable.Length) % waveTable.Length);
			
			return waveTable[index];
		}
		public virtual float GetSample(ActiveNote an, float frequency, bool bipole = true)
		{
			if(this.frequency != .0f)
				frequency = this.frequency;

			if(semitoneOffset != .0f)
				frequency = (float)(frequency * System.Math.Pow(2, semitoneOffset / 12.0f));

			float amplitude = GetFloat(frequency * an.phase);

			if(!bipole)
				amplitude = SMath.BipoleToUnipole(amplitude);

			if(envelope == null)
				return SMath.Clamp(amplitude * volume, -1.0f, 1.0f);
			
			return SMath.Clamp(amplitude * envelope.GetMultiplier(an), -1.0f, 1.0f);
		}
		public virtual float GetSample(float sampleIndex, float frequency, bool bipole = true)
		{
			if(this.frequency != .0f)
				frequency = this.frequency;

			if(semitoneOffset != .0f)
				frequency = (float)(frequency * System.Math.Pow(2, semitoneOffset / 12.0f));

			float amplitude = GetFloat(frequency * sampleIndex);

			if(!bipole)
				amplitude = SMath.BipoleToUnipole(amplitude);

			return SMath.Clamp(amplitude, -1.0f, 1.0f);
		}

		public static float GetSample(Oscillator.WaveForm form, float sampleIndex, float frequency)
		{
			float sample = frequency * sampleIndex;
			
			float[] waveTable = waveTables[form];
			/*
			uint index = (uint)((sample * DeviceSettings.SamplePeriod * waveTable.Length * waveTable.Length) % waveTable.Length);
			
			return waveTable[index];*/

			if(waveTable == null)
				return .0f;
			while(sample < .0f)
				sample += waveTable.Length;
			uint index = (uint)((sample * DeviceSettings.SamplePeriod * waveTable.Length) % waveTable.Length);
			
			return waveTable[index];
		}
	}
}
