﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Collections;
using Sounding.IO;

namespace Sounding
{
	static class Orchestra
	{
		public class InstrumentPrograms : SortedDictionary<int, Instrument>
		{
			public InstrumentPrograms() : base()
			{
				this[0] = new Instrument();
			}

			public new Instrument this[int program]
			{
				get 
				{
					program++;	//instruments are handled starting at index 0 but are specified in midi starting at 1
					while(!base.ContainsKey(program))
						program--;
					return base[program];
				}
				set { base[program] = value; }
			}

			public int GetProgram(string instrumentName)
			{
				foreach(KeyValuePair<int, Instrument> entry in this)
				{
					if(entry.Value.name.CompareTo(instrumentName) == 0)
						return entry.Key;
				}

				return -1;
			}
		}

		public static InstrumentPrograms Programs = new InstrumentPrograms();

		public static InstrumentPrograms Percussion = new InstrumentPrograms();

		public static string Name = "untitled";

		public static IOERR SaveOrchestra()
		{
			Directory.CreateDirectory(fullFolder);
			try
			{
				using(FileStream fs = new FileStream(FileName(Name), FileMode.Create, FileAccess.Write))
				using(BinaryWriter writer = new BinaryWriter(fs))
				{
					writer.Write(identifier);
					writer.Write(versionID);

					writer.Write((UInt32)Programs.Count);
					foreach(KeyValuePair<int, Instrument> entry in Programs)
					{
						writer.Write((Int32)entry.Key);
						writer.Write(entry.Value.name);
					}

					writer.Write((UInt32)Percussion.Count);
					foreach(KeyValuePair<int, Instrument> entry in Percussion)
					{
						writer.Write((Int32)entry.Key);
						writer.Write(entry.Value.name);
					}
				}
			}
			catch
			{
				return IOERR.FILEIO;
			}

			return IOERR.SUCCESS;
		}
#if UNITY
		public static IOERR LoadOrchestra(UnityEngine.TextAsset file)
		{
			return ReadStream(new BinaryReader(new MemoryStream(file.bytes)));
		}
#endif
		public static IOERR LoadOrchestra(string name)
		{
			//string fileName = FileName(name);
			Name = NameFromFilename(name);

			if(!File.Exists(name))	//check if fileName is full name
			{
				name = FileName(name);
				if(!File.Exists(name))	//check if fileName was just lacking directory and extension
					return IOERR.NOTFOUND;
			}

			IOERR ret = IOERR.FILEIO;
			//try
		//	{
				using(FileStream fs = new FileStream(name, FileMode.Open, FileAccess.Read))
				{
					ret = ReadStream(new BinaryReader(fs));
				//using(BinaryReader reader = new BinaryReader(fs))
				}

				/*{
					UInt64 id = reader.ReadUInt64();
					UInt32 version = reader.ReadUInt32();
					if(id != identifier)
						return IOERR.HEADER;
					if(version != versionID)
						return IOERR.VERSION;

					
					UInt32 count = reader.ReadUInt32();
					for(int iii = 0; iii < count; iii++)
					{
						Int32 program = reader.ReadInt32();
						string instrumentName = reader.ReadString();
						Instrument ins = Instrument.GetInstrument(instrumentName);
						if(ins != null)
							Programs[program] = ins;
					}

					count = reader.ReadUInt32();
					for(int iii = 0; iii < count; iii++)
					{
						Int32 program = reader.ReadInt32();
						string instrumentName = reader.ReadString();
						Instrument ins = Instrument.GetInstrument(instrumentName);
						if(ins != null)
							Percussion[program] = ins;
					}
				}
			}
			catch
			{
				return IOERR.OUTOFBOUNDS;
			}*/

			return ret;
		}
		static IOERR ReadStream(BinaryReader reader)
		{
			try
			{
				UInt64 id = reader.ReadUInt64();
				UInt32 version = reader.ReadUInt32();
				if(id != identifier)
					return IOERR.HEADER;
				if(version != versionID)
					return IOERR.VERSION;

					
				UInt32 count = reader.ReadUInt32();
				for(int iii = 0; iii < count; iii++)
				{
					Int32 program = reader.ReadInt32();
					string instrumentName = reader.ReadString();
					Instrument ins = Instrument.GetInstrument(instrumentName);
					if(ins != null)
						Programs[program] = ins;
				}

				count = reader.ReadUInt32();
				for(int iii = 0; iii < count; iii++)
				{
					Int32 program = reader.ReadInt32();
					string instrumentName = reader.ReadString();
					Instrument ins = Instrument.GetInstrument(instrumentName);
					if(ins != null)
						Percussion[program] = ins;
				}
				
			}
			catch
			{
				return IOERR.OUTOFBOUNDS;
			}

			return IOERR.SUCCESS;
		}

		public static string folder { get { return "Orchestra/"; } }
		public static string extension{ get { return ".orchestra"; } }
		public static string fullFolder	{ get { return DeviceSettings.Directory + DeviceSettings.ToNativeSeparators(folder); } }
		public static string FileName(string name)
		{
			return DeviceSettings.Directory + DeviceSettings.ToNativeSeparators(folder + name + extension);
		}
		public static string NameFromFilename(string fileName)
		{
			string ret = fileName;
			ret = ret.Replace(DeviceSettings.Directory + DeviceSettings.ToNativeSeparators(folder), "");
			ret = ret.Replace(extension, "");
			return ret;
		}
		const UInt64 identifier = 0xfc750fc23ee8702f;
		const UInt32 versionID = 0;
	}
}
