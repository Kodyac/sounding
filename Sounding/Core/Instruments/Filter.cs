﻿
namespace Sounding
{
	public class Filter
	{
		public enum PassType : int
		{
			Everything = 0,
			Low = 1,
			High = 2
		}

		public float Cutoff
		{
			get { return cutoff; }
			set
			{
				if(value < 10.0f)
					cutoff = 10.0f;
				cutoff = value;
				Rebuild();
			}
		}
		float cutoff = 10.0f;

		public float Resonance
		{
			get { return resonance; }
			set
			{
				if(value < .01f)
					resonance = .01f;
				resonance = value;
				Rebuild();
			}
		}
		float resonance = .05f;

		public PassType Pass
		{
			get { return pass; }
			set { pass = value; Rebuild(); }
		}
		PassType pass = PassType.Low;
		
		public Filter(){}
		public Filter(Filter o)
		{
			Cutoff = o.Cutoff;
			Resonance = o.Resonance;
			Pass = o.Pass;
		}
		public Filter(PassType pass, float cutoff, float resonance)
		{
			this.pass = pass;
			this.cutoff = cutoff;
			this.resonance = resonance;
			Rebuild();
		}

		///Arrays values, latest are in front
		public class FilterData
		{
			public float[] inputHistory = new float[2];
			public float[] outputHistory = new float[3];
			
		}
		FilterData data = new FilterData();
		float c, a1, a2, a3, b1, b2;

		void Rebuild()
		{
			switch (Pass)
			{
				case PassType.Low:
					c = 1.0f / (float)System.Math.Tan(System.Math.PI * Cutoff / DeviceSettings.SampleRate);
					a1 = 1.0f / (1.0f + Resonance * c + c * c);
					a2 = 2f * a1;
					a3 = a1;
					b1 = 2.0f * (1.0f - c * c) * a1;
					b2 = (1.0f - Resonance * c + c * c) * a1;
					break;
				case PassType.High:
					c = (float)System.Math.Tan(System.Math.PI * Cutoff / DeviceSettings.SampleRate);
					a1 = 1.0f / (1.0f + Resonance * c + c * c);
					a2 = -2f * a1;
					a3 = a1;
					b1 = 2.0f * (c * c - 1.0f) * a1;
					b2 = (1.0f - Resonance * c + c * c) * a1;
					break;
			}
		}

		public float GetFilteredValue(FilterData data, float newInput)//(float newInput)
		{
			float newOutput = a1 * newInput + a2 * data.inputHistory[0] + a3 * data.inputHistory[1] - b1 * data.outputHistory[0] - b2 * data.outputHistory[1];

			data.inputHistory[1] = data.inputHistory[0];
			data.inputHistory[0] = newInput;

			data.outputHistory[2] = data.outputHistory[1];
			data.outputHistory[1] = data.outputHistory[0];
			data.outputHistory[0] = newOutput;
			return newOutput;
		}

	}
}
