﻿using System;

namespace Sounding
{
	public static class SMath
	{
		static public float Clamp01(float value)
		{
			if(value < .0f)
				return .0f;
			else if(value > 1.0f)
				return 1.0f;
			return value;
		}
		static public float Clamp101(float value)
		{
			if(value < -1.0f)
				return -1.0f;
			else if(value > 1.0f)
				return 1.0f;
			return value;
		}
		static public float Clamp(float value, float min, float max)
		{
			if(value < min)
				value = min;
			else if(value > max)
				value = max;
			return value;
		}
		static public float BipoleToUnipole(float f)
		{
			f = f + 1.0f;
			f *= .5f;
			return f;
		}

		public static float Lerp(float from, float to, float percent) 
		{
			if (percent < 0.0f)
				return from;
			else if (percent >= 1.0f)
				return to;
			return (to - from) * percent + from;
		}
		public static float SmoothLerp(float from, float to, float percent)
		{
			return Lerp(from, to, SmoothStep(percent));
		}
		public static float SmoothStep(float percent)
		{
			return (percent * percent) * (3.0f - 2.0f * percent);
		}
		public static float Pow(float f, float power) 
		{
			return (float)Math.Pow(f, power);
		}
		public static float Abs(float f)
		{
			if(f < .0f)
				return -f;
			return f;
		}
		public static float Floor(float f)
		{
			return (float)Math.Floor(f);
		}
		public static int NearestPow2(int n)
		{
			n--;
			n |= n >> 1;
			n |= n >> 2;
			n |= n >> 4;
			n |= n >> 8;
			n |= n >> 16;
			n++;
			return n;
		}
		public static int HowManyPowersOfTwo(int n)
		{
			int count = 0;
			while(n > 1)
			{
				n = n >> 1;
				count++;
			}
			return count;
		}
		public static float MoveTowards(float current, float target, float maxDelta)
		{
			if(Math.Abs(target - current) <= maxDelta)
				return target;
			else
				return current + (Math.Sign(target - current) * maxDelta);
		}

		public const float PI = (float)System.Math.PI;
		public const float PI2 = (float)System.Math.PI * 2;
		
		
		
	}
}
