﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sounding
{
	
	static class Chord
	{
		public enum Types
		{
			None,
			Octave,
			Major,
			Minor,
			Diminished,
			Augmented,
			MajorPair,
			MinorPair
		}
		public static Types current = Types.None;
		public static int[] IntervalOffsets(Chord.Types chord)
		{
			int[] ret;

			switch (chord)
			{
				case Chord.Types.Major:
					ret = new int[3];
					ret[0] = 0;
					ret[1] = 4;
					ret[2] = 7;
					break;
				case Chord.Types.Minor:
					ret = new int[3];
					ret[0] = 0;
					ret[1] = 3;
					ret[2] = 7;
					break;
				case Chord.Types.Diminished:
					ret = new int[3];
					ret[0] = 0;
					ret[1] = 3;
					ret[2] = 6;
					break;
				case Chord.Types.Augmented:
					ret = new int[3];
					ret[0] = 0;
					ret[1] = 4;
					ret[2] = 8;
					break;
				case Types.Octave:
					ret = new int[2];
					ret[0] = 0;
					ret[1] = 12;
					break;
				case Types.MajorPair:
					ret = new int[2];
					ret[0] = 0;
					ret[1] = 4;
					break;
				case Types.MinorPair:
					ret = new int[2];
					ret[0] = 0;
					ret[1] = 3;
					break;
				default:
					ret = new int[1];
					ret[0] = 0;
					break;
			}

			return ret;
		}
		public static int[] IntervalOffsets()
		{
			return IntervalOffsets(current);
		}
	}
}
