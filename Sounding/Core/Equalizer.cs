﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Diagnostics;

namespace Sounding
{
	class Equalizer
	{
		public static readonly Equalizer standard;
		public struct Band
		{
			public float frequncy;
			public float gain;
			public Band(float frequency, float gain)
			{
				this.frequncy = frequency;
				this.gain = gain;
			}
		}

		Band[] bands = new Band[0];
		static Equalizer()
		{
			standard = new Equalizer(11);
		}
		public Equalizer(int bandCount)
		{
			bands = new Band[bandCount];
			double frequency = 15.625;
			for(int iii = 0; iii < bands.Length; iii++)
			{
				float gain = Decibels.DecibelsToLinear(DBAScale((float)frequency));
				if(gain > 1.0f)
					gain = 1.0f;
				if(gain < .0f)
					gain = .0f;
				gain = 1.0f - gain;
				gain = gain * .25f;
				gain += .75f;
				bands[iii] = new Band((float)frequency, gain);
				frequency *= 2.0;
			}
		}
		static float DBAScale(float freq)
		{
			const double log10e = 0.4342944819032518;
			float f = freq;
			if (f < 20.0f || f > 20000.0f) 
				return .0f;
			
			double f2 = f * f;
			double n1 = 12200 * 12200;
			double n2 = 20.6 * 20.6;
			double a = n1 * f2 * f2;
			
			a /= ((f2 + n2) * (f2 + n1) * Math.Sqrt(f2 + 107.7 * 107.7) * Math.Sqrt(f2 + 737.9 * 737.9));
			a /= 0.79434639580229505;
			
			a = 20 * log10e * Math.Log(a, Math.E);
			//a = 20 * log10e * log(a);
			
			if (Math.Abs(a) < 0.0001) 
				a=0;
			double c = n1 * f2;
			c /= ((f2 + n1) * (f2 + n2));
			c /= 0.9929048655202054;
			c = 20 * log10e * Math.Log(c, Math.E);
			if (Math.Abs(c) < 0.0001) 
				c=0;
			
			
			return (float)a;
		}

		/// <param name="interval">1 = 1 octave(1khz base) 2 = half octave etc</param>
		/// <returns></returns>
		static float BandFrequency(int bandIndex, int bandCount, float interval)
		{
			double frequency = 1000.0;
			int half = bandCount / 2;
			double mult = Math.Pow(10.0, 3.0 / (10.0 *  interval));
			
			if(bandIndex < half)
			{
				for(int iii = half; iii > bandIndex; iii--)
					frequency = frequency / mult;
			}
			else
			{
				for(int iii = half; iii < bandIndex; iii++)
					frequency = frequency * mult;
			}
			return (float)frequency;
		}
		/*Freq / 10 ^ (3/(10 * N))
		Freq / 10 ^ (3/(10 * N))
		1000 / 10 ^ (3/ (10 * 3)) = 794.33
		1000 * 10 ^ (3/ (10 * 12)) = 1059.25*/
		public float GetGain(float frequency)
		{
			if(frequency < bands[0].frequncy)
				return .0f;
			for(int iii = 1; iii < bands.Length; iii++)
			{
				if(frequency < bands[iii].frequncy)
				{
					float range = bands[iii].frequncy - bands[iii - 1].frequncy;
					float percent = (frequency - bands[iii - 1].frequncy) / range;
					return (bands[iii].gain - bands[iii - 1].gain) * percent + bands[iii - 1].gain;
				}
			}
			return .0f;
		}
	}
}
