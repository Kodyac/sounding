﻿using System.Collections;
using System;

namespace Sounding
{
	public struct Key
	{
		const char sharp = '#';
		const char flat = 'b';
		//const int A = 0, B = 2, C = 3, D = 5, E = 7, F = 8, G = 10;
		const int A = 9, B = 11, C = 0, D = 2, E = 4, F = 5, G = 7;
		const float intervalMult = 1.059463f;
		public const int IntervalsInOctave = 12;
		public const int MaxOctave = 9;
		public const int MinOctave = -1;
		//public const int OctaveRange = 11;
		
		public const float defaultA4 = 440.0f;
		public static float A4 = defaultA4;
		const int A4Interval = 69;

		public float Frequency { get { return A4 * (float)Math.Pow(intervalMult, Interval - A4Interval); } }
		public int Accidental 
		{
			get 
			{
				char letter = Letter;
				if(letter == 'A' || letter == 'B')
					return Interval - (IntervalsInOctave * (Octave + 1) + GetInterval(Letter) + ((GetInterval(Letter) > G) ? 0 : IntervalsInOctave)); 
				return Interval - (IntervalsInOctave * Octave + GetInterval(Letter) + ((GetInterval(Letter) > G) ? 0 : IntervalsInOctave)); 
			}
		}
		public char Letter { get { return GetLetterFromInterval(Interval); } }
		public int Octave { get { return ((Interval ) / IntervalsInOctave) - 1; } }
		public int Interval	
		{ 
			get 
			{
#if DEBUG
				System.Diagnostics.Debug.Assert(interval >= 0 && interval < 128);
#endif
				return interval; 
			} 
			set
			{
				interval = value;
				if(interval < 0)
					interval = 0;
				if(interval > 127)
					interval = 127;
			}
		}
		int interval;

		static char GetLetterFromInterval(int interval)
		{
			int let = interval % IntervalsInOctave;
			
			if(let < D)
				return 'C';
			else if(let < E)
				return 'D';
			else if(let < F)
				return 'E';
			else if(let < G)
				return 'F';
			else if(let < A)
				return 'G';
			else if(let < B)
				return 'A';
			else //if(let < C)
				return 'B';
		}
		static int GetInterval(char k)
		{
			if(k == 'A')
				return A;
			else if(k == 'B')
				return B;
			else if(k == 'C')
				return C;
			else if(k == 'D')
				return D;
			else if(k == 'E')
				return E;
			else if(k == 'F')
				return F;
			else if(k == 'G')
				return G;
			return A;
		}
		public Key(char letter, int octave, int accidental = 0) : this()
		{
			Interval = ToInterval(letter, octave, accidental);
		}
		public Key(int interval) : this()
		{
			this.Interval = interval;
		}
		
		public static implicit operator Key(int interval)
		{
			return new Key(interval);
		}
		public static implicit operator int(Key key)
		{
			return key.Interval;
		}

		static public int ToInterval(char letter, int octave, int accidental = 0)
		{
			int interval = octave * IntervalsInOctave + IntervalsInOctave;
			interval += GetInterval(letter);
			if(letter == 'A' || letter == 'B')
				interval -= IntervalsInOctave;
			interval += accidental;
			return interval;
		}
		static public Key FromString(string s)
		{
			if(s.Length == 2 || s.Length == 3)
			{
				int accidental = 0;
				if(s.Length == 3)
				{
					if(s[2] == sharp)
						accidental = 1;
					if(s[2] == flat)
						accidental = -1;
				}
				int octave = 0;
				int.TryParse(new string(s[1], 1), out octave);
				return new Key(s[0], octave, accidental);
			}
			return new Key('A', 4);
		}
		public static bool operator == (Key n1, Key n2)
		{
			return	n1.Interval == n2.Interval;
		}
		public static bool operator != (Key n1, Key n2)
		{
			return	n1.Interval != n2.Interval;
		}
		public override bool Equals(object other)
		{
			if(!(other is Key))
				return false;
		
			Key test = (Key)other;

			return test == this;
		}
		public override string ToString()
		{
			string ret = GetLetterFromInterval(Interval).ToString();
			ret += Octave;
			if(Accidental != 0)
			{
				if(Accidental < 0)
					ret += flat;
				else
					ret += sharp;
			}
			return ret;
		}
		public override int GetHashCode()
		{
			return base.GetHashCode();
		}
	}
}
