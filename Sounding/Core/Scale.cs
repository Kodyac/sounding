﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sounding
{
	class Scale
	{
		public enum Types
		{
			Major,
			NaturalMinor,
			HarmonicMinor,
			MelodicMinor
		}
		public static Types current = Types.Major;
		public static int[] IntervalOffsets(Types scale)
		{
			int[] ret;

			switch (scale)
			{
				case Types.Major:
					ret = new int[7];
					ret[0] = 0;
					ret[1] = 2;
					ret[2] = 4;
					ret[3] = 5;
					ret[4] = 7;
					ret[5] = 9;
					ret[6] = 11;
					break;
				case Types.NaturalMinor:
					ret = new int[7];
					ret[0] = 0;
					ret[1] = 2;
					ret[2] = 3;
					ret[3] = 5;
					ret[4] = 7;
					ret[5] = 8;
					ret[6] = 10;
					break;
				case Types.HarmonicMinor:
					ret = new int[7];
					ret[0] = 0;
					ret[1] = 2;
					ret[2] = 3;
					ret[3] = 5;
					ret[4] = 7;
					ret[5] = 8;
					ret[6] = 11;
					break;
				case Types.MelodicMinor:
					ret = new int[7];
					ret[0] = 0;
					ret[1] = 2;
					ret[2] = 3;
					ret[3] = 5;
					ret[4] = 7;
					ret[5] = 9;
					ret[6] = 11;
					break;
				default:
					ret = new int[1];
					ret[0] = 0;
					break;
			}

			return ret;
		}
		public static int[] IntervalOffsets()
		{
			return IntervalOffsets(current);
		}
	}
}
