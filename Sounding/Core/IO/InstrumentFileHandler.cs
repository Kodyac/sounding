﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

#if UNITY
using UnityEngine;
#endif

namespace Sounding.IO
{
	public static class FilterFileHandler
	{
		static public UInt32 SaveLength
		{ get {
			return (sizeof(int) + sizeof(float) + sizeof(float));	//passtype, Cutoff, Resonance
		} }
		static public void Save(this Filter filter, BinaryWriter writer)
		{
			writer.Write(SaveLength);
			writer.Write((int)filter.Pass);
			writer.Write(filter.Cutoff);
			writer.Write(filter.Resonance);
		}
		static public Filter Read(BinaryReader reader)
		{
			Filter.PassType pass = (Filter.PassType)reader.ReadInt32();
			float cutoff = reader.ReadSingle();
			float res = reader.ReadSingle();
			Filter filter = new Filter(pass, cutoff, res);
			return filter;
		}
	}
	public static class InstrumentFileHandler
	{
		public static string fullFolder	{ get { return DeviceSettings.Directory + DeviceSettings.ToNativeSeparators(folder); } }
		public static string FileName(string name)
		{
			string ret = DeviceSettings.ToNativeSeparators(name);
			if(!ret.StartsWith(DeviceSettings.Directory + DeviceSettings.ToNativeSeparators(folder)))
				ret = ret.Insert(0, DeviceSettings.Directory + DeviceSettings.ToNativeSeparators(folder));

			if(!ret.EndsWith(extension))
				ret = ret + extension;

			return ret;//DeviceSettings.Directory + DeviceSettings.ToNativeSeparators(folder + name + extension);
		}
		public static string NameFromFilename(string fileName)
		{
			string ret = fileName;
			ret = ret.Replace(DeviceSettings.Directory + DeviceSettings.ToNativeSeparators(folder), "");
			ret = ret.Replace(extension, "");
			return ret;
		}
		public static string folder { get { return "Instrument/"; } }
		public static string extension{ get { return ".instrument"; } }

		const UInt64 identifier = 0xfc759a204cf5263f;
		const UInt32 versionID = 1;
		enum Tags : uint
		{
			oscillators			= 1,
			envelope			= 2,
			volume				= 3,
			xxx_1		= 4,
			xxx_2		= 5,
			filter				= 6,
			xxx_3	= 7,
			xxx_4	= 8,
			xxx_5		= 9,
			end			= uint.MaxValue
		}
		
		public static string SettingsFile(this Instrument instrument)
		{
			string name = instrument.name + extension;
			name = name.Replace("/", ";");
			name = name.Replace("\\", ";");
			name += ".ini";
			return name;
		}

		/*static void SaveEffects(List<Effect> e, BinaryWriter writer)
		{
			UInt32 length = 0;
			length += sizeof(UInt32);	//count
			for(int iii = 0; iii < e.Count; iii++)
			{
				length += sizeof(UInt32);	//type
				length += e[iii].SaveLength();
			}

			writer.Write(length);
			writer.Write((UInt32)e.Count);
			for(int iii = 0; iii < e.Count; iii++)
			{
				writer.Write((UInt32)e[iii].GetEffectType());
				e[iii].Save(writer);
			}
		}
		static List<Effect> ReadEffects(BinaryReader reader)
		{
			List<Effect> e = new List<Effect>();
			uint count = reader.ReadUInt32();

			for(int iii = 0; iii < count; iii++)
			{
				Effect.EffectType et = (Effect.EffectType)reader.ReadUInt32();
				reader.ReadUInt32();
				switch (et)
				{
					case Effect.EffectType.Envelope:
						e.Add(Envelope.Read(reader));
						break;
					case Effect.EffectType.LFO:
						e.Add(LFO.Read(reader));
						break;
					case Effect.EffectType.Accelerator:
						e.Add(Accelerator.Read(reader));
						break;
					default:
						System.Diagnostics.Debugger.Break();
						break;
				}
			}

			return e;
		}*/

		public static IOERR Save(this Instrument instrument)
		{
			Directory.CreateDirectory(fullFolder);

			try
			{
				using(FileStream fs = new FileStream(FileName(instrument.name), FileMode.Create, FileAccess.Write))
				using(BinaryWriter writer = new BinaryWriter(fs))
				{
					writer.Write(identifier);
					writer.Write(versionID);

					#region oscillators
					if (instrument.oscillators.Count > 0)
					{
						writer.Write((uint)Tags.oscillators);
						UInt32 lengthOfOscillatorsSection = 0;
		
						lengthOfOscillatorsSection += sizeof(int); //count variable
						
						for(int iii = 0; iii < instrument.oscillators.Count; iii++)
							lengthOfOscillatorsSection += instrument.oscillators[iii].SaveLength();

						writer.Write(lengthOfOscillatorsSection);

						writer.Write((Int32)instrument.oscillators.Count);
						for(int iii = 0; iii < instrument.oscillators.Count; iii++)
						{
							OscillatorFileHandler.Save(writer, instrument.oscillators[iii]);
						}
					}
					#endregion

					#region envelope
					writer.Write((uint)Tags.envelope);
					instrument.envelope.Save(writer);
					#endregion

					#region volume
					writer.Write((uint)Tags.volume);
					writer.Write(sizeof(float));
					writer.Write(instrument.volume);
					#endregion

					#region filter
					if(instrument.filter != null)
					{
						writer.Write((uint)Tags.filter);
						instrument.filter.Save(writer);
					}
					#endregion
					/*
					#region amplitudeEffects
					if(instrument.amplitudeEffects.Count > 0)
					{
						writer.Write((uint)Tags.amplitudeEffects);
						SaveEffects(instrument.amplitudeEffects, writer);
					}
					#endregion

					#region phaseEffects
					if(instrument.phaseEffects.Count > 0)
					{
						writer.Write((uint)Tags.phaseEffects);
						SaveEffects(instrument.phaseEffects, writer);
					}
					#endregion

					#region frequencyEffects
					if(instrument.frequencyEffects.Count > 0)
					{
						writer.Write((uint)Tags.frequencyEffects);
						SaveEffects(instrument.frequencyEffects, writer);
					}
					#endregion
					*/
					writer.Write((uint)Tags.end);				//=======================End
				}
			}
			catch
			{
				return IOERR.FILEIO;
			}
		
			return IOERR.SUCCESS;
		}
		public static IOERR LoadFile(string name, out Instrument inst)
		{
			
			IOERR ret = IOERR.FILEIO;

			inst = new Instrument();
			inst.name = NameFromFilename(name);
#if UNITY
			name = FileName(name);
			//name = name.Replace("\\", "/");
			TextAsset file = Resources.Load("_Sounds/Midi/" + name) as TextAsset;
			if(file == null)
				ret = IOERR.NOTFOUND;
			else
				ret = ReadStream(new BinaryReader(new MemoryStream(file.bytes)), inst);
#else
			if(!File.Exists(name))	//check if fileName is full name
			{
				name = FileName(name);
				if(!File.Exists(name))	//check if fileName was just lacking directory and extension
					return IOERR.NOTFOUND;
			}

			using(FileStream fs = new FileStream(name, FileMode.Open, FileAccess.Read))
			{
				ret = ReadStream(new BinaryReader(fs), inst);
			}
#endif
			
			if(inst.oscillators.Count < 1)
				inst.oscillators.Add(new Oscillator());

			return ret;
		}
		
		public static IOERR ReadStream(BinaryReader reader, Instrument inst)
		{
			try
			{
				UInt64 id = reader.ReadUInt64();
				UInt32 version = reader.ReadUInt32();
				if(id != identifier)
					return IOERR.HEADER;
				if(version != versionID)
					return IOERR.VERSION;

				Tags tag = (Tags)reader.ReadUInt32();
				while(tag != Tags.end)
				{
					UInt32 length = reader.ReadUInt32();

					switch (tag)
					{
					#region oscillators
						case Tags.oscillators:
							inst.oscillators.Clear();
							Int32 oscCount = reader.ReadInt32();
							for(int iii = 0; iii < oscCount; iii++)
							{
								inst.oscillators.Add(OscillatorFileHandler.Read(reader));
							}
							break;
					#endregion

					#region envelope
						case Tags.envelope:
							inst.envelope = Envelope.Read(reader);
							break;
					#endregion
						
					#region volume
						case Tags.volume:
							inst.volume = reader.ReadSingle();
							break;
					#endregion

					#region filter
						case Tags.filter:
							inst.filter = FilterFileHandler.Read(reader);
							break;
					#endregion
							/*
					#region amplitudeEffects
						case Tags.amplitudeEffects:
							inst.amplitudeEffects = ReadEffects(reader);
							break;
					#endregion

					#region frequencyEffects
						case Tags.frequencyEffects:
							inst.frequencyEffects = ReadEffects(reader);
							break;
					#endregion

					#region phaseEffects
						case Tags.phaseEffects:
							inst.phaseEffects = ReadEffects(reader);
							break;
					#endregion
							*/
						case Tags.end:
							continue;
						default:
							reader.BaseStream.Seek(length, SeekOrigin.Current);
							break;
					}
					tag = (Tags)reader.ReadUInt32();
				}
				
			}
			catch
			{
				return IOERR.OUTOFBOUNDS;
			}

			if(inst.oscillators.Count < 1)
				inst.oscillators.Add(new Oscillator());
			
			return IOERR.SUCCESS;
		}
	}
}
