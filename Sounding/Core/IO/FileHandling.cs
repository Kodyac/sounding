﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

#if UNITY
using UnityEngine;
#endif

namespace Sounding.IO
{
	public enum IOERR
	{
		NOTFOUND,
		VERSION,
		HEADER,
		OUTOFBOUNDS,
		FILEIO,
		SUCCESS
	}

	static class FileHandling
	{
		public static void IOLoadErrorMessage(string name, IOERR error)
		{
			switch (error)
			{
				case IOERR.NOTFOUND:
					Debug.Log("Could not find " + name);
					return;
				case IOERR.VERSION:
					Debug.Log("Version Error \n" + name);
					return;
				case IOERR.HEADER:
					Debug.Log("File does not appear to be the correct filetype \n" + name);
					return;
				case IOERR.OUTOFBOUNDS:
					Debug.Log("Out of bounds IO Error " + name + "\n File likely corrupt");
					return;
				case IOERR.FILEIO:
					Debug.Log("Filesystem IO Error " + name);
					return;
				case IOERR.SUCCESS:
				default:
					break;
			}
		}
		public static void IOSaveErrorMessage(string name, IOERR error)
		{
			switch (error)
			{
				case IOERR.NOTFOUND:
					Debug.Log("Could not save " + name + "\nCould not write to directory.");
					break;
				case IOERR.VERSION:
				case IOERR.HEADER:
				case IOERR.OUTOFBOUNDS:
				case IOERR.FILEIO:
					Debug.Log("Could not save " + name + "\nBecause of a filesystem IO error.");
					break;
				case IOERR.SUCCESS:
				default:
					break;
			}
		}
#if !UNITY
		static class Debug
		{
			public static void Log(string s)
			{
				System.Windows.Forms.MessageBox.Show(s, "Error", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
			}
		}
#endif
	}
}
