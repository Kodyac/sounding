﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Sounding
{
	public static class OscillatorFileHandler
	{
		const UInt32 versionID = 0;
		enum Tags : uint
		{
			form			= 1,
			volume			= 2,
			octaveOffset	= 3,
			phaseOffset		= 4,
			envelope		= 5,
			mixType			= 6,
			frequency		= 7,
			end			= uint.MaxValue
		}

		static public UInt32 SaveLength(this Oscillator osc)
		{
			UInt32 size = 0;

			size += sizeof(UInt32);	//version

			size += sizeof(uint);	//formTag
			size += sizeof(int);	//length
			size += sizeof(int);	//forms full size


			size += sizeof(uint);	//volumeTag
			size += sizeof(int);	//length
			size += sizeof(float);	//volume

			size += sizeof(uint);	//octaveTag
			size += sizeof(int);	//length
			size += sizeof(float);	//octaveOffset

			size += sizeof(uint);	//phaseTag
			size += sizeof(int);	//length
			size += sizeof(float);	//phaseOffset

			size += sizeof(uint);	//mixTag
			size += sizeof(int);	//length
			size += sizeof(int);	//mixtype

			size += sizeof(uint);	//frequencyTag
			size += sizeof(int);	//length
			size += sizeof(float);	//frequency

			size += sizeof(uint);	//envelopeTag
			size += sizeof(int);	//length
			if(osc.envelope != null)
				size += osc.envelope.SaveLength(); //envelope

			size += sizeof(uint);	//endTag

			return size;
		}

		static public void Save(BinaryWriter writer, Oscillator osc)
		{
			writer.Write(versionID);

			#region form
			writer.Write((uint)Tags.form);
			writer.Write(sizeof(Int32));//writer.Write(osc.FormSize());
			writer.Write((Int32)osc.Form);
			#endregion

			#region envelope
			if(osc.envelope != null)
			{
				writer.Write((uint)Tags.envelope);
				osc.envelope.Save(writer);
			}
			#endregion

			#region mixType
			writer.Write((uint)Tags.mixType);
			writer.Write(sizeof(int));
			writer.Write((int)osc.type);
			#endregion

			#region frequency
			writer.Write((uint)Tags.frequency);
			writer.Write(sizeof(float));
			writer.Write((float)osc.frequency);
			#endregion

			#region volume
			writer.Write((uint)Tags.volume);
			writer.Write(sizeof(float));
			writer.Write(osc.volume);
			#endregion

			#region octaveOffset
			writer.Write((uint)Tags.octaveOffset);
			writer.Write(sizeof(float));
			writer.Write(osc.semitoneOffset);
			#endregion

			#region phaseOffset
			writer.Write((uint)Tags.phaseOffset);
			writer.Write(sizeof(float));
			writer.Write(osc.phaseOffset);
			#endregion

			writer.Write((uint)Tags.end);
		}
		
		static public Oscillator Read(BinaryReader reader)
		{
			Oscillator osc = new Oscillator();

			UInt32 version = reader.ReadUInt32();
			
			if(version != versionID)
				return osc;

			try
			{
				Tags tag = (Tags)reader.ReadUInt32();
				while(tag != Tags.end)
				{
					UInt32 length = reader.ReadUInt32();

					switch (tag)
					{
						#region form
						case Tags.form:
							osc.Form = (Oscillator.WaveForm)reader.ReadInt32();
							break;
						#endregion

						#region envelope
						case Tags.envelope:
							osc.envelope = Envelope.Read(reader);
							break;
						#endregion

						#region volume
						case Tags.volume:
							osc.volume = reader.ReadSingle();
							break;
						#endregion

						#region mixType
						case Tags.mixType:
							osc.type = (Oscillator.MixType)reader.ReadInt32();
							break;
						#endregion

						#region frequency
						case Tags.frequency:
							osc.frequency = reader.ReadSingle();
							break;
						#endregion

						#region octaveOffset
						case Tags.octaveOffset:
							osc.semitoneOffset = reader.ReadSingle();
							break;
						#endregion

						#region phaseOffset
						case Tags.phaseOffset:
							osc.phaseOffset = reader.ReadSingle();
							break;
						#endregion

						case Tags.end:
							break;
						default:
							reader.BaseStream.Seek(length, SeekOrigin.Current);
							break;
					}
						
					tag = (Tags)reader.ReadUInt32();
				}
			}
			catch{}

			return osc;
		}
	}
}
