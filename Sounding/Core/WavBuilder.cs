﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

using System.IO;

namespace Sounding
{
	public static class WavBuilder
	{
		static public long WriteWav(string fileName, Track t, int sampleRate = 48000)
		{
			t.Song.Reset();
			t.Song.currentTrack = t.Song.tracks.IndexOf(t);

			return WriteWav(fileName, t.Song, sampleRate, (long)Math.Ceiling((float)t.SecondsLength * sampleRate) * DeviceSettings.Channels);
		}
		static public long WriteWav(string fileName, Song song, int sampleRate = 48000)
		{
			return WriteWav(fileName, song, sampleRate, (long)Math.Ceiling((float)song.SecondsLength * sampleRate) * DeviceSettings.Channels);
		}
		static long WriteWav(string fileName, Song song, int sampleRate, long sampleCount)
		{
			song.Reset();

			FileStream fstream = new FileStream(fileName, FileMode.Create);

			BinaryWriter writer = new BinaryWriter(fstream);

			uint fileLength = 0;

			writer.Write("RIFF".ToCharArray());
			writer.Write(fileLength);
			writer.Write("WAVE".ToCharArray());

			uint chunkSize = 16;
			ushort formatTag = 3;
			ushort channels = (ushort)DeviceSettings.Channels;
			ushort bitsPerSample = 32;
		
			ushort blockAlign = (ushort)(channels * (bitsPerSample / 8));;
			uint avgBytesPerSec = (uint)sampleRate * blockAlign;
		
			writer.Write("fmt ".ToCharArray());
			writer.Write(chunkSize);		//chunkSize
			writer.Write(formatTag);		//formatTag
			writer.Write(channels);			//channels
			writer.Write((uint)sampleRate);	//fundamental frequency
			writer.Write(avgBytesPerSec);	//avgbytespersec
			writer.Write(blockAlign);		//blockalign
			writer.Write(bitsPerSample);	//bitspersample
		
			
			float[] samples = new float[sampleCount];

			Stopwatch sw = Stopwatch.StartNew();

			int segmentSize = 16;
			for(int iii = 0; iii < samples.Length; iii += segmentSize)
			{
				if(iii + segmentSize > samples.Length)
				{
					segmentSize = samples.Length - iii;
				}
				float[] segment = new float[segmentSize];
				
				song.GetSamples(ref segment);

				for(int jjj = 0; jjj < segmentSize; jjj++)
				{
					samples[iii + jjj] = segment[jjj];
				}
			}

			sw.Stop();
			long milliseconds = sw.ElapsedMilliseconds;

			uint dataChunkSize = (uint)(samples.Length * (bitsPerSample / 8));
			writer.Write("data".ToCharArray());
			writer.Write(dataChunkSize);
			for(int iii = 0; iii < samples.Length; iii++)
				writer.Write(samples[iii]);
		
			writer.Seek(4, SeekOrigin.Begin);
			uint fileSize = (uint)writer.BaseStream.Length;
			writer.Write(fileSize - 8);

			writer.Close();
			fstream.Close();

			return milliseconds;
		}
		static public long Profile(Track t, int times)
		{
			t.Song.Reset();
			t.Song.currentTrack = t.Song.tracks.IndexOf(t);
			return Profile(t.Song, times);
		}
		static public long Profile(Song song, int times)
		{
			if(times < 1)
				return 0;

			long total = 0;
			long sampleCount = 0;//(int)Math.Ceiling((float)song.SecondsLength * DeviceSettings.SampleRate) * DeviceSettings.Channels;

			if(song.trackType == Song.TrackTypes.Asynchronous)
				sampleCount = (int)Math.Ceiling((float)song.tracks[song.currentTrack].SecondsLength * DeviceSettings.SampleRate) * DeviceSettings.Channels;
			else
				sampleCount = (int)Math.Ceiling((float)song.SecondsLength * DeviceSettings.SampleRate) * DeviceSettings.Channels;

			for(int ttt = 0; ttt < times; ttt++)
			{
				
				float[] samples = new float[sampleCount];

				Stopwatch sw = Stopwatch.StartNew();

				int segmentSize = 1024;
				for(int iii = 0; iii < samples.Length; iii += segmentSize)
				{
					if(iii + segmentSize > samples.Length)
					{
						segmentSize = samples.Length - iii;
					}
					float[] segment = new float[segmentSize];
				
					song.GetSamples(ref segment);

					for(int jjj = 0; jjj < segmentSize; jjj++)
					{
						samples[iii + jjj] = segment[jjj];
					}
				}

				sw.Stop();
				total += sw.ElapsedMilliseconds;
			}
			return total / times;
		}
	}
}
