﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sounding
{
	public class Decibels
	{
		 // 20 / ln( 10 )
        private const double LOG_2_DB = 8.6858896380650365530225783783321;

        // ln( 10 ) / 20
        private const double DB_2_LOG = 0.11512925464970228420089957273422;

        public static double LinearToDecibels(double lin) { return System.Math.Log(lin) * LOG_2_DB; }
		public static float LinearToDecibels(float lin) { return (float)(System.Math.Log(lin) * LOG_2_DB); }

		public static double DecibelsToLinear(double dB) { return System.Math.Exp(dB * DB_2_LOG); }
        public static float DecibelsToLinear(float dB) { return (float)(System.Math.Exp(dB * DB_2_LOG)); }

		public static float[] DecibelsOfChunk(float[] data, int channels)
		{
			double[] sum = new double[channels];
			float[] ret = new float[channels];
			for(int ccc = 0; ccc < channels; ccc++)
			{
				for(int iii = 0; iii < data.Length; iii+= channels)
				{
					sum[ccc] += data[iii + ccc] * data[iii + ccc];
				}
				
				double rootMeanSquare = Math.Sqrt(sum[ccc] / (data.Length / channels));
				ret[ccc] = (float)(20.0 * Math.Log10(rootMeanSquare));
			}
			return ret;
		}
	}
}
