﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

namespace Sounding
{
	public class Track
	{
		static int TrackCounter = 0;
		int trackNumber = TrackCounter++;

		public List<Event> events = new List<Event>();
		
		public Channel[] channels = new Channel[16];

		MetaEvent endOfTrack = null;
		MetaEvent EndOfTrack
		{
			get
			{
				if(endOfTrack == null)
				{
					for(int iii = 0; iii < events.Count; iii++)
					{
						if(events[iii] is MetaEvent)
						{
							MetaEvent meta = events[iii] as MetaEvent;
							if(meta.MetaCode == MetaEventCodes.EndOfTrack)
							{
								endOfTrack = meta;
								break;
							}
						}
					}
					if(endOfTrack == null)
					{
						endOfTrack = new MetaEvent(TickLength, MetaEventCodes.EndOfTrack);
						events.Add(endOfTrack);
					}
				}
				return endOfTrack;
			}
		}

		TextEvent nameEvent = null;
		public TextEvent NameEvent
		{
			get
			{
				if(nameEvent != null)
					return nameEvent;
				for(int iii = 0; iii < events.Count; iii++)
				{
					if(events[iii] is TextEvent)
					{
						TextEvent teve = events[iii] as TextEvent;
						if(teve.MetaCode == MetaEventCodes.SequenceName)
						{
							nameEvent = teve;
							return nameEvent;
						}
					}
				}
				nameEvent = new TextEvent(0, MetaEventCodes.SequenceName);
				nameEvent.text = "Unnamed Track";
				events.Insert(0, nameEvent);
				eventIndex++;	//if playing the song while the name event is added bump the index up to account for insertion
				return nameEvent;
			}
		}
		

		public int TickLength;

		public double SecondsLength
		{
			get
			{
				double time = .0;
				double tempo = 120.0;
				int ticks = 0;
				for(int iii = 0; iii < events.Count; iii++)
				{
					if(events[iii] is TempoEvent)
					{
						TempoEvent te = events[iii] as TempoEvent;
						
						time += parent.TicksToBeat(te.absoluteTime - ticks) * 60.0f / tempo;
						ticks = te.absoluteTime;

						tempo = te.Tempo;
					}
				}

				time += parent.TicksToBeat(TickLength - ticks) * 60.0f / tempo;
				
				return time;
			}
		}
		
		int eventIndex = 0;
		double curTicks = 0;

		Song parent = null;

		public Song Song { get { return parent; } }

		bool trackFinished = false;

		public MetaEvent GetMetaEventAtTime(int tick, MetaEventCodes code)
		{
			MetaEvent ret = null;
			
			for(int iii = 0; iii < events.Count; iii++)
			{
				if(events[iii].absoluteTime > tick)
					break;
				
				if(events[iii].Code == MidiEventsCodes.MetaEvent)
				{
					MetaEvent meta = events[iii] as MetaEvent;
					if(meta.MetaCode != code)
						continue;
					ret = meta;
				}
			}
			return ret;
		}

		public NoteEvent GetNoteEventAtTime(int tick, MidiEventsCodes code, int channel = -1)
		{
			NoteEvent ret = null;
			
			for(int iii = 0; iii < events.Count; iii++)
			{
				if(events[iii].absoluteTime > tick)
					break;
				
				if(events[iii].Code == code)
				{
					NoteEvent note = events[iii] as NoteEvent;
					if(note.Channel == channel || channel < 0)
						ret = note;
				}
			}
			return ret;
		}

		public void SortTrack()
		{

		lock(events)
		{
			events.Sort(new EventSorter());
			TickLength = EndOfTrack.absoluteTime;
		}

		}
		public void GotoTick(int tick)
		{
			Reset();

			AdvanceTime(tick, false);
		}
		public int EventsInChannel(int channelIndex)
		{
			int ret = 0;

			for(int iii = 0; iii < events.Count; iii++)
			{
				Event eve = events[iii];
				if(eve is NoteEvent && ((NoteEvent)eve).Channel == channelIndex)
					ret++;
			}
			return ret;
		}
		public Track(Song parent)
		{
			this.parent = parent;
			for(int iii = 0; iii < channels.Length; iii++)
				channels[iii] = new Channel();
			channels[9].percussion = true;
		}
		public Track(Song parent, Song.TrackTypes type)
		{
			this.parent = parent;
			for(int iii = 0; iii < channels.Length; iii++)
				channels[iii] = new Channel();
			channels[9].percussion = true;

			switch (type)
			{
				case Song.TrackTypes.Synchronous:
					break;
				case Song.TrackTypes.Single:
				case Song.TrackTypes.Asynchronous:
					TempoEvent tempo = new TempoEvent(0);
					tempo.Tempo = 140;

					TimeSignatureEvent timeSig = new TimeSignatureEvent(0);

					MetaEvent eot = new MetaEvent(0, MetaEventCodes.EndOfTrack);

					TextEvent name = new TextEvent(0, MetaEventCodes.SequenceName);
					name.text = "Untitled Track";
			
					events.Add(name);
					events.Add(tempo);
					events.Add(timeSig);
					events.Add(eot);
					break;
				default:
					break;
			}
		}
		public Track(Song parent, Track other)
		{
			this.parent = parent;
			for(int iii = 0; iii < channels.Length; iii++)
			{
				channels[iii] = new Channel(other.channels[iii]);
			}

			events.Clear();
			
			for(int iii = 0; iii < other.events.Count; iii++)
			{
				Event e = other.events[iii];
				
				if(e is NoteOnEvent)
				{
					NoteOnEvent neve = ((NoteOnEvent)e).Copy() as NoteOnEvent;

					for(int jjj = 0; jjj < neve.SubEvents().Count; jjj++)
						events.Add(neve.SubEvents()[jjj]);
				}
				else
					events.Add(e.Copy());
			}

			SortTrack();
		}

		/// <summary>Set track to beginning</summary>
		public void Reset()
		{
			lock(events)
			{
				eventIndex = 0;
				curTicks = 0;
				trackFinished = false;
			
				foreach(Channel c in channels)
				{
					c.volume = 1.0f;
					c.AllNotesOff();
				}
			}
		}
		public void Clear()
		{
			foreach(Channel c in channels)
			{
				c.volume = 1.0f;
				c.AllSoundOff();
			}
		}

		/// <summary>
		/// Advances track time by deltaTicks.
		/// </summary>
		/// <param name="deltaTicks">amount of time to advance</param>
		/// <param name="addNotes">Whether to add the notes to the playingNotes list, set false for seeking.</param>
		/// <returns>true if the track is finished</returns>
		public bool AdvanceTime(double deltaTicks, bool addNotes = true)
		{
			if(trackFinished)
				return true;
		lock(events)
		{

			curTicks += deltaTicks;
			while(eventIndex < events.Count && curTicks > events[eventIndex].absoluteTime)
			{
				switch (events[eventIndex].Code)
				{
					case MidiEventsCodes.NoteOff:
					{
						NoteKeyEvent curEvent = events[eventIndex] as NoteKeyEvent;
						int channelIndex = curEvent.Channel;
						Key key = curEvent.key;
						for(int ccc = 0; ccc < channels[channelIndex].activeNotes.Count; ccc++)
						{
							if(channels[channelIndex].activeNotes[ccc].key == key && channels[channelIndex].activeNotes[ccc].Alive)
							{
								channels[channelIndex].activeNotes[ccc].Alive = false;
								break;
							}
						}
						
						break;
					}
					case MidiEventsCodes.NoteOn:
					{
						NoteKeyEvent on = events[eventIndex] as NoteKeyEvent;
						
						if(on.msb == 0)
							goto case MidiEventsCodes.NoteOff;

						if(!addNotes)
							break;

						ActiveNote an = channels[on.Channel].GetNote(on.key);
						if(an != null)
						{
							an.velocity = on.velocity;
						}
						else
						{
							an = new ActiveNote(on.key, on.velocity);
						
							channels[on.Channel].activeNotes.Add(an);
						}
						
						break;
					}
					case MidiEventsCodes.KeyAfterTouch:
						break;
					case MidiEventsCodes.ControlChange:
					{
						NoteEvent controlChange = events[eventIndex] as NoteEvent;
						ControlChangeCodes control = (ControlChangeCodes)controlChange.lsb;
						switch (control)
						{
							case ControlChangeCodes.ChannelVolume:
								channels[controlChange.Channel].volume = (float)controlChange.msb / 127.0f;
								break;
							case ControlChangeCodes.Balance:
								break;
							case ControlChangeCodes.Pan:
								break;
							case ControlChangeCodes.AllSoundOff:
								channels[controlChange.Channel].AllSoundOff();
								break;
							case ControlChangeCodes.AllNotesOff:
								channels[controlChange.Channel].AllNotesOff();
								break;
							default:
								break;
						}
					}

						break;
					case MidiEventsCodes.ProgramChange:
					{
						int channelIndex = ((NoteEvent)events[eventIndex]).Channel;
						channels[channelIndex].instrument = Orchestra.Programs[events[eventIndex].lsb];
					}
						break;
					case MidiEventsCodes.ChannelAfterTouch:
						break;
					case MidiEventsCodes.PitchWheelChange:
						break;
					case MidiEventsCodes.Eox:
						break;
					case MidiEventsCodes.TimingClock:
						break;
					case MidiEventsCodes.StartSequence:
						break;
					case MidiEventsCodes.ContinueSequence:
						break;
					case MidiEventsCodes.StopSequence:
						break;
					case MidiEventsCodes.AutoSensing:
						break;
					case MidiEventsCodes.Sysex:
						break;
					case MidiEventsCodes.MetaEvent:
						MetaEvent me = events[eventIndex] as MetaEvent;
						switch (me.MetaCode)
						{
							case MetaEventCodes.SetTempo:
								parent.tempo = ((TempoEvent)me).Tempo;
								break;
							
							default:
								break;
						}
						break;
					default:
						break;
				}

				eventIndex++;
			}


			if((eventIndex >= events.Count))
				TrackFinished();
		}

			return trackFinished;
		}

		public void GetSamples(ref float[] samples, int sampleOffset = 0)
		{

		lock(events)
		{
			for(int iii = 0; iii < channels.Length; iii++)//(Channel c in channels)
			{
#if SoundingEditor
				if(!Song.channelPlayIndexes.Contains(iii))
					continue;
#endif
				channels[iii].GetSamples(ref samples, sampleOffset);
			}
		}

		}
		
		void TrackFinished()
		{
			curTicks = TickLength;
			if(trackFinished)
				return;
			foreach(Channel c in channels)
			{
				c.AllNotesOff();
			}
			trackFinished = true;
		}
	}
}
