﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Diagnostics;

namespace Sounding
{
	public class Song
	{
		public enum TrackTypes : ushort
		{
			Single = 0,
			Synchronous = 1,
			Asynchronous = 2
		}

		public TrackTypes trackType = TrackTypes.Asynchronous;

#if SoundingEditor
		static public List<int> trackPlayIndexes = new List<int>();
		static public List<int> channelPlayIndexes = new List<int>();
#endif
		public double tempo = 120.0;

		public string name;
		public List<Track> tracks = new List<Track>();
		public int currentTrack = 0;

		double currentTick;

		public int		CurrentTick { get { return (int)currentTick; } }
		public float	CurrentBeat { get { return TicksToBeat((float)currentTick); } }
		public float	BeatsPerSecond { get { return (float)tempo / 60.0f; } }
		public int		BeatToTicks(float beat) { return (int)(beat * ticksPerBeat); }
		public float	TicksToBeat(int ticks) { return (float)ticks / ticksPerBeat; }
		public float	TicksToBeat(float ticks) { return ticks / ticksPerBeat; }
		public int		TicksToSamples(int ticks)
		{
			return (int)((ticks / ticksPerBeat) * DeviceSettings.SampleRate / BeatsPerSecond);
		}
		public double	SecondsToTicks(double seconds)
		{
			return (seconds * BeatsPerSecond * ticksPerBeat);
		}
		public int		TickLength
		{
			get
			{
				int longest = 0;
				foreach(Track t in tracks)
					if(t.TickLength > longest)
						longest = t.TickLength;
				return longest;
			}
		}
		public float ticksPerBeat = 384.0f; //saved and loaded as a short just kept as a float because most of the calcs it's used in are float based

		public double SecondsLength
		{
			get
			{
				double longest = .0;
				foreach(Track t in tracks)
					if(t.SecondsLength > longest)
						longest = t.SecondsLength;
				return longest;
			}
		}

		/// <summary>Seeks the song to tick</summary>
		public void GotoTick(int tick)
		{
			currentTick = tick;
			foreach(Track t in tracks)
				t.GotoTick(tick);
		}

		/// <summary>returns true if all tracks are complete</summary>
		public bool GetSamples(ref float[] samples)
		{
			double deltaTime = (((double)samples.Length / DeviceSettings.Channels) / (double)DeviceSettings.SampleRate);
			double deltaTicks = deltaTime * BeatsPerSecond * ticksPerBeat * DeviceSettings.tempoMultiplier;

			currentTick += deltaTicks;

			bool allDone = true;

			if(trackType == TrackTypes.Synchronous)
			{
				for(int iii = 0; iii < tracks.Count; iii++)
				{
	#if SoundingEditor
					if(!trackPlayIndexes.Contains(iii) && currentTrack != iii)
						continue;
	#endif
					allDone &= tracks[iii].AdvanceTime(deltaTicks);
					tracks[iii].GetSamples(ref samples);
				}
			}
			else
			{
				allDone &= tracks[currentTrack].AdvanceTime(deltaTicks);
				tracks[currentTrack].GetSamples(ref samples);
			}

			
			if(allDone)
				Reset();
			
			return allDone;
		}

		public void Reset()
		{
			currentTick = 0;
			foreach(Track t in tracks)
				t.Reset();
		}
		public void Clear()
		{
			Reset();
			foreach(Track t in tracks)
				t.Clear();
		}

		public Song()
		{
		}
		public Song(Song other)
		{
			tempo = other.tempo;
			name = other.name;

			tracks.Clear();
			for(int iii = 0; iii < other.tracks.Count; iii++)
				tracks.Add(new Track(this, other.tracks[iii]));
			
			currentTick = other.currentTick;
			ticksPerBeat = other.ticksPerBeat;
		}
	}
	
}
