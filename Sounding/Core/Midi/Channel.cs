﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sounding
{
	/// <summary></summary>
	public class Channel
	{
		public bool percussion = false;
		public float volume = 1.0f;
		public List<ActiveNote> activeNotes = new List<ActiveNote>();
		public Instrument instrument = new Instrument();

		public ActiveNote GetNote(Key k)
		{
			for(int iii = 0; iii < activeNotes.Count; iii++)
				if(activeNotes[iii].key == k && activeNotes[iii].Alive)
					return activeNotes[iii];
			return null;
		}

		public void GetSamples(ref float[] samples, int sampleOffset = 0)
		{
			
			lock(activeNotes)
			{
				//instrument.GetSamples(activeNotes, ref samples, volume, sampleOffset);

				for(int iii = activeNotes.Count - 1; iii >= 0; iii--)
				{
					if(percussion)
						instrument = Orchestra.Percussion[activeNotes[iii].key.Interval];
					if(instrument.GetSamples(activeNotes[iii], ref samples, volume, sampleOffset))
						activeNotes.RemoveAt(iii);
				}
			}
		}

		/// <summary>
		/// Tells all notes to stop. But still plays their release.
		/// </summary>
		public void AllNotesOff()
		{
			lock(activeNotes)
			{
				for(int iii = activeNotes.Count - 1; iii >=0; iii--)
				{
					if(activeNotes[iii].Alive)
						activeNotes[iii].Alive = false;
				}
			}
		}

		/// <summary>
		/// Tells all notes to stop immediately. Does not play their release.
		/// </summary>
		public void AllSoundOff()
		{
			lock(activeNotes)
			{
				activeNotes.Clear();
			}
		}

		public Channel(Channel o)
		{
			percussion = o.percussion;
			volume = o.volume;
			instrument = o.instrument;
		}
		public Channel(){}
	}
}
