﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sounding
{
	public class ActiveNote
	{
		public const int MaxSimultaneous = 256;
		static UInt16 IdentifierCounter = 0;
		UInt16 identifier;

		public int sample;
		public float phase = .0f;

		public float velocity = 1.0f;
		
		public int sampleDied = -1;
		
		public Key key;

		public ActiveNote(Key key, float velocity)
		{
			this.key = key;
			this.velocity = velocity;

			if(IdentifierCounter >= MaxSimultaneous)
				IdentifierCounter = 0;
			identifier = IdentifierCounter++;
		}
		List<Filter.FilterData> filter = null;
		public Filter.FilterData GetFilterData(int channel)
		{
			if(filter == null)
				filter = new List<Filter.FilterData>();
			while(filter.Count < channel + 1)
				filter.Add(new Filter.FilterData());
			return filter[channel];
		}
		
		public bool Alive
		{
			get { return sampleDied < 0; }
			set { if(value)sampleDied = -1; else sampleDied = sample; }
		}

		public UInt16 Identifier { get { return identifier; } }
	}
}
