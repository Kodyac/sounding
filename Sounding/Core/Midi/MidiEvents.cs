﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sounding
{
	public enum MidiEventsCodes : byte 
	{
		NoteOff				= 0x80,
		NoteOn				= 0x90,
		KeyAfterTouch		= 0xA0,
		ControlChange		= 0xB0,
		ProgramChange		= 0xC0,
		ChannelAfterTouch	= 0xD0,
		PitchWheelChange	= 0xE0,
		
		Sysex				= 0xF0,
		Eox					= 0xF7,
		TimingClock			= 0xF8,
		StartSequence		= 0xFA,
		ContinueSequence	= 0xFB,
		StopSequence		= 0xFC,
		AutoSensing			= 0xFE,

		MetaEvent			= 0xFF,
	}
	public enum MetaEventCodes : byte
	{
		SequenceNumber	= 0x00,

		TextEvent		= 0x01,
		Copyright		= 0x02,
		SequenceName	= 0x03,
		InstrumentName	= 0x04,
		Lyric			= 0x05,
		Marker			= 0x06,
		CuePoint		= 0x07,

		MIDIChanPrefix	= 0x20,
		PrefixPort		= 0x21,
		EndOfTrack		= 0x2F,
		SetTempo		= 0x51,
		SMPTEOffset		= 0x54,
		TimeSignature	= 0x58,
		KeySignature	= 0x59,

		SeqSpecific		= 0x7F
	}
	public enum ControlChangeCodes : byte
	{
		BankSelect				= 0x00,
		ModulationWheel			= 0x01,
		BreathControl			= 0x02,
		FootController			= 0x04,
		PartamentoTime			= 0x05,
		DataEntry				= 0x06,
		ChannelVolume			= 0x07,
		Balance					= 0x08,
		Pan						= 0x0A,
		ExpressionController	= 0x0B,
		EffectControl1			= 0x0C,
		EffectControl2			= 0x0D,
		GeneralController1		= 0x10,
		GeneralController2		= 0x11,
		GeneralController3		= 0x12,
		GeneralController4		= 0x13,
		
		AllSoundOff				= 0x78,
		ResetAllControllers		= 0x79,
		LocalControl			= 0x7A,
		AllNotesOff				= 0x7B,
		OmniModeOff				= 0x7C,
		OmniModeOn				= 0x7D,
		PolyModeOff				= 0x7E,
		PolyModeOn				= 0x7F
	}
		
	public class Event
	{
		public Int32 absoluteTime;
		public MidiEventsCodes Code { get { return code; } }
		protected MidiEventsCodes code;

		public byte msb = 0;
		public byte lsb = 0;

		public Event(Int32 absoluteTime, MidiEventsCodes midiCode)
		{
			code = midiCode;
			this.absoluteTime = absoluteTime;
		}
		protected Event(Event o)
		{
			absoluteTime = o.absoluteTime;
			code = o.code;
			msb = o.msb;
			lsb = o.lsb;
		}
		public virtual Event Copy() { return new Event(this); }
	}
	public class SysexEvent : Event
	{
		public SysexEvent(Int32 absoluteTime) : base(absoluteTime, MidiEventsCodes.Sysex){}
	}
	public class MetaEvent : Event
	{
		public virtual MetaEventCodes MetaCode { get { return metaCode; } }
		protected MetaEventCodes metaCode;
		
		public MetaEvent(Int32 absoluteTime, MetaEventCodes metaCode) : base(absoluteTime, MidiEventsCodes.MetaEvent) 
		{
			this.metaCode = metaCode;
		}
		protected MetaEvent(MetaEvent o) : base(o) { metaCode = o.metaCode; }
		public override Event Copy() { return new MetaEvent(this); }

		public override string ToString()
		{
			return metaCode.ToString();
		}
	}
	public class TextEvent : MetaEvent
	{
		public void SetTextType(MetaEventCodes value)
		{ 
			metaCode = value; 
			if(metaCode < MetaEventCodes.TextEvent)
				metaCode = MetaEventCodes.TextEvent;
			else if(metaCode > MetaEventCodes.CuePoint)
				metaCode = MetaEventCodes.CuePoint;
		}
		public string text;
		public TextEvent(Int32 absoluteTime, MetaEventCodes metaEventCode) : base(absoluteTime, metaEventCode) 
		{
			SetTextType(metaEventCode);
		}
		protected TextEvent(TextEvent o) : base(o){ text = o.text;	}
		public override Event Copy() { return new TextEvent(this); }

		public override string ToString()
		{
			return text;
		}
	}
	public class TempoEvent : MetaEvent
	{
		public double Tempo 
		{
			get { return (60000000.0 / (float)MicroSecondsPerBeat); } 
			set { MicroSecondsPerBeat = (int)(60000000.0 / value); }
		}
		public int MicroSecondsPerBeat;
		public TempoEvent(Int32 absoluteTime) : base(absoluteTime, MetaEventCodes.SetTempo) {}
		protected TempoEvent(TempoEvent o) : base(o) { MicroSecondsPerBeat = o.MicroSecondsPerBeat; }
		public override Event Copy() { return new TempoEvent(this); }

		public override string ToString()
		{
			return "Tempo " + Tempo.ToString("F2");
		}
	}
	public class TimeSignatureEvent : MetaEvent
	{
		public TimeSignature time = new TimeSignature(4,4);
		public TimeSignatureEvent(Int32 absoluteTime) : base(absoluteTime, MetaEventCodes.TimeSignature) {}
		protected TimeSignatureEvent(TimeSignatureEvent o) : base(o) { time = o.time; }
		public override Event Copy() { return new TimeSignatureEvent(this); }

		public override string ToString()
		{
			return "Time Sig " + time.ToString();
		}
	}
	
	public class NoteEvent : Event
	{
		public virtual byte Channel { get { return channel; } set { channel = value; } }
		byte channel;

		public NoteEvent(Int32 absoluteTime, MidiEventsCodes midiEventCode) : base(absoluteTime, midiEventCode){}
		protected NoteEvent(NoteEvent o) : base(o) { Channel = o.Channel; }
		public override Event Copy() { return new NoteEvent(this); }

		public override string ToString()
		{
			string chan = "Channel " + (Channel + 1).ToString();
			switch (code)
			{
				case MidiEventsCodes.ControlChange:
				{
					switch ((ControlChangeCodes)lsb)
					{
						case ControlChangeCodes.ChannelVolume:
						{
							string s = chan + " Volume Change " + ((float)msb / 127.0f).ToString("F2");
							return s;
						}
						case ControlChangeCodes.Pan:
						{
							string s = chan + " Pan " + msb.ToString();
							return s;
						}
						case ControlChangeCodes.AllNotesOff:
						{
							string s = chan + " All Notes Off";
							return s;
						}
						
						default:
						{
							string s = chan + " Code " + ((ControlChangeCodes)lsb).ToString();
							return s;
						}
					}
				}
				case MidiEventsCodes.ProgramChange:
				{
					string s = chan + " " + Orchestra.Programs[lsb].name;
					return s;
				}
				default:
				{
					string s = chan + " Code " + code.ToString();
					return s;
				}
			}
		}
	}
	
	public class NoteKeyEvent : NoteEvent
	{
		public NoteKeyEvent(Int32 absoluteTime) : base(absoluteTime, MidiEventsCodes.NoteOn){}
		protected NoteKeyEvent(NoteKeyEvent o) : base(o){}
		public override Event Copy() { return new NoteKeyEvent(this); }

		/// <summary>MSB as velocity/volume/amplitude</summary>
		public float velocity { get { return ((float)msb / 127.0f); } set { msb = (byte)(value * 127.0f); } }
		/// <summary>LSB as key</summary>
		public virtual Key key { get { return lsb; } set { lsb = (byte)value.Interval; } }

		public override string ToString()
		{
			string s = "Channel " + Channel.ToString() + " " + key.ToString();
			return s;
		}
	}
	public class NoteOnEvent : NoteKeyEvent
	{
		public List<NoteKeyEvent> volumeChanges = new List<NoteKeyEvent>();
		public NoteKeyEvent NoteOff 
		{ 
			get 
			{ 
				if(noteOff == null) 
				{
					noteOff = new NoteKeyEvent(absoluteTime + 64); 
					noteOff.Channel = Channel;
					noteOff.lsb = lsb;
					noteOff.msb = msb;
				}
				
				return noteOff; 
			} 
			set { noteOff = value; }
		}
		NoteKeyEvent noteOff;
		public NoteOnEvent(Int32 absoluteTime) : base(absoluteTime) {}
		protected NoteOnEvent(NoteOnEvent o) : base(o)
		{
			lsb = o.lsb;
			msb = o.msb;
			Channel = o.Channel;

			//if(o.NoteOff != null)
				noteOff = new NoteKeyEvent(o.NoteOff.absoluteTime);
			//else
				//NoteOff = new NoteKeyEvent(absoluteTime + 64);
			
			NoteOff.lsb = lsb;
			NoteOff.msb = 0;
			NoteOff.Channel = Channel;

			volumeChanges.Clear();
			for(int iii = 0; iii < o.volumeChanges.Count; iii++)
				volumeChanges.Add( o.volumeChanges[iii].Copy() as NoteKeyEvent );
#if DEBUG
			System.Diagnostics.Debug.Assert(TickDuration > 0);
#endif
		}
		public override Event Copy() { return new NoteOnEvent(this); }
		public int TickDuration { get { if(NoteOff == null) return -1; return NoteOff.absoluteTime - absoluteTime; } }
		public void SetChannel(byte chann)
		{
			foreach(NoteKeyEvent nke in SubEvents())
				nke.Channel = chann;
		}
		public override Key key
		{
			get { return base.key; }
			set 
			{
				foreach(NoteKeyEvent nke in SubEvents())
					nke.lsb = (byte)value.Interval;
			}
		}
		/// <summary>Sets the absolute tick time of the event. Moves its corresponding note off & volume events to preserve the duration</summary>
		public void SetTime(int tick)
		{
			if(tick < 0)
				tick = 0;
			int baseAbsolute = absoluteTime;
			foreach(NoteKeyEvent nke in SubEvents())
				nke.absoluteTime = nke.absoluteTime - baseAbsolute + tick;
		}
		/// <summary>Creates the corresponding noteoff event at absolute time. Does NOT add it to the track.</summary>
		public NoteKeyEvent MakeNoteOffByAbsolute(int tick)
		{
			noteOff = new NoteKeyEvent(tick);
			noteOff.Channel = Channel;
			noteOff.key = key;
			noteOff.velocity = 0;
			return noteOff;
		}
		
		/// <summary>Returns the noteon event, all of the volume change events, and the noteoff event.</summary>
		public List<NoteKeyEvent> SubEvents()
		{
			List<NoteKeyEvent> ret = new List<NoteKeyEvent>();
			ret.Add(this);

			for(int iii = 0; iii < volumeChanges.Count; iii++)
				ret.Add(volumeChanges[iii]);

			ret.Add(NoteOff);

			return ret;
		}

		public override byte Channel
		{
			get	{ return base.Channel; }
			set
			{
				base.Channel = value;
				for(int iii = 1/*skip the first one else it will infinitely loop*/; iii < SubEvents().Count; iii++)
					SubEvents()[iii].Channel = value;
			}
		}
	}

	public class KeyEventSorter : IComparer<NoteKeyEvent>
	{
		public int Compare(NoteKeyEvent a, NoteKeyEvent b)
		{
			return EventSorter.bCompare(a, b);
		}
	}
	public class EventSorter : IComparer<Event>
	{
		public int Compare(Event a, Event b)
		{
			return bCompare(a, b);
		}
		public static int bCompare(Event a, Event b)
		{
			// 1=a after b

			if(a == b)
				return 0;

			MetaEvent test = a as MetaEvent;
			if(test != null && test.MetaCode == MetaEventCodes.EndOfTrack)
			{
				test.absoluteTime = (a.absoluteTime > b.absoluteTime) ? a.absoluteTime : b.absoluteTime;
				return 1;
			}
			test = b as MetaEvent;
			if(test != null && test.MetaCode == MetaEventCodes.EndOfTrack)
			{
				test.absoluteTime = (a.absoluteTime > b.absoluteTime) ? a.absoluteTime : b.absoluteTime;
				return -1;
			}
			
			if(a.absoluteTime > b.absoluteTime)
				return 1;
			else if(a.absoluteTime < b.absoluteTime)
				return -1;
			
			if(a.Code == MidiEventsCodes.MetaEvent && b.Code != MidiEventsCodes.MetaEvent)
				return -1;
			else if(b.Code == MidiEventsCodes.MetaEvent && a.Code != MidiEventsCodes.MetaEvent)
				return 1;
			else if(b.Code == MidiEventsCodes.MetaEvent && a.Code == MidiEventsCodes.MetaEvent)
			{
				MetaEvent ma = a as MetaEvent;
				MetaEvent mb = b as MetaEvent;
				return ma.MetaCode.CompareTo(mb.MetaCode);
			}

			if(IsPriorityEvent(a.Code) && !IsPriorityEvent(b.Code))
				return -1;
			else if(IsPriorityEvent(b.Code) && !IsPriorityEvent(a.Code))
				return 1;
			
			
			if(a is NoteEvent && b is NoteEvent)
			{
				NoteEvent an = a as NoteEvent;
				NoteEvent bn = b as NoteEvent;

				if(an.Channel > bn.Channel)
					return -1;
				else if(an.Channel < bn.Channel)
					return 1;

				if(an.Code != MidiEventsCodes.NoteOn && bn.Code != MidiEventsCodes.NoteOn 
				&& an.Code != MidiEventsCodes.NoteOff && bn.Code != MidiEventsCodes.NoteOff)
				{
					if(an.Code > bn.Code)
						return -1;
					else if(an.Code < bn.Code)
						return 1;
				}
			}

			if(a is NoteKeyEvent && b is NoteKeyEvent)
			{
				NoteKeyEvent an = a as NoteKeyEvent;
				NoteKeyEvent bn = b as NoteKeyEvent;

				if(an.msb > bn.msb)
					return 1;
				else if(an.msb < bn.msb)
					return -1;
			}
						
			return 0;
		}

		/// <summary>Priority events are events that must happen before NoteOn or NoteOff events</summary>
		static bool IsPriorityEvent(MidiEventsCodes code)
		{
			switch (code)
			{
				case MidiEventsCodes.ControlChange:
				case MidiEventsCodes.ProgramChange:
				case MidiEventsCodes.MetaEvent:
					return true;	
			}
			return false;
		}
	}

	
}
