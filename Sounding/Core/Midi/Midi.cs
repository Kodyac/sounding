﻿using System;
using System.Collections.Generic;
using System.Text;

using System.IO;

using System.Diagnostics;

#if UNITY
using UnityEngine;
#endif

namespace Sounding
{
	public class MidiFile
	{
		public static void SaveMidi(string file, Song song)
		{
			
			using(FileStream fs = new FileStream(file, FileMode.Create, FileAccess.Write))
			using(BinaryWriter writer = new BinaryWriter(fs))
			{
				writer.Write(Encoding.UTF8.GetBytes("MThd"));
				writer.Write(ReverseBytes((uint)6)); // chunk size

				writer.Write(ReverseBytes((UInt16)song.trackType));

				writer.Write(ReverseBytes((UInt16)song.tracks.Count));

				writer.Write(ReverseBytes((UInt16)song.ticksPerBeat));

				for(int iii = 0; iii < song.tracks.Count; iii++)
				{
					Track track = song.tracks[iii];

					MergeOverlappingNotes(track);
					RemoveUnusedNoteOffs(track);

					writer.Write(Encoding.UTF8.GetBytes("MTrk"));

					Int64 trackLengthPos = writer.BaseStream.Position;
					writer.Write((UInt32)0);	//come back later to write the length

					track.SortTrack();

					Int32 currentTick = 0;
					for(int eee = 0; eee < track.events.Count; eee++)
					{
						if(track.events[eee] is NoteEvent)
						{
							int deltaTick = track.events[eee].absoluteTime - currentTick;
							WriteVariableInt(writer, deltaTick);

							NoteEvent nev = track.events[eee] as NoteEvent;
							bool running = false;
							if(eee > 0)
							{
								NoteEvent prev = track.events[eee - 1] as NoteEvent;
								if(prev != null && prev.Code == nev.Code && prev.Channel == nev.Channel)
									running = true;
							}
							if(!running)
							{
								byte b = (byte)((byte)nev.Code | (byte)nev.Channel);
								writer.Write( b );
							}
							if(nev.Code == MidiEventsCodes.ChannelAfterTouch || nev.Code == MidiEventsCodes.ProgramChange)
								writer.Write(nev.lsb);
							else
							{
								writer.Write(nev.lsb);
								writer.Write(nev.msb);
							}

						}
						else if(track.events[eee] is SysexEvent)
						{
							continue;
						}
						else if(track.events[eee] is MetaEvent)
						{
							switch (((MetaEvent)track.events[eee]).MetaCode)
							{
								case MetaEventCodes.TextEvent:
								case MetaEventCodes.Copyright:
								case MetaEventCodes.SequenceName:
								case MetaEventCodes.InstrumentName:
								case MetaEventCodes.Lyric:
								case MetaEventCodes.Marker:
								case MetaEventCodes.CuePoint:
								{
									WriteVariableInt(writer, track.events[eee].absoluteTime - currentTick);
									writer.Write((byte)0xFF);
									writer.Write( (byte)((MetaEvent)track.events[eee]).MetaCode );

									TextEvent tev = track.events[eee] as TextEvent;
									byte[] asciiBytes = Encoding.ASCII.GetBytes(tev.text);

									WriteVariableInt(writer, asciiBytes.Length);

									writer.Write(asciiBytes);
								}	break;
								case MetaEventCodes.SetTempo:
								{
									WriteVariableInt(writer, track.events[eee].absoluteTime - currentTick);
									writer.Write((byte)0xFF);
									writer.Write( (byte)((MetaEvent)track.events[eee]).MetaCode );

									WriteVariableInt(writer, 3);

									int microsPerBeat = ((TempoEvent)track.events[eee]).MicroSecondsPerBeat;

									writer.Write((byte) ((microsPerBeat >> 16) & 0xFF));
									writer.Write((byte) ((microsPerBeat >> 8) & 0xFF));
									writer.Write((byte) (microsPerBeat & 0xFF));
								}	break;
								case MetaEventCodes.TimeSignature:
								{
									WriteVariableInt(writer, track.events[eee].absoluteTime - currentTick);
									writer.Write((byte)0xFF);
									writer.Write( (byte)((MetaEvent)track.events[eee]).MetaCode );

									WriteVariableInt(writer, 4);

									TimeSignatureEvent timeSig = track.events[eee] as TimeSignatureEvent;

									writer.Write((byte)timeSig.time.counts);
									writer.Write((byte)SMath.HowManyPowersOfTwo(timeSig.time.value));
									writer.Write((byte)timeSig.lsb);
									writer.Write((byte)timeSig.msb);
								}	break;
								case MetaEventCodes.EndOfTrack:
								{
									WriteVariableInt(writer, track.events[eee].absoluteTime - currentTick);
									writer.Write((byte)0xFF);
									writer.Write((byte)MetaEventCodes.EndOfTrack);
									WriteVariableInt(writer, 0);
								}	break;
								default:
									continue;
							}
						}

						currentTick = track.events[eee].absoluteTime;
					}

					Int64 position = writer.BaseStream.Position;
					writer.BaseStream.Position = trackLengthPos;
					UInt32 length = (UInt32)(position - trackLengthPos - 4);
					writer.Write(ReverseBytes(length));
					writer.BaseStream.Position = position;
				}
			}
		}

		static Song ReadStream(BinaryReader reader)
		{
			Song s = new Song();

			/*string primaryHeader = */Encoding.UTF8.GetString(reader.ReadBytes(4));

			UInt32 primarySize = ReverseBytes(reader.ReadUInt32());
				
			System.Diagnostics.Debug.Assert(primarySize == 6, "Unrecognized Midi format");

			s.trackType = (Song.TrackTypes)ReverseBytes(reader.ReadUInt16());

			UInt16 trackCount = ReverseBytes(reader.ReadUInt16());

			UInt16 deltaTicksPerBeat = ReverseBytes(reader.ReadUInt16());

			s.ticksPerBeat = (float)deltaTicksPerBeat;
				
			for(int iii = 0; iii < trackCount; iii++)
			{
				NoteBuilder builder = new NoteBuilder();
				Track track = new Track(s);
				/*string header = */Encoding.UTF8.GetString(reader.ReadBytes(4));
				UInt32 length = ReverseBytes(reader.ReadUInt32());

				long trackStart = reader.BaseStream.Position;

				MidiEventsCodes previousCode = 0;
				byte previousChannel = 0;
				while(reader.BaseStream.Position < (trackStart + length))
				{
					Event chunk;

					int deltaTime = ReadVariableInt(reader);

					track.TickLength += deltaTime;

					MidiEventsCodes code;
					byte b = reader.ReadByte();
					code = (MidiEventsCodes) b;

					byte channel = 0;

					if((b & 0x80) == 0) //not a midi event, must be running status
					{
						code = previousCode;
						channel = previousChannel;
						reader.BaseStream.Position--;
					}
					else 
					{
						if((b & 0xF0) == 0xF0) //sysex or meta event
							code = (MidiEventsCodes) b;
						else 
						{
							code = (MidiEventsCodes) (b & 0xF0);
							channel = (byte)((b & 0x0F));
						}
					}

					#region MetaEvents
					if (code == MidiEventsCodes.MetaEvent)
					{
						MetaEvent metaChunk = null;
							
						MetaEventCodes metEvent = (MetaEventCodes)reader.ReadByte();
							
						int vlength = ReadVariableInt(reader);
						switch (metEvent)
						{
							case MetaEventCodes.SequenceNumber:
								metaChunk = new MetaEvent(track.TickLength, metEvent);
								metaChunk.msb = reader.ReadByte();
								metaChunk.lsb = reader.ReadByte();
								break;
							case MetaEventCodes.TextEvent:
							case MetaEventCodes.Copyright:
							case MetaEventCodes.SequenceName:
							case MetaEventCodes.InstrumentName:
							case MetaEventCodes.Lyric:
							case MetaEventCodes.Marker:
							case MetaEventCodes.CuePoint:
								metaChunk = new TextEvent(track.TickLength, metEvent);
								((TextEvent)metaChunk).text = Encoding.ASCII.GetString(reader.ReadBytes(vlength));
								break;
							case MetaEventCodes.MIDIChanPrefix:
								metaChunk = new MetaEvent(track.TickLength, metEvent);
								metaChunk.lsb = metaChunk.msb = reader.ReadByte();
								break;
							case MetaEventCodes.EndOfTrack:
								metaChunk = new MetaEvent(track.TickLength, metEvent);
								break;
							case MetaEventCodes.SetTempo:
								metaChunk = new TempoEvent(track.TickLength);
								((TempoEvent)metaChunk).MicroSecondsPerBeat = (reader.ReadByte() << 16) + (reader.ReadByte() << 8) + reader.ReadByte();
								break;
							case MetaEventCodes.TimeSignature:
								metaChunk = new TimeSignatureEvent(track.TickLength);
								((TimeSignatureEvent)metaChunk).time.counts = reader.ReadByte();
								int value = (int)Math.Pow(2, (double)reader.ReadByte());
								((TimeSignatureEvent)metaChunk).time.value = value;
								metaChunk.lsb = reader.ReadByte();
								metaChunk.msb = reader.ReadByte();
								break;
							case MetaEventCodes.KeySignature:
								metaChunk = new MetaEvent(track.TickLength, metEvent);
								metaChunk.lsb = reader.ReadByte();
								metaChunk.msb = reader.ReadByte();
								break;

							case MetaEventCodes.SMPTEOffset:
							case MetaEventCodes.SeqSpecific:
							default:
								metaChunk = new MetaEvent(track.TickLength, metEvent);
								reader.BaseStream.Seek(vlength, SeekOrigin.Current);
								break;

						}
						System.Diagnostics.Debug.Assert(metaChunk != null, "Error reading metaChunk");
						//metaChunk.MetaCode = metEvent;

						chunk = metaChunk;
					}
					#endregion
					#region SysExEvents
					else if(code == MidiEventsCodes.Sysex)
					{
						SysexEvent sysexChunk = new SysexEvent(track.TickLength);
							
						int vlength = ReadVariableInt(reader);
						reader.BaseStream.Seek(vlength, SeekOrigin.Current);

						chunk = sysexChunk;
					}
					#endregion
					else
					{
						NoteEvent midiChunk;
							
						switch (code)
						{
							case MidiEventsCodes.NoteOff:
							case MidiEventsCodes.NoteOn:
								byte lsb = reader.ReadByte();
								byte msb = reader.ReadByte();

								NoteOnEvent running = builder.channels[channel].Playing(lsb);

								if(msb == 0 || code == MidiEventsCodes.NoteOff)
								{
									midiChunk = new NoteKeyEvent(track.TickLength);
									midiChunk.lsb = lsb;
									midiChunk.msb = 0;

									if(running == null)
										continue;
									running.NoteOff = midiChunk as NoteKeyEvent;
									builder.channels[channel].noteOns.Remove(running);

									break;
								}
									
								if(running == null)
								{
									midiChunk = new NoteOnEvent(track.TickLength);
									builder.channels[channel].noteOns.Add(midiChunk as NoteOnEvent);
								}
								else
								{
									midiChunk = new NoteKeyEvent(track.TickLength);
									running.volumeChanges.Add(midiChunk as NoteKeyEvent);
								}

								midiChunk.lsb = lsb;
								midiChunk.msb = msb;


								break;

							case MidiEventsCodes.ProgramChange:
							case MidiEventsCodes.ChannelAfterTouch:
								midiChunk = new NoteEvent(track.TickLength, code);
								midiChunk.msb = midiChunk.lsb = reader.ReadByte();
								break;
								
							default:
								midiChunk = new NoteEvent(track.TickLength, code);
								midiChunk.lsb = reader.ReadByte();
								midiChunk.msb = reader.ReadByte();
								break;
						}
							

						midiChunk.Channel = channel;
						chunk = midiChunk;
					}

					track.events.Add(chunk);
					previousCode = chunk.Code;
					if(chunk is NoteEvent)
						previousChannel = ((NoteEvent)chunk).Channel;
				}
				System.Diagnostics.Debug.Assert(reader.BaseStream.Position == (trackStart + length));

				//FillNotePairs(track);
				s.tracks.Add(track);
			}
			return s;
		}
#if UNITY
		public static Song LoadMidi(TextAsset file)
		{
			return ReadStream(new BinaryReader(new MemoryStream(file.bytes)));
		}
#endif
		public static Song LoadMidi(string file)
		{
			if(!File.Exists(file))
				return null;
			
			Song s = new Song();

			using(FileStream fs = new FileStream(file, FileMode.Open, FileAccess.Read))
			using(BinaryReader reader = new BinaryReader(fs))
			{
				s = ReadStream(reader);
			}
			
			return s;
		}
		
		class NoteBuilder
		{
			public class Chan
			{
				public List<NoteOnEvent> noteOns = new List<NoteOnEvent>();
				public NoteOnEvent Playing(byte lsb)
				{
					for(int iii = 0; iii < noteOns.Count; iii++)
					{
						if(noteOns[iii].lsb == lsb) 
							return noteOns[iii];
					}
					return null;
				}
			}
			public Chan[] channels = new Chan[16];
			public NoteBuilder()
			{
				for(int iii = 0; iii < channels.Length; iii++)
					channels[iii] = new Chan();
			}
		}

		/// <summary>
		/// Goes through track to find any noteoff events that occur inside a note, happens if the user makes overlapping note pairs.
		/// Turns the interior noteoffs into a volume change to 1/127. 
		/// </summary>
		static void MergeOverlappingNotes(Track t)
		{
			restart:
			t.SortTrack();
			for(int iii = 0; iii < t.events.Count; iii++)
			{
				NoteOnEvent noteOn = t.events[iii] as NoteOnEvent;
				if(noteOn == null)
					continue;

				int offIndex = t.events.IndexOf(noteOn.NoteOff);
				for(int jjj = iii; jjj < offIndex; jjj++)
				{
					if(!(t.events[jjj] is NoteOnEvent))
						continue;
					NoteOnEvent current = t.events[jjj] as NoteOnEvent;

					if(noteOn.SubEvents().Contains(current))
						continue;

					if(	current.Channel == noteOn.Channel && current.lsb == noteOn.lsb && 
						noteOn.absoluteTime < current.absoluteTime && noteOn.NoteOff.absoluteTime > current.absoluteTime)
					{
						MergeNoteOns(noteOn, current, t);
						goto restart;	
					}
				}
				noteOn.volumeChanges.Sort(new KeyEventSorter());
			}
		}
		static void RemoveUnusedNoteOffs(Track t)
		{
			List<NoteOnEvent> noteOns = new List<NoteOnEvent>();

			for(int iii = 0; iii < t.events.Count; iii++)
			{
				if(t.events[iii] is NoteOnEvent)
					noteOns.Add(t.events[iii] as NoteOnEvent);
			}

			for(int iii = t.events.Count - 1; iii >= 0; iii--)
			{
				NoteKeyEvent current = t.events[iii] as NoteKeyEvent;
				if(current == null)
					continue;
				if(current.Code != MidiEventsCodes.NoteOff && (current.Code != MidiEventsCodes.NoteOn && current.msb != 0))
					continue;

				
				for(int jjj = 0; jjj < noteOns.Count; jjj++)
				{
					if(noteOns[jjj].SubEvents().Contains(current))
						goto con;
					if(noteOns[jjj].NoteOff == current)
						goto con;
				}
				t.events.RemoveAt(iii);

				con:{}
			}

			t.SortTrack();
		}
		static void MergeNoteOns(NoteOnEvent a, NoteOnEvent b, Track t)
		{
			if(a.key != b.key || a.Channel != b.Channel)
				return;

			List<NoteKeyEvent> all = a.SubEvents();

			all.AddRange(b.SubEvents());

			all.Sort(new KeyEventSorter());

			NoteOnEvent on = all[0] as NoteOnEvent;
			System.Diagnostics.Debug.Assert(on != null);

			List<NoteKeyEvent> volumes = new List<NoteKeyEvent>();

			for(int iii = 1; iii < all.Count - 1; iii++)
			{
				int trackIndex = t.events.IndexOf(all[iii]);

				if(all[iii].absoluteTime < all[iii - 1].absoluteTime + 3)	//just delete anything less than three ticks from previous
				{
					t.events.RemoveAt(trackIndex);
					continue;
				}

				NoteKeyEvent n;
				if(all[iii] is NoteOnEvent)
				{
					n = new NoteKeyEvent(all[iii].absoluteTime);
					n.lsb = all[iii].lsb;
					n.msb = all[iii].msb;
					n.Channel = all[iii].Channel;
					
					t.events[trackIndex] = n;
				}
				else
				{
					n = all[iii];
				}
				if(n.msb < 1)
					n.msb = 1;

				volumes.Add(n);
			}

			on.volumeChanges.Clear();
			on.volumeChanges.AddRange(volumes);
			on.NoteOff = all[all.Count - 1];
			on.NoteOff.msb = 0;

			/*if(other.noteOff.absoluteTime > noteOff.absoluteTime)
			{
				noteOff.msb = 1;
				volumeChanges.Add(noteOff);
				noteOff = other.noteOff;
			}*/
		}
		/// <summary>Goes through track to find and pair all not on events with their ends. Creates ends at track's length if none exist.</summary>
		/*static void FillNotePairs(Track t)
		{
			for(int iii = 0; iii < t.events.Count; iii++)
			{
				NoteOnEvent noteOn = t.events[iii] as NoteOnEvent;
				if(noteOn == null)
					continue;
					
				NoteOffEvent noteOff = null;
				for(int jjj = iii + 1; jjj < t.events.Count; jjj++)
				{
					NoteOffEvent off = t.events[jjj] as NoteOffEvent;
					if(off == null || off is NoteOnEvent)
						continue;

					if(off.channel == noteOn.channel && off.key == noteOn.key && (off.msb == 0 || off.Code == MidiEventsCodes.NoteOff))
					{
						noteOff = off;
						break;
					}
				}
				if(noteOff == null)
				{
					noteOff = new NoteOffEvent(t.TickLength);
					noteOff.channel = noteOn.channel;
					noteOff.lsb = noteOn.lsb;
					noteOff.msb = 0;
				}
				noteOn.noteOff = noteOff;
			}
		}*/
		
		public static int ReadVariableInt(BinaryReader reader) 
		{
			int value = 0;
			byte b;
			for(int iii = 0; iii < 4; iii++) 
			{
				b = reader.ReadByte();
				value <<= 7;
				value += (b & 0x7F);
				if((b & 0x80) == 0) 
				{
					return value;
				}
			}
			throw new FormatException("Invalid Var Int");
		}
		
		public static void WriteVariableInt(BinaryWriter writer, Int32 value) 
		{
			if (value < 0)
			{
				throw new ArgumentOutOfRangeException("value", value, "Cannot write a negative Var Int");
			}
			if (value > 0x0FFFFFFF)
			{
				throw new ArgumentOutOfRangeException("value", value, "Maximum allowed Var Int is 0x0FFFFFFF");
			}

			int n = 0;
			byte[] buffer = new byte[4];
			do
			{
				buffer[n++] = (byte)(value & 0x7F);
				value >>= 7;
			} while (value > 0);
			
			while (n > 0)
			{
				n--;
				if(n > 0)
					writer.Write((byte) (buffer[n] | 0x80));
				else 
					writer.Write(buffer[n]);
			}
		}
		
		public static UInt16 ReverseBytes(UInt16 value)
		{
			return (UInt16)((value & 0xFFU) << 8 | (value & 0xFF00U) >> 8);
		}
		public static UInt32 ReverseBytes(UInt32 value)
		{
			return (value & 0x000000FFU) << 24 | (value & 0x0000FF00U) << 8 | (value & 0x00FF0000U) >> 8 | (value & 0xFF000000U) >> 24;
		}
		public static UInt64 ReverseBytes(UInt64 value)
		{
			return	(value & 0x00000000000000FFUL) << 56	| (value & 0x000000000000FF00UL) << 40 | 
					(value & 0x0000000000FF0000UL) << 24	| (value & 0x00000000FF000000UL) << 8 |
					(value & 0x000000FF00000000UL) >> 8		| (value & 0x0000FF0000000000UL) >> 24 | 
					(value & 0x00FF000000000000UL) >> 40	| (value & 0xFF00000000000000UL) >> 56;
		}
	}
}
