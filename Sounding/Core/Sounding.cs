﻿
using System.Collections;
using System.IO;

namespace Sounding
{
	public static class DeviceSettings
	{
		static DeviceSettings()
		{
			SampleRate = 44100;
			Channels = 2;
		}

		static int sampleRate = 44100;
		public static int SampleRate
		{
			get { return sampleRate; }
			set { sampleRate = value; samplePeriod = 1.0f / (float)sampleRate; Oscillator.RebuildWaveTables(sampleRate); }
		}
		static int channels = 2;
		/// <summary>Either Mono or Stereo</summary>
		public static int Channels
		{
			get { return channels; }
			set { if(value < 2) channels = 1; else channels = 2; }
		}
		static float samplePeriod = 0;
		/// <summary>Amount of time a sample takes in seconds</summary>
		public static float SamplePeriod { get { return samplePeriod; } }
		
		public static float tempoMultiplier = 1.0f;

		public static int tempo = 140;
		public static float samplesPerBeat { get { return sampleRate / beatsPerSecond; } }
		public static float BeatsPerSample { get { return beatsPerSecond * SamplePeriod; } }
		public static float beatsPerSecond { get { return (float)tempo / 60.0f; } }
		public static float secondsPerBeat { get { return 60.0f / (float)tempo; } }	//a beat is a 1/4th note. a beat is one second at 60bpm|tempo
		public static float SecondsPerNote(float noteValue = 1.0f)					//a whole note is 4 beats
		{
			return secondsPerBeat * noteValue;
		}
		public static void SetDirectory(string directory)
		{
			dir = directory;
		}
		static string dir = Folder();
		public static string Directory { get { return dir; } }

		public static string Folder()
		{
#if UNITY
			return "";
#else
			string path = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
			path += Path.DirectorySeparatorChar + "Sounding" + Path.DirectorySeparatorChar;
			if(!System.IO.Directory.Exists(path))
				System.IO.Directory.CreateDirectory(path);
			return path;
#endif
		}

		public static string ToNativeSeparators(string fileName)
		{
#if UNITY
			return fileName.Replace('\\', '/');
#else
			if(Path.DirectorySeparatorChar == '\\')
				return fileName.Replace('/', '\\');
			else
				return fileName.Replace('\\', '/');
#endif
		}	
	}

	public struct TimeSignature
	{
		int icounts;
		int ivalue;
		public int counts
		{
			get 
			{
				if(icounts < 1)
					return 1;
				else if(icounts > 32)
					return 32;
				return icounts; 
			}
			set 
			{
				if(value < 1)
					icounts = 1;
				else if(value > 32)
					icounts = 32;
				else
					icounts = value;
			}
		}
		public int value
		{
			get 
			{
				if(ivalue < 1)
					return 1;
				else if (ivalue > 32)
					return 32;
				return ivalue;
			}
			set 
			{ 
				if(value < 1)
					ivalue = 1;
				else if(value > 32)
					ivalue = 32;
				else
					ivalue = value;
			}
		}
		public TimeSignature(int beats = 4, int value = 4)
		{
			this.icounts = beats;
			this.ivalue = value;
		}
		public override string ToString()
		{
			return icounts.ToString() + "/" + ivalue.ToString();
		}
	}
}