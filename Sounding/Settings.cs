﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Reflection;

namespace Sounding
{
	class Settings
	{
		const string defaultFileName = "settings";
		static string SettingsFullFile(string fileName)
		{
			return Folder() + fileName;
		}
		public static string Folder()
		{
			string path = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
			path += Path.DirectorySeparatorChar + "Sounding" + Path.DirectorySeparatorChar + "Settings" + Path.DirectorySeparatorChar;
			if(!System.IO.Directory.Exists(path))
				System.IO.Directory.CreateDirectory(path);
			return path;

		}

		public static void DeleteSetting(params string[] keys) { DeleteSetting(defaultFileName, keys); }
		public static void DeleteSetting(string settingsFile, params string[] keys)
		{
			string[] lines;
			if(File.Exists(SettingsFullFile(settingsFile)))
				lines = File.ReadAllLines(SettingsFullFile(settingsFile), System.Text.Encoding.Unicode);
			else
				lines = new string[0];

			if(lines.Length < 1)
				return;

			List<string> newFile = new List<string>();
		
			for(int iii = 0; iii < lines.Length; iii++)
			{
				bool skip = false;
				foreach(string key in keys)
					if(lines[iii].StartsWith(key + " = "))
						skip = true;
			
				if(!skip)
					newFile.Add(lines[iii]);
			}
		
			File.WriteAllLines(SettingsFullFile(settingsFile), newFile.ToArray(), System.Text.Encoding.Unicode);
		}
		

		public static void WriteSetting(string key, string value) { WriteSetting(defaultFileName, key, value); }
		public static void WriteSetting(string settingsFile, string key, string value)
		{
			bool replaced = false;
			key = key + " = ";

			string[] lines;
			if(File.Exists(SettingsFullFile(settingsFile)))
				lines = File.ReadAllLines(SettingsFullFile(settingsFile), System.Text.Encoding.Unicode);
			else
				lines = new string[0];
		
			for(int iii = 0; iii < lines.Length; iii++)
			{
				if(lines[iii].StartsWith(key))
				{
					replaced = true;
					lines[iii] = key + value;
					break;
				}
			}
			if(!replaced)
			{
				System.Array.Resize(ref lines, lines.Length+1);
				lines[lines.Length-1] = key + value;
			}

			File.WriteAllLines(SettingsFullFile(settingsFile), lines, System.Text.Encoding.Unicode);
		}
		
		public static void WriteSetting<T>(string settingsFile, string key, T value)
		{
			WriteSetting(settingsFile, key, value.ToString());
		}

		public static string ReadSetting(string key) { return ReadSetting(defaultFileName, key); }
		public static string ReadSetting(string settingsFile, string key)
		{
			if(!File.Exists(SettingsFullFile(settingsFile)))
				return "";

			string[] lines = File.ReadAllLines(SettingsFullFile(settingsFile), System.Text.Encoding.Unicode);

			key = key + " = ";

			for(int iii = 0; iii < lines.Length; iii++)
			{
				if(lines[iii].StartsWith(key))
				{
					return lines[iii].Replace(key, "");
				}
			}
			return "";
		}
		
		public static void WriteStructSetting<T>(string key, T value) where T : struct { WriteStructSetting(defaultFileName, key, value); }
		public static void WriteStructSetting<T>(string settingsFile, string key, T value) where T : struct
		{
			string write = "";
			PropertyInfo[] props = typeof(T).GetProperties
				(BindingFlags.FlattenHierarchy | BindingFlags.IgnoreCase | BindingFlags.Instance | BindingFlags.Public);
			
			for(int iii = 0; iii < props.Length; iii++)
			{
				if(props[iii].GetSetMethod() == null)
					continue;
				write += props[iii].GetValue(value, null);
				write += '|';
			}
			WriteSetting(settingsFile, key, write);
		}
		
		public static bool TryParseEnumSetting<T>(string key, out T result) where T : struct, IConvertible { return TryParseEnumSetting(defaultFileName, key, out result); }
		public static bool TryParseEnumSetting<T>(string settingsFile, string key, out T result) where T : struct, IConvertible
		{
			result = default(T);
			if(!typeof(T).IsEnum)
				return false;

			string value = ReadSetting(settingsFile, key);

			bool foundValue = false;
			foreach(T item in Enum.GetValues(typeof(T)))
			{
				if(item.ToString().ToUpperInvariant().Equals(value.ToUpperInvariant()))
				{
					foundValue = true;
					result = item;
				}
			}
			if(!foundValue)
				return false;

			return true;
		}
				
		public static bool TryParseStructSetting<T>(string key, out T result) where T : struct { return TryParseStructSetting(defaultFileName, key, out result); }
		public static bool TryParseStructSetting<T>(string settingsFile, string key, out T result) where T : struct
		{
			result = Activator.CreateInstance<T>();

			object ret = default(T);

			string value = ReadSetting(settingsFile, key);
			string[] readFields = value.Split(new char[] {',', '|'}, StringSplitOptions.RemoveEmptyEntries);

			List<PropertyInfo> props = typeof(T).GetProperties
				(BindingFlags.FlattenHierarchy | BindingFlags.IgnoreCase | BindingFlags.Instance | BindingFlags.Public).ToList();

			for(int iii = props.Count - 1; iii >= 0; iii--)
			{
				if(props[iii].GetSetMethod() == null)
					props.RemoveAt(iii);
			}

			if(props.Count != readFields.Length)
				return false;
			for(int iii = 0; iii < props.Count; iii++)
			{
				var t = Activator.CreateInstance(props[iii].PropertyType);
				Type type = t.GetType();
				var tryParse = type.GetMethod("TryParse", new [] {typeof(string), type.MakeByRefType()});
				if(tryParse == null)
					return false;
				
				var parameters = new object[] {readFields[iii], t};
				var success = tryParse.Invoke(null, parameters);
				if(!(bool)success)
					return false;
				t = Convert.ChangeType(parameters[1], t.GetType());

				props[iii].SetValue(ret, t, null);
			}
			result = (T)ret;
			return true;
		}
		
		public static bool TryParseSetting<T>(string key, out T result) where T : IConvertible { return TryParseSetting(defaultFileName, key, out result); }
		public static bool TryParseSetting<T>(string settingsFile, string key, out T result) where T : IConvertible
		{
			return TryParse<T>(ReadSetting(settingsFile, key), out result);
		}
		
		static bool TryParse<T>(string s, out T result) where T : IConvertible
		{
			result = default(T);
			var tryParse = typeof(T).GetMethod("TryParse", new [] {typeof(string), typeof(T).MakeByRefType()});
			if(tryParse == null)
				return false;

			var parameters = new object[] {s, result};
			var success = tryParse.Invoke(null, parameters);
			if(!(bool)success)
				return false;
			result = (T)parameters[1];
			return true;
		}
	}
}
